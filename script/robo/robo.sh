#!/bin/bash
model=$2
logic=$4
fileout=$(pwd)'/log/logic/'${model,,}'out.txt'
fileerr=$(pwd)'/log/logic/'${model,,}'err.txt'

user=$1
purpose=$3

echo $fileout
if [ -f $fileout ]
then
	echo 'fileout will be rewrited'
fi
echo $fileerr
if [ -f $fileerr ]
then
	echo 'fileerr will be rewrited'
fi
php ../../public/index.php robo $user $model $purpose $logic > $fileout 2> $fileerr &
# Example:
#
# $user ablair@hgh1.com
# $model User
# $purpose logic
# $logic mailfetch
