#!/bin/bash
model=$1
logic=$2
fileout='log/logic/'${model,,}'out.txt'
fileerr='log/logic/'${model,,}'err.txt'

user=zoho@gmail.com
db=wepo_zoho

echo $fileout
if [ -f $fileout ]
then
	echo 'fileout will be rewrited'
fi
echo $fileerr
if [ -f $fileerr ]
then
	echo 'fileerr will be rewrited'
fi
php ..\public\index.php robo db user $model logic $logic > $fileout 2> $fileerr &
