#!/bin/bash
model=$1
fileout='dbclonelog/copy/'${model,,}'out.txt'
fileerr='dbclonelog/copy/'${model,,}'err.txt'
echo $fileout
if [ -f $fileout ]
then
	echo 'fileout will be rewrited'
fi
echo $fileerr
if [ -f $fileerr ]
then
	echo 'fileerr will be rewrited'
fi
php index.php importdb wepo_zoho zoho@gmail.com $model copy > $fileout 2> $fileerr &
