#!/bin/bash
model=$1
logic=$2
fileout='dbclonelog/logic/'${model,,}'out.txt'
fileerr='dbclonelog/logic/'${model,,}'err.txt'
echo $fileout
if [ -f $fileout ]
then
	echo 'fileout will be rewrited'
fi
echo $fileerr
if [ -f $fileerr ]
then
	echo 'fileerr will be rewrited'
fi
php index.php importdb wepo_zoho zoho@gmail.com $model logic $logic > $fileout 2> $fileerr &
