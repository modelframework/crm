$(document).ready(function() {

   var availableTags = [
		"ActionScript",
		"AppleScript",
		"Asp",
		"BASIC",
		"C",
		"C++",
		"Clojure",
		"COBOL",
		"ColdFusion",
		"Erlang",
		"Fortran",
		"Groovy",
		"Haskell",
		"Java",
		"JavaScript",
		"Lisp",
		"Perl",
		"PHP",
		"Python",
		"Ruby",
		"Scala",
		"Scheme"
	];
	$('input.autocomplete_input').autocomplete({
		source: availableTags
	});

	$('.table table tbody tr,#mainMenu li, div.lead_block table tr').bind('mouseenter',function(){
		$(this).next().addClass('borderLess');
	}).bind('mouseleave',function(){
		$(this).next().removeClass('borderLess');
	});

	$('#mainMenu .create_submenu a').click(function(e){
		e.preventDefault();
		$(this).parent().toggleClass('visible_submenu');
	});

	$('form a.submitLink').click(function(e){
		e.preventDefault();
		$(this).parents('form').submit();
	});

	$('#header .with_subLinks > a').click(function(){
		$(this).parent().find('.subLinks').toggle();
	});

	$('#search_form .input').insideLabel();
	
	$( "#rightCol, #leftCol" ).sortable({
		connectWith: ".connected",
		items: 'div.table',
		forceHelperSize:false,
		forcePlaceholderSize:false,
		//containment: '.content',
		//helper: "clone",
		cursor: "move",
		cursorAt:{ left: 190, top:20},
		change:function(event,ui){
			console.log(ui.item)
		}
	});
	$('select').selectBox();
	$( "#form_date, .input_date input" ).datepicker({ dateFormat: "mm/dd/yy" });

	$('.steps_popup').each(function(){
		var $t=$(this);
		$t.nav=$t.find('.popup_nav a');
		/*$t.find('.popup_nav:first a').each(function(){
			console.log($($(this).attr('href')));
		});*/
		$($t.nav.filter(':first').attr('href')).addClass('active');
		$t.nav.click(function(e){
			e.preventDefault();
			if($(this).hasClass('active')) return $(this);
			$t.children('div.active').animate({left:'-110%'},300);
			$($(this).attr('href')).animate({left:'20px'},300,function(){
				$t.children('div.active').removeClass('active').removeAttr('style');
				$(this).addClass('active').removeAttr('style');
				
			});
		});
	});

	$('a.table_head_link').click(function(e){
		e.preventDefault();
		$(this).toggleClass('active');
		$(this).parents('.table, .clearfix').toggleClass('bigZindex');
		$(this).next('div.table_popup').toggle();
		$('div.popup_overlay').insertBefore($(this)).toggle().click(function(){
			$('div.table_popup:visible').prev('a.table_head_link').click();
		});
	});
	
	$('input[type="checkbox"]').customCheckbox(); 
	$('input[type="radio"]').customRadio();

	$('form.validate').validate();

});
var scrolled=false;

$(window).bind('scroll', function(){
	if($(window).scrollTop()>30){
		if(!scrolled){
			$('#mainMenu').stop().animate({margin:'0 -40px'},200);
			$('#headerBlock').stop().slideUp(200);
			scrolled=true;
		}
	}else{
		if(scrolled){
			$('#mainMenu').stop().animate({margin:'0 0'},200);
			$('#headerBlock').stop().slideDown(200);
			scrolled=false;
		}
	}
});

jQuery.fn.insideLabel=function(){
	return $(this).each(function(){
		var $t=$(this);
		$t.input=$t.find('input');
		$t.label=$t.find('label');
		
		if($t.input.val()!=''){
			$t.label.hide();
		}
		
		$t.input.focus(function(){
			$t.label.hide();
		}).blur(function(){
			if($t.input.val()==''){
				$t.label.show();
			}
		});
	});
}
