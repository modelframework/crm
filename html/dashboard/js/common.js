$(document).ready(function() {
	$('.table table tbody tr,#mainMenu li').bind('mouseenter',function(){
		$(this).next().addClass('borderLess');
	}).bind('mouseleave',function(){
		$(this).next().removeClass('borderLess');
	});

	$('#mainMenu .create_submenu a').click(function(e){
		e.preventDefault();
		$(this).parent().toggleClass('visible_submenu');
	});

	$('form a.submitLink').click(function(e){
		e.preventDefault();
		$(this).parents('form').submit();
	});

	$('#header .with_subLinks > a').click(function(){
		$(this).parent().find('.subLinks').toggle();
	});

	$('#search_form .input').insideLabel();
});
var scrolled=false;

$(window).bind('scroll', function(){
	if($(window).scrollTop()>30){
		if(!scrolled){
			$('#mainMenu').stop().animate({margin:'0 -40px'},200);
			$('#headerBlock').stop().slideUp(300);
			scrolled=true;
		}
	}else{
		if(scrolled){
			$('#mainMenu').stop().animate({margin:'0 0'},200);
			$('#headerBlock').stop().slideDown(300);
			scrolled=false;
		}
	}
});

jQuery.fn.insideLabel=function(){
	return $(this).each(function(){
		var $t=$(this);
		$t.input=$t.find('input');
		$t.label=$t.find('label');
		
		if($t.input.val()!=''){
			$t.label.hide();
		}
		
		$t.input.focus(function(){
			$t.label.hide();
		}).blur(function(){
			if($t.input.val()==''){
				$t.label.show();
			}
		});
	});
}
