/**
 * Created by vlad on 22.01.15.
 */

var i = 1;
var cursor = db.Lead.find().addOption(DBQuery.Option.noTimeout);
var count = cursor.count();
// set acl to lead
cursor.forEach(function (item) {
    if (item.credit_card_number !== undefined) {

        var cardLead = {
            "_acl": [
                {"type": "owner", "role": "owner", "role_id": item.owner_id, "data": ["read", "write"]},
                {
                    "type": "hierarchy",
                    "role": "senior",
                    "role_id": ObjectId("5295fdf7c5b9f222acd3c406"),
                    "data": ["read", "write", "delete"]
                },
                {
                    "type": "hierarchy",
                    "role": "admin",
                    "role_id": ObjectId("5295fdf7c5b9f222acd3c405"),
                    "data": ["read", "write", "delete"]
                },
                {"type": "hierarchy", "role": "user", "role_id": ObjectId("5295fdf7c5b9f222acd3c404"), "data": []}
            ],
            "title": item.credit_card_type_export + " **** " + item.credit_card_number.substring(item.credit_card_number.length - 4),
            "lead_title": item.title,
            "lead_id": item._id,
            "card_type_title": item.credit_card_type_export,
            "card_type_id": item.credit_card_type_id,
            "card_status_id": "1",
            "card_status_title": 'Primary Card',
            "card_number": item.credit_card_number,
            "sec_code": item.sec_code,
            "exp_date": item.expiration_date,
            "creator_title": "George Kingsberg",
            "creator_id": ObjectId("54c0f83d5d257b7e188db646"),
            "changer_title": "George Kingsberg",
            "changer_id": ObjectId("54c0f83d5d257b7e188db646"),
            "owner_title": item.owner_title,
            "owner_id": item.owner_id,
            "changed_dtm": item.changed_dtm,
            "created_dtm": item.created_dtm,
            "status_title": "Normal",
            "status_id": ObjectId("5295fdf7c5b9f222acd3c74c")
        };

        if (cardLead.card_number.length) {
            //print( cardLead.card_number, cardLead.card_number.substring( cardLead.card_number.length - 4 ) );
            db.CardLead.insert(cardLead);
            print('Lead [' + (i++) + '/' + count + '] : ' + cardLead.card_number);
        }


    }
});
