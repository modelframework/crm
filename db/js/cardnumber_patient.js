/**
 * Created by vlad on 22.01.15.
 */

function validateCardNumber(number) {
    var regex = new RegExp("^[0-9]{16}$");
    if (!regex.test(number))
        return false;

    return luhnCheck(number);
}

function luhnCheck(val) {
    var sum = 0;
    for (var i = 0; i < val.length; i++) {
        var intVal = parseInt(val.substr(i, 1));
        if (i % 2 == 0) {
            intVal *= 2;
            if (intVal > 9) {
                intVal = 1 + (intVal % 10);
            }
        }
        sum += intVal;
    }
    return (sum % 10) == 0;
}
var i = 1;
var cursor = db.Contacts_copy.find({'Credit Card Number': {$nin: [null, '', '0']}}).addOption(DBQuery.Option.noTimeout);
var count = cursor.count();
// set acl to lead
cursor.forEach(function (item) {

        //print('Contacts [' + (i++) + '/' + count + '] : ' + '' + item['Credit Card Number']);
        //db.Patient.find({'_id_export': item['Patients Id']}).forEach(function (patient) {
        //    print('Patient [' + (i++) + '/' + count + '] : ' + item['Credit Card Number']
        //        + ' ' + patient.card_number
        //    );
        //
        //});

        result = db.Patient.update({'_id_export': item['Patients Id']},
            {
                $set: {
                    'card_number':  '' + item['Credit Card Number']
                }
            },
            {}
        );

    print( 'Patient Card ' + item['Patients Id'] +' [' + (i++) + '/' + count + '] : ' + item['Credit Card Number'] + ' ' + validateCardNumber(item['Credit Card Number']) + ' [' + result.nModified + '/' + result.nMatched + '] ' );
});

 i = 1;
cursor = db.Contacts_copy.find({'Credit Card Number 2': {$nin: [null, '', '0']}}).addOption(DBQuery.Option.noTimeout);
count = cursor.count();
// set acl to lead
cursor.forEach(function (item) {

        //print('Contacts [' + (i++) + '/' + count + '] : ' + '' + item['Credit Card Number']);
        //db.Patient.find({'_id_export': item['Patients Id']}).forEach(function (patient) {
        //    print('Patient [' + (i++) + '/' + count + '] : ' + item['Credit Card Number 2']
        //        + ' ' + patient.card_number_2
        //    );
        //
        //});

        result = db.Patient.update({'_id_export': item['Patients Id']},
            {
                $set: {
                    'card_number_2':  '' + item['Credit Card Number 2']
                }
            },
            {}
        );

    print( 'Patient Card ' + item['Patients Id'] + ' [' + (i++) + '/' + count + '] : ' + item['Credit Card Number 2'] + ' ' + validateCardNumber(item['Credit Card Number 2']) + ' [' + result.nModified + '/' + result.nMatched + '] ' );
});
