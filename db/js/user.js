/**
 * Created by vlad on 22.01.15.
 */

var i = 1;
var cursor = db.User.find().addOption(DBQuery.Option.noTimeout);
var count = cursor.count();
// update users to not see each other
cursor.forEach(function (item) {
    print('User [' + (i++) + '/' + count + '] : ' + item.title);
    db.User.update(
        {'_id': item._id},
        {
            $set: {
                '_acl': [
                    {
                        "type": "hierarchy",
                        "role": "senior",
                        "role_id": ObjectId("5295fdf7c5b9f222acd3c406"),
                        "data": ["read", "write", "delete"]
                    },
                    {
                        "type": "hierarchy",
                        "role": "admin",
                        "role_id": ObjectId("5295fdf7c5b9f222acd3c405"),
                        "data": ["read", "write", "delete"]
                    },
                    {
                        "type": "hierarchy",
                        "role": "user",
                        "role_id": ObjectId("5295fdf7c5b9f222acd3c404"),
                        "data": ["read"]
                    },
                    {"type": "owner", "role": "owner", "role_id": item._id, "data": ["read", "write"]}
                ]
            }
        },
        {}
    );
});
