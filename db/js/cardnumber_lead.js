/**
 * Created by vlad on 22.01.15.
 */
function validateCardNumber(number) {
    var regex = new RegExp("^[0-9]{16}$");
    if (!regex.test(number))
        return false;

    return luhnCheck(number);
}

function luhnCheck(val) {
    var sum = 0;
    for (var i = 0; i < val.length; i++) {
        var intVal = parseInt(val.substr(i, 1));
        if (i % 2 == 0) {
            intVal *= 2;
            if (intVal > 9) {
                intVal = 1 + (intVal % 10);
            }
        }
        sum += intVal;
    }
    return (sum % 10) == 0;
}

var i = 1;
var cursor = db.Leads_copy.find({'Credit Card Number': {$nin: [null, '', '0']}}).addOption(DBQuery.Option.noTimeout);
var count = cursor.count();
// set acl to lead
cursor.forEach(function (item) {
    //print( 'Lead Card [' + (i++) + '/' + count + '] : ' + item['Credit Card Number'] + ' ' + validateCardNumber(item['Credit Card Number']) );

        //print('Lead [' + (i++) + '/' + count + '] : ' + '' + item['Credit Card Number']);
        //db.Lead.find({'_id_export': item['Lead Id']}).forEach(function (lead) {
            //print('Lead [' + (i++) + '/' + count + '] : ' + item['Credit Card Number']
            //    + ' ' + lead.credit_card_number+' '+validateCardNumber(item['Credit Card Number'])
            //);
            //print(lead.credit_card_number);

        //});

        result = db.Lead.update({'_id_export': item['Lead Id']},
            {
                $set: {
                    'credit_card_number':  '' + item['Credit Card Number']
                }
            },
            {}
        );
    print( 'Lead Card [' + (i++) + '/' + count + '] : ' + item['Credit Card Number'] + ' ' + validateCardNumber(item['Credit Card Number']) + ' updated [' + result.nModified + '/' + result.nMatched + '] ' );
        //print('updated [' + result.nModified + '/' + result.nMatched + '] ' );
});


