/**
 * Created by vlad on 22.01.15.
 */

var i = 1;
var cursor = db.Pricebook.find({'_id': {$ne: ObjectId('a00000000000000000000001')}}).addOption(DBQuery.Option.noTimeout);
var count = cursor.count();
// set quote detail owner to quote owner
cursor.forEach(function (item) {
    var result = db.PricebookDetail.update(
        {'pricebook_id': item._id},
        {
            $set: {
                'owner_id': item.owner_id,
                'owner_title': item.owner_title,
                '_acl': [
                    {
                        "type": "hierarchy",
                        "role": "senior",
                        "role_id": ObjectId("5295fdf7c5b9f222acd3c406"),
                        "data": ["read", "write", "delete"]
                    },
                    {
                        "type": "hierarchy",
                        "role": "admin",
                        "role_id": ObjectId("5295fdf7c5b9f222acd3c405"),
                        "data": ["read", "write", "delete"]
                    },
                    {"type": "hierarchy", "role": "user", "role_id": ObjectId("5295fdf7c5b9f222acd3c404"), "data": ["read"]},
                    {"type": "owner", "role": "owner", "role_id": item.owner_id, "data": ["read", "write","delete"]}
                ]
            }
        },
        {'multi': 1}
    );
    print('PricebookDetail [' + (i++) + '/' + count + '] : [' + result.nModified + '/' + result.nMatched + '] ' + item.title);
});

i = 1;
cursor = db.Pricebook.find({'_id': ObjectId('a00000000000000000000001')}).addOption(DBQuery.Option.noTimeout);
count = cursor.count();
// set quote detail owner to quote owner
cursor.forEach(function (item) {
    var result = db.PricebookDetail.update(
        {'pricebook_id': item._id},
        {
            $set: {
                'owner_id': item.owner_id,
                'owner_title': item.owner_title,
                '_acl': [
                    {
                        "type": "hierarchy",
                        "role": "senior",
                        "role_id": ObjectId("5295fdf7c5b9f222acd3c406"),
                        "data": ["read", "write"]
                    },
                    {
                        "type": "hierarchy",
                        "role": "admin",
                        "role_id": ObjectId("5295fdf7c5b9f222acd3c405"),
                        "data": ["read", "write", "delete"]
                    },
                    {"type": "hierarchy", "role": "user", "role_id": ObjectId("5295fdf7c5b9f222acd3c404"), "data": ["read"]},
                    {"type": "owner", "role": "owner", "role_id": item.owner_id, "data": ["read", "write","delete"]}
                ]
            }
        },
        {'multi': 1}
    );
    print('PricebookDetail [' + (i++) + '/' + count + '] : [' + result.nModified + '/' + result.nMatched + '] ' + item.title);
});
