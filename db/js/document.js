/**
 * Created by vlad on 22.01.15.
 */

i = 1;
count = db.Document.find().count();
// set acl to patients
db.Document.find().addOption(DBQuery.Option.noTimeout).forEach(function (item) {
    print('Document [' + (i++) + '/' + count + '] : ' + item.title);
    db.Document.update({'_id': item._id}, {
        $set: {
            '_acl': [
                {
                    "type": "hierarchy",
                    "role": "senior",
                    "role_id": ObjectId("5295fdf7c5b9f222acd3c406"),
                    "data": ["read", "write", "delete"]
                },
                {
                    "type": "hierarchy",
                    "role": "admin",
                    "role_id": ObjectId("5295fdf7c5b9f222acd3c405"),
                    "data": ["read", "write", "delete"]
                },
                {"type": "hierarchy", "role": "user", "role_id": ObjectId("5295fdf7c5b9f222acd3c404"), "data": ["read"]},
                {"type": "owner", "role": "owner", "role_id": item.owner_id, "data": ["read", "write"]}
            ]
        }
    }, {})
});
