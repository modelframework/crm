/**
 * Created by vlad on 22.01.15.
 */


var i = 1;
var cursor = db.PricebookDetail.find({'pricebook_id': {$ne: ObjectId('a00000000000000000000001')}}).addOption(DBQuery.Option.noTimeout);
var count = cursor.count();

// set acl to patients
cursor.forEach(function (item) {
    print('PricebookDetail [' + (i++) + '/' + count + '] : ' + item.title);
    db.PricebookDetail.update(
        {'_id': item._id},
        {
            $set: {
                '_acl': [
                    {
                        "type": "hierarchy",
                        "role": "senior",
                        "role_id": ObjectId("5295fdf7c5b9f222acd3c406"),
                        "data": ["read", "write", "delete"]
                    },
                    {
                        "type": "hierarchy",
                        "role": "admin",
                        "role_id": ObjectId("5295fdf7c5b9f222acd3c405"),
                        "data": ["read", "write", "delete"]
                    },
                    {
                        "type": "hierarchy",
                        "role": "user",
                        "role_id": ObjectId("5295fdf7c5b9f222acd3c404"),
                        "data": ["read"]
                    },
                    {"type": "owner", "role": "owner", "role_id": item.owner_id, "data": ["read", "write", "delete"]}
                ]
            }
        },
        {}
    );
});

i = 1;
cursor = db.PricebookDetail.find({'pricebook_id': ObjectId('a00000000000000000000001')}).addOption(DBQuery.Option.noTimeout);
count = cursor.count();
// set acl to patients
cursor.forEach(function (item) {
    print('Default PricebookDetail [' + (i++) + '/' + count + '] : ' + item.title);
    db.PricebookDetail.update(
        {'_id': item._id},
        {
            $set: {
                '_acl': [
                    {
                        "type": "hierarchy",
                        "role": "senior",
                        "role_id": ObjectId("5295fdf7c5b9f222acd3c406"),
                        "data": ["read"]
                    },
                    {
                        "type": "hierarchy",
                        "role": "admin",
                        "role_id": ObjectId("5295fdf7c5b9f222acd3c405"),
                        "data": ["read", "write", "delete"]
                    },
                    {
                        "type": "hierarchy",
                        "role": "user",
                        "role_id": ObjectId("5295fdf7c5b9f222acd3c404"),
                        "data": ["read"]
                    },
                    {"type": "owner", "role": "owner", "role_id": item.owner_id, "data": ["read", "write", "delete"]}
                ]
            }
        },
        {}
    );
});
