/**
 * Created by vlad on 22.01.15.
 */


var i = 1;
var cursor = db.Pricebook.find({'_id': {$ne: ObjectId('a00000000000000000000001')}}).addOption(DBQuery.Option.noTimeout);
var count = cursor.count();
// set acl to patients
// set acl to patients
cursor.forEach(function (item) {
    print('Pricebook [' + (i++) + '/' + count + '] : ' + item.title);
    db.Pricebook.update(
        {'_id': item._id},
        {
            $set: {
                '_acl': [
                    {
                        "type": "hierarchy",
                        "role": "senior",
                        "role_id": ObjectId("5295fdf7c5b9f222acd3c406"),
                        "data": ["read", "write", "delete"]
                    },
                    {
                        "type": "hierarchy",
                        "role": "admin",
                        "role_id": ObjectId("5295fdf7c5b9f222acd3c405"),
                        "data": ["read", "write", "delete"]
                    },
                    {
                        "type": "hierarchy",
                        "role": "user",
                        "role_id": ObjectId("5295fdf7c5b9f222acd3c404"),
                        "data": ["read"]
                    },
                    {"type": "owner", "role": "owner", "role_id": item.owner_id, "data": ["read", "write"]}
                ]
            }
        },
        {}
    );
});


print('and Default Pricebook Special Setting');
var result = db.Pricebook.update(
    {'_id': ObjectId('a00000000000000000000001')},
    {
        $set: {
            '_acl': [
                {
                    "type": "hierarchy",
                    "role": "senior",
                    "role_id": ObjectId("5295fdf7c5b9f222acd3c406"),
                    "data": ["read"]
                },
                {
                    "type": "hierarchy",
                    "role": "admin",
                    "role_id": ObjectId("5295fdf7c5b9f222acd3c405"),
                    "data": ["read", "write", "delete"]
                },
                {
                    "type": "hierarchy",
                    "role": "user",
                    "role_id": ObjectId("5295fdf7c5b9f222acd3c404"),
                    "data": ["read"]
                },
                {
                    "type": "owner",
                    "role": "owner",
                    "role_id": ObjectId("54c0f83d5d257b7e188db646"),
                    "data": ["read", "write", "delete"]
                }
            ]
        }
    },
    {}
);

print('[' + result.nModified + '/' + result.nMatched + ']');

