/**
 * Created by vlad on 22.01.15.
 */


var i = 1;
var cursor = db.PricebookDetail0.find({'old_product_id': {$exists: false}}).addOption(DBQuery.Option.noTimeout);
var count = cursor.count();
// set acl to patients
cursor.forEach(function (item) {
    //print('PricebookDetail0 [' + (i++) + '/' + count + '] : ' + item.title);
    var pc0 = db.Product0.findOne({'_id': item.product_id});
    if (pc0) {
        //print('Product0 -> ' + pc0.title);
        var pc1 = db.Product.findOne({'_id_export': pc0._id_export});
        if (pc1) {
            print(' -> ' + pc1.title);
            var result = db.PricebookDetail0.update({'_id': item._id}, {
                $set: {
                    'old_product_id': item.product_id,
                    'product_id': pc1._id
                }
            });
            print('updated [' + result.nModified + '/' + result.nMatched + '] ' + item.title);
        }
        else {
                print('Product0 -> ' + pc0.title + 'not found');
        }

    } else {
        print('nf [' + (i++) + '/' + count + '] : ' + item.title);
    }

});
