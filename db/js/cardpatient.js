/**
 * Created by vlad on 22.01.15.
 */

var i = 1;
var cursor = db.Patient.find().addOption(DBQuery.Option.noTimeout);
var count = cursor.count();
// set acl to lead
cursor.forEach(function (item) {

    if (item.card_number !== undefined) {

        var cardPatient = {
            "_acl": [
                {"type": "owner", "role": "owner", "role_id": item.owner_id, "data": ["read", "write"]},
                {
                    "type": "hierarchy",
                    "role": "senior",
                    "role_id": ObjectId("5295fdf7c5b9f222acd3c406"),
                    "data": ["read", "write", "delete"]
                },
                {
                    "type": "hierarchy",
                    "role": "admin",
                    "role_id": ObjectId("5295fdf7c5b9f222acd3c405"),
                    "data": ["read", "write", "delete"]
                },
                {"type": "hierarchy", "role": "user", "role_id": ObjectId("5295fdf7c5b9f222acd3c404"), "data": []}
            ],
            "title": item.card_type_export + " **** " + item.card_number.substring(item.card_number.length - 4),
            "patient_title": item.title,
            "patient_id": item._id,
            "card_type_title": item.card_type_export,
            "card_type_id": item.card_type_id,
            "card_status_id": "1",
            "card_status_title": 'Primary Card',
            "card_number": item.card_number,
            "sec_code": item.sec_code,
            "exp_date": item.exp_date,
            "exact_name_on_card": item.exact_name_on_card,
            "creator_title": "George Kingsberg",
            "creator_id": ObjectId("54c0f83d5d257b7e188db646"),
            "changer_title": "George Kingsberg",
            "changer_id": ObjectId("54c0f83d5d257b7e188db646"),
            "owner_title": item.owner_title,
            "owner_id": item.owner_id,
            "changed_dtm": item.changed_dtm,
            "created_dtm": item.created_dtm,
            "status_title": "Normal",
            "status_id": ObjectId("5295fdf7c5b9f222acd3c74c")
        };


        if (cardPatient.card_number.length) {
            db.CardPatient.insert(cardPatient);
            print('Card [' + (i++) + '/' + count + '] : ' + cardPatient.card_number);
        }
    }
    if (item.card_number_2 !== undefined) {
        var cardPatient2 = {
            "_acl": [
                {"type": "owner", "role": "owner", "role_id": item.owner_id, "data": ["read", "write"]},
                {
                    "type": "hierarchy",
                    "role": "senior",
                    "role_id": ObjectId("5295fdf7c5b9f222acd3c406"),
                    "data": ["read", "write", "delete"]
                },
                {
                    "type": "hierarchy",
                    "role": "admin",
                    "role_id": ObjectId("5295fdf7c5b9f222acd3c405"),
                    "data": ["read", "write", "delete"]
                },
                {"type": "hierarchy", "role": "user", "role_id": ObjectId("5295fdf7c5b9f222acd3c404"), "data": []}
            ],
            "title": item.card_type_2_export + " **** " + item.card_number_2.substring(item.card_number_2.length - 4),
            "patient_title": item.title,
            "patient_id": item._id,
            "card_type_title": item.card_type_2_export,
            "card_type_id": item.card_type_2_id,
            "card_number": item.card_number_2,
            "card_status_id": "2",
            "card_status_title": 'Secondary Card',
            "sec_code": item.sec_code_2,
            "exp_date": item.exp_date_2,
            "exact_name_on_card": item.exact_name_on_card_2,
            "creator_title": "George Kingsberg",
            "creator_id": ObjectId("54c0f83d5d257b7e188db646"),
            "changer_title": "George Kingsberg",
            "changer_id": ObjectId("54c0f83d5d257b7e188db646"),
            "owner_title": item.owner_title,
            "owner_id": item.owner_id,
            "changed_dtm": item.changed_dtm,
            "created_dtm": item.created_dtm,
            "status_title": "Normal",
            "status_id": ObjectId("5295fdf7c5b9f222acd3c74c")
        };
        if (cardPatient2.card_number.length) {
            db.CardPatient.insert(cardPatient2);
            print('Card [' + (i++) + '/' + count + '] : ' + cardPatient2.card_number);


        }
    }


    //print('Patient [' + (i++) + '/' + count + '] : ' + item.title);
});
