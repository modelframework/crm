/**
 * Created by vlad on 22.01.15.
 */

var i = 1;
var cursor = db.Order.find().addOption(DBQuery.Option.noTimeout);
var count = cursor.count();
// set quote detail owner to quote owner
cursor.forEach(function (item) {
    var result = db.OrderDetail.update(
        {'order_id': item._id},
        {
            $set: {
                'owner_id': item.owner_id
            }
        },
        {'multi': 1}
    );
    print('Order [' + (i++) + '/' + count + '] : [' + result.nModified + '/' + result.nMatched + '] ' + item.title);
});
