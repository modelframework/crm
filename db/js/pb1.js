/**
 * Created by vlad on 22.01.15.
 */


var i = 1;
var cursor = db.PricebookDetail0.find({}, {'_id': 0}).addOption(DBQuery.Option.noTimeout);
var count = cursor.count();
// set acl to patients
cursor.forEach(function (item) {
    //print('PricebookDetail0 [' + (i++) + '/' + count + '] : ' + item.title);
    var pc1 = db.PricebookDetail.findOne({'pricebook_id': item.pricebook_id, 'product_id': item.product_id});
    if (pc1) {
        print(pc1.title + ' -> found!');
    } else {
        print('not found prd ' + item.title);
        var result = db.PricebookDetail.insert(item);
        print('inserted [' + result.nModified + '/' + result.nMatched + '] ' + item.title);
    }

});
