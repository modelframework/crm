/**
 * Created by vlad on 22.01.15.
 */

var i = 1;
var cursor = db.Payment.find().addOption(DBQuery.Option.noTimeout);
var count = cursor.count();
// set Invoice detail owner to Invoice owner
cursor.forEach(function (item) {
    var result = db.PaymentDetail.update(
        {'payment_id': item._id},
        {
            $set: {
                'owner_id': item.owner_id
            }
        },
        {'multi': 1}
    );
    print('Payment [' + (i++) + '/' + count + '] : [' + result.nModified + '/' + result.nMatched + '] ' + item.title);
});
