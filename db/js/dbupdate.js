/**
 * Created by vlad on 22.01.15.
 */

load('user.js');
load('lead.js');

load('account.js');
load('patient.js');
load('doctor.js');

load('product.js');

load('pricebook.js');
load('pricebookdetail.js');

load('quote.js');
load('quotedetail_owner.js');
load('quotedetail.js');

load('order.js');
load('orderdetail_owner.js');
load('orderdetail.js');

load('invoice.js');
load('invoicedetail_owner.js');
load('invoicedetail.js');

load('payment.js');
load('paymentdetail_owner.js');
load('paymentdetail.js');

load('activity.js');
load('document.js');

