/**
 * Created by vlad on 22.01.15.
 */


var i = 1;
var cursor = db.PricebookDetail.find().addOption(DBQuery.Option.noTimeout);
var count = cursor.count();
// set acl to patients
cursor.forEach(function (item) {
    //print('PricebookDetail0 [' + (i++) + '/' + count + '] : ' + item.title);
    var pc1 = db.Product.findOne({'_id': item.product_id});
    if (pc1) {
        //print(' -> ' + pc1.title);
    } else {
        print('not found prd ' + (i++) + '/' + count + '] : ' + item.product_title);

    }

    //print('PricebookDetail0 [' + (i++) + '/' + count + '] : ' + item.title);
    var pb = db.Pricebook.findOne({'_id': item.pricebook_id});
    if (pb) {
        //print(' -> ' + pb.title);
    } else {
        print('not found pbk ' + (i++) + '/' + count + '] : ' + item.pricebook_title);
    }

    if (pb && pc1) {
        print(pb.title + ' -> ' + item.list_price + ' -> ' + pc1.title);

    }

});
