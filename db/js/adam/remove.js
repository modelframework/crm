/**
 * Created by vlad on 30.01.15.
 */
var ownerId = ObjectId("54c0f83d5d257b7e188db65c");
db.Account.remove({'owner_id': {$ne: ownerId }} );
db.Activity.remove( { 'owner_id': { $ne: ownerId }} );
// Doctor
db.InvoiceDetail.remove( { 'owner_id': { $ne: ownerId }} );
db.Invoice.remove( { 'owner_id': { $ne: ownerId }} );
db.Note.remove( { 'owner_id': { $ne: ownerId }} );
db.NoteLead.remove( { 'owner_id': { $ne: ownerId }} );
db.NotePatient.remove( { 'owner_id': { $ne: ownerId }} );

db.OrderDetail.remove( { 'owner_id': { $ne: ownerId }} );
db.Order.remove( { 'owner_id': { $ne: ownerId }} );
db.Patient.remove( { 'owner_id': { $ne: ownerId }} );

// PricebookDetail
// Pricebook
// Product
db.QuoteDetail.remove( { 'owner_id': { $ne: ownerId }} );
db.Quote.remove( { 'owner_id': { $ne: ownerId }} );

