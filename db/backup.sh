#!/bin/bash
path="backup1"

fullpath=$path/$(date +%F)
if [ ! -d $fullpath ];
then
    mkdir $fullpath
fi

echo $fullpath 
mongodump --out "$fullpath"
