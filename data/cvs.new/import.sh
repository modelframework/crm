#!/bin/bash

echo "Importing\n"
printf "\tFile\t\t\tCollection\n"
for fullname in ./*.csv
do 
	filename=$(basename "$fullname")
	cname= "${filename%.*}"
 	printf " ... %s \t" $filename   
	printf " ->  %s \n" $cname  
	#mongoimport --host 192.168.10.170 --db zoho_csv  --type=csv --headerline --file Accounts1.csv -c Accounts
	#mongoimport --db zoho_csv --type=csv --headerline --file $fullname -c $cname
done

