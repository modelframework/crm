<?php

namespace Wepo\Lib;

use Zend\InputFilter\InputFilterAwareInterface;
use Zend\InputFilter\InputFilterInterface;
use Zend\InputFilter\Factory as InputFactory;
use Zend\InputFilter\InputFilter;

class WepoModel implements InputFilterAwareInterface
{
    public $table_name  = null;
    public $adapterName = 'wepo_company';
    protected $_fields  = [ ];
    protected $_joins   = [ ];
    protected $_unique  = [ ];
    protected $_filters = [ ];
    protected $inputFilter;
//    protected $fieldsets;
    protected $_cfs     = [
        '_fields'     => 'fields',
        '_joins'      => 'joins',
        '_unique'     => 'unique',
        '_filters'    => 'filters',
        'adapterName' => 'adapter',
        'table_name'  => 'table',
        'model'       => 'model',
        'fieldsets'       => 'fieldsets',
    ];
    public $_data       = [ ];

    public function __construct($data = array())
    {
        $this->exchangeArray($data);
    }

    public function __set($name, $value)
    {
        if (isset($this->_fields[ $name ])) {
            $this->_data[ $name ] = $value;
            if (isset($this->_fields[ $name ][ 'datatype' ]) && $value !== null) {
                settype($this->_data[ $name ], $this->_fields[ $name ][ 'datatype' ]);
            }

            return $this->_data[ $name ];
        } else {
            $this->$name = $value;

            return $this->$name;
        }
    }

    public function __call($name, $arguments)
    {
        //        prn( $name, $arguments );
        if (!isset($this->_fields[ $name ])) {
            $trace = debug_backtrace();
            trigger_error(
                'Undefined property в __call(): '.$name.
                ' in file '.$trace[ 0 ][ 'file' ].
                ' row '.$trace[ 0 ][ 'line' ], E_USER_NOTICE);
        }

        if (count($arguments) == 0) {
            return $this->_data[ $name ] ?: $this->_fields[ $name ][ 'default' ];
        }

        $_result = [ ];
        foreach ($arguments as $value) {
            if (isset($this->_fields[ $name ][ 'datatype' ]) && $value !== null) {
                settype($value, $this->_fields[ $name ][ 'datatype' ]);
            }
            $_result[] = $value;
        }

        if (count($_result) == 1) {
            $_result = array_shift($_result);
        }

        return $_result;
    }

    public function __get($name)
    {
        //        prn( $name );
        if (array_key_exists($name, $this->_data)) {
            return $this->_data[ $name ];
        }

        return;
    }

    public function __isset($name)
    {
        return isset($this->_data[ $name ]);
    }

    public function __unset($name)
    {
        unset($this->_data[ $name ]);
    }

    public function getConfig()
    {
        $config = [ ];
        foreach ($this->_cfs as $_k => $_v) {
            //            if ( $_k == 'model' )
//            {
//                $config[ $_v ] = substr( get_class( $this ), 11 );
//            }
//            else
//            {

            $config[ $_v ] = $this->$_k;
//            }
        }
//        $config[ 'filters' ] = $this -> getInputFilter() -> _data;
        $config[ 'filters' ] = $this->_filters;

        return $config;
    }

    public function parseconfig(array $config)
    {
        foreach ($this->_cfs as $_k => $_v) {
            if (isset($config[ $_v ])) {
                $this->$_k = $config[ $_v ];
            }
        }
        $this->createInputFilter($config[ 'filters' ]);
        $this->exchangeArray([ ]);

        return $this;
    }

    protected function createInputFilter(array $config)
    {
        $this->_filters = $config;
//        if ( !$this -> inputFilter )
//        {
        $inputFilter      = new InputFilter();
        $factory          = new InputFactory();

        foreach ($config as $_filter) {
            $inputFilter->add($factory->createInput($_filter));
        }
        $this->inputFilter = $inputFilter;
//        }
        return $this->inputFilter;
    }

    public function getInputFilter()
    {
        if (!$this->inputFilter) {
            $this->createInputFilter($this->_filters);
        }

        return $this->inputFilter;
    }

    protected function _cat($name, $value = null)
    {
        if (func_num_args() > 1) {
            $this->$name = $value;

            return $this;
        }

        return $this->$name;
    }

    public function unique()
    {
        return $this->_cat('_unique');
    }

    public function exchangeArray($data)
    {
        if (!isset($this->_fields)) {
            throw new \Exception(' _fields property is not set in '.get_class());
        }
        foreach ($this->_fields as $_field => $_properties) {
            if (isset($data[ $_field ])) {
                $this->$_field = $data[ $_field ];
            } else {
                $this->$_field = isset($_properties[ 'default' ]) ? $_properties[ 'default' ] : null;
            }
        }
        foreach ($this->_joins as $_join) {
            foreach (array_keys($_join[ 'fields' ]) as $_field) {
                if (isset($data[ $_field ])) {
                    $this->$_field = $data[ $_field ];
                }
            }
        }

        return $this;
    }

    public function exchange($data)
    {
        if (is_object($data)) {
            if (method_exists($data, 'toArray')) {
                $data = $data->toArray();
            } else {
                $data = get_object_vars($data);
            }
        }
        if (!is_array($data)) {
            throw new \Exception(' Array or object expected ');
        }

        return $this->exchangeArray($data);
    }

    public function postExchange()
    {
        return $this;
    }

    public function getData($params = array(), $return_config = false)
    {
        $_data = [ ];
        foreach ($this->_fields as $_field => $_config) {
            if (in_array($_config[ 'type' ], $params)) {
                $_data[ $_field ] = $return_config ? $_config : $this->$_field;
            }
        }

        return $_data;
    }

    public function getFieldCount($params = [ 'pk', 'field', 'source', 'alias' ])
    {
        $count = 0;
        foreach ($this->_fields as $_config) {
            if (in_array($_config[ 'type' ], $params)) {
                $count++;
            }
        }

        return $count;
    }

    public function getFieldNames($params = [ 'pk', 'field', 'source' ])
    {
        $_fieldnames = [ ];
        foreach ($this->_fields as $_field => $_config) {
            if (in_array($_config[ 'type' ], $params)) {
                $_fieldnames[] = $_field;
            }
        }

        return $_fieldnames;
    }

    public function getFields()
    {
        //        prn( 'getFields' );
        return $this->getData([ 'pk', 'field', 'source' ]);
    }

    public function pk()
    {
        return $this->getData([ 'pk' ]);
    }

    public function toArray()
    {
        return $this->getData([ 'field', 'source' ]);
    }

    public function toSaveArray()
    {
        return $this->getData([ 'field', 'source', 'alias', 'address', 'description' ]);
    }

    public function toView()
    {
        return $this->getData([ 'field', 'alias' ]);
    }

    public function aliasmap()
    {
        $_data         = [ ];
        foreach ($this->getData([ 'alias' ], $return_config = true) as $_alias => $_config) {
            $_data[ $_alias ] = $_config[ 'source' ];
        }

        return $_data;
    }

    public function fieldmap()
    {
        return $this->_fields;
    }

    public function data()
    {
        $data = $this->getArrayCopy();
        unset($data['_id']);

        return $data;
    }

    public function getArrayCopy()
    {
        return $this->_data;
    }

    public function values($data)
    {
        static $results = [ ];
        if (is_object($data)) {
            if (method_exists($data, 'toArray')) {
                $data = array_keys($data->toArray());
            } else {
                $data = array_keys(get_object_vars($data));
            }
        }
        if (is_array($data)) {
            foreach ($data as $_v) {
                $this->values($_v);
            }
        } elseif (!isset($results[ $data ])) {
            $results[ $data ] = $data;
        }

        return $results;
    }

    public function split($ValidationGroup)
    {
        $fields = $this->values($ValidationGroup);
        $data   = $this->getFields();
        foreach ($fields as $_v) {
            unset($data[ $_v ]);
        }

        return $data;
    }

    public function setInputFilter(InputFilterInterface $inputFilter)
    {
        throw new \Exception("Not used");
    }

    public function merge($data)
    {
        if (!isset($this->_fields)) {
            throw new \Exception(' _fields property is not set in '.get_class());
        }
        if (is_object($data)) {
            if (method_exists($data, 'getFields')) {
                $data = $this->getFields();
            } else {
                $data = get_object_vars($data);
            }
        }
        if (!is_array($data)) {
            throw new \Exception(' Array or object expected ');
        }
        foreach (array_keys($this->_fields) as $_field) {
            if (isset($data[ $_field ])) {
                $this->$_field = $data[ $_field ];
            }
        }

        return $this;
    }

    public function getJoins()
    {
        return $this->_joins;
    }

    public function getModelName()
    {
        if (empty($this->model)) {
            $_wrs  = explode('\\', get_class($this));
            $model = array_pop($_wrs);
        } else {
            $model = $this->model;
        }

        return $model;
    }

    public function getModelLabel()
    {
        return $this->getModelName();
    }

    public function id()
    {
        if (isset($this->id)) {
            return $this->id;
        }

        return;
    }
}
