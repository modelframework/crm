<?php

namespace Wepo\Lib;

use Zend\Db\Adapter\AdapterInterface;
use Zend\Db\ResultSet\ResultSet;
use Zend\Db\ResultSet\ResultSetInterface;
use Zend\Db\Sql\TableIdentifier;
use Zend\Db\TableGateway\Exception;
use Zend\Db\TableGateway\Feature\FeatureSet;
use Wepo\Lib\WepoGatewayInterface;
use Zend\ServiceManager\ServiceLocatorAwareInterface;
use Zend\ServiceManager\ServiceLocatorInterface;
use MonZend\Paginator\Adapter\MongoCursor;

class WepoMongoLiteGateway implements WepoGatewayInterface, ServiceLocatorAwareInterface
{
    /**+
     * @var bool
     */
    protected $isInitialized = false;

    /**
     * @var AdapterInterface
     */
    protected $adapter = null;

    /**
     * @var string
     */
    protected $table = null;

    /**
     * @var \MongoCollection
     */
    protected $collection = null;

    /**
     * @var array
     */
    protected $columns = array();

    /**
     * @var Feature\FeatureSet
     */
    protected $featureSet = null;

    /**
     * @var ResultSetInterface
     */
    protected $resultSetPrototype = null;

    /**
     * @var Profiler\ProfilerInterface
     */
    protected $profiler = null;

    /**
     *
     * @var \MongoId
     */
    protected $lastInsertValue = null;
    protected $serviceLocator = null;

    /**
     * Constructor
     *
     * @param string                                                               $table
     * @param AdapterInterface                                                     $adapter
     * @param Feature\AbstractFeature|Feature\FeatureSet|Feature\AbstractFeature[] $features
     * @param ResultSetInterface                                                   $resultSetPrototype
     * @param Sql                                                                  $sql
     *
     * @throws Exception\InvalidArgumentException
     */
    public function __construct($table, AdapterInterface $adapter, $features = null,
                                 ResultSetInterface $resultSetPrototype = null, Sql $sql = null)
    {
        // table
        if (!(is_string($table) || $table instanceof TableIdentifier)) {
            throw new Exception\InvalidArgumentException('Table name must be a string or an instance of Zend\Db\Sql\TableIdentifier');
        }
        $this->table = $table;

        // adapter
        $this->adapter = $adapter;

        // process features
        if ($features !== null) {
            if ($features instanceof Feature\AbstractFeature) {
                $features = array( $features );
            }
            if (is_array($features)) {
                $this->featureSet = new FeatureSet($features);
            } elseif ($features instanceof FeatureSet) {
                $this->featureSet = $features;
            } else {
                throw new Exception\InvalidArgumentException(
                    'TableGateway expects $feature to be an instance of an AbstractFeature or a FeatureSet, or an array of AbstractFeatures'
                );
            }
        } else {
            $this->featureSet = new FeatureSet();
        }

        // result prototype
        $this->resultSetPrototype = ($resultSetPrototype) ?: new ResultSet();

        $this->initialize();
    }

    /**
     * Set serviceManager instance
     *
     * @param ServiceLocatorInterface $serviceLocator
     *
     * @return void
     */
    public function setServiceLocator(ServiceLocatorInterface $serviceLocator)
    {
        $this->serviceLocator = $serviceLocator;
    }

    /**
     * Retrieve serviceManager instance
     *
     * @return ServiceLocatorInterface
     */
    public function getServiceLocator()
    {
        return $this->serviceLocator;
    }

    /**
     * @return bool
     */
    public function isInitialized()
    {
        return $this->isInitialized;
    }

    /**
     * Initialize
     *
     * @throws Exception\RuntimeException
     * @return null
     */
    public function initialize()
    {
        if ($this->isInitialized()) {
            return;
        }

        if (!$this->featureSet instanceof FeatureSet) {
            $this->featureSet = new FeatureSet();
        }

        // :FIXME: add featureSet compat
//        $this -> featureSet -> setTableGateway( $this );
//        $this -> featureSet -> apply( 'preInitialize', array() );

        if (!$this->adapter instanceof AdapterInterface) {
            throw new Exception\RuntimeException('This table does not have an Adapter setup');
        }

        if (!is_string($this->table) && !$this->table instanceof TableIdentifier) {
            throw new Exception\RuntimeException('This table object does not have a valid table set.');
        }

        if (!$this->resultSetPrototype instanceof ResultSetInterface) {
            $this->resultSetPrototype = new ResultSet();
        }

        $this->collection =
            $this->adapter->getDriver()->getConnection()->getDB()->selectCollection($this->getTable());
//        $this -> featureSet -> apply( 'postInitialize', array() );

        if (($object = $this->resultSetPrototype->getArrayObjectPrototype()) instanceof WepoModel &&
             method_exists($object, 'getFieldNames') && is_array($columns = $object->getFieldNames())
        ) {
            $this->columns = $columns;
        }

        $this->isInitialized = true;
    }

    /**
     * Get table name
     *
     * @return string
     */
    public function getTable()
    {
        return $this->table instanceof TableIdentifier ? $this->table->getTable() : $this->table;
    }

    /**
     * Get adapter
     *
     * @return AdapterInterface
     */
    public function getAdapter()
    {
        return $this->adapter;
    }

    /**
     * @return array
     */
    public function getColumns()
    {
        return $this->columns;
    }

    /**
     * @return Feature\FeatureSet
     */
    public function getFeatureSet()
    {
        return $this->featureSet;
    }

    /**
     * Get select result prototype
     *
     * @return ResultSet
     */
    public function getResultSetPrototype()
    {
        return $this->resultSetPrototype;
    }

    /**
     * @param Profiler\ProfilerInterface $profiler
     *
     * @return Statement
     */
    public function setProfiler(Profiler\ProfilerInterface $profiler)
    {
        $this->profiler = $profiler;

        return $this;
    }

    /**
     * @return null|Profiler\ProfilerInterface
     */
    public function getProfiler()
    {
        return $this->profiler;
    }

    protected function convertid($ids)
    {
        if (is_array($ids)) {
            foreach ($ids as $_k => $_v) {
                $ids[ $_k ] = $this->convertid($_v);
            }
        } elseif (!$ids instanceof \MongoId && preg_match('/^[0123456789abcdefABCDEF]{24}$/', $ids)) {
            $ids = new \MongoId($ids);
        }

        return $ids;
    }

    protected function _convertids($fields = array())
    {
        if ($fields == null) {
            $fields = [ ];
        }
        foreach ($fields as $_key => $_value) {
            $_value          = $this->convertid($_value);
            $fields[ $_key ] = $_value;
        }

        return $fields;
    }

    /**
     * Select
     *
     * @param Where|\Closure|string|array $where
     *
     * @return array
     */
    protected function _where0($where = array())
    {
        $where = $this->_convertids($where);
        foreach ($where as $_key => $_value) {
            if ($_key{0} == '-') {
                if (is_array($_value)) {
                    $where[ substr($_key, 1) ] = [ '$nin' => $_value ];
                } else {
                    $where[ substr($_key, 1) ] = [ '$ne' => $_value ];
                }
                unset($where[ $_key ]);
            } else {
                if (is_array($_value)) {
                    $where[ $_key ] = [ '$in' => $_value ];
                }
            }
        }

        return $where;
    }

    protected function _where($where = array())
    {
        return $this->_mongowhere($this->_convertids($where));
    }

    protected function _mongowhere($where = array())
    {
        $_where = [ ];
        foreach ($where as $_key => $_value) {
            if ($_key{0} == '$' || is_numeric($_key)) {
                if (is_array($_value)) {
                    $_value = $this->_mongowhere($_value);
                }
            } elseif (substr($_key, 0, 2) == '<=') {
                $_value = [ '$lte' => $_value ];
                $_key   = substr($_key, 2);
            } elseif (substr($_key, 0, 2) == '>=') {
                $_value = [ '$gte' => $_value ];
                $_key   = substr($_key, 2);
            } elseif ($_key{0} == '>') {
                $_value = [ '$gt' => $_value ];
                $_key   = substr($_key, 1);
            } elseif ($_key{0} == '<') {
                $_value = [ '$lt' => $_value ];
                $_key   = substr($_key, 1);
            } elseif ($_key{0} == '-' && is_array($_value)) {
                $_value = [ '$nin' => $_value ];
                $_key   = substr($_key, 1);
            } elseif ($_key{0} == '-' && !is_array($_value)) {
                $_value = [ '$ne' => $_value ];
                $_key   = substr($_key, 1);
            } elseif (is_array($_value)) {
                $_value = [ '$in' => $_value ];
            }
            $_where[ $_key ] = $_value;
        }

        return $_where;
    }

    protected function _orders($orders = array())
    {
        if ($orders == null) {
            $orders = [ ];
        }
        foreach ($orders as $_k => $_v) {
            if (strtolower($_v) == 'desc' || $_v < 0) {
                $orders[ $_k ] = -1;
            } else {
                $orders[ $_k ] = 1;
            }
        }

        return $orders;
    }

    /**
     * Select
     *
     * @param Where|\Closure|string|array $where
     *
     * @return ResultSet
     */
    public function select($where = null)
    {
        if ($this->profiler) {
            $this->profiler->profilerStart($this);
        }

        // apply preSelect features
        // $this -> featureSet -> apply( 'preSelect', array( $select ) );

        $return = $this->collection->find($this->_where($where));

        if ($this->profiler) {
            $this->profiler->profilerFinish();
        }

        if ($return === false) {
            throw new Exception\RuntimeException($this->getDriver()->getConnection()->getDB()->lastError());
        }

        $result = $this->adapter->getDriver()->createResult($return, $this->getTable());

        $resultSet = clone $this->resultSetPrototype;
        $resultSet->initialize($result);

        // apply postSelect features
        // $this -> featureSet -> apply( 'postSelect', array( $result, $resultSet ) );

        return $resultSet;
    }

    /**
     * Insert
     *
     * @param array $set
     *
     * @return array
     */
    public function insert($set)
    {
        $return                = $this->collection->insert($set);
        $this->lastInsertValue = $set[ '_id' ];

        return $return;
    }

    /**
     * Update
     *
     * @param array $set
     * @param array $where
     *
     * @return array
     */
    public function update($set, $where = null)
    {
        $return                =
            $this->collection->update($this->_where($where), [ '$set' => $this->_convertids($set) ],
                                       [ 'multiple' => true ]);
        $this->lastInsertValue = 0;

        return $return;
    }

    /**
     * Delete
     *
     * @param Where|\Closure|string|array $where
     *
     * @return int
     */
    public function delete($where)
    {
        $return                = $this->collection->remove($this->_where($where));
        $this->lastInsertValue = 0;

        return $return;
    }

    /**
     * @return \Zend\Db\ResultSet\ResultSet
     * @throws Exception\RuntimeException
     */
    public function fetchAll()
    {
        return $this->select();
    }

    /**
     * @param string $id
     *
     * @return WepoModel
     */
    public function get($id)
    {
        if ($this->profiler) {
            $this->profiler->profilerStart($this);
        }

        // apply preSelect features
//        $this -> featureSet -> apply( 'preSelect', array( $select ) );

        try {
            //          $where = [ '_id' => $this -> convertid( $id ) ];
            $return = $this->collection->findOne($this->_where([ '_id' => $id ]));
        } catch (\Exception $ex) {
            throw new \Exception('wrong mongo id object.'.$ex->getMessage());
        }

        if ($this->profiler) {
            $this->profiler->profilerFinish();
        }

        if ($return === false) {
            throw new Exception\RuntimeException($this->getDriver()->getConnection()->getDB()->lastError());
        }

        if ($return === null) {
            return;
        }

        $model  = clone $this->resultSetPrototype->getArrayObjectPrototype();
        $result = $model->exchangeArray($return);

        return $result;
    }

    /**
     * @param string $id
     *
     * @return WepoModel
     */
    public function findOne($where)
    {
        if ($this->profiler) {
            $this->profiler->profilerStart($this);
        }

        // apply preSelect features
        // $this -> featureSet -> apply( 'preSelect', array( $select ) );

        $return = $this->collection->findOne($this->_where($where));

        if ($this->profiler) {
            $this->profiler->profilerFinish();
        }

        if ($return === false) {
            throw new Exception\RuntimeException($this->getDriver()->getConnection()->getDB()->lastError());
        }

        if ($return === null) {
            return;
        }

        $model = clone $this->resultSetPrototype->getArrayObjectPrototype();
        if ($model instanceof \ArrayObject) {
            $model->exchangeArray($return);
            $result = $model;
        } elseif (method_exists($model, 'exchangeArray')) {
            $model->exchangeArray($return);
            $result = $model;
        } else {
            $result = $return;
        }

        return $result;
    }

    /**
     * @param string $id
     *
     * @return WepoModel
     */
    public function raw($id)
    {
        if ($this->profiler) {
            $this->profiler->profilerStart($this);
        }

        // apply preSelect features
//        $this -> featureSet -> apply( 'preSelect', array( $select ) );

        try {
            if (!$id instanceof \MongoId) {
                $_id = new \MongoId($id);
            } else {
                $_id = $id;
            }
            $where = [ '_id' => $_id ];
        } catch (\Exception $ex) {
            throw new \Exception('wrong mongo id object.'.$ex->getMessage());
        }

        $return = $this->collection->findOne($where);

        return $return;
    }

    protected function assemble($fields = array(), $orders = array(), $limit = null, $offset = null)
    {
        prn('assemble');
    }

    public function gather($fields = array(), $orders = array(), $limit = null, $offset = null)
    {
        # :FIXME: add joins functionality
        return $this->find($fields, $orders, $limit, $offset);
    }

    /**
     * @param array    $fields
     * @param array    $orders
     * @param null|int $limit
     * @param null|int $offset
     *
     * @return ResultSet|ResultSetInterface
     * @throws \Zend\Db\TableGateway\Exception\RuntimeException
     * @throws \Exception
     */
    public function find($fields = array(), $orders = array(), $limit = null, $offset = null)
    {
        if (!is_array($fields) || !is_array($orders)) {
            throw new \Exception("Wrong input type of parameters !");
        }

        if ($this->profiler) {
            $this->profiler->profilerStart($this);
        }

        // apply preSelect features
//        $this -> featureSet -> apply( 'preSelect', array( $select ) );

        $return = $this->collection->find($this->_where($fields));

        foreach ($orders as $_k => $_v) {
            if (strtolower($_v) == 'desc' || $_v < 0) {
                $orders[ $_k ] = -1;
            } else {
                $orders[ $_k ] = 1;
            }
        }

        if (!empty($orders)) {
            $return->sort($this->_orders($orders));
        }

        if ($limit !== null) {
            $return->limit($limit);
        }

        if ($offset !== null) {
            $return->skip($offset);
        }

        if ($this->profiler) {
            $this->profiler->profilerFinish();
        }

        if ($return === false) {
            throw new RuntimeException($this->getDriver()->getConnection()->getDB()->lastError());
        }
        $result    = $this->adapter->getDriver()->createResult($return, $this->getTable());
        $resultSet = clone $this->resultSetPrototype;
        $resultSet->initialize($result);

        // apply postSelect features
//        $this -> featureSet -> apply( 'postSelect', array( $result, $resultSet ) );

        return $resultSet;
    }

    public function isUnique(WepoModel $model)
    {
        foreach ($model->unique() as $_unique) {
            $_data = [ ];
            foreach ((array) $_unique as $_key) {
                $_data[ $_key ] = $model->$_key;
            }
            $check = $this->find($_data);
            if ($check->count() > 0 && $check->current()->id() != $model->id()) {
                return false;
            }
        }

        return true;
    }

    public function save(WepoModel $model)
    {
        $result = false;
        $data   = $this->_convertids($model->toSaveArray());
        $_id    = $model->_id;
        if (!$this->isUnique($model)) {
            throw new \Exception('Data is not unique');
        }
        if (empty($_id)) {
            $iresult = $this->insert($data);
            $result  = empty($iresult[ 'ok' ]) ? 0 : $iresult[ 'ok' ];
        } else {
            if ($this->get($_id)) {
                $result = $this->update($data, $this->_where([ '_id' => $_id ]));
            } else {
                throw new \Exception('Model id does not exist');
            }
        }

        return $result;
    }

    public function getPages($fields = array(), $where = array(), $orders = array() /* $params = array( ) */)
    {
        #:FIXME: add joins functionality
        $cursor = $this->collection->find($this->_where($where));
//        $cursor->fields($fields);
        if (!empty($orders)) {
            $cursor->sort($this->_orders($orders));
        }

        $adapter   = new MongoCursor($cursor, $this->getResultSetPrototype());
        $paginator = new \Zend\Paginator\Paginator($adapter);

        return $paginator;
    }

    public function getPaginator($where = array(), $orders = array())
    {
        $cursor = $this->collection->find($this->_where($where));
        if (!empty($orders)) {
            $cursor->sort($this->_orders($orders));
        }
        $adapter   = new MongoCursor($cursor, $this->getResultSetPrototype());
        $paginator = new \Zend\Paginator\Paginator($adapter);

        return $paginator;
    }

    public function getLastInsertId()
    {
        return $this->lastInsertValue;
    }

    public function model()
    {
        return clone $this->resultSetPrototype->getArrayObjectPrototype();
    }

    public function ensureIndex($index)
    {
        $this->collection->deleteIndexes();
        $this->collection->ensureIndex($index, array(
            'name' => $this->table.'TextIndex',
        ));
    }
}
