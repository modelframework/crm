<?php

namespace Wepo\Lib;

class WepoMongoGateway_old implements WepoGatewayInterface
{
    protected $services;
    protected $_tables = [ ];
    private static $_connections;
    private $connection;
    private $modelname;
    private $collection;
    private $table;

    public function __construct($modelname, $dbname = 'default')
    {
        $this->setConnection($dbname);
        $this->modelname = $_m                = 'Wepo\\Model\\'.$modelname;

        $this->table      = $_m::TABLE_NAME;
//        var_dump( $this -> connection );
//        $this -> collection = $this ->connection -> {$this -> table};
        $this->collection = $this->connection->selectCollection($this->table);
        //var_dump($this -> collection);
    }

    public function get($id)
    {
        $id     = (int) $id;
        $rowset = $this->find(array( 'id' => $id ));
        $row    = $rowset->current();
        if (!$row) {
            throw new \Exception("Could not find row $id");
        }

        return $row;
    }

    public function getPages($fields = array(), $conditions = array(), $orders = array()/* $params = array( ) */)
    {
        //        $_query = $this -> collection -> find();

        $paginator = new \Zend\Paginator\Paginator(new \Zend\Paginator\Adapter\ArrayAdapter($this->fetchAll()));

        return $paginator;
    }

    public function delete($where)
    {
        $result = false;

        if ($this->collection->find($where)) {
            $criteria = array( '_id' => new \MongoId($id) );
            $this->collection->remove($criteria, array( "justone" => true ));
            $result   = true;
        }

        return $result;
    }

    public function fetchAll()
    {
        $res = $this->find();

        return $res;
    }

    public function find($fields = array(), $orders = array(), $limit = null, $offset = null)
    {
        $results = array();

//        var_dump( $this -> collection );
        if (count($fields)) {
            $result = $this->collection->find($fields);
        } else {
            $result = $this->collection->find();
        }
        if ($limit != null) {
            $result = $result->limit($limit);
        }
        if ($offset != null) {
            $result = $result->skip($offset);
        }
        if (count($orders)) {
            foreach ($orders as $_fieldname => &$_order) {
                switch ($_order) {
                    case 'asc': $_order = 1;
                        break;
                    case 'desc': $_order = -1;
                        break;
                }
                if ($_fieldname == 'id') {
                    $_order          = new \MongoId($_order);
                    $orders[ '_id' ] = $orders[ $_fieldname ];
                    unset($orders[ $_fieldname ]);
                }
            }
            $result = $result->sort($orders);
        }
        prn($result);
        exit();
        foreach ($result as $cursor) {
            //            $results[] = new $this -> modelname( $cursor );
            $results[] = $cursor;
        }

        return $results;
    }

    public function gather($fields = array(), $orders = array(), $limit = null, $offset = null)
    {
        return $this->find($fields, $orders, $limit, $offset);
    }

    public function isUnique(WepoModel $model)
    {
        foreach ($model->unique() as $_unique) {
            $_data = [ ];
            foreach ((array) $_unique as $_key) {
                $_data[ $_key ] = $model->$_key;
            }
            $check = $this->find($_data);
//            var_dump($check[0]);
            for ($i = 0; $i < count($check); $i++) {
                foreach ((array) $_unique as $_key) {
                    if (($check[ $i ]->$_key == $model->$_key) && ($check[ $i ]->id != $model->id)) {
                        return false;
                    }
                }
            }
        }

        return true;
    }

    public function save(WepoModel $model)
    {
        $result = false;
        $data   = $model->toArray();
        $id     = $model->__getset('id');

        if (!$this->isUnique($model)) {
            //throw new \Exception( 'Data is not unique' );
            return false;
        }

        if ($id == 0) {
            //insert
//            echo 'iam in insert';
            $this->collection->insert($data, array( "w" => 1 ));
            $data[ 'id' ] = $data[ '_id' ];
            $result       = $data[ 'id' ];
        } else {
            //update
//            echo 'iam in update';
            if ($this->find(['id' => $id ])) {
                $data[ '_id' ] = new \MongoId($id);
                $this->collection->save($data, array( "w" => 1 ));
                $result        = $id;
            } else {
                throw new \Exception('Form id does not exist');
            }
        }

        return $result;
    }

    public function setConnection($dbname)
    {
        if (!isset(self::$_connections[ $dbname ])) {
            self::$_connections[ $dbname ] = self::createConnection($dbname);
        }
        $this->connection = self::$_connections[ $dbname ];

        return $this;
    }

    public static function createConnection($dbname)
    {
        // $conndata = ConnectionData::getConnectionData( $dbname );

        $h  = new \MongoClient("mongodb://localhost");
//        $h = new MongoClient( sprintf( $conndata[ 'dsn' ], $conndata[ 'username' ], $conndata[ 'password' ] ), array( "db" => $conndata[ 'dbname' ] ) );
        $db = new \MongoDB($h, 'wepo_company_1');
//        $h = new MongoClient(
////            sprintf( $conndata[ 'dsn' ], $conndata[ 'username' ], $conndata[ 'password' ] )
//            sprintf( $conndata[ 'dsn' ], $conndata[ 'username' ], $conndata[ 'password' ],  )
//        );
//        $h -> selectDB( $conndata[ 'dbname' ] );
        return $db;
    }
}
