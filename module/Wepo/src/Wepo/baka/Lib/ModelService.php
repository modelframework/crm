<?php

namespace Wepo\Lib;

use Zend\ServiceManager\ServiceLocatorAwareInterface;
use Zend\ServiceManager\ServiceLocatorInterface;
use Wepo\Model\ConfigModel2;
use Wepo\Model\ConfigForm;
use ModelFramework\Utility\Obj;

class ModelService implements ServiceLocatorAwareInterface
{
    protected $services;
    protected $_adapters = [
        'wepo_company',
    ];
    protected $_permissions = [
        'Test' => [
        ],
    ];
    protected $_modelconfig = [
        'Test'     => [
            'adapter' => 'wepo_company',
            'model'   => 'Test',
            'fields'  => [
                'fname'      => [ 'type' => 'text', 'group' => 'fields', 'label' => 'First Name' ],
                'lname'      => [ 'type' => 'text', 'group' => 'fields', 'label' => 'Last Name' ],
                'email'      => [ 'type' => 'email', 'group' => 'fields', 'label' => 'E-mail' ],
                'phone'      => [ 'type' => 'phone', 'group' => 'fields', 'label' => 'Phone' ],
                'price'      => [ 'type' => 'integer', 'group' => 'fields', 'label' => 'Price' ],
                'birth_date' => [ 'type' => 'date', 'group' => 'fields', 'label' => 'Birthdate' ],
                'remind_dtm' => [ 'type' => 'datetime', 'group' => 'activity', 'label' => 'RemindDate' ],
                'notes' => [ 'type' => 'text', 'group' => 'notes', 'label' => 'Notes' ],
                'owner'      => [
                    'type' => 'lookup', 'group' => 'activity', 'model' => 'User', 'fields' => [ 'login' => 'Owner' ],
                ],
            ],
            'groups'  => [
                'fields'   => [ 'label' => 'Test  Information', 'base' => true ],
                'activity' => [ 'label' => 'Activity Information' ],
                'notes' => [ 'label' => 'Detail Information' ],
            ],
        ],
        'Lead'     => [
            'adapter' => 'wepo_company',
            'model'   => 'Lead',
            'fields'  => [
                'owner'       => [
                    'type' => 'lookup', 'group' => 'fields', 'model' => 'User', 'fields' => [ 'login' => 'Owner' ],
                ],
                'fname'       => [
                    'type' => 'text', 'group' => 'fields', 'max' => 100, 'required' => 1, 'label' => 'First Name',
                ],
                'lname'       => [
                    'type' => 'text', 'group' => 'fields', 'max' => 100, 'required' => 1, 'label' => 'Last Name',
                ],
                'phone'       => [ 'type' => 'phone', 'group' => 'adress', 'max' => 100, 'label' => 'Phone' ],
                'mobile'      => [ 'type' => 'phone', 'group' => 'adress', 'max' => 100, 'label' => 'Cellphone' ],
                'email'       => [ 'type' => 'email', 'group' => 'notes', 'max' => 100, 'label' => 'E-mail' ],
                'birth_date'  => [ 'type' => 'date', 'group' => 'fields', 'label' => 'Birthdate' ],
                'changer'     => [
                    'type' => 'lookup', 'group' => 'fields', 'model' => 'User', 'fields' => [ 'login' => 'Changer' ],
                ],
                'changed_dtm' => [ 'type' => 'datetime', 'group' => 'fields', 'label' => 'Changed Date' ],
                'created_dtm' => [ 'type' => 'datetime', 'group' => 'fields', 'label' => 'Created Date' ],
                'status'      => [
                    'type' => 'lookup', 'group' => 'fields', 'model' => 'Status', 'fields' => [ 'status' => 'Status' ],
                ],
            ],
            'groups'  => [
                'fields'   => [ 'label' => 'Lead  Information', 'base' => true ],
                'adress'   => [ 'label' => 'Adress Information'],
                'notes'    => [ 'label' => 'Notes' ],
            ],
        ],
        'Contact'  => [
            'adapter' => 'wepo_company',
            'model'   => 'Contact',
            'fields'  => [
                'owner'       => [
                    'type' => 'lookup', 'group' => 'fields', 'model' => 'User', 'fields' => [ 'login' => 'Owner' ],
                ],
                'client'      => [
                    'type' => 'lookup', 'group' => 'fields', 'model' => 'Client', 'fields' => [ 'email' => 'Client' ],
                ],
                'fname'       => [ 'type' => 'text', 'group' => 'fields', 'max' => 100, 'required' => 1 ],
                'lname'       => [ 'type' => 'text', 'group' => 'fields', 'max' => 100, 'required' => 1 ],
                //              'login'       => [ 'type' => 'text', 'group' => 'fields', 'max' => 100 ],
                'phone'       => [ 'type' => 'phone', 'group' => 'fields', 'max' => 100 ],
                'mobile'      => [ 'type' => 'phone', 'group' => 'fields', 'max' => 100 ],
                'email'       => [ 'type' => 'email', 'group' => 'fields', 'max' => 100 ],
                'birth_date'  => [ 'type' => 'date', 'group' => 'fields' ],
                'address'     => [ 'type' => 'text', 'group' => 'fields', 'max' => 256 ],
                'changer'     => [
                    'type' => 'lookup', 'group' => 'fields', 'model' => 'User', 'fields' => [ 'login' => 'Changer' ],
                ],
                'changed_dtm' => [ 'type' => 'datetime', 'group' => 'fields' ],
                'created_dtm' => [ 'type' => 'datetime', 'group' => 'fields' ],
                'status'      => [
                    'type' => 'lookup', 'group' => 'fields', 'model' => 'Status', 'fields' => [ 'status' => 'Status' ],
                ],
            ],
            'groups'  => [
                'fields',
            ],
        ],
        'Client'   => [
            'adapter' => 'wepo_company',
            'model'   => 'Client',
            'fields'  => [
                'owner'       => [
                    'type' => 'lookup', 'group' => 'fields', 'model' => 'User', 'fields' => [ 'login' => 'Owner' ],
                ],
                'name'        => [ 'type' => 'text', 'group' => 'fields', 'max' => 100, 'required' => 1 ],
                'phone'       => [ 'type' => 'phone', 'group' => 'fields', 'max' => 100 ],
                'email'       => [ 'type' => 'email', 'group' => 'fields', 'max' => 100 ],
                'changer'     => [
                    'type' => 'lookup', 'group' => 'fields', 'model' => 'User', 'fields' => [ 'login' => 'Changer' ],
                ],
                'changed_dtm' => [ 'type' => 'datetime', 'group' => 'fields' ],
                'created_dtm' => [ 'type' => 'datetime', 'group' => 'fields' ],
                'status'      => [
                    'type' => 'lookup', 'group' => 'fields', 'model' => 'Status', 'fields' => [ 'status' => 'Status' ],
                ],
            ],
            'groups'  => [
                'fields',
            ],
        ],
        'Document' => [
            'adapter' => 'wepo_company',
            'model'   => 'Document',
            'fields'  => [
                'owner'              => [
                    'type' => 'lookup', 'group' => 'fields', 'model' => 'User', 'fields' => [ 'login' => 'Owner' ],
                ],
                'document_name'      => [ 'type' => 'text', 'group' => 'fields', 'max' => 100, 'required' => 1 ],
                'document_way'       => [ 'type' => 'text', 'group' => 'fields', 'max' => 100, 'required' => 1 ],
                'document_real_name' => [ 'type' => 'text', 'group' => 'fields', 'max' => 100, 'required' => 1 ],
                'file_size'          => [ 'type' => 'text', 'group' => 'fields', 'max' => 100, 'required' => 1 ],
                'changer'            => [
                    'type' => 'lookup', 'group' => 'fields', 'model' => 'User', 'fields' => [ 'login' => 'Changer' ],
                ],
                'changed_dtm'        => [ 'type' => 'datetime', 'group' => 'fields' ],
                'created_dtm'        => [ 'type' => 'datetime', 'group' => 'fields' ],
                'status'             => [
                    'type' => 'lookup', 'group' => 'fields', 'model' => 'Status', 'fields' => [ 'status' => 'Status' ],
                ],
            ],
            'groups'  => [
                'fields',
            ],
        ],
        'Product'  => [
            'adapter' => 'wepo_company',
            'model'   => 'Product',
            'fields'  => [
                'owner'       => [
                    'type' => 'lookup', 'group' => 'fields', 'model' => 'User', 'fields' => [ 'login' => 'Owner' ],
                ],
                'name'        => [ 'type' => 'text', 'group' => 'fields', 'max' => 100, 'required' => 1 ],
                'price'       => [ 'type' => 'text', 'group' => 'fields', 'max' => 100, 'required' => 1 ],
                'description' => [ 'type' => 'textarea', 'group' => 'fields', 'max' => 200, 'required' => 1 ],
                'changer'     => [
                    'type' => 'lookup', 'group' => 'fields', 'model' => 'User', 'fields' => [ 'login' => 'Changer' ],
                ],
                'changed_dtm' => [ 'type' => 'datetime', 'group' => 'fields' ],
                'created_dtm' => [ 'type' => 'datetime', 'group' => 'fields' ],
                'status'      => [
                    'type' => 'lookup', 'group' => 'fields', 'model' => 'Status', 'fields' => [ 'status' => 'Status' ],
                ],
            ],
            'groups'  => [
                'fields',
            ],
        ],
        'Product1' => [
            'adapter' => 'wepo_company',
            'model'   => 'Product',
            'fields'  => [
                'name'        => [ 'type' => 'text', 'group' => 'fields', 'max' => 100, 'required' => 1 ],
                'price'       => [ 'type' => 'text', 'group' => 'fields', 'max' => 100, 'required' => 1 ],
                'description' => [ 'type' => 'textarea', 'group' => 'fields', 'max' => 200, 'required' => 1 ],
                'changer'     => [
                    'type' => 'lookup', 'group' => 'fields', 'model' => 'User', 'fields' => [ 'login' ],
                ],
                'changed_dtm' => [ 'type' => 'datetime', 'group' => 'fields' ],
                'created_dtm' => [ 'type' => 'datetime', 'group' => 'fields' ],
                'status'      => [
                    'type' => 'lookup', 'group' => 'fields', 'model' => 'Status', 'fields' => [ 'status' ],
                ],
            ],
            'groups'  => [
                'fields',
            ],
        ],
        'Activity' => [
            'adapter' => 'wepo_company',
            'model'   => 'Product',
            'fields'  => [
                'type'        => [
                    'type' => 'lookup', 'group' => 'fields', 'model' => 'Table', 'fields' => [ 'label' ],
                ],
                'owner'       => [
                    'type' => 'lookup', 'group' => 'fields', 'model' => 'User', 'fields' => [ 'login' ],
                ],
                'table'       => [
                    'type' => 'lookup', 'group' => 'fields', 'model' => 'Table', 'fields' => [ 'table' ],
                ],
                'target'      => [
                    'type' => 'lookup', 'group' => 'fields', 'model' => 'Lead', 'fields' => [ 'login' ],
                ],
                'subject'     => [ 'type' => 'text', 'group' => 'fields', 'max' => 100, 'required' => 1 ],
                'changer'     => [
                    'type' => 'lookup', 'group' => 'fields', 'model' => 'User', 'fields' => [ 'login' ],
                ],
                'changed_dtm' => [ 'type' => 'datetime', 'group' => 'fields' ],
                'created_dtm' => [ 'type' => 'datetime', 'group' => 'fields' ],
                'remind_dtm'  => [ 'type' => 'datetime', 'group' => 'fields' ],
            ],
            'groups'  => [
                'fields',
            ],
        ],
        'Call'     => [
            'adapter' => 'wepo_company',
            'model'   => 'Call',
            'fields'  => [
                'type'           => [
                    'type' => 'lookup', 'group' => 'fields', 'model' => 'Table', 'fields' => [ 'label' ],
                ],
                'owner'          => [
                    'type' => 'lookup', 'group' => 'fields', 'model' => 'User', 'fields' => [ 'login' ],
                ],
                'table'          => [
                    'type' => 'lookup', 'group' => 'fields', 'model' => 'Table', 'fields' => [ 'table' ],
                ],
                'target'         => [
                    'type' => 'lookup', 'group' => 'fields', 'model' => 'Lead', 'fields' => [ 'login' ],
                ],
                'subject'        => [ 'type' => 'text', 'group' => 'fields', 'max' => 100, 'required' => 1 ],
                'call_type'      => [ 'type' => 'text', 'group' => 'fields', 'max' => 100, 'required' => 1 ],
                'call_purpose'   => [ 'type' => 'text', 'group' => 'fields', 'max' => 100, 'required' => 1 ],
                'call_detail'    => [ 'type' => 'text', 'group' => 'fields', 'max' => 100, 'required' => 1 ],
                'call_result'    => [ 'type' => 'text', 'group' => 'fields', 'max' => 100, 'required' => 1 ],
                'description'    => [ 'type' => 'textarea', 'group' => 'fields', 'max' => 100, 'required' => 1 ],
                'bilable'        => [ 'type' => 'text', 'group' => 'fields', 'max' => 100, 'required' => 1 ],
                'call_start_dtm' => [ 'type' => 'detetime', 'group' => 'fields' ],
                'call_duration'  => [ 'type' => 'text', 'group' => 'fields' ],
                'changer'        => [
                    'type' => 'lookup', 'group' => 'fields', 'model' => 'User', 'fields' => [ 'login' ],
                ],
                'changed_dtm'    => [ 'type' => 'datetime', 'group' => 'fields' ],
                'created_dtm'    => [ 'type' => 'datetime', 'group' => 'fields' ],
                'remind_dtm'     => [ 'type' => 'datetime', 'group' => 'fields' ],
            ],
            'groups'  => [
                'fields',
            ],
        ],
        'Task'     => [
            'adapter' => 'wepo_company',
            'model'   => 'Task',
            'fields'  => [
                'type'               => [
                    'type' => 'lookup', 'group' => 'fields', 'model' => 'Table', 'fields' => [ 'label' ],
                ],
                'owner'              => [
                    'type' => 'lookup', 'group' => 'fields', 'model' => 'User', 'fields' => [ 'login' ],
                ],
                'table'              => [
                    'type' => 'lookup', 'group' => 'fields', 'model' => 'Table', 'fields' => [ 'table' ],
                ],
                'target'             => [
                    'type' => 'lookup', 'group' => 'fields', 'model' => 'Lead', 'fields' => [ 'login' ],
                ],
                'subject'            => [ 'type' => 'text', 'group' => 'fields', 'max' => 100, 'required' => 1 ],
                'changer'            => [
                    'type' => 'lookup', 'group' => 'fields', 'model' => 'User', 'fields' => [ 'login' ],
                ],
                'description'        => [ 'type' => 'textarea', 'group' => 'fields', 'max' => 100, 'required' => 1 ],
                'priority'           => [ 'type' => 'text', 'group' => 'fields', 'max' => 100, 'required' => 1 ],
                'recurring'          => [ 'type' => 'text', 'group' => 'fields', 'max' => 100, 'required' => 1 ],
                'rec_startdate'      => [ 'type' => 'datetime', 'group' => 'fields' ],
                'rec_enddate'        => [ 'type' => 'datetime', 'group' => 'fields' ],
                'notification_email' => [ 'type' => 'text', 'group' => 'fields', 'max' => 100, 'required' => 1 ],
                'changed_dtm'        => [ 'type' => 'datetime', 'group' => 'fields' ],
                'created_dtm'        => [ 'type' => 'datetime', 'group' => 'fields' ],
                'due_dtm'            => [ 'type' => 'datetime', 'group' => 'fields' ],
                'remind_dtm'         => [ 'type' => 'datetime', 'group' => 'fields' ],
                'status'             => [
                    'type' => 'lookup', 'group' => 'fields', 'model' => 'Status', 'fields' => [ 'status' ],
                ],
            ],
            'groups'  => [
                'fields',
            ],
        ],
        'Event'    => [
            'adapter' => 'wepo_company',
            'model'   => 'Event',
            'fields'  => [
                'type'               => [
                    'type' => 'lookup', 'group' => 'fields', 'model' => 'Table', 'fields' => [ 'label' ],
                ],
                'owner'              => [
                    'type' => 'lookup', 'group' => 'fields', 'model' => 'User', 'fields' => [ 'login' ],
                ],
                'table'              => [
                    'type' => 'lookup', 'group' => 'fields', 'model' => 'Table', 'fields' => [ 'table' ],
                ],
                'target'             => [
                    'type' => 'lookup', 'group' => 'fields', 'model' => 'Lead', 'fields' => [ 'login' ],
                ],
                'subject'            => [ 'type' => 'text', 'group' => 'fields', 'max' => 100, 'required' => 1 ],
                'changer'            => [
                    'type' => 'lookup', 'group' => 'fields', 'model' => 'User', 'fields' => [ 'login' ],
                ],
                'changed_dtm'        => [ 'type' => 'datetime', 'group' => 'fields' ],
                'created_dtm'        => [ 'type' => 'datetime', 'group' => 'fields' ],
                'remind_dtm'         => [ 'type' => 'datetime', 'group' => 'fields' ],
                'start_dtm'          => [ 'type' => 'datetime', 'group' => 'fields' ],
                'end_dtm'            => [ 'type' => 'datetime', 'group' => 'fields' ],
                'r_when'             => [ 'type' => 'datetime', 'group' => 'fields' ],
                'r_repeat'           => [ 'type' => 'text', 'group' => 'fields', 'max' => 100, 'required' => 1 ],
                'r_alert'            => [ 'type' => 'text', 'group' => 'fields', 'max' => 100, 'required' => 1 ],
                'rec_startdate'      => [ 'type' => 'datetime', 'group' => 'fields' ],
                'rec_enddate'        => [ 'type' => 'datetime', 'group' => 'fields' ],
                'invites'            => [ 'type' => 'text', 'group' => 'fields', 'max' => 100, 'required' => 1 ],
                'venue'              => [ 'type' => 'text', 'group' => 'fields', 'max' => 100, 'required' => 1 ],
                'notification_email' => [ 'type' => 'text', 'group' => 'fields', 'max' => 100, 'required' => 1 ],
                'description'        => [ 'type' => 'textarea', 'group' => 'fields', 'max' => 100, 'required' => 1 ],
                'recurring'          => [ 'type' => 'text', 'group' => 'fields', 'max' => 100, 'required' => 1 ],
            ],
            'groups'  => [
                'fields',
            ],
        ],
        //        'Order'       => [ ],
        //        'Pricebook'   => [ ],
        //        'Quote'       => [ ],
        //        'QuoteDetail' => [ ],
    ];
    protected $_fieldtypes = [
        'text'     => [
            'field'       => [ 'type' => 'field', 'datatype' => 'string', 'default' => '' ],
            'inputFilter' => [
                'name'       => 'field',
                'required'   => false,
                'filters'    => [ [ 'name' => 'StripTags' ], [ 'name' => 'StringTrim' ] ],
                'validators' => [
                    [
                        'name'    => 'StringLength',
                        'options' => [ 'encoding' => 'UTF-8', 'min' => 3, 'max' => 100 ],
                    ],
                ],
            ],
            'formElement' => [
                'type'       => 'Zend\\Form\\Element',
                'attributes' => [
                    'name'  => 'field',
                    'type'  => 'text',
                    'class' => '',
                ],
                'options'    => [
                    'label'            => 'Label',
                    'label_attributes' => [ 'class' => 'Label' ],
                ],
            ],
        ],
        'textarea' => [
            'field'       => [ 'type' => 'field', 'datatype' => 'string', 'default' => '' ],
            'inputFilter' => [
                'name'       => 'field',
                'required'   => false,
                'filters'    => [ [ 'name' => 'StripTags' ], [ 'name' => 'StringTrim' ] ],
                'validators' => [
                    [
                        'name'    => 'StringLength',
                        'options' => [ 'encoding' => 'UTF-8', 'min' => 1, 'max' => 100 ],
                    ],
                ],
            ],
            'formElement' => [
                'type'       => 'Zend\\Form\\Element',
                'attributes' => [
                    'name'  => 'field',
                    'type'  => 'textarea',
                    'class' => '',
                ],
                'options'    => [
                    'label' => 'Label',
                ],
            ],
        ],
        'integer'  => [
            'field'       => [ 'type' => 'field', 'datatype' => 'string', 'default' => '' ],
            'inputFilter' => [
                'name'     => 'field',
                'required' => false,
                'filters'  => [ [ 'name' => 'Int' ] ],
            ],
            'formElement' => [
                'type'       => 'Zend\\Form\\Element',
                'attributes' => [
                    'name'  => 'field',
                    'type'  => 'text',
                    'class' => '',
                ],
                'options'    => [
                    'label_attributes' => [ 'class' => 'required' ],
                    'label'            => 'Label',
                ],
            ],
        ],
        'date'     => [
            'field'       => [ 'type' => 'field', 'datatype' => 'string', 'default' => '' ],
            'inputFilter' => [
                'name'       => 'field',
                'required'   => false,
                'filters'    => [ [ 'name' => 'StripTags' ], [ 'name' => 'StringTrim' ] ],
                'validators' => [
                    [
                        'name'    => 'StringLength',
                        'options' => [
                            'encoding' => 'UTF-8',
                            'min'      => 10,
                            'max'      => 20,
                        ],
                    ],
                    [
                        'name'    => 'Date',
                        'options' => [ 'format' => 'Y-m-d' ]
                    ],
                    [
                        'name'    => 'Between',
                        'options' => [
                            'min' => '1940-01-01',
                            'max' => '2114-01-24',
                        ]
                    ],
                ],
            ],
            'formElement' => [
                'type'       => 'Zend\\Form\\Element',
                'attributes' => [
                    'name' => 'birth_date',
                    'type' => 'date',
                    'min'  => '1960-01-01',
                    'max'  => '2244-01-29',
                ],
                'options'    => [ 'label' => 'Label' ],
            ],
        ],
        'datetime' => [
            'field'       => [ 'type' => 'field', 'datatype' => 'string', 'default' => '' ],
            'inputFilter' => [
                'name'       => 'field',
                'required'   => false,
                'filters'    => [ [ 'name' => 'StripTags' ], [ 'name' => 'StringTrim' ] ],
                'validators' => [
                    [
                        'name'    => 'StringLength',
                        'options' => [
                            'encoding' => 'UTF-8',
                            'min'      => 5,
                            'max'      => 20,
                        ],
                    ],
                    //                    [
                    //                        'name'    => 'Date',
                    //                        'options' => [ 'format' => "d.m.Y H:i" ]
                    //                    ],
                    [
                        'name'    => 'Between',
                        'options' => [
                            'min' => '1940-01-01 00:00:00',
                            'max' => '2114-01-24 00:00:00',
                        ]
                    ],
                ],
            ],
            'formElement' => [
                'type'       => 'Zend\\Form\\Element\\DateTimeLocal',
                'attributes' => [
                    'type' => 'datetime-local',
                    'name' => 'call_start_dtm',
                ],
                'options'    => [ 'label' => 'Start call' ],
            ],
        ],
        'email'    => [
            'field'       => [ 'type' => 'field', 'datatype' => 'string', 'default' => '' ],
            'inputFilter' => [
                'name'       => 'field',
                'required'   => false,
                'filters'    => [ [ 'name' => 'StripTags' ], [ 'name' => 'StringTrim' ] ],
                'validators' => [
                    [
                        'name'    => 'StringLength',
                        'options' => [ 'encoding' => 'UTF-8', 'min' => 3, 'max' => 100 ],
                    ],
                ],
            ],
            'formElement' => [
                'type'       => 'Zend\\Form\\Element',
                'attributes' => [
                    'name' => 'login',
                    'type' => 'text',
                ],
                'options'    => [
                    'label' => 'Account Name',
                ],
            ],
        ],
        'phone'    => [
            'field'       => [ 'type' => 'field', 'datatype' => 'string', 'default' => '' ],
            'inputFilter' => [
                'name'       => 'field',
                'required'   => false,
                'filters'    => [ [ 'name' => 'StripTags' ], [ 'name' => 'StringTrim' ] ],
                'validators' => [
                    [
                        'name'    => 'StringLength',
                        'options' => [ 'encoding' => 'UTF-8', 'min' => 7, 'max' => 100 ],
                    ],
                ],
            ],
            'formElement' => [
                'type'       => 'Zend\\Form\\Element',
                'attributes' => [
                    'name' => 'phone',
                    'type' => 'text',
                ],
                'options'    => [
                    'label' => 'Phone',
                ],
            ],
        ],
        'lookup'   => [
            'field'       => [ 'type' => 'source', 'datatype' => 'string', 'default' => '', 'alias' => 'lookup' ],
            'inputFilter' => [
                'name'       => 'field',
                'required'   => false,
                'filters'    => [ [ 'name' => 'StripTags' ], [ 'name' => 'StringTrim' ] ],
                'validators' => [
                    [
                        'name'    => 'StringLength',
                        'options' => [ 'encoding' => 'UTF-8', 'min' => 1, 'max' => 100 ],
                    ],
                ],
            ],
            'formElement' => [
                'type'       => 'Zend\Form\Element\Select',
                'name'       => 'field',
                'attributes' => [
                    'id' => 'field',
                ],
                'options'    => [
                    'label'         => 'Field',
                    'value_options' => [ 0 => 'Please select ... ' ],
                ],
            ],
        ],
    ];

    public function setServiceLocator(ServiceLocatorInterface $serviceLocator)
    {
        $this->services = $serviceLocator;
    }

    public function getServiceLocator()
    {
        return $this->services;
    }

    protected function getModelConfig($model)
    {
        return isset($this->_modelconfig[ $model ]) ? $this->_modelconfig[ $model ] : null;
    }

    protected function getAdaptername($model)
    {
        return reset($this->_adapters);
    }

    protected function getUtilityFields($modelname)
    {
        return [
            'fields'  => [
                    '_id' => [ 'type' => 'pk', 'datatype' => 'string', 'default' => '', 'label' => 'ID' ],
                    'acl' => [ 'type' => 'field', 'datatype' => 'array', 'default' => [ ], 'label' => 'acl' ],
                    //                'changer_id'  => [ 'type' => 'source', 'datatype' => 'string', 'default' => '', 'alias' => 'changer' ],
                    //                'changed_dtm' => [ 'type' => 'field', 'datatype' => 'string', 'default' => '' ],
                    //                'created_dtm' => [ 'type' => 'field', 'datatype' => 'string', 'default' => '' ],
                    //                'status_id'   => [ 'type' => 'source', 'datatype' => 'string', 'default' => '', 'alias' => 'status' ],
                    //                'role'        => [ 'type' => 'alias', 'datatype' => 'string', 'default' => '', 'source' => 'role_id' ],
                    //                'changer'     => [ 'type' => 'alias', 'datatype' => 'string', 'default' => '', 'source' => 'changer_id' ],
                    //                'status'      => [ 'type' => 'alias', 'datatype' => 'string', 'default' => '', 'source' => 'status_id' ]
                ],
            'filters' => [ '_id' => $this->_fieldtypes[ 'text' ][ 'inputFilter' ] ],
        ];
    }

    protected function createFormElement($name, $conf)
    {
        $type                                 = $conf[ 'type' ];
        $_elementconf                         = $this->_fieldtypes[ $type ][ 'formElement' ];
        $_elementconf[ 'options' ][ 'label' ] = isset($conf[ 'label' ]) ? $conf[ 'label' ] : ucfirst($name);
        if ($type == 'lookup') {
            $name .= '_id';
            //$conf[ 'fields' ] это не совесем порядок сортировки
            $_lall    = $this->table($conf[ 'model' ])->find([ ], $conf[ 'fields' ]);
            $_options = [ ];
            foreach ($_lall as $_lrow) {
                $_llabel = '';
                $_lvalue = $_lrow->id();
                foreach (array_keys($conf[ 'fields' ]) as $_k) {
                    if (strlen($_llabel)) {
                        $_llabel .= ' ';
                    }
                    $_llabel .= $_lrow->$_k;
                }
                $_options[ $_lvalue ] = $_llabel;
            }
            $_elementconf[ 'options' ][ 'value_options' ] += $_options;
        }
        $_elementconf[ 'attributes' ][ 'name' ] = $name;
        if (isset($conf[ 'required' ])) {
            $_elementconf[ 'attributes' ][ 'required' ] = 'required';
            if (isset($_elementconf[ 'options' ][ 'label_attributes' ][ 'class' ]) &&
                 strlen($_elementconf[ 'options' ][ 'label_attributes' ][ 'class' ])
            ) {
                $_elementconf[ 'options' ][ 'label_attributes' ][ 'class' ] .= ' required';
            } else {
                $_elementconf[ 'options' ][ 'label_attributes' ] = [ 'class' => 'required' ];
            }
        }
        $result = [ $name => $_elementconf ];

        return $result;
    }

    protected function createField($name, $conf)
    {
        $type       = $conf[ 'type' ];
        $_fieldconf = $this->_fieldtypes[ $type ][ 'field' ];

        $_fieldsets = [ ];
        $_joins                = [ ];
        $_fieldconf[ 'label' ] = isset($conf[ 'label' ]) ? $conf[ 'label' ] : ucfirst($name);

        if ($type == 'lookup') {
            $_sign = '_';
//            'owner'      => [ 'type' => 'lookup', 'group' => 'activity', 'model' => 'User', 'fields' => [ 'login' ] ]
            $_joinfields = [ ];
            $_i          = 0;
            $_fields     = [ ];
//            if ( isset( $conf[ 'group' ] ) )
//            {
//                $_fieldsets[ $conf[ 'group' ] ][ 'elements' ] = [ $name . '_id' ];
//            }
            foreach ($conf[ 'fields' ] as $_jfield => $_jlabel) {
                if (!$_i++) {
                    $_fieldconf[ 'alias' ] = $name.$_sign.$_jfield;
                }
                $_fields[ $name.$_sign.$_jfield ]     = [
                    'type'  => 'alias', 'datatype' => 'string', 'default' => '', 'source' => $name.'_id',
                    'label' => $_jlabel,
                ];
                $_joinfields[ $name.$_sign.$_jfield ] = $_jfield;
                if (isset($conf[ 'group' ])) {
                    $_fieldsets[ $conf[ 'group' ] ][ 'elements' ][ $name.$_sign.$_jfield ] = $_jlabel;
                }
            }
            $_joins[ ]                =
                [ 'model' => $conf[ 'model' ], 'on' => [ $name.'_id' => '_id' ], 'fields' => $_joinfields ];
            $_fields[ $name.'_id' ] = $_fieldconf;
            $name .= '_id';
        } else {
            if (isset($conf[ 'group' ])) {
                $_fieldsets[ $conf[ 'group' ] ][ 'elements' ][ $name ] = $_fieldconf[ 'label' ];
            }
            $_fields = [ $name => $_fieldconf ];
        }

        $_infilter = $this->_fieldtypes[ $type ][ 'inputFilter' ];
        if (isset($conf[ 'required' ])) {
            $_infilter[ 'required' ] = true;
        }
        $_infilter[ 'name' ] = $name;
        $_filters            = [ $name => $_infilter ];
        $result              = [
            'fields'    => $_fields,
            'filters'   => $_filters,
            'joins'     => $_joins,
            'fieldsets' => $_fieldsets,
        ];

        return $result;
    }

    public function getPermittedConfig($modelName, $model, $mode = Acl::MODE_READ)
    {
        $fieldPermissions = $this->getFieldPermissions($this->user(), $modelName, $model, $mode);

        $cm = $this->getConfig($modelName);

        $allowedFields = [ ];
        foreach ($cm->fields as $k => $v) {
            if (in_array($k, $fieldPermissions)) {
                $allowedFields[ $k ] = $v;
            }
        }
        $cm->fields = $allowedFields;

        return $cm;
    }

    public function getFieldPermissions($user, $modelName, $model, $mode = Acl::MODE_READ)
    {
        $acl = $this->table('Acl')->findOne([ 'role_id' => $user->role_id, 'resource' => $modelName ]);

        if ($acl) {
            $modelPermissions = $acl->permissions;
            $groups           = $user->groups;
            $groups[ ]        = $user->_id;
            foreach ($groups as $group_id) {
                foreach ($model->acl as $_acl) {
                    if (!empty($_acl[ 'role_id' ]) && $_acl[ 'role_id' ] == $group_id) {
                        $modelPermissions = array_merge($modelPermissions, $_acl[ 'permissions' ]);
                    }
                }
            }
            $modelPermissions = array_unique($modelPermissions);
            if (!in_array($mode, $modelPermissions)) {
                throw new \Exception("This action is not allowed for you");
            }
            $fieldPermissions = [ ];
            $fieldModes       = ACL::getFieldPerms($mode);
            foreach ($acl->fields as $k => $v) {
                if (in_array($v, $fieldModes)) {
                    $fieldPermissions[ ] = $k;
                }
            }
        } else {
            throw new \Exception("Incorrect acl data is in your account");
        }

        return $fieldPermissions;
    }

    public function getConfig($modelName)
    {
        $cm = $this->table('ConfigModel2')->findOne([ 'model' => $modelName ]);
        if ($cm == null) {
            $_statconfig = $this->getModelConfig($modelName);
            if ($_statconfig !== null) {
                $cm = new ConfigModel2();
                $cm->exchangeArray($_statconfig);
//                $this -> table( 'ConfigModel2' ) -> save( $cm );
            }
        }

        return $cm;
    }

    public function prepareConfig($modelname)
    {
        $cm = $this->getConfig($modelname);
        if ($cm == null) {
            throw new \Exception('can\'t find configuration for the '.$cm->model.' model ');
        }

        return $this->parseConfig($cm);
    }

    public function parseConfig($cm = null)
    {
        $start_config = [
            'fields'    => [ ],
            'joins'     => [ ],
            'unique'    => [ ],
            'adapter'   => $cm->adapter,
            'model'     => $cm->model,
            'table'     => $cm->model,
            'fieldsets' => [ ],
        ];
        foreach ($cm->groups as $_grp => $_fls) {
            if (is_numeric($_grp)) {
                $_grp   = $_fls;
                $_label = $cm->model.' information';
                if ($_grp == 'fields') {
                    $_baseFieldSet = true;
                } else {
                    $_baseFieldSet = false;
                }
                $_fls = [ 'label' => $_label, 'elements' => [ ], 'base' => $_baseFieldSet ];
            } else {
                $_fls[ 'elements' ] = [ ];
                $_fls[ 'base' ]     = isset($_fls[ 'base' ]) && $_fls[ 'base' ] == true;
            }
            $start_config[ 'fieldsets' ][ $_grp ] = $_fls;
        }
        $config = array_merge_recursive($start_config, $this->getUtilityFields($cm->model));
        foreach ($cm->fields as $field_name => $field_conf) {
            $config = array_merge_recursive($config, $this->createField($field_name, $field_conf));
        }

        return $config;
    }

    protected function getUtilityFieldsets($modelname)
    {
        $fs = [ ];

        $fs[ ] = new \Wepo\Form\ButtonFieldset();
        $fs[ ] = new \Wepo\Form\SaUrlFieldset();

        return $fs;
    }

    public function getModel($modelname)
    {
        $config = $this->prepareConfig($modelname);

        return $model = $this->createModelFromConfig($config);
    }

    public function createModelFromConfig($config)
    {
        $drivername = $this->getDriverName($config[ 'adapter' ]);
        $model      = Obj::create('\\Wepo\\Lib\\Wepo'.$drivername.'Model');
        if ($model === null) {
            throw new Exception('Unknown classname \\Wepo\\Lib\\Wepo'.$drivername.'Model in '.
                                 $config[ 'adapter' ].' adapter settings');
        }
        $model->parseConfig($config);

        return $model;
    }

    public function createModelFromConfig2($config)
    {
        prn($config);
        $drivername = $this->getDriverName($config[ 'adapter' ]);
        $viewModel      = Obj::create('\\Wepo\\Lib\\WepoViewModel');
        $viewModel->parseConfig($config);
        prn($viewModel);
        exit();

        return $model;
    }

    public function getForm($modelName, $model, $mode)
    {
        $cm = $this->getPermittedConfig($modelName, $model, $mode);

        if ($cm == null) {
            throw new \Exception('can\'t find configuration for the '.$modelName.' model ');
        }

        $formconfig = [
            'name'            => $modelName.'Form',
            'group'           => 'form',
            'type'            => 'form',
            'options'         => [ ],
            'attributes'      => [ 'method' => 'post', 'name' => 'form'.$modelName ], // , 'action' => 'reg'
            'fieldsets'       => [ ],
            'elements'        => [ ],
            'validationGroup' => [ ],
        ];
        $fss        = [ ];

        $_fsgroups = [ ];
        foreach ($cm->fields as $field_name => $field_conf) {
            $_grp = $field_conf[ 'group' ];
            if (!isset($_fsgroups[ $_grp ])) {
                $_fsgroups[ $_grp ] = [ ];
            }
            $_element = $this->createFormElement($field_name, $field_conf);

            $_fsgroups[ $_grp ] += $_element;

            foreach (array_keys($_element) as $_k) {
                $formconfig[ 'validationGroup' ][ $_grp ][ ] = $_k;
            }
        }

        foreach ($cm->groups as $_grp => $_fls) {
            if (is_numeric($_grp)) {
                $_grp = $_fls;
                $_lbl = $cm->model.' information';
                if ($_grp == 'fields') {
                    $_baseFieldSet = true;
                } else {
                    $_baseFieldSet = false;
                }
            } else {
                $_lbl = $_fls[ 'label' ];
                $_baseFieldSet = isset($_fls[ 'base' ]) && $_fls[ 'base' ] == true;
            }

            $fsconfig = [
                'name'            => $modelName.'Fieldset',
                'group'           => $_grp,
                'type'            => 'fieldset',
                'options'         => [ 'label' => $_lbl ],
                'attributes'      => [ 'name' => $_grp, 'class' => 'table' ],
                'fieldsets'       => [ ],
                'elements'        => [ ],
                'validationGroup' => [ ],
            ];

            if (isset($_fsgroups[ $_grp ])) {
                $fsconfig[ 'elements' ] = $_fsgroups[ $_grp ];
            }

//            foreach ( $cm -> fields as $field_name => $field_conf )
//            {
//                if ( $field_conf[ 'group' ] == $_grp )
//                {
//                    $fsconfig = array_merge_recursive( $fsconfig, $this -> createFormElement( $field_name, $field_conf ) );
//
//                    $formconfig[ 'validaionGroup' ][ $_grp ][] = $field_name;
//                }
//            }

            $cfs          = new ConfigForm();
            $fieldset     = Obj::create('\\Wepo\\Lib\\WepoFieldset');
            $fss[ $_grp ] = $fieldset->parseconfig($cfs->exchangeArray($fsconfig), [ ]);

            $formconfig[ 'fieldsets' ][ $_grp ] = [ 'type' => $modelName.'Fieldset' ];
            if ($_baseFieldSet == true) {
                $formconfig[ 'fieldsets' ][ $_grp ] = [ 'options' => [ 'use_as_base_fieldset' => true ] ];
            }
        }

        # add
        $utilityfs = $this->getUtilityFieldsets($modelName);
        foreach ($utilityfs as $fieldset) {
            $fss[ $fieldset->getName() ]                       = $fieldset;
            $formconfig[ 'fieldsets' ][ $fieldset->getName() ] = [ 'type' => get_class($fieldset) ];
        }

        $cf = new ConfigForm();
        $cf->exchangeArray($formconfig);
        $form = Obj::create('\\Wepo\\Lib\\WepoForm');

        return $form->parseconfig($cf, $fss);
    }

    public function getViewModel($modelName, $model, $mode)
    {
        $cm         = $this->getPermittedConfig($modelName, $model, $mode);
        $viewConfig = $this->parseConfig($cm);
        $viewModel  = $this->createModelFromConfig($viewConfig);

        return $viewModel->exchangeArray($model->getArrayCopy());
    }

    public function getViewModel2($modelName, $model, $mode)
    {
        $cm         = $this->getPermittedConfig($modelName, $model, $mode);
        $viewConfig = $this->parseConfig($cm);
        $viewModel  = $this->createModelFromConfig2($viewConfig);

        return $viewModel->exchangeArray($model->getArrayCopy());
    }

    protected function getDriverName($adapter)
    {
        $sm = $this->getServiceLocator();
        $tm = $sm->get('Wepo\Lib\GatewayService');

        return $tm->getDriverName($adapter);
    }

    /**
     * @param string $name
     *
     * @return WepoMongoLiteGateway
     */
    public function table($name)
    {
        $sm = $this->getServiceLocator();
        $tm = $sm->get('Wepo\Lib\GatewayService');

        return $tm->getGateway($name);
    }

    /**
     * @param string $name
     *
     * @return User
     */
    public function user()
    {
        $sm = $this->getServiceLocator();
        $tm = $sm->get('ModelFramework\AuthService');

        return $tm->getUser();
    }
}
