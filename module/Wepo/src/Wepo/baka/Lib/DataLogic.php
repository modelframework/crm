<?php

namespace Wepo\Lib;

use Wepo\Model\Email;
use Wepo\Model\Status;
use Wepo\Model\Table;
class DataLogic
{
    public static $adapter = 'wepo_company';
    private $_transport = null;
    private $_event = null;
    protected $_rules = null;
    protected $_exchange_converted_fields = null;

    public function __construct(GatewayService $transport)
    {
        $this->_transport = $transport;
    }

    protected function setEvent($event)
    {
        $this->_event = $event;

        return $this;
    }

    protected function getEvent()
    {
        return $this->_event;
    }

    protected function setTransport($transport)
    {
        $this->_transport = $transport;

        return $this;
    }

    protected function getTransport()
    {
        return $this->_transport;
    }

    protected function getModel()
    {
        $params = $this->_event->getParams();
        if (is_array($params) && isset($params['model'])) {
            return $params['model'];
        } else {
            return $params;
        }
//        return $this->_event->getParams();
    }

    protected function getParam($name = null)
    {
        $params = $this->_event->getParams();

        return isset($name) && isset($params[$name]) ? $params[$name] : $params;
    }

    /**
     * @return WepoController
     */
    protected function getController()
    {
        return $this->_event->getTarget();
    }

    protected function getAction()
    {
        return $this->getController()->params('action');
    }

    protected function getRules($ruletype)
    {
        $_rules = array();
        if (isset($this->_rules[ $ruletype ])) {
            $_rules = $this->_rules[ $ruletype ];
        }

        return $_rules;
    }

    public function fillJoins()
    {
        $models = $this->getModel();
        if (!is_array($models)) {
            $models = [ $models ];
        }
        foreach ($models as $mymodel) {
            foreach ($mymodel->getJoins() as $_k => $join) {
                $othergw = $this->_transport->getGateway($join[ 'model' ]);
                foreach ($join[ 'on' ] as $myfield => $otherfield) {
                    $othermodel = $othergw->find([ $otherfield => $mymodel->$myfield ])->current();
                    if ($othermodel !== null) {
                        foreach ($join[ 'fields' ] as $myfield => $otherfield) {
                            $mymodel->$myfield = $othermodel->$otherfield;
                        }
                    } else {
                        foreach ($join[ 'fields' ] as $myfield => $otherfield) {
                            unset($mymodel->$myfield);
                        }
                    }
                }
            }
        }
    }

    public function fillJoinsConvert($model)
    {
        ////////////////////////////////////////////////////
        $mymodel = $model;
        foreach ($mymodel->getJoins() as $_k => $join) {
            $othergw = $this->_transport->getGateway($join[ 'model' ]);
            foreach ($join[ 'on' ] as $myfield => $otherfield) {
                $othermodel = $othergw->find([ $otherfield => $mymodel->$myfield ])->current();
                if ($othermodel !== null) {
                    foreach ($join[ 'fields' ] as $myfield => $otherfield) {
                        $mymodel->$myfield = $othermodel->$otherfield;
                    }
                } else {
                    foreach ($join[ 'fields' ] as $myfield => $otherfield) {
                        unset($mymodel->$myfield);
                    }
                }
            }
        }
        ////////////////////////////////////////////////////
    }

    public function formatFields()
    {
        $models = $this->getModel();
        if (!is_array($models)) {
            $models = [ $models ];
        }
        foreach ($models as $mymodel) {
            //TODO Add field type functionality
            foreach ($mymodel->toArray() as $_name => $_value) {
                if ($_name == 'mobile' || $_name == 'phone' || $_name == 'fax') {
                    $_value = preg_replace('/[^0-9]/', '', $_value);

                    $phone = '';
                    if (strlen($_value) > 7) {
                        $_cntry = substr($_value, -13, -10);
                        $mask   = '%s(%s) %s-%s';
                        if (strlen($_cntry)) {
                            $mask = '+%s (%s) %s-%s';
                        }

                        $phone = sprintf($mask, $_cntry, substr($_value, -10, -7), substr($_value, -7, -4),
                                          substr($_value, -4));
                    }
//                    $length      = strlen( $_value );
//                    $loop_length = $length - 13 < 0 ? 0 : $length - 13;
//                    if ( $length >= 7 )
//                    {
//                        for ( $i = $length - 1; $i >= $loop_length; $i-- )
//                        {
//                            switch ( $length - $i )
//                            {
//                                case 0:
//                                case 1:
//                                case 2:
//                                case 3:
//                                    $phone = $_value[ $i ] . $phone;
//                                    break;
//                                case 4:
//                                    $phone = '-' . $_value[ $i ] . $phone;
//                                    break;
//                                case 5:
//                                case 6:
//                                    $phone = $_value[ $i ] . $phone;
//                                    break;
//                                case 7:
//                                    $phone = $_value[ $i ] . $phone;
//                                    break;
//                                case 8:
//                                    $phone = $_value[ $i ] . ') ' . $phone;
//                                    break;
//                                case 9:
//                                    $phone = $_value[ $i ] . $phone;
//                                    break;
//                                case 10:
//                                    $phone = ' (' . $_value[ $i ] . $phone;
//                                    break;
//                            }
//                            if ( $length - $i > 10 && $length - $i < 14 )
//                            {
//                                $phone = $_value[ $i ] . $phone;
//                            }
//                        }
//                        if ( $length > 10 )
//                        {
//                            $phone = '+' . $phone;
//                        }
//                    }
                    $mymodel->$_name = $phone;
                }
            }
        }
    }

    public function presave($event)
    {
        $this->setEvent($event);
        $this->forge();
        $this->fillJoins();
        $this->formatFields();
    }

    public function update($event)
    {
        $this->setEvent($event);
        $model = $this->getModel();
        $this->fillJoins();
    }

    public function postsave($event)
    {
        $this->setEvent($event);
        $this->saveLog();
//        $this -> updateEmailLinks();
    }

    public function prerecycle($event)
    {
        $this->setEvent($event);
    }

    public function recycle($event)
    {
        $this->setEvent($event);
        $ids = [ ];
        if (!is_array($this->getModel())) {
            $modelname = $this->getModel()->getModelName();
            $ids[ ]    = $this->getModel()->id();
        } else {
            $models = $this->getModel();
            foreach ($models as $model) {
                $ids[ ] = $model->id();
            }
            $modelname = reset($models)->getModelName();
        }
        switch ($this->getAction()) {
            case 'restore':
                $this->_transport->getGateway($modelname)->update([
                                                                         'status'    => Status::getLabel(Status::NORMAL),
                                                                         'status_id' => Status::NORMAL,
                                                                     ], [ '_id' => $ids ]);
                break;
            case 'delete':
                $this->_transport->getGateway($modelname)->update([
                                                                         'status'    => Status::getLabel(Status::DELETED),
                                                                         'status_id' => Status::DELETED,
                                                                     ], [ '_id' => $ids ]);
                break;
            case 'clean':
                $this->_transport->getGateway($modelname)->delete([ '_id' => $ids ]);
                break;
        }
    }

    public function postrecycle($event)
    {
        $this->setEvent($event);
        $this->saveLog();
//        $this->deleteEmailLinks();
    }

    public function convert($event)
    {
        $this->setEvent($event);
        $fromModel = $this->getModel();
        $toModel = $this->getEvent()->getParams()['to_model'];
        $this->exchangeConvertedFields($fromModel, $toModel);

        $current_event = $this->getEvent();
        $action = 'convert_to_'.strtolower($toModel->getModelName());
        $this->forge($action);
        $this->_transport->getGateway($fromModel->getModelName())->save($fromModel);
        $this->getController()->trigger('postconvert', [ 'model' => $toModel, 'from_model' => $fromModel ]);
        $this->_transport->getGateway($toModel->getModelName())->save($toModel);
        $this->setEvent($current_event);
    }

    public function postconvert($event)
    {
        //        prn('heare');
        $this->setEvent($event);
        $fromModel = $this->getEvent()->getParams()['from_model'];
        $action = 'convert_from_'.strtolower($fromModel->getModelName());
//        prn($this->getModel());
        $this->forge($action);
//        prn($this->getModel());
//        exit;
    }

    public function payment($event)
    {
        $this->setEvent($event);
    }

    protected function forge($action = null)
    {
        $model = $this->getModel();
        $action = isset($action) ? $action : $this->getAction();
        foreach ($this->getRules($action) as $_key => $_rules) {
            if (!isset($model->$_key)) {
                continue;
//                throw new \Exception( 'wrong field name ' . "'$_key'" );
            }
            if ($_rules[ 'type' ] == 'function' && method_exists($this, $_rules[ 'value' ])) {
                $this->{$_rules[ 'value' ]}($model, $_key, isset($_rules[ 'params' ]) ? $_rules[ 'params' ] : null);
            } elseif ($_rules[ 'type' ] == 'const') {
                $model->$_key = $_rules[ 'value' ];
            }
        }

        return $this->getModel();
    }

    protected function exchangeConvertedFields($fromModel, $toModel)
    {
        $exchangeRules = $this->_exchange_converted_fields;
//        prn($this);
        if (is_array($exchangeRules)) {
            if (isset($exchangeRules['from_model']) && isset($exchangeRules['to_model'])) {
                foreach ($exchangeRules['from_model'] as $from_field => $to_field) {
                    $fromModel->$from_field = $toModel->$to_field;
                }
                foreach ($exchangeRules['to_model'] as $from_field => $to_field) {
                    $toModel->$to_field = $fromModel->$from_field;
                }
            }
        }

        return;
    }

    protected function countPrice($model, $key, $params = [])
    {
        $qty = isset($params['qty']) ? $model->$params['qty'] : 1;
        if (isset($params['dsc']) && isset($params['dsc_type']) && isset($params['prd_price'])) {
            $dsc = $params['dsc'];
            $dsc_type = $params['dsc_type'];
            $prd_price = $params['prd_price'];
        } else {
            throw new \Exception('discount, product price and discount type doesn\'t set as params in countPrice logic method');
        }
//        prn($model,$dsc_type,$prd_price,$dsc,$qty);

        if ($model->$dsc_type == '% of Price') {
            $model->$key = round(max([ $model->$prd_price * $qty * $model->$dsc/100, 0 ]), 2);
        } else {
            //            prn('heare');
//            prn($model -> $prd_price,$model->$dsc,$qty);
            $model->$key = round(max([ $model->$prd_price * $qty - $model->$dsc, 0 ]), 2);
        }

//        prn($model->$key);
//        exit;

        return $model->$key;
    }

    protected function countDetailsSumPrice($model, $key, $params = [])
    {
        $requiredSettingField = [
            'dtl_model',
            'dtl_link_field',
//            'link_field',
            'dtl_price_field',
        ];
        $diffRes = array_diff($requiredSettingField, array_keys($params));
        if (count($diffRes)) {
            $message = 'Params array not initialized correctly, some fields are lost:'.implode(', ', $diffRes);
            throw new \Exception($message);
        }

        $detailModel = $params['dtl_model'];
        $detailLinkField = $params['dtl_link_field'];
//        $currentField = $params['link_field'];
        $detailPriceField = $params['dtl_price_field'];

//        $details = $this -> _transport -> getGateway( 'QuoteDetail' ) -> find( array( 'quote_id' => $model -> id() ) );
        $details = $this->_transport->getGateway($detailModel)->find(array( $detailLinkField => $model->id() ));
        $raw_price     = 0;
        foreach ($details as $_data) {
            $raw_price += $_data->$detailPriceField;
        }
        $model->$key = $raw_price;

        return $model->$key;
    }

    protected function setTaxesAndAdjustment($model, $key, $params = [])
    {
        $adjustment = isset($params['adjustment']) ? $model->$params['adjustment'] : 0;
//        prn($adjustment);
        if (isset($params['txs_fields']) && isset($params['ttl_field'])) {
            $taxes = 0;
            foreach ($params['txs_fields'] as $taxField) {
                $taxes += $model->$taxField;
            }
            $taxes = $model->$params['ttl_field']*$taxes/100;
            $model->$params['ttl_field'] = max([$model->$params['ttl_field']+$taxes + $adjustment, 0]);
            $model->$key = $taxes;

            return $model->$key;
        } else {
            throw new \Exception('txs_fields or ttl_fields doesn\'t set');
        }
    }

    protected function setDate($model, $key, $params = null)
    {
        //        if ( empty( $model->$key ) )
//        {
        if ($params != null) {
            $model->$key = date($params);
        } else {
            $model->$key = date('Y-m-d H:i:s');
        }

//        }

        return $model->$key;
    }

    protected function setTitle($model, $key, $params = [ ])
    {
        $this->fillJoins();
        $model->$key = '';
        if (!is_array($params)) {
            return $model->$key;
        }

        foreach ($params as $_f) {
            $model->$key .= (strlen($model->$key) ? ' ' : '').$model->$_f;
        }

        return $model->$key;
    }

    protected function setOwner($model, $key)
    {
        if (empty($model->$key)) {
            $model->$key = $this->getController()->User()->id();
        }

        return $model->$key;
    }

//    protected function updateEmailLinks()
//    {
//        $nameRules = \Wepo\Model\Email::getLinkRules();
//        $model = $this->getModel();
//        $modelName = $model->getModelName();
//        $action = $this->getAction();
//        if( !$model->id() )
//        {
//            $data = $model->toArray();
//            $model = $this->getController()->table($modelName)->findOne($data);
//        }
//        if(in_array($modelName,array_keys($nameRules))&&(in_array($action,$nameRules[$modelName]['updAction']))&&(!empty($model->$nameRules[$modelName]['email'])))
//        {
//            $tr = $this->getController()->table('Email');
//            $foundEmail = $tr->findOne(['email'=>$model->$nameRules[$modelName]['email']]);
//            if(isset($foundEmail))
//            {
//                if(array_key_exists($modelName,$foundEmail->models))
//                {
//                    $modelIds = $foundEmail->models[$modelName];
//                    if(!in_array($model->id(),$modelIds))
//                    {
//                        array_push($modelIds,$model->id());
//                    }
//                }
//                else
//                {
//                    $modelIds = [$model->id()];
//                }
//                $models = $foundEmail->models;
//                $models[$modelName] = $modelIds;
//
//                $oldNamePriority = $nameRules[$foundEmail->user_name_source]['priority'];
//                $newNamePriority = $nameRules[$modelName]['priority'];
//
//                if($newNamePriority>$oldNamePriority)
//                {
//                    $newName = '';
//                    foreach($nameRules[$modelName]['name'] as $value)
//                    {
//                        $newName = $newName.' '.$model->$value;
//                    }
//                    $foundEmail->user_name = $newName;
//                    $foundEmail->user_name_source = $modelName;
//                }
//
//                $foundEmail->models = $models;
//            }
//            else
//            {
//                $newName = '';
//                foreach($nameRules[$modelName]['name'] as $value)
//                {
//                    $newName = $newName.' '.$model->$value;
//                }
//                $foundEmail = new \Wepo\Model\Email([
//                    'email'             => $model->email,
//                    'user_name'         => $newName,
//                    'user_name_source'  => $modelName,
//                    'models'            => [$modelName => [$model->id()]]
//                ]);
//            }
//            $tr->save($foundEmail);
//        }
//    }

    protected function updateEmails($model, $key)
    {
        $nameRules = \Wepo\Model\Email::getLinkRules();
        $modelName = $model->getModelName();

        $tr         = $this->getController()->table('Email');
        $foundEmail = $tr->findOne([ 'email' => $model->$nameRules[ $modelName ][ 'email' ] ]);

        $newName = '';
        foreach ($nameRules[ $modelName ][ 'name' ] as $value) {
            $newName = $newName.$model->$value.' ';
        }
        $newName = trim($newName);

        if (isset($foundEmail)) {
            $oldNamePriority = $nameRules[ $foundEmail->user_name_source ][ 'priority' ];
            $newNamePriority = $nameRules[ $modelName ][ 'priority' ];

            if ($newNamePriority > $oldNamePriority) {
                $foundEmail->user_name        = $newName;
                $foundEmail->user_name_source = $modelName;
            }
        } else {
            $foundEmail = new \Wepo\Model\Email([
                                                     'email'            => $model->email,
                                                     'user_name'        => $newName,
                                                     'user_name_source' => $modelName,
                                                 ]);
        }
        $tr->save($foundEmail);

//        $model -> $key = $tr -> getLastInsertId();

        return $model->$key;
    }

    protected function setEmailId($model, $key)
    {
        $nameRules = \Wepo\Model\Email::getLinkRules();
        $modelName = $model->getModelName();

        $tr         = $this->getController()->table('Email');
        $foundEmail = $tr->findOne([ 'email' => $model->$nameRules[ $modelName ][ 'email' ] ]);

        $model->$key = $foundEmail->id();

        return $model->$key;
    }

//    protected function deleteEmailLinks()
//    {
//        $nameRules = \Wepo\Model\Email::getLinkRules();
//        $models = $this->getModel();
//        foreach($models as $model)
//        {
//            $modelName = $model->getModelName();
//            $action = $this->getAction();
//
//            if(in_array($modelName,array_keys($nameRules))&&(in_array($action,$nameRules[$modelName]['delAction']))&&(!empty($model->$nameRules[$modelName]['email'])))
//            {
//                try
//                {
//                    $deleted =  $this->getController()->table($modelName)->get($model->id());
//                    $deleted =  empty($deleted);
//                }
//                catch(\Exception $ex)
//                {
//                    $deleted = false;
//                }
//                if($deleted)
//                {
//                    $tr = $this->getController()->table('Email');
//                    $modelId = $model->id();
//                    $emails = $tr -> find(['models.'.$modelName=>[$modelId]]);
//                    foreach($emails as $email)
//                    {
//                        $models = $email->models;
//                        $mailNameArr = $models[$modelName];
//                        $mailNameArr = array_diff($mailNameArr,[$model->id()]);
//                        if(count($mailNameArr))
//                        {
//                            $models[$modelName] = $mailNameArr;
//                            $email->models = $models;
//                        }
//                        else
//                        {
//                            unset($models[$modelName]);
//                            $email->models = $models;
//                        }
//                        $tr->save($email);
//                    }
//                }
//            }
//        }
//    }

    protected function setUser($model, $key)
    {
        return $model->$key = $this->getController()->User()->id();
    }

    protected function setClientIp($model, $key)
    {
        return $model->$key = $this->getController()->getClientIp();
    }

    protected function saveLog()
    {
        $models = $this->getModel();
        if (!is_array($models)) {
            $models = [ $models ];
        }
        foreach ($models as $model) {
            if ($model->id() == 0) {
                $arr = $model->toArray();
                foreach ($arr as $_key => $_value) {
                    if ($_value == null || is_array($_value)) {
                        unset($arr[ $_key ]);
                    }
                }
                $model = $this->_transport->getGateway($model->getModelName())->find($arr)->current();
            }
            if ($model != null) {
                $eventlog = $this->_transport->getModel('EventLog');
                $this->setDate($eventlog, 'created_dtm');
                $this->setUser($eventlog, 'executor_id');
                $eventlog->table_id  =
                    Table:: getTableId($model->getModelName()); //. strtoupper( $model -> getModelName() ) );
                $eventlog->target = $model->title;
                $eventlog->target_id = $model->id();
                $eventlog->event_id  = constant('\Wepo\Model\EventType::'.strtoupper($this->getAction()));
                $this->fillJoinsConvert($eventlog);
                $this->_transport->getGateway('EventLog')->save($eventlog);
            }
        }
    }

    public function looked($model, $key)
    {
        if ($model->$key == Status::NEW_) {
            $modelname   = $model->getModelName();
            $model->$key = Status::NORMAL;
            $id          = $model->owner_id;
            $user        = $this->_transport->getGateway('User')->find(array( '_id' => $id ))->current();
            $newItems    = $user->newitems;
            if ((int) $newItems[ $modelname ] > 0) {
                $newItems[ $modelname ] = (int) $newItems[ $modelname ] - 1;
            }
            $this->_transport->getGateway('User')->update([ 'newitems' => $newItems ], [ '_id' => $id ]);
        }

        return $model->$key;
    }

    public function newItem($model, $key)
    {
        $modelname   = $model->getModelName();
        $model->$key = Status::NEW_;
        $id          = $model->owner_id;
        $user        = $this->_transport->getGateway('User')->find(array( '_id' => $id ))->current();
        $newItems    = $user->newitems;

        $newItems[ $modelname ] = (int) $newItems[ $modelname ] + 1;
        $this->_transport->getGateway('User')->update([ 'newitems' => $newItems ], [ '_id' => $id ]);

        return $model->$key;
    }

    public function setAvatar()
    {
        $user     = $this->getModel();
        $request  = $this->getController()->getRequest();
        $filename = false;
        if ($request->isPost()) {
            $files    = $request->getFiles();
            $fileinfo = $files->get('fields')[ 'avatar-file' ];
            $fs       = $this->getController()->getServiceLocator()->get('\Wepo\Lib\FileService');
            $filename = $fs->saveFile($fileinfo[ 'name' ], $fileinfo[ 'tmp_name' ], true, $user->id());
        }
        if ($filename) {
            $user->avatar = basename($filename);
        }
    }

    public function setAge($model, $key, $params)
    {
        $model->$key = '';
        if ($model->$params) {
            $birthday = strtotime($model->$params);
            $age = date('Y') - date('Y', $birthday);
            if (date('md', $birthday) > date('md')) {
                $age--;
            }
            $model->$key = $age;
        }

        return $model->$key;
    }
}
