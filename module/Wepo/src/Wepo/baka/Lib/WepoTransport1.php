<?php

namespace Wepo\Lib;

use Zend\Db\ResultSet\ResultSet;
use Zend\ServiceManager\ServiceLocatorAwareInterface;
use Zend\ServiceManager\ServiceLocatorInterface;

class WepoTransport1 implements ServiceLocatorAwareInterface
{
    protected $services;
    protected $_tables = [ ];

    public function get($name)
    {
        $dbObjectName = '\\Wepo\\Model\\'.$name;
        if (!class_exists($dbObjectName)) {
            throw new \Exception('Wrong model name');
        }
        if (!isset($this->_tables[ $name ])) {
            $ObjectLogicName = '\\Wepo\\Model\\Logic\\'.$name;
            if (!class_exists($ObjectLogicName)) {
                $ObjectLogicName = '\\Wepo\\Lib\\DataLogic';
            }
            $dbAdapter            = $this->services->get($ObjectLogicName::$adapter);
            $connectionParameters = $dbAdapter->getDriver()->getConnection()->getConnectionParameters();
            if (!empty($connectionParameters[ 'gateway' ])) {
                $gwName = $connectionParameters[ 'gateway' ];
            } elseif (!empty($connectionParameters[ 'driver' ])) {
                $gwName = $connectionParameters[ 'driver' ];
            } else {
                throw new \Exception('unknown gateway ');
            }

            // create custom transport for the model
            $tableGatewayObject = '\\Wepo\\Model\\Transport\\'.$name;
            if (!class_exists($tableGatewayObject)) {
                // use general gw class
                $tableGatewayObject = '\\Wepo\\Lib\\Wepo'.$gwName.'Gateway';
                if (!class_exists($tableGatewayObject)) {
                    throw new \Exception(' unknown gateway class ');
                }
            }

            // create resultset proto
            $resultSetPrototype       = new ResultSet();
            $resultSetPrototype->setArrayObjectPrototype(new $dbObjectName());

            // create gw object and store it to the cache
            $tableGateway             = new $tableGatewayObject($dbObjectName::TABLE_NAME, $dbAdapter, null, $resultSetPrototype);
            $this->_tables[ $name ] = $tableGateway;
        }

        return $this->_tables[ $name ];
    }

    public function setServiceLocator(ServiceLocatorInterface $serviceLocator)
    {
        $this->services = $serviceLocator;
    }

    public function getServiceLocator()
    {
        return $this->services;
    }
}
