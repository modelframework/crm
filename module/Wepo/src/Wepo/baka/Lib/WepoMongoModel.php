<?php

namespace Wepo\Lib;


class WepoMongoModel extends WepoModel
{
    public function __set($name, $value)
    {
        if (isset($this->_fields[ $name ])) {
            $this->_data[ $name ] = $value;
            if (isset($this->_fields[ $name ][ 'datatype' ]) && $value !== null && !$value instanceof \MongoId) {
                if (substr($name, -3) == '_id' && ($this->_fields[ $name ][ 'datatype' ] == 'string') &&
                     preg_match('/^[0123456789abcdefABCDEF]{24}$/', $value)
                ) {
                    $this->_data[ $name ] = new \MongoId($value);
                } else {
                    settype($this->_data[ $name ], $this->_fields[ $name ][ 'datatype' ]);
                }
            }

            return $this->_data[ $name ];
        } else {
            $this->$name = $value;

            return $this->$name;
        }
    }

    public function __call($name, $arguments)
    {
        if (!isset($this->_fields[ $name ])) {
            $trace = debug_backtrace();
            trigger_error(
                'Udefined property в __call(): '.$name);

            return;
        }

        if (count($arguments) == 0) {
            return $this->_fields[ $name ][ 'default' ];
        }

        $_result = [ ];
        foreach ($arguments as $value) {
            if (isset($this->_fields[ $name ][ 'datatype' ]) && $value !== null && !$value instanceof \MongoId) {
                if (substr($name, -3) == '_id' && preg_match('/^[0123456789abcdefABCDEF]{24}$/', $value)) {
                    $value = new \MongoId($value);
                } else {
                    settype($value, $this->_fields[ $name ][ 'datatype' ]);
                }
            }
            $_result[ ] = $value;
        }

        if (count($_result) == 1) {
            $_result = array_shift($_result);
        }

        return $_result;
    }

    public function __get($name)
    {
        if (array_key_exists($name, $this->_data)) {
            return $this->_data[ $name ];
        }

//        $trace = debug_backtrace();
//        trigger_error(
//            'Неопределенное свойство в __get(): ' . $name .
//            ' в файле ' . $trace[ 0 ][ 'file' ] .
//            ' на строке ' . $trace[ 0 ][ 'line' ], E_USER_NOTICE );
        return;
    }

    public function exchangeArray($data)
    {
        if (!isset($this->_fields)) {
            throw new \Exception(' _fields property is not set in '.get_class());
        }
        foreach ($this->_fields as $_field => $_properties) {
            if (isset($data[ $_field ])) {
                $this->$_field = $data[ $_field ];
            } else {
                $this->$_field = isset($_properties[ 'default' ]) ? $_properties[ 'default' ] : null;
            }

            if (isset($_properties[ 'datatype' ]) && $this->$_field !== null) {
                if ($this->$_field instanceof \MongoId) {
                    if ($_properties[ 'type' ] !== 'pk') {
                        //                        prn( $_properties );
                    }
                } elseif ($this->$_field instanceof \MongoDate) {
                    $this->$_field =
                        date($_properties[ 'datatype' ] == 'date' ? 'm/d/y' : 'm/d/y g:i a', $this->$_field->sec);
//                    $this -> $_field = date( $_properties[ 'datatype' ] == 'date' ? 'Y-m-d' : 'Y-m-d h:i:s', $this -> $_field -> sec );
//                    settype( $this -> $_field, 'string' );
                } elseif (!$_properties[ 'datatype' ] == 'date' && !$_properties[ 'datatype' ] == 'datetime') {
                    settype($this->$_field, $_properties[ 'datatype' ]);
                }

            /**
             *
             * if( substr($_field, -5) == '_date' )
             * {
             * if ( preg_match( '/(\d{4})[\-\.\/](\d{2})[\-\.\/](\d{2})/', $this -> $_field, $matches ) )
             * {
             * $this -> $_field = \DateTime::createFromFormat('Y-m-d', $this -> $_field)->format('m/d/y');
             * }
             * //                    $this -> $_field = \DateTime::createFromFormat('Y-m-d', $this -> $_field);
             *
             * }
             * elseif( substr($_field, -4) == '_dtm' )
             * {
             * if ( preg_match( '/(\d{4})[\-\.\/](\d{2})[\-\.\/](\d{2}) (\d{2}):(\d{2}):(\d{2})/', $this -> $_field, $matches ) )
             * {
             * $this -> $_field = \DateTime::createFromFormat('Y-m-d H:i:s', $this -> $_field)->format('m/d/y g:i a');
             * }
             * elseif ( preg_match( '/(\d{4})[\-\.\/](\d{2})[\-\.\/](\d{2})T(\d{2}):(\d{2}):(\d{2})/', $this -> $_field, $matches ) )
             * {
             * $this -> $_field = \DateTime::createFromFormat('Y-m-dTH:i:s', $this -> $_field)->format('m/d/y g:i a');
             * }
             * elseif ( preg_match( '/(\d{4})[\-\.\/](\d{2})[\-\.\/](\d{2})T(\d{2}):(\d{2})/', $this -> $_field, $matches ) )
             * {
             * $this -> $_field = \DateTime::createFromFormat('Y-m-d\TH:i', $this -> $_field)->format('m/d/y g:i a');
             * }
             * elseif ( preg_match( '/(\d{4})[\-\.\/](\d{2})[\-\.\/](\d{2}) (\d{2}):(\d{2})/', $this -> $_field, $matches ) )
             * {
             * $this -> $_field = \DateTime::createFromFormat('Y-m-d H:i', $this -> $_field)->format('m/d/y g:i a');
             * }
             * }
             *
             */
            }
        }
        foreach ($this->_joins as $_join) {
            foreach (array_keys($_join[ 'fields' ]) as $_field) {
                if (isset($data[ $_field ])) {
                    $this->$_field = $data[ $_field ];
                }
            }
        }

        return $this;
    }

    public function id()
    {
        if (isset($this->_id)) {
            return (string) $this->_id;
        }

        return;
    }

    public function setId($id)
    {
        if ($id instanceof \MongoId) {
            $this->_id = $id;
        } elseif (preg_match('/^[0123456789abcdefABCDEF]{24}$/', $id)) {
            $this->_id = new \MongoId($id);
        }
    }

    public function getName()
    {
        return $this->getModelName();
    }
}
