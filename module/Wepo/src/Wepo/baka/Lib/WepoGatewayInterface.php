<?php

namespace Wepo\Lib;

use Zend\Db\TableGateway\TableGatewayInterface;

interface WepoGatewayInterface extends TableGatewayInterface
{
    /**
     * @param  string    $id
     * @return WepoModel
     */
    public function get($id);

    /**
     * @return \Zend\Db\ResultSet\ResultSet
     */
    public function find($fields = array(), $orders = array(), $limit = null, $offset = null);

    /**
     * @return \Zend\Db\ResultSet\ResultSet
     */
    public function fetchAll();

    /**
     * @return boolean
     */
    public function isUnique(WepoModel $model);

    /**
     * @return int
     */
    public function save(WepoModel $model);
}
