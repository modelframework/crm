<?php

namespace Wepo\Lib;

use Zend\Db\ResultSet\ResultSet;
use Zend\ServiceManager\ServiceLocatorAwareInterface;
use Zend\ServiceManager\ServiceLocatorInterface;
use Wepo\Model\Config;

class WepoModelFactory_old implements ServiceLocatorAwareInterface
{
    protected $services;
    protected $_models     = [ ];
    protected $_modelproto = null;

    public function __construct()
    {
        //        $this->_modelproto = new WepoModel();
    }

    protected function getconfig($adaptername)
    {
        $servconf = $this->services->get('Config');
        if (empty($servconf[ 'db' ][ 'adapters' ])) {
            throw new \Exception('config corrupted');
        }

        if (empty($servconf[ 'db' ][ 'adapters' ][ $adaptername ])) {
            throw new \Exception('wrong adpater name in '.$adaptername.' model');
        }

        return $servconf[ 'db' ][ 'adapters' ][ $adaptername ];
    }

    protected function getdrivername($name)
    {
        $config = $this->getconfig($name);
        if (!empty($config[ 'driver' ])) {
            $drivername = $config[ 'driver' ];
        } else {
            $drivername = 'Pdo';
        }

        return $drivername;
    }

    public function getModel($modelname)
    {
        $configgw = $this->get('Config');
        $config   = $configgw->findOne([ 'model' => $modelname ]);
        if ($config == null) {
            //            $config = new Config();
            throw new \Exception(' Unknown model name ');
        }

//        $config -> model   = $modelname;
//        $config -> adapter = 'wepo_company_mongo';
//        $config -> table   = 'lead';
//        $config -> fields  = [
//            '_id'         => [ 'type' => 'pk', 'datatype' => 'string', 'default' => '' ],
//            'owner_id'    => [ 'type' => 'source', 'datatype' => 'int', 'default' => 0, '', 'alias' => 'owner' ],
//            'fname'       => [ 'type' => 'field', 'datatype' => 'string', 'default' => '' ],
//            'lname'       => [ 'type' => 'field', 'datatype' => 'string', 'default' => '' ],
//            'login'       => [ 'type' => 'field', 'datatype' => 'string', 'default' => '' ],
//            'phone'       => [ 'type' => 'field', 'datatype' => 'string', 'default' => '' ],
//            'mobile'      => [ 'type' => 'field', 'datatype' => 'string', 'default' => '' ],
//            'email'       => [ 'type' => 'field', 'datatype' => 'string', 'default' => '' ],
//            'birth_date'  => [ 'type' => 'field', 'datatype' => 'string', 'default' => '' ],
//            'changer_id'  => [ 'type' => 'source', 'datatype' => 'int', 'default' => 0, 'alias' => 'changer' ],
//            'changed_dtm' => [ 'type' => 'field', 'datatype' => 'string', 'default' => '' ],
//            'created_dtm' => [ 'type' => 'field', 'datatype' => 'string', 'default' => '' ],
//            'status_id'   => [ 'type' => 'source', 'datatype' => 'int', 'default' => 1, 'alias' => 'status' ],
//            'owner'       => [ 'type' => 'alias', 'datatype' => 'string', 'default' => '', 'source' => 'owner_id' ],
//            'changer'     => [ 'type' => 'alias', 'datatype' => 'string', 'default' => '', 'source' => 'changer_id' ],
//            'status'      => [ 'type' => 'alias', 'datatype' => 'string', 'default' => '', 'source' => 'status_id' ]
//        ];
//        $config -> unique  = [ 'login' ];
//        $config -> joins   = [
//            [ 'model' => 'User', 'on' => [ 'owner_id' => 'id' ], 'fields' => [ 'owner' => 'login' ] ],
//            [ 'model' => 'User', 'on' => [ 'changer_id' => 'id' ], 'fields' => [ 'changer' => 'login' ] ],
//            [ 'model' => 'Status', 'on' => [ 'status_id' => 'id' ], 'fields' => [ 'status' => 'status' ] ]
//        ];
//        $config -> filters = [
//            [
//                'name'     => 'id',
//                'required' => true,
//                'filters'  => [ [ 'name' => 'Int' ] ],
//            ],
//            [
//                'name'     => 'owner_id',
//                'required' => true,
//                'filters'  => [ [ 'name' => 'Int' ] ],
//            ],
//            [
//                'name'       => 'fname',
//                'required'   => true,
//                'filters'    => array(
//                    array( 'name' => 'StripTags' ),
//                    array( 'name' => 'StringTrim' ),
//                ),
//                'validators' => array(
//                    array(
//                        'name'    => 'StringLength',
//                        'options' => array(
//                            'encoding' => 'UTF-8',
//                            'min'      => 1,
//                            'max'      => 100,
//                        ),
//                    ),
//                ),
//            ],
//            [
//                'name'       => 'lname',
//                'required'   => true,
//                'filters'    => array(
//                    array( 'name' => 'StripTags' ),
//                    array( 'name' => 'StringTrim' ),
//                ),
//                'validators' => array(
//                    array(
//                        'name'    => 'StringLength',
//                        'options' => array(
//                            'encoding' => 'UTF-8',
//                            'min'      => 1,
//                            'max'      => 100,
//                        ),
//                    ),
//                ),
//            ],
//            [
//                'name'       => 'login',
//                'required'   => true,
//                'filters'    => array(
//                    array( 'name' => 'StripTags' ),
//                    array( 'name' => 'StringTrim' ),
//                ),
//                'validators' => array(
//                    array(
//                        'name'    => 'StringLength',
//                        'options' => array(
//                            'encoding' => 'UTF-8',
//                            'min'      => 3,
//                            'max'      => 100,
//                        ),
//                    ),
//                ),
//                'attributes' => array(
//                    'required' => 'required'
//                )
//            ],
//            [
//                'name'       => 'phone',
//                'required'   => true,
//                'filters'    => array(
//                    array( 'name' => 'StripTags' ),
//                    array( 'name' => 'StringTrim' ),
//                ),
//                'validators' => array(
//                    array(
//                        'name'    => 'StringLength',
//                        'options' => array(
//                            'encoding' => 'UTF-8',
//                            'min'      => 5,
//                            'max'      => 100,
//                        ),
//                    ),
//                ),
//            ],
//            [
//                'name'       => 'mobile',
//                'required'   => false,
//                'filters'    => array(
//                    array( 'name' => 'StripTags' ),
//                    array( 'name' => 'StringTrim' ),
//                ),
//                'validators' => array(
//                    array(
//                        'name'    => 'StringLength',
//                        'options' => array(
//                            'encoding' => 'UTF-8',
//                            'min'      => 8,
//                            'max'      => 100,
//                        ),
//                    ),
//                ),
//            ],
//            [
//                'name'       => 'email',
//                'required'   => false,
//                'filters'    => array(
//                    array( 'name' => 'StripTags' ),
//                    array( 'name' => 'StringTrim' ),
//                ),
//                'validators' => array(
//                    array(
//                        'name'    => 'StringLength',
//                        'options' => array(
//                            'encoding' => 'UTF-8',
//                            'min'      => 6,
//                            'max'      => 100,
//                        ),
//                    ),
//                ),
//            ],
//            [
//                'name'       => 'birth_date',
//                'required'   => false,
//                'filters'    => array(
//                    array( 'name' => 'StripTags' ),
//                    array( 'name' => 'StringTrim' ),
//                ),
//                'validators' => array(
//                    array(
//                        'name'    => 'StringLength',
//                        'options' => array(
//                            'encoding' => 'UTF-8',
//                            'min'      => 10,
//                            'max'      => 20,
//                        ),
//                    ),
//                    array(
//                        'name'    => 'Date',
//                        'options' => array(
//                            'format' => 'Y-m-d',
//                        ),
//                    ),
//                    array(
//                        'name'    => 'Between',
//                        'options' => array(
//                            'min' => '1940-01-01',
//                            'max' => date( 'Y-m-d', time() )
//                        ),
//                    ),
//                ),
//            ]
//            // end
//        ];
//        prn( $configgw -> save( $config ) );

        $modelClassname = '\\Wepo\\Lib\\Wepo'.$this->getdrivername($config->adapter).'Model';

        if (!class_exists($modelClassname)) {
            throw new \Exception('\''.$modelClassname.'\' class not exists in '.$config->adapter.' adapter  driver settings ');
        } else {
            $model = new $modelClassname();
        }

        $model->parseconfig($config->toArray());
        $model->exchangeArray([ ]);

        return $model;
    }

    public function get($name)
    {
        $dbObjectName = '\\Wepo\\Model\\'.$name;
        if (!class_exists($dbObjectName)) {
            $model = $this->getModel($name);
        } else {
            $model = new $dbObjectName();
        }
        if (!isset($this->_tables[ $name ])) {
            $ObjectLogicName = '\\Wepo\\Model\\Logic\\'.$name;
            if (!class_exists($ObjectLogicName)) {
                $ObjectLogicName = '\\Wepo\\Lib\\DataLogic';
            }
            $dbAdapter            = $this->services->get($ObjectLogicName::$adapter);
            $connectionParameters = $dbAdapter->getDriver()->getConnection()->getConnectionParameters();
            if (!empty($connectionParameters[ 'gateway' ])) {
                $gwName = $connectionParameters[ 'gateway' ];
            } elseif (!empty($connectionParameters[ 'driver' ])) {
                $gwName = $connectionParameters[ 'driver' ];
            } else {
                throw new \Exception('unknown gateway ');
            }

            // create custom transport for the model
            $tableGatewayObject = '\\Wepo\\Model\\Transport\\'.$name;
            if (!class_exists($tableGatewayObject)) {
                // use general gw class
                $tableGatewayObject = '\\Wepo\\Lib\\Wepo'.$gwName.'Gateway';
                if (!class_exists($tableGatewayObject)) {
                    throw new \Exception(' unknown gateway class ');
                }
            }

            // create resultset proto
            $resultSetPrototype = new ResultSet();
            $resultSetPrototype->setArrayObjectPrototype($model);

            // create gw object and store it to the cache
            $tableGateway             = new $tableGatewayObject($dbObjectName::TABLE_NAME, $dbAdapter, null, $resultSetPrototype);
            $this->_tables[ $name ] = $tableGateway;
        }

        return $this->_tables[ $name ];
    }

    public function setServiceLocator(ServiceLocatorInterface $serviceLocator)
    {
        $this->services = $serviceLocator;
    }

    public function getServiceLocator()
    {
        return $this->services;
    }
}
