<?php

namespace Wepo\Lib;

use ModelFramework\BaseService\AbstractCachedService;

class DataMaster extends AbstractCachedService
{
    private $_serviceManager = null;
    private $_transport = null;
    protected $_logics = [ ];

    public function __construct(\Zend\ServiceManager\ServiceManager $serviceManager)
    {
        $this->_serviceManager      = $serviceManager;
        $this->_transport           = $serviceManager->get('Wepo\Lib\GatewayService');
        $this->_logics[ 'default' ] = new \Wepo\Lib\DataLogic($this->_transport);
    }

    public function get($model)
    {
        if (is_array($model)) {
            $model = reset($model);
        }
        $logicName = $model->model;
        if (!isset($this->_logics[ $logicName ])) {
            $logic = '\Wepo\Model\Logic\\'.$logicName;
            if (!class_exists($logic)) {
                $this->_logics[ $logicName ] = $this->_logics[ 'default' ];
            } else {
                $this->_logics[ $logicName ] = new $logic($this->_transport);
            }
        }

        return $this->_logics[ $logicName ];
    }

    public function dispatch($event)
    {
        prn('77');

        return call_user_func([ $this->get($event->getParams()), $event->getName() ], $event);
    }
}
