<?php

namespace Wepo\Lib;

use MonZend\Db\NoSql\NoSql;
use Zend\Db\Adapter\AdapterInterface;
use Zend\Db\ResultSet\ResultSet;
use Zend\Db\ResultSet\ResultSetInterface;
use Zend\Db\Sql\TableIdentifier;
use Zend\Db\TableGateway\Feature\FeatureSet;
use Zend\Db\TableGateway\AbstractTableGateway;
use Zend\Db\Sql\Select;

class WepoMongoGateway extends AbstractTableGateway implements WepoGatewayInterface
{
    /**
     * @var bool
     */
    protected $isInitialized = false;

    /**
     * @var AdapterInterface
     */
    protected $adapter = null;

    /**
     * @var string
     */
    protected $table = null;

    /**
     * @var array
     */
    protected $columns = array();

    /**
     * @var Feature\FeatureSet
     */
    protected $featureSet = null;

    /**
     * @var ResultSetInterface
     */
    protected $resultSetPrototype = null;

    /**
     * @var Sql
     */
    protected $sql = null;

    /**
     *
     * @var int
     */
    protected $lastInsertValue = null;

    /**
     * Constructor
     *
     * @param  string                                                               $table
     * @param  AdapterInterface                                                     $adapter
     * @param  Feature\AbstractFeature|Feature\FeatureSet|Feature\AbstractFeature[] $features
     * @param  ResultSetInterface                                                   $resultSetPrototype
     * @param  Sql                                                                  $sql
     * @throws Exception\InvalidArgumentException
     */
    public function __construct($table, AdapterInterface $adapter, $features = null, ResultSetInterface $resultSetPrototype = null, Sql $sql = null)
    {
        // table
        if (!(is_string($table) || $table instanceof TableIdentifier)) {
            throw new Exception\InvalidArgumentException('Table name must be a string or an instance of Zend\Db\Sql\TableIdentifier');
        }
        $this->table = $table;

        // adapter
        $this->adapter = $adapter;

        // process features
        if ($features !== null) {
            if ($features instanceof Feature\AbstractFeature) {
                $features = array( $features );
            }
            if (is_array($features)) {
                $this->featureSet = new FeatureSet($features);
            } elseif ($features instanceof FeatureSet) {
                $this->featureSet = $features;
            } else {
                throw new Exception\InvalidArgumentException(
                'TableGateway expects $feature to be an instance of an AbstractFeature or a FeatureSet, or an array of AbstractFeatures'
                );
            }
        } else {
            $this->featureSet = new FeatureSet();
        }

        // result prototype
        $this->resultSetPrototype = ($resultSetPrototype) ?: new ResultSet();

        // Sql object (factory for select, insert, update, delete)
//        $this -> sql = ($sql) ? : new Sql( $this -> adapter, $this -> table );
        $this->sql = new NoSql($this->adapter, $this->table, new \MonZend\Db\NoSql\Platform\Mongo\Mongo());
        // check sql object bound to same table
        if ($this->sql->getTable() != $this->table) {
            throw new Exception\InvalidArgumentException('The table inside the provided Sql object must match the table of this TableGateway');
        }

        $this->initialize();
    }

    /**
     * @return bool
     */
    public function isInitialized()
    {
        return $this->isInitialized;
    }

    /**
     * Initialize
     *
     * @throws Exception\RuntimeException
     * @return null
     */
    public function initialize()
    {
        if ($this->isInitialized) {
            return;
        }

        if (!$this->featureSet instanceof FeatureSet) {
            $this->featureSet = new FeatureSet();
        }

        // :FIXME: add featureSet compat
        $this->featureSet->setTableGateway($this);
        $this->featureSet->apply('preInitialize', array());

        if (!$this->adapter instanceof AdapterInterface) {
            throw new Exception\RuntimeException('This table does not have an Adapter setup');
        }

        if (!is_string($this->table) && !$this->table instanceof TableIdentifier) {
            throw new Exception\RuntimeException('This table object does not have a valid table set.');
        }

        if (!$this->resultSetPrototype instanceof ResultSetInterface) {
            $this->resultSetPrototype = new ResultSet();
        }

        if (!$this->sql instanceof NoSql) {
            $this->sql = new NoSql($this->adapter, $this->table, new \MonZend\Db\NoSql\Platform\Mongo\Mongo());
        }

        $this->featureSet->apply('postInitialize', array());

        $this->isInitialized = true;

        if (($object  = $this->resultSetPrototype->getArrayObjectPrototype()) instanceof WepoModel &&
            method_exists($object, 'getFieldNames') && is_array($columns = $object->getFieldNames())) {
            $this->columns = $columns;
        }
    }

    /**
     * Get table name
     *
     * @return string
     */
    public function getTable()
    {
        return $this->table;
    }

    /**
     * Get adapter
     *
     * @return AdapterInterface
     */
    public function getAdapter()
    {
        return $this->adapter;
    }

    /**
     * @return array
     */
    public function getColumns()
    {
        return $this->columns;
    }

    /**
     * @return Feature\FeatureSet
     */
    public function getFeatureSet()
    {
        return $this->featureSet;
    }

    /**
     * Get select result prototype
     *
     * @return ResultSet
     */
    public function getResultSetPrototype()
    {
        return $this->resultSetPrototype;
    }

    /**
     * @return NoSql
     */
    public function getSql()
    {
        return $this->sql;
    }

    /**
     * Select
     *
     * @param  Where|\Closure|string|array $where
     * @return ResultSet
     */
    public function select($where = null)
    {
        if (!$this->isInitialized) {
            $this->initialize();
        }

        $select = $this->sql->select();

        if ($where instanceof \Closure) {
            $where($select);
        } elseif ($where !== null) {
            $select->where($where);
        }

        return $this->selectWith($select);
    }

    /**
     * @param  Select                  $select
     * @return null|ResultSetInterface
     * @throws \RuntimeException
     */
    public function selectWith(Select $select)
    {
        if (!$this->isInitialized) {
            $this->initialize();
        }

        return $this->executeSelect($select);
    }

    /**
     * @param  Select                     $select
     * @return ResultSet
     * @throws Exception\RuntimeException
     */
    protected function executeSelect(Select $select)
    {
        $selectState = $select->getRawState();
        if ($selectState[ 'table' ] != $this->table) {
            throw new Exception\RuntimeException('The table name of the provided select object must match that of the table');
        }

        if ($selectState[ 'columns' ] == array( Select::SQL_STAR ) && $this->columns !== array()) {
            $select->columns($this->columns);
        }

        // apply preSelect features
        $this->featureSet->apply('preSelect', array( $select ));

        // prepare and execute
        $statement = $this->sql->prepareStatementForSqlObject($select);
        $result    = $statement->execute();
        if (($object    = $this->resultSetPrototype->getArrayObjectPrototype()) instanceof WepoModel &&
            method_exists($object, 'getFieldNames') && is_array($columns   = $object->getFieldNames())) {
            $this->columns = $columns;
            $result->setFieldCount($this->resultSetPrototype->getArrayObjectPrototype()->getFieldCount());
        }

        // build result set
        $resultSet = clone $this->resultSetPrototype;
        $resultSet->initialize($result);

        // apply postSelect features
        $this->featureSet->apply('postSelect', array( $statement, $result, $resultSet ));

        return $resultSet;
    }

    /**
     * Insert
     *
     * @param  array $set
     * @return int
     */
    public function insert($set)
    {
        if (!$this->isInitialized) {
            $this->initialize();
        }

        $insert = $this->sql->insert();
        $insert->values($set);

        return $this->executeInsert($insert);
    }

    public function update($set, $where = null)
    {
        if (!$this->isInitialized) {
            $this->initialize();
        }
        $sql    = $this->sql;
        $update = $sql->update();
        $update->set($set);

        if ($where !== null) {
            $update->where($where);
        }

        return $this->executeUpdate($update);
    }

    /**
     * Delete
     *
     * @param  Where|\Closure|string|array $where
     * @return int
     */
    public function delete($where)
    {
        if (!$this->isInitialized) {
            $this->initialize();
        }
        $delete = $this->sql->delete();
        if ($where instanceof \Closure) {
            $where($delete);
        } else {
            $delete->where($where);
        }

        return $this->executeDelete($delete);
    }

    /**
     * @return \Zend\Db\ResultSet\ResultSet
     * @throws Exception\RuntimeException
     */
    public function fetchAll()
    {
        $resultSet = $this->select();

        return $resultSet;
    }

    /**
     * @param  string    $id
     * @return WepoModel
     */
    public function get($id)
    {
        $id     = $id;
        $rowset = $this->select(array( 'id' => $id ));
        $row    = $rowset->current();

        return $row;
    }

    protected function assemble($fields = array(), $orders = array(), $limit = null, $offset = null)
    {
        prn('assemble');
    }

    public function gather($fields = array(), $orders = array(), $limit = null, $offset = null)
    {
        # :FIXME: add joins functionality
        return $this->find($fields = array(), $orders = array(), $limit  = null, $offset = null);
    }

    /**
     * @param  array                        $fields
     * @param  array                        $orders
     * @param  null|int                     $limit
     * @param  null|int                     $offset
     * @return \Zend\Db\ResultSet\ResultSet
     * @throws Exception\RuntimeException
     */
    public function find($fields = array(), $orders = array(), $limit = null, $offset = null)
    {
        if (!is_array($fields)) {
            throw new \Exception("Wrong input type - not error !");
        }
        $select = $this->getSql()->select();
        if (count($fields)) {
            $select->where($fields);
        }
        if (count($orders)) {
            $select->order($orders);
        }
        if ($limit !== null) {
            $select->limit($limit);
        }
        if ($offset !== null) {
            $select->offset($offset);
        }
        $rowset = $this->selectWith($select);

        return $rowset;
    }

    public function isUnique(WepoModel $model)
    {
        foreach ($model->unique() as $_unique) {
            $_data = [ ];
            foreach ((array) $_unique as $_key) {
                $_data[ $_key ] = $model->$_key;
            }
            $check = $this->find($_data);
            if ($check->count() > 0 && $check->current()->id != $model->id) {
                return false;
            }
        }

        return true;
    }

    public function save(WepoModel $model)
    {
        $result = false;
//        $data   = $model -> toArray();
        $data   = $model->getFields();
        $id     = (int) $model->id;
        if (!$this->isUnique($model)) {
            throw new \Exception('Data is not unique');
        }

        if ($id == 0) {
            $iresult      = $this->insert($data);
            /* MongoId shaman magic */
            $mongoid      = $this->getLastInsertValue();
            $data[ 'id' ] = (string) $mongoid;
            $re2 = $this->update($data, array( '_id' => $mongoid ));
            /* /MongoId shaman magic */
            $result       = empty($iresult[ 'ok' ]) ? 0 : $iresult[ 'ok' ];
        } else {
            if ($this->get($id)) {
                $result = $this->update($data, array( 'id' => $id ));
            } else {
                throw new \Exception('Model id does not exist');
            }
        }

        return $result;
    }

    public function getPages($fields = array(), $conditions = array(), $orders = array()/* $params = array( ) */)
    {
        $sql    = $this->getSql();
        $select = $sql->select();
        if (count($fields)) {
            $select->columns($fields);
        }
        $_fieldmap = [ ];

        $model     = $this->getResultSetPrototype()->getArrayObjectPrototype();

        foreach (array_keys($model->getFields()) as $_field) {
            $_fieldmap[ $_field ] = $sql->getTable().'.'.$_field;
        }

        $_aliasmap = $model->aliasmap();

        if (0) {
            foreach ($model->getJoins() as $_i => $_join) {
                $_on         = '';
                $_class      = 'Wepo\Model\\'.$_join[ 'model' ];
                $_table      = $_class::TABLE_NAME;
                $_tablealias = is_string($_i) ? $_i : $_table.$_i;

                foreach ($_join[ 'on' ] as $_key => $_value) {
                    if (strlen($_on)) {
                        $_on .= ' AND ';
                    }
                    $_on .= $sql->getTable().'.'.$_key.' = '.$_tablealias.'.'.$_value;
                }
//         $select -> join( 'user', 'lead.owner_id = user.id', array( 'owner_login' => 'login' ) );
                foreach ($_join[ 'fields' ] as $_alias => $_field) {
                    $_fieldmap[ $_alias ] = $_tablealias.'.'.$_field;
                }

                $_bjoin = false;
                foreach (array_keys($_join[ 'fields' ]) as $_field) {
                    $_src   = $_aliasmap[ $_field ];
                    $_bjoin = in_array($_src, $fields);
                    if ($_bjoin) {
                        break;
                    }
                }
                if ($_bjoin) {
                    $select->join([ $_tablealias => $_table ], $_on, $_join[ 'fields' ], \Zend\Db\Sql\Select::JOIN_LEFT);
                }
            }
        }

        if (count($conditions)) {
            //            foreach ( $conditions as $_k => $_value )
//            {
//                if ( isset( $_fieldmap[ $_k ] ) )
//                {
//                    $conditions[ $_fieldmap[ $_k ] ] = $_value;
//                    unset( $conditions[ $_k ] );
//                }
//            }
            $select->where($conditions);
        }

        if (count($orders)) {
            $_ord = [ ];
            foreach ($orders as $_k => $_value) {
                $_fieldname = is_string($_k) ? $_k : $_value;
                $_keyname   = $_k;
                $_keyvalue  = $_value;

                if (isset($_fieldmap[ $_fieldname ])) {
                    $_keyname  = is_string($_k) ? $_k : $_fieldmap[ $_fieldname ];
                    $_keyvalue = is_string($_k) ? $_value : $_fieldmap[ $_fieldname ];
                }

                $_ord[ $_keyname ] = $_keyvalue;
            }

            $select->order($_ord);
        }

//        exit();
//        return $this->  selectWith($select);
//        $adapter   = new \Zend\Paginator\Adapter\DbSelect( $select, $sql, $this -> getResultSetPrototype() );
        $adapter   = new \MonZend\Paginator\Adapter\DbSelect($select, $sql, $this->getResultSetPrototype());
        $paginator = new \Zend\Paginator\Paginator($adapter);

        return $paginator;
    }
}
