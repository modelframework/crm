<?php

namespace Wepo\Form;

use Wepo\Lib\WepoForm;

class ResetPassForm extends WepoForm
{
    protected $filter;

    public function __construct()
    {
        parent::__construct('form');
        $this->setAttribute('action', '');
        $this->setAttribute('method', 'post');
        $this->setAttribute('class', 'validate');
        $this->add(array(
            'type' => 'Wepo\Form\ResetPassFieldset',
            'options' => array(
                'use_as_base_fieldset' => true,
            ),
        ));
        $this->add(array(
            'type' => 'Wepo\Form\ButtonFieldset',
            'name' => 'buttons',
        ));
//        $this -> setValidationGroup( array(
//            'fields' => array(
//                'login',
//                'password',
//            ),
//        ) );
    }
}
