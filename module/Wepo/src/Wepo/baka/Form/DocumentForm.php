<?php

namespace Wepo\Form;

use Wepo\Lib\WepoForm;

class DocumentForm extends WepoForm
{
    public function __construct($name = null)
    {
        parent::__construct('form');

        $this->setAttribute('action', 'reg');
        $this->setAttribute('method', 'post');
        $this->setAttribute('class', 'validate');
        $this->add(array(
            'type' => 'Wepo\Form\DocumentAttachFieldset',
        ));
        $this->add(array(
            'type'    => 'Wepo\Form\DocumentFieldset',
            'options' => array(
                'use_as_base_fieldset' => true,
            ),
        ));
        $this->add(array(
            'type' => 'Wepo\Form\ButtonFieldset',
        ));
        $this->setValidationGroup(array(
//            'attach' => array(
//                'table_id',
//                'target_id',
//            ),
            'fields' => array(
                'document_name',
                'document_description',
            ),
        ));
    }
}
