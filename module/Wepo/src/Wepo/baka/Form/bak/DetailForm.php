<?php

namespace Wepo\Form;

use Wepo\Lib\WepoForm;

class DetailForm extends WepoForm
{
    public function __construct($name = null)
    {
        parent::__construct('form');
        $this->setAttribute('action', 'reg');
        $this->setAttribute('method', 'post');
        $this->add(array(
            'type' => 'Wepo\Form\DetailFieldset',
            'options' => array(
                'use_as_base_fieldset' => true,
            ),
        ));
        $this->add(array(
            'type' => 'Wepo\Form\SaUrlFieldset',
        ));
        $this->add(array(
            'type' => 'Wepo\Form\ButtonFieldset',
        ));
        $this->setValidationGroup(array(
            'fields' => array(
                'pricebook_id',
                'product_id',
                'product_price',
                'discount',
                'qty',
            ),
        ));
    }
}
