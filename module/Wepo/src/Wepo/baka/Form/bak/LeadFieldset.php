<?php

namespace Wepo\Form;

use Wepo\Lib\WepoFieldset;

class LeadFieldset extends WepoFieldset
{
    public function __construct($name = null)
    {
        parent::__construct('fields');
        $this->setLabel('Lead information');
        $this->setAttribute('class', 'table');
        $this->add(array(
            'name'       => 'fname',
            'attributes' => array(
                'type'     => 'text',
                'required' => 'required',
                'class'    => '',
            ),
            'options'    => array(
                'label_attributes' => array(
                    'class' => 'required',
                ),
                'label'            => 'First Name',
            ),
        ));
        $this->add(array(
            'name'       => 'lname',
            'attributes' => array(
                'type'     => 'text',
                'required' => 'required',
//                'placeholder' => '* required field'
            ),
            'options'    => array(
                'label'            => 'Last Name',
                'label_attributes' => array(
                    'class' => 'required',
                ),
            ),
        ));
        $this->add(array(
            'name'       => 'login',
            'attributes' => array(
                'type'     => 'text',
                'required' => 'required',
//                'placeholder' => '* required field'
            ),
            'options'    => array(
                'label'            => 'Account Name',
                'label_attributes' => array(
                    'class' => 'required',
                ),
            ),
        ));
        $this->add(array(
            'name'       => 'phone',
            'attributes' => array(
                'type'     => 'text',
                'required' => 'required',
//                'placeholder' => '* required field'
            ),
            'options'    => array(
                'label'            => 'Phone',
                'label_attributes' => array(
                    'class' => 'required',
                ),
            ),
        ));
        $this->add(array(
            'name'       => 'mobile',
            'attributes' => array(
                'type' => 'text',
            ),
            'options'    => array(
                'label' => 'Mobile',
            ),
        ));
        $this->add(array(
            'name'       => 'email',
            'attributes' => array(
                'type'        => 'email',
                'placeholder' => 'login@email.com',
            ),
            'options'    => array(
                'label' => 'Email',
            ),
        ));
        $this->add(array(
            'name'       => 'birth_date',
            'attributes' => array(
                'type'  => 'date',
                'min'   => '1960-01-01',
                'max'   => date('Y-m-d'), //'2015-01-01',
                'value' => date('Y-m-d'),
            ),
            'options'    => array(
                'label' => 'Date of birth',
            ),
        ));
    }
}
