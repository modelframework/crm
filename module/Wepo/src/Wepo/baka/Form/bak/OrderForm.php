<?php

namespace Wepo\Form;

use Wepo\Lib\WepoForm;

class OrderForm extends WepoForm
{
    public function __construct($name = null)
    {
        parent::__construct('form');
        $this->setAttribute('action', 'reg');
        $this->setAttribute('method', 'post');
        $this->add(array(
            'type' => 'Wepo\Form\OrderFieldset',
            'options' => array(
                'use_as_base_fieldset' => true,
            ),
        ));
        $this->add(array(
            'type' => 'Wepo\Form\ButtonFieldset',
        ));
        $this->setValidationGroup(array(
            'fields' => array(
                'contact_id',
                'subject',
                'description',
                'total_discount',
                'pricebook_id',
            ),
        ));
    }
}
