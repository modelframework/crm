<?php

namespace Wepo\Form;

use Wepo\Lib\WepoForm;

class DoctorForm extends WepoForm
{
    public function __construct($name = null)
    {
        parent::__construct('form');

        $this->setAttribute('action', 'reg');
        $this->setAttribute('method', 'post');
        $this->setAttribute('class', 'validate');
        $this->add(array(
            'type'    => 'Wepo\Form\DoctorFieldset',
            'options' => array(
                'use_as_base_fieldset' => true,
            ),
        ));
        $this->add(array(
            'type' => 'Wepo\Form\AddressFieldset',
        ));
        $this->add(array(
            'type' => 'Wepo\Form\ButtonFieldset',
        ));
        $this->setValidationGroup(array(
            'fields' => array(
                'name',
                'dea',
                'license',
            ),
            "address" => [
                "country",
                "zip",
                "state",
                "address",
                "city",
        ],
        ));
    }
}
