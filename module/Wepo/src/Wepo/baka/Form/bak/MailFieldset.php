<?php

namespace Wepo\Form;

use Wepo\Lib\WepoFieldset;

class MailFieldset extends WepoFieldset
{
    public function __construct($name = null)
    {
        parent::__construct('fields');
        $this->setLabel('Mail information');
        $this->setAttribute('class', 'table');
//        $this -> add( array(
//            'name' => 'from_addr',
//            'attributes' => array(
//                'type' => 'text',
//                'required' => 'required',
//            ),
//            'options' => array(
//                'label_attributes' => array(
//                    'class' => 'required'
//                ),
//                'label' => 'Sender',
//            ),
//        ) );
        $this->add(array(
            'type' => 'Zend\Form\Element\Select',
            'name' => 'to_addr',
            'attributes' => array(
                'id' => 'to_addr',
                'disabled' => '',
            ),
            'options' => array(
                'label' => ' Receiver',
                'value_options' => array( '0' => 'Please select...' ),
            ),
        ));
        $this->add(array(
            'name' => 'subject',
            'attributes' => array(
                'type' => 'text',
                'required' => 'required',
            ),
            'options' => array(
                'label_attributes' => array(
                    'class' => 'required',
                ),
                'label' => 'Subject',
            ),
        ));
        $this->add(array(
            'name' => 'body',
            'attributes' => array(
                'type' => 'textarea',
                'required' => 'required',
            ),
            'options' => array(
                'label_attributes' => array(
                    'class' => 'required',
                ),
                'label' => 'Body',
            ),
        ));
    }
}
