<?php

namespace Wepo\Form;

use Wepo\Lib\WepoForm;

class LeadMongoForm extends WepoForm
{
    public function __construct($name = null)
    {
        // we want to ignore the name passed
        parent::__construct('form');
        $this->setAttribute('action', 'reg');
        $this->setAttribute('method', 'post');
        $this->add(array(
            'type' => 'Wepo\Form\LeadFieldset',
            'options' => array(
                'use_as_base_fieldset' => true,
            ),
        ));
        $this->add(array(
            'type' => 'Wepo\Form\ButtonFieldset',
        ));
        $this->add(array(
            'type' => 'Wepo\Form\SaUrlFieldset',
        ));
        $this->setValidationGroup(array(
            'fields' => array(
                'fname',
                'lname',
                'login',
                'phone',
                'mobile',
                'email',
                'birth_date',
            ),
        ));
    }
}
