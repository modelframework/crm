<?php

namespace Wepo\Form;

use Wepo\Lib\WepoForm;

class AdduserForm extends WepoForm
{
    public function __construct($name = null)
    {
        // we want to ignore the name passed
        parent::__construct('form');
        $this->setAttribute('action', 'reg');
        $this->setAttribute('method', 'post');
        $this->add(array(
            'type' => 'Wepo\Form\AdduserFieldset',
            'options' => array(
                'use_as_base_fieldset' => true,
            ),
        ));
        $this->add(array(
            'type' => 'Wepo\Form\ButtonFieldset',
        ));
        $this->setValidationGroup(array(
            'fields' => array(
                'login',
                'password',
                'fname',
                'lname',
                'birth_date',
                'role_id',
            ),
        ));
    }
}
