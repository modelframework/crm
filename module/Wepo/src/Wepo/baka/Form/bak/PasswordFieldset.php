<?php

namespace Wepo\Form;

use Wepo\Lib\WepoFieldset;

class PasswordFieldset extends WepoFieldset
{
    public function __construct($name = null)
    {
        parent::__construct('pwd');
        $this->setLabel('Pasword information');
        $this->setAttribute('class', 'table');
        $this->add(array(
            'name' => 'password',
            'attributes' => array(
                'type' => 'password',
                'required' => 'required',
            ),
            'options' => array(
                'label_attributes' => array(
                    'class' => 'required',
                ),
                'label' => 'Current password',
            ),
        ));
        $this->add(array(
            'name' => 'newpass',
            'attributes' => array(
                'type' => 'password',
                'required' => 'required',
            ),
            'options' => array(
                'label_attributes' => array(
                    'class' => 'required',
                ),
                'label' => 'New password',
            ),
        ));
        $this->add(array(
            'name' => 'newpass2',
            'attributes' => array(
                'type' => 'password',
                'required' => 'required',
            ),
            'options' => array(
                'label_attributes' => array(
                    'class' => 'required',
                ),
                'label' => 'Confirm new password',
            ),
        ));
    }

    public function getInputFilterSpecification()
    {
        return array(
            'password' => array(
                'required' => true,
                'filters' => array(
                    array( 'name' => 'StripTags' ),
                    array( 'name' => 'StringTrim' ),
                ),
                'validators' => array(
                    array(
                        'name' => 'StringLength',
                        'options' => array(
                            'encoding' => 'UTF-8',
                            'min' => 3,
                            'max' => 50,
                        ),
                    ),
                ),
            ),
            'newpass' => array(
                'required' => true,
                'filters' => array(
                    array( 'name' => 'StripTags' ),
                    array( 'name' => 'StringTrim' ),
                ),
                'validators' => array(
                    array(
                        'name' => 'StringLength',
                        'options' => array(
                            'encoding' => 'UTF-8',
                            'min' => 3,
                            'max' => 50,
                        ),
                    ),
                ),
            ),
            'newpass2' => array(
                'required' => true,
                'filters' => array(
                    array( 'name' => 'StripTags' ),
                    array( 'name' => 'StringTrim' ),
                ),
                'validators' => array(
                    array(
                        'name' => 'StringLength',
                        'options' => array(
                            'encoding' => 'UTF-8',
                            'min' => 3,
                            'max' => 50,
                        ),
                    ),
                ),
            ),
        );
    }
}
