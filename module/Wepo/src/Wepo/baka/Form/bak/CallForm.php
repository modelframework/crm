<?php

namespace Wepo\Form;

use Wepo\Lib\WepoForm;

class CallForm extends WepoForm
{
    public function __construct($name = null)
    {
        parent::__construct('form');
        $this->setAttribute('action', 'reg');
        $this->setAttribute('method', 'post');
        $this->add(array(
            'type' => 'Wepo\Form\ActivityAdditonalFieldset',
        ));
        $this->add(array(
            'type' => 'Wepo\Form\CallFieldset',
            'options' => array(
                'use_as_base_fieldset' => true,
            ),
        ));
        $this->add(array(
            'type' => 'Wepo\Form\ButtonFieldset',
        ));
        $this->setValidationGroup(array(
            'additional' => array(
                'table_id',
                'target_id',
            ),
            'fields' => array(
                'subject',
                'call_type',
                'call_purpose',
                'client_id',
                'call_detail',
                'call_start_dtm',
                'call_duration',
                'call_result',
                'description',
                'billable',
                'remind_dtm',
            ),
        ));
    }
}
