<?php

namespace Wepo\Form;

use Wepo\Lib\WepoFieldset;

class EdituserFieldset extends WepoFieldset
{
    public function __construct($name = null)
    {
        parent::__construct('fields');
        $this->setLabel('User information');
        $this->setAttribute('class', 'table');
        $this->add(array(
            'name' => 'login',
            'attributes' => array(
                'type' => 'email',
                'required' => 'required',
            ),
            'options' => array(
                'label_attributes' => array(
                    'class' => 'required',
                ),
                'label' => 'Login (Email)',
            ),
        ));
        $this->add(array(
            'name' => 'fname',
            'attributes' => array(
                'type' => 'text',
                'required' => 'required',
            ),
            'options' => array(
                'label_attributes' => array(
                    'class' => 'required',
                ),
                'label' => 'Name',
            ),
        ));
        $this->add(array(
            'name' => 'lname',
            'attributes' => array(
                'type' => 'text',
                'required' => 'required',
            ),
            'options' => array(
                'label_attributes' => array(
                    'class' => 'required',
                ),
                'label' => 'Lastname',
            ),
        ));
        $this->add(array(
            'name' => 'birth_date',
            'attributes' => array(
                'type' => 'date',
                'min' => '1950-01-01',
                'max' => date('Y-m-d'),
                'value' => date('Y-m-d'),
                'required' => 'required',
            ),
            'options' => array(
                'label_attributes' => array(
                    'class' => 'required',
                ),
                'label' => 'Date of birth',
            ),
        ));
    }
}
