<?php

namespace Wepo\Form;

use Wepo\Lib\WepoForm;

class ProductForm extends WepoForm
{
    public function __construct($name = null)
    {
        parent::__construct('form');
        $this->setAttribute('action', 'reg');
        $this->setAttribute('method', 'post');
        $this->add(array(
            'type' => 'Wepo\Form\ProductFieldset',
            'options' => array(
                'use_as_base_fieldset' => true,
            ),
        ));
        $this->add(array(
            'type' => 'Wepo\Form\ButtonFieldset',
        ));
        $this->add(array(
            'type' => 'Wepo\Form\SaUrlFieldset',
        ));
        $this->setValidationGroup(array(
            'fields' => array(
                'name',
                'price',
                'description',
            ),
        ));
    }
}
