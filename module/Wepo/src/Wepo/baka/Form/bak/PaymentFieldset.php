<?php

namespace Wepo\Form;

use Wepo\Lib\WepoFieldset;

class PaymentFieldset extends WepoFieldset
{
    public function __construct($name = null)
    {
        parent::__construct('fields');
        $this->setLabel('Payment information');
        $this->setAttribute('class', 'table');
        $this->add(array(
            'type' => 'Zend\Form\Element\Select',
            'name' => 'contact_id',
            'attributes' => array(
                'id' => 'contact_id',
                'required' => 'required',
            ),
            'options' => array(
                'label_attributes' => array(
                    'class' => 'required',
                ),
                'label' => 'Contact',
                'value_options' => array(
                    0 => 'Please Select ... ',
                ),
            ),
        ));
        $this->add(array(
            'name' => 'sum',
            'attributes' => array(
                'type' => 'text',
                'required' => 'required',
            ),
            'options' => array(
                'label_attributes' => array(
                    'class' => 'required',
                ),
                'label' => 'Sum',
            ),
        ));
        $this->add(array(
            'name' => 'performed_dtm',
            'type' => 'Zend\Form\Element\DateTimeLocal',
            'attributes' => array(
                'required' => 'required',
            ),
            'options' => array(
                'label_attributes' => array(
                    'class' => 'required',
                ),
                'label' => 'Date',
            ),
        ));
        $this->add(array(
            'name' => 'description',
            'attributes' => array(
                'type' => 'text',
                'required' => 'required',
            ),
            'options' => array(
                'label_attributes' => array(
                    'class' => 'required',
                ),
                'label' => 'Description',
            ),
        ));
    }
}
