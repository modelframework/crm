<?php

namespace Wepo\Form;

use Wepo\Lib\WepoFieldset;

class EditDetailFieldset extends WepoFieldset
{
    public function __construct($name = null)
    {
        parent::__construct('fields');
        $this->setLabel('Product information');
        $this->setAttribute('class', 'table');
        $this->add(array(
            'type' => 'Zend\Form\Element\Select',
            'name' => 'pricebook_id',
            'attributes' => array(
                'id' => 'pricebook_id',
                'required' => 'required',
            ),
            'options' => array(
                'label' => 'Pricebook',
                'value_options' => array(
                    0 => 'Please Select ... ',
                ),
            ),
        ));
        $this->add(array(
            'name' => 'product_price',
            'attributes' => array(
                'required' => 'required',
                'id' => 'price',
            ),
            'options' => array(
                'label' => 'Price',
            ),
        ));
        $this->add(array(
            'name' => 'discount',
            'attributes' => array(
                'type' => 'text',
                'id' => 'disc',
            ),
            'options' => array(
                'label' => 'Discount',
            ),
        ));
        $this->add(array(
            'name' => 'qty',
            'attributes' => array(
                'type' => 'text',
                'id' => 'qty',
            ),
            'options' => array(
                'label' => 'Quantity',
            ),
        ));
    }
}
