<?php

namespace Wepo\Form;

use Wepo\Lib\WepoFieldset;

class OrderFieldset extends WepoFieldset
{
    public function __construct($name = null)
    {
        parent::__construct('fields');
        $this->setLabel('Order information');
        $this->setAttribute('class', 'table');
        $this->add(array(
            'type' => 'Zend\Form\Element\Select',
            'name' => 'contact_id',
            'attributes' => array(
                'id' => 'iduser',
                'required' => 'required',
            ),
            'options' => array(
                'label_attributes' => array(
                    'class' => 'required',
                ),
                'label' => 'Contact',
                'value_options' => array(
                    0 => 'Please Select ... ',
                ),
            ),
        ));
        $this->add(array(
            'name' => 'subject',
            'attributes' => array(
                'type' => 'text',
                'required' => 'required',
            ),
            'options' => array(
                'label_attributes' => array(
                    'class' => 'required',
                ),
                'label' => 'Subject',
            ),
        ));
        $this->add(array(
            'name' => 'description',
            'attributes' => array(
                'type' => 'text',
                'required' => 'required',
            ),
            'options' => array(
                'label_attributes' => array(
                    'class' => 'required',
                ),
                'label' => 'Description',
            ),
        ));
        $this->add(array(
            'name' => 'total_discount',
            'attributes' => array(
                'type' => 'text',
                'required' => 'required',
            ),
            'options' => array(
                'label_attributes' => array(
                    'class' => 'required',
                ),
                'label' => 'Total discount',
            ),
        ));
        $this->add(array(
            'type' => 'Zend\Form\Element\Select',
            'name' => 'pricebook_id',
            'attributes' => array(
                'id' => 'iduser',
                'required' => 'required',
            ),
            'options' => array(
//                'label_attributes' => array(
//                    'class' => 'required'
//                ),
                'label' => 'Global pricebook',
                'value_options' => array(
                    0 => 'Please Select ... ',
                ),
            ),
        ));
    }
}
