<?php

namespace Wepo\Form;

use Wepo\Lib\WepoFieldset;

class CallFieldset extends WepoFieldset
{
    public function __construct($name = null)
    {
        parent::__construct('fields');
        $this->setLabel('Call information');
        $this->setAttribute('class', 'table');
        $this->add(array(
            'name' => 'subject',
            'attributes' => array(
                'type' => 'text',
                'required' => 'required',
            ),
            'options' => array(
                'label_attributes' => array(
                    'class' => 'required',
                ),
                'label' => 'Subject',
            ),
        ));
        $this->add(array(
            'type' => 'Zend\Form\Element\Select',
            'name' => 'call_type',
            'attributes' => array(
                'id' => 'call_type',
            ),
            'options' => array(
                'label' => 'Call type',
                'value_options' => array(
                    '0' => 'Please select...',
                    '1' => 'incoming',
                    '2' => 'outcoming',
                ),
            ),
        ));
        $this->add(array(
            'name' => 'call_purpose',
            'attributes' => array(
                'type' => 'text',
            ),
            'options' => array(
                'label' => 'Call purpose',
            ),
        ));
//        $this -> add( array(
//            'type' => 'Zend\Form\Element\Select',
//            'name' => 'owner_id',
//            'attributes' => array(
//                'id' => 'owner_id',
//            ),
//            'options' => array(
//                'label' => 'Owner',
//                'value_options' => array(
//                    '0' => 'Please select',
//                ),
//            ),
//        ) );
        $this->add(array(
            'type' => 'Zend\Form\Element\Select',
            'name' => 'client_id',
            'attributes' => array(
                'id' => 'client_id',
                'required' => 'required',
            ),

            'options' => array(
                'label' => 'Client',
                'label_attributes' => array(
                    'class' => 'required',
                ),
                'value_options' => array(
                    '0' => 'Please select...',
                ),
            ),
        ));
        $this->add(array(
            'name' => 'call_detail',
            'attributes' => array(
                'type' => 'text',
            ),
            'options' => array(
                'label' => 'Call detail',
            ),
        ));
        $this->add(array(
            'type' => 'Zend\Form\Element\DateTimeLocal',
            'name' => 'call_start_dtm',
//            'attributes' => array(
//            'disabled'       => false,
//                'min'  => '2010-01-01T00:00:00',
//                'max'  => '2020-01-01T00:00:00'
//            ),
            'options' => array(
                'label' => 'Start call',
            ),
        ));
        $this->add(array(
            'name' => 'call_duration',
            'attributes' => array(
                'type' => 'text',
            ),
            'options' => array(
                'label' => 'Call duration',
            ),
        ));
        $this->add(array(
            'type' => 'Zend\Form\Element\Select',
            'name' => 'call_result',
            'attributes' => array(
                'id' => 'call_result',
            ),
            'options' => array(
                'label' => 'Call result',
                'value_options' => array(
                    '0' => 'Please select...',
                    '1' => 'success',
                    '2' => 'failure',
                ),
            ),
        ));
        $this->add(array(
            'name' => 'description',
            'attributes' => array(
                'type' => 'textarea',
            ),
            'options' => array(
                'label' => 'Description',
            ),
        ));
        $this->add(array(
            'type' => 'Zend\Form\Element\Select',
            'name' => 'billable',
            'attributes' => array(
                'id' => 'billable',
            ),
            'options' => array(
                'label' => 'Billable',
                'value_options' => array(
                    '0' => 'Please select...',
                    '1' => 'able',
                    '2' => 'unable',
                ),
            ),
        ));
        $this->add(array(
            'type' => 'Zend\Form\Element\DateTimeLocal',
            'name' => 'remind_dtm',
            'attributes' => array(
                'required' => 'required',
            ),
//            'attributes' => array(
//                'type' => 'date',
//                'min' => '1950-01-01',
//                'max' => '2015-01-01',
//                'value' => date( 'Y-m-d' )
//            ),
            'options' => array(
                'label' => 'Remind',
                'label_attributes' => array(
                    'class' => 'required',
                ),
            ),
        ));
    }
}
