<?php

namespace Wepo\Form;

use Wepo\Lib\WepoFieldset;

class ProductFieldset extends WepoFieldset
{
    public function __construct($name = null)
    {
        parent::__construct('fields');
        $this->setLabel('Product information');
        $this->setAttribute('class', 'table');
        $this->add(array(
            'name' => 'name',
            'attributes' => array(
                'type' => 'text',
                'required' => 'required',
            ),
            'options' => array(
                'label_attributes' => array(
                    'class' => 'required',
                ),
                'label' => 'Product name',
            ),
        ));
        $this->add(array(
            'name' => 'price',
            'attributes' => array(
                'type' => 'text',
                'required' => 'required',
            ),
            'options' => array(
                'label_attributes' => array(
                    'class' => 'required',
                ),
                'label' => 'Price',
            ),
        ));
        $this->add(array(
            'name' => 'description',
            'attributes' => array(
                'type' => 'textarea',
                'required' => 'required',
            ),
            'options' => array(
                'label_attributes' => array(
                    'class' => 'required',
                ),
                'label' => 'Description',
            ),
        ));
    }
}
