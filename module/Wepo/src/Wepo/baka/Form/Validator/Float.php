<?php

namespace Wepo\Form\Validator;

use Zend\Validator\AbstractValidator;

class Float extends AbstractValidator
{
    const FLOAT = 'FLOAT';

    protected $messageTemplates = array(
        self::FLOAT => "'%value%' is not a floating point value",
    );

    public function isValid($value)
    {
        $this->setValue($value);
        if (!is_numeric($value) || $value[ 0 ] == "." || substr($value, -1) == ".") {
            $this->error(self::FLOAT);

            return false;
        }

        return true;
    }
}
