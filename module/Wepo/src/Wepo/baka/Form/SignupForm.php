<?php

namespace Wepo\Form;

use Wepo\Lib\WepoForm;

class SignupForm extends WepoForm
{
    public function __construct($name = null)
    {
        parent::__construct('form');
        $this->setAttribute('action', 'login');
        $this->setAttribute('method', 'post');
        $this->setAttribute('class', 'validate');
        $this->add(array(
            'type'    => 'Wepo\Form\MainUserFieldset',
            'name'    => 'user',
            'options' => array(
                'use_as_base_fieldset' => true,
            ),
        ));
        $this->add(array(
            'type' => 'Wepo\Form\CompanyFieldset',
            'name' => 'company',
        ));
        $this->add(array(
            'type' => 'Wepo\Form\ButtonFieldset',
            'name' => 'buttons',
        ));
        $this->setValidationGroup(array(
            'user'  => array(
                'login',
                'password',
                'password2',
                'fname',
                'lname',
            ),
            'company' => array(
                'company', ),
        ));
    }

    public function isValid()
    {
        return parent::isValid();
    }
}
