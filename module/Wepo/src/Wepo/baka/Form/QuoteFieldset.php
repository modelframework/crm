<?php

namespace Wepo\Form;

use Wepo\Lib\WepoFieldset;

class QuoteFieldset extends WepoFieldset
{
    public function __construct($name = null)
    {
        parent::__construct('fields');
        $this->setLabel('Quote information');
        $this->setAttribute('class', 'table');
        $this->add(array(
            'type' => 'Zend\Form\Element\Select',
            'name' => 'contact_id',
            'attributes' => array(
                'id' => 'iduser',
//                'required' => 'required',
            ),
            'options' => array(
                'label_attributes' => array(
//                    'class' => 'required'
                ),
                'label' => 'Patient',
                'value_options' => array(
                    0 => 'Please Select ... ',
                ),
            ),
        ));
        $this->add(array(
            'name' => 'subject',
            'attributes' => array(
                'type' => 'text',
                'required' => 'required',
            ),
            'options' => array(
                'label_attributes' => array(
                    'class' => 'required',
                ),
                'label' => 'Subject',
            ),
        ));
        $this->add(array(
            'name' => 'description',
            'attributes' => array(
                'type' => 'text',
                'required' => 'required',
            ),
            'options' => array(
                'label_attributes' => array(
                    'class' => 'required',
                ),
                'label' => 'Description',
            ),
        ));
        $this->add(array(
            'type' => 'Zend\Form\Element\Select',
            'name' => 'pricebook_id',
            'attributes' => array(
                'id' => 'iduser',
                'required' => 'required',
            ),
            'options' => array(
//                'label_attributes' => array(
//                    'class' => 'required'
//                ),
                'label' => 'Global pricebook',
                'value_options' => array(
                    0 => 'Please Select ... ',
                ),
            ),
        ));
        $this->add(array(
            'name'       => 'adjustment',
            'attributes' => array(
                'type'     => 'text',
                'id'       => 'modifier',
                'required' => 'required',
                'value' => 0,
            ),
            'options'    => array(
                'label_attributes' => array(
                    'class' => 'required',
                ),
                'label'            => 'Adjustment',
            ),
        ));
        $this->add(array(
            'type'       => 'Zend\Form\Element\Select',
            'name'       => 'price_type',
            'attributes' => array(
                'id'       => 'price_type',
                'required' => 'required',
            ),
            'options'    => array(
                'label_attributes' => array(
                    'class' => 'required',
                ),
                'label'            => 'Discount type',
                'value_options'    => array(
                    'Direct Price Reduction'  => 'Direct Price Reduction',
                    '% of Price' => '% of Price',
                ),
            ),
        ));
        $this->add(array(
            'name'       => 'modifier',
            'attributes' => array(
                'type'     => 'text',
                'id'       => 'modifier',
                'required' => 'required',
                'value'    => 0,
            ),
            'options'    => array(
                'label_attributes' => array(
                    'class' => 'required',
                ),
                'label'            => 'Discount',
            ),
        ));
        $this->add(array(
            'name'       => 'sales_tax',
            'attributes' => array(
                'type'     => 'text',
                'id'       => 'modifier',
                'required' => 'required',
                'value' => 0,
            ),
            'options'    => array(
                'label_attributes' => array(
                    'class' => 'required',
                ),
                'label'            => 'Sales tax',
            ),
        ));
        $this->add(array(
            'name'       => 'vat',
            'attributes' => array(
                'type'     => 'text',
                'id'       => 'modifier',
                'required' => 'required',
                'value' => 0,
            ),
            'options'    => array(
                'label_attributes' => array(
                    'class' => 'required',
                ),
                'label'            => 'Vat',
            ),
        ));
    }
}
