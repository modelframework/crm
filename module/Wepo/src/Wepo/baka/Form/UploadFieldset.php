<?php

namespace Wepo\Form;

use Wepo\Lib\WepoFieldset;

class UploadFieldset extends WepoFieldset
{
    public function __construct($name = null)
    {
        parent::__construct('fields');
//        $this -> setLabel( 'Document information' );
        $this->setAttribute('class', 'table');

        $this->add(array(
            'type'       => 'Zend\Form\Element\File',
            'name'       => 'upload-file',
            'attributes' => array(
            ),
            'options'    => array(
                'label' => 'File field',
            ),
        ));
    }
}
