<?php

namespace Wepo\Form;

use Wepo\Lib\WepoFieldset;

class SaUrlFieldset extends WepoFieldset
{
    public function __construct($name = null)
    {
        parent::__construct('saurl');
        $this->add(array(
            'name' => 'back',
            'attributes' => array(
                'type' => 'hidden',
            ),
        ));
    }
}
