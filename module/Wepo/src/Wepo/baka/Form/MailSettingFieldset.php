<?php

namespace Wepo\Form;

use Wepo\Lib\WepoFieldset;

class MailSettingFieldset extends WepoFieldset
{
    public function __construct($name = null)
    {
        parent::__construct('fields');
//        $this->setObject(new \Wepo\Model\MailSetting())
//             ->setHydrator(new \Zend\Stdlib\Hydrator\ArraySerializable());

        $this->setLabel('Main information');
        $this->setAttribute('class', 'table');

//        $this -> add( array(
//            'type'       => 'Zend\Form\Element\Select',
//            'name'       => 'user_id',
//            'attributes' => array(
//                'id'       => 'owner_id',
//                'required' => 'required',
//            ),
//            'options'    => array(
//                'label_attributes' => array(
//                    'class' => 'required'
//                ),
//                'label'            => 'User',
//                'value_options'    => array(
//                    0 => 'Please Select ... ',
//                ),
//            ),
//        ) );
        $this->add(array(
            'name'       => 'email',
            'attributes' => array(
                'type'     => 'text',
                'required' => 'required',
                'id'       => 'email',
            ),
            'options'    => array(
                'label_attributes' => array(
                    'class' => 'required',
                ),
                'label'            => 'Email',
            ),
        ));
        $this->add(array(
            'name'       => 'pass',
            'attributes' => array(
                'type'     => 'password',
                'required' => 'required',
                'id'       => 'email',
            ),
            'options'    => array(
                'label_attributes' => array(
                    'class' => 'required',
                ),
                'label'            => 'Pass',
            ),
        ));
    }
}
