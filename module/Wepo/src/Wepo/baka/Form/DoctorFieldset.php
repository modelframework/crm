<?php

namespace Wepo\Form;

use Wepo\Lib\WepoFieldset;

class DoctorFieldset extends WepoFieldset
{
    public function __construct($name = null)
    {
        parent::__construct('fields');
        $this->setLabel('Doctor information');
        $this->setAttribute('class', 'table');
        $this->add(array(
            'name' => 'name',
            'attributes' => array(
                'type' => 'text',
                'required' => 'required',
            ),
            'options' => array(
                'label_attributes' => array(
                    'class' => 'required',
                ),
                'label' => 'Name',
            ),
        ));
        $this->add(array(
            'name' => 'dea',
            'attributes' => array(
                'type' => 'text',
            ),
            'options' => array(
                'label' => 'DEA',
            ),
        ));
        $this->add(array(
            'name' => 'license',
            'attributes' => array(
                'type' => 'text',
                'required' => 'required',
            ),
            'options' => array(
                'label_attributes' => array(
                    'class' => 'required',
                ),
                'label' => 'License',
            ),
        ));
        $this->add(array(
            'name' => 'phone',
            'attributes' => array(
                'type' => 'text',
                'required' => 'required',
            ),
            'options' => array(
                'label_attributes' => array(
                    'class' => 'required',
                ),
                'label' => 'Phone',
            ),
        ));
        $this->add(array(
            'name' => 'fax',
            'attributes' => array(
                'type' => 'text',
                'required' => 'required',
            ),
            'options' => array(
                'label_attributes' => array(
                    'class' => 'required',
                ),
                'label' => 'Fax',
            ),
        ));
    }
}
