<?php

namespace Wepo\Form;

use Wepo\Lib\WepoForm;

class TaskForm extends WepoForm
{
    public function __construct($name = null)
    {
        parent::__construct('form');
        $this->setAttribute('action', 'reg');
        $this->setAttribute('method', 'post');
        $this->setAttribute('class', 'ajaxform validate');
        $this->add(array(
            'type' => 'Wepo\Form\ActivityAdditonalFieldset',
        ));
        $this->add(array(
            'type' => 'Wepo\Form\TaskFieldset',
            'options' => array(
                'use_as_base_fieldset' => true,
            ),
        ));
        $this->add(array(
            'type' => 'Wepo\Form\ButtonFieldset',
        ));
        $this->setValidationGroup(array(
            'additional' => array(
                'table_id',
                'target_id',
            ),
            'fields' => array(
                'subject',
                'due_dtm',
                'description',
                'priority',
//                'client_id',
                'status',
                'recurring',
                'rec_startdate',
                'rec_enddate',
                'remind_dtm',
//                'notification_email',
            ),
        ));
    }
}
