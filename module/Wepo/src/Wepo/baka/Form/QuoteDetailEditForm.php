<?php

namespace Wepo\Form;

use Wepo\Lib\WepoForm;

class QuoteDetailEditForm extends WepoForm
{
    public function __construct($name = null)
    {
        parent::__construct('form');
        $this->setAttribute('action', 'reg');
        $this->setAttribute('method', 'post');
        $this->setAttribute('class', 'ajaxform validate');
        $this->add(array(
            'type' => 'Wepo\Form\QuoteDetailEditFieldset',
            'options' => array(
                'use_as_base_fieldset' => true,
            ),
        ));
        $this->add(array(
            'type' => 'Wepo\Form\ButtonFieldset',
        ));
        $this->setValidationGroup(array(
            'fields' => array(
                'pricebook_id',
                'product_price',
                'discount_type',
                'discount',
                'qty',
            ),
        ));
    }
}
