<?php

namespace Wepo\Form;

use Wepo\Lib\WepoFieldset;

class CompanyFieldset extends WepoFieldset
{
    public function __construct($name = null)
    {
        parent::__construct('company');
        $this->setLabel('Company information');
        $this->setAttribute('class', 'table');
        $this->add(array(
            'name' => 'company',
            'attributes' => array(
                'type' => 'text',
                'required' => 'required',
            ),
            'options' => array(
                'label_attributes' => array(
                    'class' => 'required',
                ),
                'label' => 'Company name',
            ),
        ));
    }
}
