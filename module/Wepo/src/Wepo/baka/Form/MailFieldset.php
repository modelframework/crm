<?php

namespace Wepo\Form;

use Wepo\Lib\WepoFieldset;

class MailFieldset extends WepoFieldset
{
    public function __construct($name = null)
    {
        parent::__construct('fields');
        $this->setLabel('Email information');
        $this->setAttribute('class', 'table');
        $this->add(array(
            'type' => 'Zend\Form\Element\Select',
            'name' => 'setting_id',
            'attributes' => array(
                'id' => 'iduser',
                'required' => 'required',
            ),
            'options' => array(
                'label_attributes' => array(
                    'class' => 'required',
                ),
                'label' => 'From',
                'value_options' => array(
                    0 => 'Please Select ... ',
                ),
            ),
        ));
        $this->add(array(
//            'type' => 'Zend\Form\Element\Select',
            'name' => 'to',
            'attributes' => array(
                'id' => 'email',
                'required' => 'required',
            ),
            'options' => array(
                'label_attributes' => array(
                    'class' => 'required',
                ),
                'label' => 'To',
            ),
        ));
        $this->add(array(
            'name' => 'title',
            'attributes' => array(
                'type' => 'text',
                'required' => 'required',
            ),
            'options' => array(
                'label_attributes' => array(
                    'class' => 'required',
                ),
                'label' => 'Title',
            ),
        ));
        $this->add(array(
            'name' => 'text',
            'attributes' => array(
                'type' => 'textarea',
                'required' => 'required',
            ),
            'options' => array(
                'label_attributes' => array(
                    'class' => 'required',
                ),
                'label' => 'Message',
            ),
        ));
    }
}
