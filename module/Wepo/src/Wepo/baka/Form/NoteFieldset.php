<?php

namespace Wepo\Form;

use Wepo\Lib\WepoFieldset;

class NoteFieldset extends WepoFieldset
{
    public function __construct($name = null)
    {
        parent::__construct('fields');
        $this->setLabel('');
        $this->setAttribute('class', 'table');

        $this->add(array(
            'name'       => 'title',
            'attributes' => array(
                'type'     => 'text',
                'required' => 'required',
                'id' => 'note_title',
            ),
            'options'    => array(
                'label_attributes' => array(
                    'class' => 'required',
                ),
                'label'            => 'Title',
            ),
        ));
        $this->add(array(
            'type'       => 'Zend\Form\Element\Textarea',
            'name'       => 'content',
            'attributes' => array(
                'required' => 'required',
                'class' => 'note_textarea',
                'id' => 'note_input',
                'rows' => 5,
            ),
            'options'    => array(
                'label_attributes' => array(
                    'class' => 'required',
                ),
                'label'            => 'Content',
            ),
        ));
    }
}
