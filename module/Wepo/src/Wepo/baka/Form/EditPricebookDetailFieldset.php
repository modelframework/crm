<?php

namespace Wepo\Form;

use Wepo\Lib\WepoFieldset;

class EditPricebookDetailFieldset extends WepoFieldset
{
    public function __construct($name = null)
    {
        parent::__construct('fields');
        $this->setLabel('Product information');
        $this->setAttribute('class', 'table');

        $this->add(array(
            'type' => 'Zend\Form\Element\Select',
            'name' => 'discount_type',
            'attributes' => array(
                'id' => 'discount_type',
                'required' => 'required',
            ),
            'options' => array(
                'label_attributes' => array(
                    'class' => 'required',
                ),
                'label' => 'Discount type',
                'value_options' => array(
                    'Direct Price Reduction'  => 'Direct Price Reduction',
                    '% of Price' => '% of Price',
                ),
            ),
        ));
        $this->add(array(
            'name' => 'discount',
            'attributes' => array(
                'type' => 'text',
                'id' => 'discount',
            ),
            'options' => array(
                'label' => 'Discount',
            ),
        ));
    }
}
