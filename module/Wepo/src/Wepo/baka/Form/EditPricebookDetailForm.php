<?php

namespace Wepo\Form;

use Wepo\Lib\WepoForm;

class EditPricebookDetailForm extends WepoForm
{
    public function __construct($name = null)
    {
        parent::__construct('form');
        $this->setAttribute('action', 'reg');
        $this->setAttribute('method', 'post');
        $this->setAttribute('class', 'ajaxform validate');
        $this->add(array(
            'type' => 'Wepo\Form\EditPricebookDetailFieldset',
            'options' => array(
                'use_as_base_fieldset' => true,
            ),
        ));
        $this->add(array(
            'type' => 'Wepo\Form\ButtonFieldset',
        ));
        $this->add(array(
            'type' => 'Wepo\Form\SaUrlFieldset',
        ));

//        impliment
        $this->setValidationGroup(array(
            'fields' => array(
                'discount',
                'discount_type',
            ),
        ));
    }
}
