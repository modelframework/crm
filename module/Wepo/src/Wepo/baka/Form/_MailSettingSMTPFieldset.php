<?php

namespace Wepo\Form;

use Wepo\Lib\WepoFieldset;

class _MailSettingSMTPFieldset extends WepoFieldset
{
    public function __construct($name = null)
    {
        parent::__construct('smtp');
        $this->setLabel('SMTP settings');
        $this->setAttribute('class', 'table');

        $this->add(array(
            'name'       => 'smtp_user',
            'attributes' => array(
                'type'     => 'text',
                'required' => 'required',
                'id'       => 'smtp_user',
            ),
            'options'    => array(
                'label_attributes' => array(
                    'class' => 'required',
                ),
                'label'            => 'SMTP User',
            ),
        ));
        $this->add(array(
            'name'       => 'smtp_host',
            'attributes' => array(
                'type'     => 'text',
                'required' => 'required',
                'id'       => 'smtp_host',
            ),
            'options'    => array(
                'label_attributes' => array(
                    'class' => 'required',
                ),
                'label'            => 'SMTP Host',
            ),
        ));
        $this->add(array(
            'name'       => 'smtp_port',
            'attributes' => array(
                'type'     => 'text',
                'required' => 'required',
                'id'       => 'smtp_port',
            ),
            'options'    => array(
                'label_attributes' => array(
                    'class' => 'required',
                ),
                'label'            => 'SMTP Port',
            ),
        ));
        $this->add(array(
            'type'       => 'Zend\Form\Element\Select',
            'name'       => 'smtp_security',
            'attributes' => array(
                'id'       => 'smtp_security',
                'required' => 'required',
            ),
            'options'    => array(
                'label_attributes' => array(
                    'class' => 'required',
                ),
                'label'            => 'SMTP Security Type',
                'value_options'    => array(
                    'ssl'  => 'ssl',
                    'tls' => 'tls',
                ),
            ),
        ));
    }
}
