<?php

namespace Wepo\Form;

use Wepo\Lib\WepoFieldset;

class DocumentFieldset extends WepoFieldset
{
    public function __construct($name = null)
    {
        parent::__construct('fields');
        $this->setLabel('Document information');
        $this->setAttribute('class', 'table');

        $this->add(array(
            'name'       => 'document_name',
            'attributes' => array(
                'type'     => 'text',
                'required' => 'required',
            ),
            'options'    => array(
                'label_attributes' => array(
                    'class' => 'required',
                ),
                'label'            => 'Document name',
            ),
        ));
        $this->add(array(
            'type'       => 'Zend\Form\Element\File',
            'name'       => 'upload-file',
            'attributes' => array(
                'required' => 'required',
            ),
            'options'    => array(
                'label' => 'File field',
                'label_attributes' => array(
                    'class' => 'required',
                ),
            ),
        ));
        $this->add(array(
            'name' => 'document_description',
            'attributes' => array(
                'type' => 'textarea',
                'required' => 'required',
            ),
            'options' => array(
                'label_attributes' => array(
                    'class' => 'required',
                ),
                'label' => 'Description',
            ),
        ));
    }
}
