<?php

namespace Wepo\Form;

use Wepo\Lib\WepoFieldset;

class MailReceiveSettingFieldset extends WepoFieldset
{
    public function __construct($name = null)
    {
        parent::__construct('protocol');
        $this->setLabel('Protocol settings');
        $this->setAttribute('class', 'table');

        $this->add(array(
            'type'       => 'Zend\Form\Element\Select',
            'name'       => 'setting_protocol',
            'attributes' => array(
                'id'       => 'protocol_type',
                'required' => 'required',
            ),
            'options'    => array(
                'label_attributes' => array(
                    'class' => 'required',
                ),
                'label'            => 'Protocol Type',
//                'value_options'    => array(
//                    'Imap'  => 'Imap',
//                    'Pop3' => 'Pop3',
//                    'SMTP' => 'SMTP',
//                ),
            ),
        ));
        $this->add(array(
            'name'       => 'setting_user',
            'attributes' => array(
                'type'     => 'text',
                'required' => 'required',
                'id'       => 'protocol_user',
            ),
            'options'    => array(
                'label_attributes' => array(
                    'class' => 'required',
                ),
                'label'            => 'Protocol User',
            ),
        ));
        $this->add(array(
            'name'       => 'setting_host',
            'attributes' => array(
                'type'     => 'text',
                'required' => 'required',
                'id'       => 'protocol_host',
            ),
            'options'    => array(
                'label_attributes' => array(
                    'class' => 'required',
                ),
                'label'            => 'Protocol Host',
            ),
        ));
        $this->add(array(
            'name'       => 'setting_port',
            'attributes' => array(
                'type'     => 'text',
                'required' => 'required',
                'id'       => 'protocol_port',
            ),
            'options'    => array(
                'label_attributes' => array(
                    'class' => 'required',
                ),
                'label'            => 'Protocol Port',
            ),
        ));
        $this->add(array(
            'type'       => 'Zend\Form\Element\Select',
            'name'       => 'setting_security',
            'attributes' => array(
                'id'       => 'protocol_security',
                'required' => 'required',
            ),
            'options'    => array(
                'label_attributes' => array(
                    'class' => 'required',
                ),
                'label'            => 'Security Type',
//                'value_options'    => array(
//                    'ssl'  => 'ssl',
//                    'tls' => 'tls',
//                ),
            ),
        ));
    }
}
