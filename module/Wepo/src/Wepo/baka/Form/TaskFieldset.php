<?php

namespace Wepo\Form;

use Wepo\Lib\WepoFieldset;

class TaskFieldset extends WepoFieldset
{
    public function __construct($name = null)
    {
        parent::__construct('fields');
        $this->setLabel('Task information');
        $this->setAttribute('class', 'table');
        $this->add(array(
            'name'       => 'subject',
            'attributes' => array(
                'type'     => 'text',
                'required' => 'required',
            ),
            'options'    => array(
                'label_attributes' => array(
                    'class' => 'required',
                ),
                'label'            => 'Subject',
            ),
        ));
        $this->add(array(
            'type'       => 'Zend\Form\Element\DateTimeLocal',
            'name'       => 'due_dtm',
            'attributes' => array(
//            'disabled'       => false,
//                'min'  => '2010-01-01T00:00:00Z',
//                'max'  => '2020-01-01T00:00:00Z',
//                'value' => date( 'Y-m-d' )
            ),
            'options'    => array(
                'label' => 'Due date',
            ),
        ));
        $this->add(array(
            'name'       => 'description',
            'attributes' => array(
                'type' => 'textarea',
            ),
            'options'    => array(
                'label' => 'Description',
            ),
        ));
        $this->add(array(
            'type'       => 'Zend\Form\Element\Select',
            'name'       => 'priority',
            'attributes' => array(
                'id' => 'priority',
            ),
            'options'    => array(
                'label'         => 'Priority',
                'value_options' => array(
//                    '' => 'Please select...',
                    'Low' => 'Low',
                    'Medium' => 'Medium',
                    'High' => 'High',
                ),
            ),
        ));
//        $this -> add( array(
////            'type' => 'Zend\Form\Element\Select',
//            'name' => 'table_id',
////            'attributes' => array(
////                'id' => 'table_id',
////            ),
////            'options' => array(
////                'label' => 'Table',
////                'value_options' => array(
////                    '0' => 'Please select...',
////                    '3' => 'Leads',
////                    '4' => 'Contacts',
////                ),
////            ),
//        ) );
//        $this -> add( array(
//            'name' => 'target_id',
////            'attributes' => array(
////                'type' => 'text',
////            ),
////            'options' => array(
////                'label' => 'Target',
////            ),
//        ) );
//        $this -> add( array(
//            'type'       => 'Zend\Form\Element\Select',
//            'name'       => 'client_id',
//            'attributes' => array(
//                'id' => 'client_id',
//            ),
//            'options'    => array(
//                'label'            => 'Client',
//                'label_attributes' => array(
//                    'class' => 'required'
//                ),
//                'value_options'    => array(
//                    '0' => 'Please select...',
//                ),
//            ),
//        ) );
        $this->add(array(
            'type'       => 'Zend\Form\Element\Select',
            'name'       => 'status',
            'attributes' => array(
                'id' => 'status',
            ),
            'options'    => array(
                'label'         => 'Status',
                'value_options' => array(
//                    '' => 'Please select...',
                    'new' => 'New',
                    'runing' => 'Runing',
                    'complete' => 'Complete',
                ),
            ),
        ));
        $this->add(array(
            'name'       => 'recurring',
            'attributes' => array(
                'type' => 'text',
            ),
            'options'    => array(
                'label' => 'Recurring',
            ),
        ));
        $this->add(array(
            'name'       => 'rec_startdate',
            'attributes' => array(
                'type'  => 'date',
                'min'   => '1950-01-01',
                'max'   => '2015-01-01',
                'value' => date('Y-m-d'),
            ),
            'options'    => array(
                'label' => 'Start date time',
            ),
        ));
        $this->add(array(
            'name'       => 'rec_enddate',
            'attributes' => array(
                'type'  => 'date',
                'min'   => '1950-01-01',
                'max'   => '2015-01-01',
                'value' => date('Y-m-d'),
            ),
            'options'    => array(
                'label' => 'End date time',
            ),
        ));
        $this->add(array(
            'type'       => 'Zend\Form\Element\DateTimeLocal',
            'name'       => 'remind_dtm',
            'attributes' => array(
                'required' => 'required',
            ),
//            'attributes' => array(
//                'type' => 'date',
//                'min' => '1950-01-01',
//                'max' => '2015-01-01',
//                'value' => date( 'Y-m-d' )
//            ),
            'options'    => array(
                'label' => 'Remind',
                'label_attributes' => array(
                    'class' => 'required',
                ),
            ),
        ));
//        $this -> add( array(
//            'type'       => 'Zend\Form\Element\Select',
//            'name'       => 'notification_email',
//            'attributes' => array(
//                'id' => 'notification_email',
//            ),
//            'options'    => array(
//                'label'         => 'Notification email',
//                'value_options' => array(
//                    '' => 'Please select...',
//                    'yes' => 'yes',
//                    'no' => 'no',
//                ),
//            ),
//        ) );
    }
}
