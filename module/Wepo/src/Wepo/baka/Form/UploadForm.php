<?php

namespace Wepo\Form;

use Wepo\Lib\WepoForm;

class UploadForm extends WepoForm
{
    public function __construct($name = null)
    {
        parent::__construct('form');

        $this->setAttribute('action', 'reg');
        $this->setAttribute('method', 'post');
        $this->setAttribute('class', 'validate');
        $this->add(array(
            'type' => 'Wepo\Form\UploadFieldset',
            'options' => array(
                'use_as_base_fieldset' => true,
            ),
        ));
        $this->setValidationGroup(array(
            'fields' => array(
                'upload-file',
            ),
        ));
    }
}
