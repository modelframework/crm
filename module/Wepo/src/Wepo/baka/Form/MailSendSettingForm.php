<?php

namespace Wepo\Form;

use Wepo\Lib\WepoForm;

class MailSendSettingForm extends WepoForm
{
    public function __construct($name = null)
    {
        parent::__construct('form');
        $this->setAttribute('action', 'reg');
        $this->setAttribute('method', 'post');
        $this->setAttribute('class', 'ajaxform validate');
        $this->add(array(
            'type'    => 'Wepo\Form\MailSettingFieldset',
            'options' => array(
                'use_as_base_fieldset' => true,
            ),
        ));
        $this->add(array(
            'type' => 'Wepo\Form\MailSendSettingFieldset',
        ));
        $this->add(array(
            'type' => 'Wepo\Form\ButtonFieldset',
        ));
        $this->add(array(
            'type' => 'Wepo\Form\SaUrlFieldset',
        ));
//        impliment
        $this->setValidationGroup(array(
            'fields'   => array(
                'email',
                'pass',
                'user_id',
            ),
            'protocol' => array(
                'setting_user',
                'setting_protocol',
                'setting_host',
                'setting_port',
                'setting_security',
                'is_default',
            ),
        ));
    }
}
