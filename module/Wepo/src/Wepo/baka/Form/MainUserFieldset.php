<?php

namespace Wepo\Form;

use Wepo\Lib\WepoFieldset;

class MainUserFieldset extends WepoFieldset
{
    public function __construct($name = null)
    {
        parent::__construct('user');
        $this->setLabel('User information');
        $this->setAttribute('class', 'table');
        $this->add(array(
            'name'       => 'login',
            'attributes' => array(
                'type'     => 'email',
                'required' => 'required',
            ),
            'options'    => array(
                'label_attributes' => array(
                    'class' => 'required',
                ),
                'label'            => 'Login (Email)',
            ),
        ));
        $this->add(array(
            'name'       => 'password',
            'attributes' => array(
                'type'     => 'password',
                'required' => 'required',
            ),
            'options'    => array(
                'label_attributes' => array(
                    'class' => 'required',
                ),
                'label'            => 'Password',
            ),
        ));
        $this->add(array(
            'name'       => 'password2',
            'attributes' => array(
                'type'     => 'password',
                'required' => 'required',
            ),
            'options'    => array(
                'label_attributes' => array(
                    'class' => 'required',
                ),
                'label'            => 'Confirm Password',
            ),
        ));
        $this->add(array(
            'name'       => 'fname',
            'attributes' => array(
                'type'     => 'text',
                'required' => 'required',
            ),
            'options'    => array(
                'label_attributes' => array(
                    'class' => 'required',
                ),
                'label'            => 'Name',
            ),
        ));
        $this->add(array(
            'name'       => 'lname',
            'attributes' => array(
                'type'     => 'text',
                'required' => 'required',
            ),
            'options'    => array(
                'label_attributes' => array(
                    'class' => 'required',
                ),
                'label'            => 'Lastname',
            ),
        ));
    }
}
