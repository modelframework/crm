<?php

namespace Wepo\Form;

use Wepo\Lib\WepoFieldset;

class DocumentAttachFieldset extends WepoFieldset
{
    public function __construct($name = null)
    {
        parent::__construct('attach');
        $this->setLabel('Attach document');
        $this->setAttribute('class', 'additional');

//        $this -> add( array(
//            'type'    => 'Zend\Form\Element\Radio',
//            'name'    => 'table_id',
//            'options' => array(
//                'label'         => 'To',
//                'value_options' => array(
//                    '5295fdf7c5b9f222acd3c752' => 'Lead',
//                    '5295fdf7c5b9f222acd3c753' => 'Contact',
//                    '5295fdf7c5b9f222acd3c751' => 'Me',
//                ),
//            ),
//        ) );
//        $this -> add( array(
//            'type'       => 'Zend\Form\Element\Select',
//            'name'       => 'target_id',
//            'options'    => array(
//                'disable_inarray_validator' => true,
//                'label'            => '',
//                'value_options'    => array(
//                    0 => 'Please select',
//                ),
//            ),
//        ) );
        $this->add(array(
//            'type'       => 'Zend\Form\Element\Select',
            'name' => 'leads_id',
            'attributes' => array(
                'id' => 'attach_lead',
//                'data-data' => '{&quot;title:&quot;aaaa&quot;,&quot;_id&quot;:&quot;bbb&quot;}'
            ),
            'options' => array(
                'label_attributes' => array(
                ),
                'label' => 'Leads',
            ),
        ));
        $this->add(array(
            'name' => 'contacts_id',
            'attributes' => array(
                'id' => 'attach_contact',
            ),
            'options' => array(
                'label_attributes' => array(
                ),
                'label' => 'Patients',
            ),
        ));
        $this->add(array(
            'name' => 'users_id',
            'attributes' => array(
                'id' => 'attach_user',
            ),
            'options' => array(
                'label_attributes' => array(
                ),
                'label' => 'Users',
            ),
        ));
    }
}
