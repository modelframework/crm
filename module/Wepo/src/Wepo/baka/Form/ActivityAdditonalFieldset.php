<?php

namespace Wepo\Form;

use Wepo\Lib\WepoFieldset;

class ActivityAdditonalFieldset extends WepoFieldset
{
    public function __construct($name = null)
    {
        parent::__construct('additional');
        $this->setLabel('Patient information');
        $this->setAttribute('class', 'additional');

        ////////////////////////
        $this->add(array(
            'type'       => 'Zend\Form\Element\Radio',
            'name'       => 'table_id',
            'attributes' => array( //                'value' => '4',
            ),
            'options'    => array(
                'label'         => 'Contact type',
                'value_options' => array(
                    '5295fdf7c5b9f222acd3c752' => 'Lead',
                    '5295fdf7c5b9f222acd3c753' => 'Patient',
                ),
            ),
        ));
        $this->add(array(
            'type'       => 'Zend\Form\Element\Select',
            'name'       => 'target_id',
            'attributes' => array(
                'required' => 'required',
            ),
            'options'    => array(
                'label_attributes' => array(
                    'for'   => 'table_id_2',
                    'class' => 'required',
                ),
                'label'            => '',
                'value_options'    => array(
                    0 => 'Please select',
                ),
            ),
        ));
        /////////////////////////////
    }
}
