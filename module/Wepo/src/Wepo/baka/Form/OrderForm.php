<?php

namespace Wepo\Form;

use Wepo\Lib\WepoForm;

class OrderForm extends WepoForm
{
    public function __construct($name = null)
    {
        parent::__construct('form');
        $this->setAttribute('action', 'reg');
        $this->setAttribute('method', 'post');
        $this->setAttribute('class', 'ajaxform validate');
        $this->add(array(
            'type' => 'Wepo\Form\OrderFieldset',
            'options' => array(
                'use_as_base_fieldset' => true,
            ),
        ));
        $this->add(array(
            'type' => 'Wepo\Form\ButtonFieldset',
        ));
        $this->setValidationGroup(array(
            'fields' => array(
                'contact_id',
                'subject',
                'description',
                'modifier',
                'price_type',
                'sales_tax',
                'vat',
                'adjustment',
                'pricebook_id',
            ),
        ));
    }
}
