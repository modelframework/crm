<?php

namespace Wepo\Form;

use Wepo\Lib\WepoFieldset;

class EditDocumentFieldset extends WepoFieldset
{
    public function __construct($name = null)
    {
        parent::__construct('fields');
        $this->setLabel('Document information');
        $this->setAttribute('class', 'table');

        $this->add(array(
            'name'       => 'document_name',
            'attributes' => array(
                'type'     => 'text',
                'required' => 'required',
            ),
            'options'    => array(
                'label_attributes' => array(
                    'class' => 'required',
                ),
                'label'            => 'Document name',
            ),
        ));
        $this->add(array(
            'name' => 'document_description',
            'attributes' => array(
                'type' => 'textarea',
                'required' => 'required',
            ),
            'options' => array(
                'label_attributes' => array(
                    'class' => 'required',
                ),
                'label' => 'Description',
            ),
        ));
//        $this -> add( array(
//            'type' => 'Zend\Form\Element\Select',
//            'name' => 'document_owner_id',
//            'attributes' => array(
//                'id' => 'iduser',
//                'required' => 'required',
//            ),
//            'options' => array(
//                'label_attributes' => array(
//                    'class' => 'required'
//                ),
//                'label' => 'Owner',
//                'value_options' => array(
//                    0 => 'Please Select ... ',
//                ),
//       ) ) );
    }
}
