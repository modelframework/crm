<?php

namespace Wepo\Form;

use Wepo\Lib\WepoFieldset;

class EventFieldset extends WepoFieldset
{
    public function __construct($name = null)
    {
        parent::__construct('fields');
        $this->setLabel('Event information');
        $this->setAttribute('class', 'table');
        $this->add(array(
            'name' => 'subject',
            'attributes' => array(
                'type' => 'text',
                'required' => true,
            ),
            'options' => array(
                'label_attributes' => array(
                    'class' => 'required',
                ),
                'label' => 'Subject',
            ),
        ));
        $this->add(array(
            'name' => 'description',
            'attributes' => array(
                'type' => 'textarea',
            ),
            'options' => array(
                'label' => 'Description',
            ),
        ));
        $this->add(array(
            'type' => 'Zend\Form\Element\DateTimeLocal',
            'name' => 'start_dtm',
//            'attributes' => array(
//                'min'  => '2010-01-01T00:00:00Z',
//                'max'  => '2020-01-01T00:00:00Z',
//                'value' => date( 'Y-m-d' )
//            ),
            'options' => array(
                'label' => 'Start',
            ),
        ));
        $this->add(array(
            'type' => 'Zend\Form\Element\DateTimeLocal',
            'name' => 'end_dtm',
//            'attributes' => array(
//                'min'  => '2010-01-01T00:00:00Z',
//                'max'  => '2020-01-01T00:00:00Z',
//                'value' => date( 'Y-m-d' )
//            ),
            'options' => array(
                'label' => 'End',
            ),
        ));
        $this->add(array(
            'type' => 'Zend\Form\Element\DateTimeLocal',
            'name' => 'remind_dtm',
            'attributes' => array(
//                'type' => 'date',
//                'min' => '1950-01-01',
//                'max' => '2015-01-01',
//                'value' => date( 'Y-m-d' )
                'required' => true,
            ),
            'options' => array(
                'label' => 'Remind',
                'label_attributes' => array(
                    'class' => 'required',
                ),
            ),
        ));
        $this->add(array(
            'name' => 'r_when',
            'attributes' => array(
                'type' => 'date',
                'min' => '1950-01-01',
                'max' => '2015-01-01',
                'value' => date('Y-m-d'),
            ),
            'options' => array(
                'label' => 'Remind when',
            ),
        ));
        $this->add(array(
            'name' => 'r_repeat',
            'attributes' => array(
                'type' => 'date',
                'min' => '1950-01-01',
                'max' => '2015-01-01',
                'value' => date('Y-m-d'),
            ),
            'options' => array(
                'label' => 'Remind repeat',
            ),
        ));
        $this->add(array(
            'name' => 'r_alert',
            'attributes' => array(
                'type' => 'text',
            ),
            'options' => array(
                'label' => 'Remind alert',
            ),
        ));
        $this->add(array(
            'name' => 'recurring',
            'attributes' => array(
                'type' => 'text',
            ),
            'options' => array(
                'label' => 'Recurring',
            ),
        ));
        $this->add(array(
            'name' => 'rec_startdate',
            'attributes' => array(
                'type' => 'date',
                'min' => '1950-01-01',
                'max' => '2015-01-01',
                'value' => date('Y-m-d'),
            ),
            'options' => array(
                'label' => 'Start date time',
            ),
        ));
        $this->add(array(
            'name' => 'rec_enddate',
            'attributes' => array(
                'type' => 'date',
                'min' => '1950-01-01',
                'max' => '2015-01-01',
                'value' => date('Y-m-d'),
            ),
            'options' => array(
                'label' => 'End date time',
            ),
        ));
        $this->add(array(
            'name' => 'invites',
            'attributes' => array(
                'type' => 'text',
            ),
            'options' => array(
                'label' => 'Invites',
            ),
        ));
        $this->add(array(
            'name' => 'venue',
            'attributes' => array(
                'type' => 'text',
            ),
            'options' => array(
                'label' => 'Venue',
            ),
        ));
//        $this -> add( array(
//            'name' => 'table_id',
////            'attributes' => array(
////                'id' => 'table_id',
////            ),
////            'options' => array(
////                'label' => 'Table',
////                'value_options' => array(
////                    '0' => 'Please select',
////                    '3' => 'Leads',
////                    '4' => 'Contacts',
////                ),
////            ),
//        ) );
//        $this -> add( array(
////            'type' => 'Zend\Form\Element\Select'
//            'name' => 'target_id',
////            'attributes' => array(
////                'type' => 'text',
////            ),
////            'options' => array(
////                'label' => 'Target',
////            ),
//        ) );
//        $this -> add( array(
//            'type' => 'Zend\Form\Element\Select',
//            'name' => 'client_id',
//            'attributes' => array(
////                'id' => 'client_id',
//                'required' => TRUE,
//            ),
//            'options' => array(
//                'label' => 'Client',
//                'label_attributes' => array(
//                    'class' => 'required'
//                ),
//                'value_options' => array(
//                    '0' => 'Please select',
//                ),
//            ),
//        ) );
//        $this -> add( array(
//            'type' => 'Zend\Form\Element\Select',
//            'name' => 'notification_email',
//            'attributes' => array(
//                'id' => 'notification_email',
//                'required' => TRUE,
//            ),
//            'options' => array(
//                'label' => 'Notification email',
//                'value_options' => array(
//                    '' => 'Please select...',
//                    'yes' => 'yes',
//                    'no' => 'no',
//                ),
//            ),
//        ) );
    }
}
