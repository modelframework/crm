<?php

namespace Wepo\Form;

use Wepo\Lib\WepoFieldset;

class ResetPassFieldset extends WepoFieldset
{
    public function __construct($name = null)
    {
        parent::__construct('fields');
        $this->setLabel('');
        $this->setAttribute('class', 'table');
        $this->add(array(
            'name' => 'login',
            'attributes' => array(
                'type' => 'email',
            ),
            'attributes' => array(
                'id' => 'wepologin',
                'required' => true,
                'placeholder' => 'Username',
                'class' => 'placehold',
                ),
            'options' => array(
             //   'label' => 'Login',
             //   'label_attributes' => array(
                  //  'class' => 'required',
               // ),
            ),
        ));
    }
}
