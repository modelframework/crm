<?php

namespace Wepo\Form;

use Wepo\Lib\WepoForm;

class EditDocumentForm extends WepoForm
{
    public function __construct($name = null)
    {
        parent::__construct('form');

        $this->setAttribute('action', 'reg');
        $this->setAttribute('method', 'post');
        $this->setAttribute('class', 'ajaxform validate');
//        $this -> setAttribute( 'class', 'validate' );
        $this->add(array(
            'type' => 'Wepo\Form\EditDocumentFieldset',
            'options' => array(
                'use_as_base_fieldset' => true,
            ),
        ));
        $this->add(array(
            'type' => 'Wepo\Form\ButtonFieldset',
        ));
        $this->setValidationGroup(array(
            'fields' => array(
                'owner_id',
                'document_name',
                'document_description',
            ),
        ));
    }
}
