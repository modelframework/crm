<?php

namespace Wepo\Form;

use Wepo\Lib\WepoForm;

class NoteForm extends WepoForm
{
    public function __construct($name = null)
    {
        parent::__construct('form');

        $this->setAttribute('action', 'reg');
        $this->setAttribute('method', 'post');
        $this->setAttribute('class', 'validate');
        $this->add(array(
            'type'    => 'Wepo\Form\NoteFieldset',
            'options' => array(
                'use_as_base_fieldset' => true,
            ),
        ));
        $this->add(array(
            'type' => 'Wepo\Form\ButtonFieldset',
        ));
        $this->setValidationGroup(array(
//            'attach' => array(
//                'table_id',
//                'target_id',
//            ),
            'fields' => array(
                'title',
                'content',
            ),
        ));
    }
}
