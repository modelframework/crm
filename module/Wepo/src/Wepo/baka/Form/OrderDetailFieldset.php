<?php

namespace Wepo\Form;

use Wepo\Lib\WepoFieldset;

class OrderDetailFieldset extends WepoFieldset
{
    public function __construct($name = null)
    {
        parent::__construct('fields');
        $this->setLabel('Product information');
        $this->setAttribute('class', 'table');
        $this->add(array(
            'type' => 'Zend\Form\Element\Select',
            'name' => 'pricebook_id',
            'attributes' => array(
                'id' => 'pricebook_id',
                'required' => 'required',
            ),
            'options' => array(
                'disable_inarray_validator' => true,
//                'label_attributes' => array(
//                    'class' => 'required'
//                ),
                'label' => 'Pricebook',
//                'value_options' => array(
//                    0 => 'Please Select ... ',
//                ),
            ),
        ));
        $this->add(array(
            'type' => 'Zend\Form\Element\Select',
            'name' => 'product_id',
            'attributes' => array(
                'id' => 'product_id',
                'required' => 'required',
            ),
            'options' => array(
                'label_attributes' => array(
                    'class' => 'required',
                ),
                'disable_inarray_validator' => true,
                'label' => 'Product',
//                'value_options' => array(
//                    0 => 'Please Select ... ',
//                ),
            ),
        ));
        $this->add(array(
            'name' => 'product_price',
            'attributes' => array(
                'required' => 'required',
                'id' => 'raw_price',
            ),
            'options' => array(
                'label_attributes' => array(
                    'class' => 'required',
                ),
                'label' => 'Net cost',
            ),
        ));
        $this->add(array(
            'type'       => 'Zend\Form\Element\Select',
            'name'       => 'discount_type',
            'attributes' => array(
                'id'       => 'discount_type',
                'required' => 'required',
            ),
            'options'    => array(
//                'disable_inarray_validator' => true,
                'label_attributes' => array(
                    'class' => 'required',
                ),
                'label'            => 'Discount type',
                'value_options'    => array(
                    'Direct Price Reduction'  => 'Direct Price Reduction',
                    '% of Price' => '% of Price',
                ),
            ),
        ));
        $this->add(array(
            'name' => 'discount',
            'attributes' => array(
                'type' => 'text',
                'id' => 'discount',
                'value' => 0,
            ),
            'options' => array(
                'label' => 'Discount',
            ),
        ));
        $this->add(array(
            'name' => 'qty',
            'attributes' => array(
                'type' => 'text',
                'id' => 'qty',
                'value' => 1,
            ),
            'options' => array(
                'label' => 'Quantity',
            ),
        ));
    }
}
