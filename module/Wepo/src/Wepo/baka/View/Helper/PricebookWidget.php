<?php

namespace Wepo\View\Helper;

use Wepo\Lib\WepoWidget;
use ModelFramework\AuthService\AuthService as Auth;

class PricebookWidget extends WepoWidget
{
    public function __invoke($owner_id, $permission)
    {
        if ($permission == Auth::ALL || $permission == Auth::OWN) {
            $fields  = array( 'owner_id' => $owner_id );
            $fields[ 'status_id' ] = array(1,2,3);
            $items   = $this->table->find($fields, ['pricebook' => 'asc'], 5);
            $results = null;
            foreach ($items as $i => $_item) {
                $results[ 'pricebook' ][ $_item->id ] = $_item;
            }

            return $this->getView()->partial('wepo/widget/pricebook', $results);
        } else {
            return;
        }
    }
}
