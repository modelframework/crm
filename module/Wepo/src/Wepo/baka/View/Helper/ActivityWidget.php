<?php

namespace Wepo\View\Helper;

use Wepo\Lib\WepoWidget;

class ActivityWidget extends WepoWidget
{
    public function __invoke($owner_id, $remind, $table_id = null, $target_id = null)
    {
        $fields = array( 'owner_id' => $owner_id );
        if ($remind) {
            $fields[ 'remind' ] = $remind;
        }
        if ($table_id) {
            $fields[ 'table_id' ] = $table_id;
        }
        if ($target_id) {
            $fields[ 'target_id' ] = $target_id;
        }
        $items = $this->table->find($fields, ['remind_dtm' => 'desc' ], 5);

        $results = [ ];
        foreach ($items as $i => $_item) {
            $results[ 'activity' ][ $_item->id ] = $_item;
            switch ($results[ 'activity' ][ $_item->id ]->type_id) {
                case '5295fdf7c5b9f222acd3c757':
                    $results[ 'activity' ][ $_item->id ]->type = 'Call';
                    break;
                case '5295fdf7c5b9f222acd3c758':
                    $results[ 'activity' ][ $_item->id ]->type = 'Task';
                    break;
                case '5295fdf7c5b9f222acd3c759':
                    $results[ 'activity' ][ $_item->id ]->type = 'Event';
                    break;
            }
        }

        return $this->getView()->partial('wepo/widget/activity', $results);
    }
}
