<?php

namespace Wepo\View\Helper;

use Wepo\Lib\WepoWidget;
use ModelFramework\AuthService\AuthService as Auth;
use Wepo\Model\Status;

class DocumentWidget extends WepoWidget
{
    public function __invoke($owner_id, $permission)
    {
        if ($permission == Auth::ALL || $permission == Auth::OWN) {
            $fields  = array( 'owner_id' => $owner_id );
            $fields['status_id'] = array(Status::NEW_, Status::NORMAL, Status::CONVERTED, Status::DEAD);
            $items   = $this->table->find($fields, ['document_name' => 'asc'], 5);
            $results = null;
            foreach ($items as $i => $_item) {
                $results[ 'document' ][ $_item->id() ] = $_item;
            }

            return $this->getView()->partial('wepo/widget/document', $results);
        } else {
            return;
        }
    }
}
