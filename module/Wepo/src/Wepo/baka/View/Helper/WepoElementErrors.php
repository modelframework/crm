<?php

namespace Wepo\View\Helper;

use Zend\Form\View\Helper\FormElementErrors;

class WepoElementErrors extends FormElementErrors
{
    //    <label for="" class="error">This field is required.</label>

    protected $messageCloseString = '</label>';
    protected $messageOpenFormat = '<label class="error" %s>';
    protected $messageSeparatorString = '. ';
}
