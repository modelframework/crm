<?php

namespace Wepo\Controller;

use Wepo\Lib\WepoController;
use Wepo\Model\Table;
use Wepo\Model\Status;
use ModelFramework\AuthService\AuthService as Auth;

class UserController extends WepoController
{
    public function indexAction()
    {
        return $this->redirect()->toRoute('user', array( 'action' => 'list' ));
    }

    protected function listAction()
    {
        return $this->getListing('User', [ 'status_id' => [ Status::NEW_, Status::NORMAL, Status::DEAD ] ]);
    }

    protected function recyclelistAction()
    {
        $results                         = $this->getListing('User', [ 'status_id' => [ Status::DELETED ] ]);
        $results[ 'params' ][ 'action' ] = 'recyclelist';

        return $results;
    }

    public function addAction()
    {
        $permission = $this->getPermission('data:User');
        $form       = $this->form('AdduserForm');
        $form->setRoute('user')->setAction('add')->setActionParams([ 'id' => null ]);
        if ($permission == Auth::ALL) {
            $form->getFieldsets()[ 'fields' ]->add(array(
                                                        'type'       => 'Zend\Form\Element\Select',
                                                        'name'       => 'role_id',
                                                        'attributes' => array(
                                                            'id' => 'usernames',
                                                        ),
                                                        'options'    => array(
                                                            'label'         => 'User\'s rights',
                                                            'value_options' => array(
                                                                '5295fdf7c5b9f222acd3c404' => 'user',
                                                                '5295fdf7c5b9f222acd3c406' => 'senior',
                                                                '5295fdf7c5b9f222acd3c405' => 'admin',
                                                            ),
                                                        ),
                                                    ));
        }

        return $this->add($form, 'User', $permission);
    }

    public function editAction()
    {
        $id         = (string) $this->params()->fromRoute('id', 0);
        $permission = $this->getPermission('data:User');
        if (!($permission == Auth::ALL || ($permission == Auth::OWN && $id == $this->user()->id()))) {
            return $this->showerror('You don\'t have access to this page.', $this->url()->fromRoute('dashboard'));
        }
        $form = $this->form('EdituserForm');
        $form->setRoute('user')->setAction('edit')->setActionParams([ 'id' => $id ]);

        if ($permission == Auth::ALL) {
            $form->getFieldsets()[ 'fields' ]->add(array(
                                                        'type'       => 'Zend\Form\Element\Select',
                                                        'name'       => 'role_id',
                                                        'attributes' => array(
                                                            'id' => 'usernames',
                                                        ),
                                                        'options'    => array(
                                                            'label'         => 'User\'s rights',
                                                            'value_options' => array(
                                                                '5295fdf7c5b9f222acd3c404' => 'user',
                                                                '5295fdf7c5b9f222acd3c406' => 'senior',
                                                                '5295fdf7c5b9f222acd3c405' => 'admin',
                                                            ),
                                                        ),
                                                    ));
        }

        return $this->edit($form, 'User', $permission);
    }

    public function deleteAction()
    {
        return $results = $this->recycle('User');
    }

    public function cleanAction()
    {
        return $results = $this->recycle('User');
    }

    public function restoreAction()
    {
        return $results = $this->recycle('User');
    }

    public function viewAction()
    {
        $modelname  = 'User';
        $permission = $this->getPermission('data:'.$modelname);
        $id         = (string) $this->params()->fromRoute('id', 0);
        $_atts      = array( '_id' => $id );
        if (!($permission == Auth::ALL || ($permission == Auth::OWN && $id == $this->user()->id()))) {
            return $this->showerror('You don\'t have access to this page.', $this->url()->fromRoute('dashboard'));
        }
        $model = $this->table($modelname)->gather($_atts)->current();
        if (!$model) {
            return $this->showerror('Data not found', $this->url()->fromRoute('dashboard'));
        }
        $results[ 'widgets' ]        = $this->widgets('userview', $model);
        $table_id                    = constant('\Wepo\Model\Table::'.strtoupper($modelname));
        $results[ 'user' ]           = $this->user();
        $results[ 'field_labels' ]   = $this->fields(array( 'table_id' => $table_id ))[ 'labels' ];
        $results[ 'model' ]          = $model;
        $results[ 'table_id' ]       = $table_id;
        $results[ 'tableHandler' ]   = new Table();
        $results[ 'params' ][ 'id' ] = $id;
        $results[ 'saurl' ]          = '?back='.$this->generateLabel();
        $results[ 'saurlback' ]      = $this->getSaurlBack($this->params()->fromQuery('back', 'home'));
        $results[ 'saurldoubleback' ] = '?back='.$this->params()->fromQuery('back');
        $results[ 'permission' ]     = $permission;
        $results[ 'widgets' ]        = $this->widgets('userview', $this->table('User')
                                                                        ->gather(array( '_id' => (string) $this->params()
                                                                                                                ->fromRoute('id',
                                                                                                                             0),
                                                                                  ))->current());

        return $results;
    }

    public function rowscountAction()
    {
        $results = $this->RowsCount(Table::USER);

        return $this->redirect()->toRoute('user', $results);
    }
}
