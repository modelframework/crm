<?php

namespace Wepo\Controller;

use Wepo\Lib\WepoController;
use Wepo\Model\Table;
use Wepo\Model\Status;
use Wepo\Lib\AuthService as Auth;

class DoctorController extends WepoController
{
    public function indexAction()
    {
        return $this->redirect()->toRoute('doctor', array( 'action' => 'list' ));
    }

    protected function listAction()
    {
        prn('i need a dokhtur ');

        return $this->getListing('Doctor', [
            'status_id' => [
                Status::NEW_, Status::NORMAL,
            ]
        ]);
    }

    protected function recyclelistAction()
    {
        $results                         = $this->getListing('Doctor', [ 'status_id' => [ Status::DELETED ] ]);
        $results[ 'params' ][ 'action' ] = 'recyclelist';

        return $results;
    }

    public function deleteAction()
    {
        return $results = $this->recycle('Doctor');
    }

    public function cleanAction()
    {
        return $results = $this->recycle('Doctor');
    }

    public function restoreAction()
    {
        return $results = $this->recycle('Doctor');
    }

    public function addAction()
    {
        $permission = $this->getPermission('data:Doctor');
        $form       = $this->form('DoctorForm');
        $form->setRoute('doctor')->setAction('add')->setActionParams([ 'id' => null ]);
        if ($permission == Auth::ALL) {
            $form->getFieldsets()[ 'fields' ]->add(array(
                                                        'type'       => 'Zend\Form\Element\Select',
                                                        'name'       => 'owner_id',
                                                        'attributes' => array(
                                                            'id'       => 'iduser',
                                                            'disabled' => '',
                                                            'value'    => $this->user()->id(),
                                                        ),
                                                        'options'    => array(
                                                            'label'         => 'Owner',
                                                            'value_options' => $this->trueTableHash('User', '_id',
                                                                    ['fname', 'lname'], [
                                                                        'status_id' => [
                                                                            Status::NEW_, Status::NORMAL,
                                                                        ]
                                                                    ]),
                                                        ),
                                                    ));
            $form->addValidationField('fields', 'owner_id');
        }

        return $this->add($form, 'Doctor', $permission);
    }

    public function editAction()
    {
        $permission = $this->getPermission('data:Doctor');
        $form       = $this->form('DoctorForm');
        $form->setRoute('doctor')->setAction('edit')->setActionParams([
                                                                             'id' => (string) $this->params()
                                                                                                   ->fromRoute('id',
                                                                                                                0),
                                                                         ]);
        if ($permission == Auth::ALL) {
            $form->getFieldsets()[ 'fields' ]->add(array(
                                                        'type'       => 'Zend\Form\Element\Select',
                                                        'name'       => 'owner_id',
                                                        'attributes' => array(
                                                            'id'       => 'iduser',
                                                            'disabled' => '',
                                                        ),
                                                        'options'    => array(
                                                            'label'         => 'Owner',
                                                            'value_options' => $this->trueTableHash('User', '_id',
                                                                    ['fname', 'lname'], [
                                                                        'status_id' => [
                                                                            Status::NEW_, Status::NORMAL,
                                                                        ]
                                                                    ]),
                                                        ),
                                                    ));
            $form->addValidationField('fields', 'owner_id');
        }

        return $this->edit($form, 'Doctor', $permission);
    }

    public function viewAction()
    {
        return $this->view('Doctor');
    }

    public function rowscountAction()
    {
        $results = $this->RowsCount(Table::DOCTOR);

        return $this->redirect()->toUrl($results['saurlback']);
    }
}
