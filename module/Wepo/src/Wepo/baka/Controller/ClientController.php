<?php

namespace Wepo\Controller;

use Wepo\Lib\WepoController;
use Wepo\Model\Table;
use Wepo\Model\Status;
use ModelFramework\AuthService\AuthService as Auth;

class ClientController extends WepoController
{
    protected $_datasource = 'data:Client';

    public function indexAction()
    {
        return $this->redirect()->toRoute('client', array( 'action' => 'list' ));
    }

    protected function listAction()
    {
        return $this->getListing('Client', [
            'status_id' => [
                Status::NEW_, Status::NORMAL, Status::CONVERTED, Status::DEAD,
            ]
        ]);
    }

    protected function recyclelistAction()
    {
        $results = $this->getListing('Client', [ 'status_id' => [ Status::DELETED ] ]);
        $results[ 'params' ][ 'action' ] = 'recyclelist';

        return $results;
    }

    public function deleteAction()
    {
        return $results = $this->recycle('Client');
    }

    public function cleanAction()
    {
        return $results = $this->recycle('Client');
    }

    public function restoreAction()
    {
        return $results = $this->recycle('Client');
    }

    public function addAction()
    {
        $permission = $this->getPermission('data:Client');
        $form       = $this->form('ClientForm');
        $form->setRoute('client')->setAction('add')->setActionParams([ 'id' => null ]);
        if ($permission == Auth::ALL) {
            $form->getFieldsets()[ 'fields' ]->add(array(
                'type'       => 'Zend\Form\Element\Select',
                'name'       => 'owner_id',
                'attributes' => array(
                    'id'       => 'iduser',
                    'disabled' => '',
                    'value'         => $this->user()->id(),
                ),
                'options'    => array(
                    'label'         => 'Owner',
                    'value_options' => $this->trueTableHash('User', '_id', ['fname', 'lname'], ['status_id' => [  Status::NEW_,  Status::NORMAL]]),
                ),
            ));
            $form->addValidationField('fields', 'owner_id');
        }

        return $this->add($form, 'Client', $permission);
    }

    public function editAction()
    {
        $permission = $this->getPermission('data:Client');
        $form       = $this->form('ClientForm');
        $form->setRoute('client')->setAction('edit')->setActionParams([ 'id' => (string) $this->params()->fromRoute('id', 0) ]);
        if ($permission == Auth::ALL) {
            $form->getFieldsets()[ 'fields' ]->add(array(
                'type'       => 'Zend\Form\Element\Select',
                'name'       => 'owner_id',
                'attributes' => array(
                    'id'       => 'iduser',
                    'disabled' => '',
                ),
                'options'    => array(
                    'label'         => 'Owner',
                    'value_options' => $this->trueTableHash('User', '_id', ['fname', 'lname'], ['status_id' => [  Status::NEW_,  Status::NORMAL]]),
                ),
            ));
            $form->addValidationField('fields', 'owner_id');
        }

        return $this->edit($form, 'Client', $permission);
    }

    public function viewAction()
    {
        return $this->view('Client');
    }

    public function rowscountAction()
    {
        $results = $this->RowsCount(Table::CLIENT);
//        return $this -> redirect() -> toRoute( 'client', $results );

        return $this->redirect()->toUrl($results['saurlback']);
    }
}
