<?php

namespace Wepo\Controller;

use Wepo\Lib\WepoController;
use Wepo\Model\Table;

class DashboardController extends WepoController
{
    public function indexAction()
    {
        $results[ 'permission' ] = 1;
        $results[ 'saurl' ]      = '?back='.'dashboard';
        $results[ 'user' ]       = $this->user();

        $results[ 'widgets' ]    = $this->widgets('Dashboard', $this->user());
        $results[ 'tableHandler' ] = new Table();
        $this->trigger('dashboard');
//      prn($results);

        return $results;
    }

    public function testAction()
    {
        foreach ($this->table('Document')->fetchAll() as $_k => $document) {
            prn($document, $document->file_size);
//          $this->table( 'Document' )->save( $document );
        }
    }

    public function test1Action()
    {
        $configmodels = [ ];

        foreach ($this->table('ConfigModel')->fetchAll() as $_k => $modelconfig) {
            if ($modelconfig->model == 'ModelConfiguration') {
                continue;
            }
            if ($modelconfig->adapter == 'wepo_company' && empty($modelconfig->fields[ '_id' ])) {
                $modelconfig->fields[ '_id' ] = [ 'type' => 'pk', 'datatype' => 'string', 'default' => '' ];
                $modelconfig->fields[ 'id' ]  = [ 'type' => 'field', 'datatype' => 'int', 'default' => 0 ];
                $this->table('ConfigModel')->save($modelconfig);
                prn('config for '.$modelconfig->model.'patched ');
            }

            $modelconfig->_model = $this->model($modelconfig->model);
            $configmodels[ ]     = $modelconfig;
        }

        if (isset($_GET[ 'm' ]) && isset($configmodels[ $_GET[ 'm' ] ])) {
            $modelconfig = $configmodels[ $_GET[ 'm' ] ];
            prn($modelconfig->model, $modelconfig);
            $gw = $this->table($modelconfig->model);
            foreach ($gw->fetchAll() as $_m => $mymodel) {
                $oldmodel            = clone $mymodel;
                $oldmodel->_original = true;
                foreach ($modelconfig->joins as $_k => $join) {
                    $othergw = $this->table($join[ 'model' ]);

//                    prn( $join, $othergw );
                    foreach ($join[ 'on' ] as $myfield => $otherfield) {
                        if ($mymodel->$myfield instanceof \MongoId) {
                            $otherfield = '_id';
                        }

                        $othermodel = $othergw->find([ $otherfield => $mymodel->$myfield ])->current();

                        if ($othermodel !== null) {
                            $mymodel->$myfield = $othermodel->_id;

                            foreach ($join[ 'fields' ] as $myfield => $otherfield) {
                                $mymodel->$myfield = $othermodel->$otherfield;
                            }
                            $oldmodel->_original = false;
                        }
                    }
                }
                if (!$oldmodel->_original) {
                    prn([ 'was' => $oldmodel, 'become' => $mymodel ]);
                    $gw->save($mymodel);
                }
            }
        }

        return [ 'models' => $configmodels ];
    }

    public function test2Action()
    {
        $transport1 = $this->table('Field');
        $transport2 = $this->table('Table');
        foreach ($transport1->fetchAll() as $model1) {
            $old_model1 = clone $model1;
            if (!$model1->table_id instanceof \MongoId) {
                $model2 = $transport2->find([ 'id' => $model1->table_id ])->current();
                if ($model2 != null) {
                    $model1->table_id = $model2->_id;
                }
            }
//            else
//            {
//                $owner = $us -> find( [ '_id' => $user -> owner_id ] ) -> current();
//                if ( $owner != null )
//                {
//                    $user -> owner = $owner -> fname;
//                }
//            }
            prn('old', $old_model1, 'new', $model1);
//            $transport1 -> save( $model1 );
        }
        exit();
    }

    public function test3Action()
    {
        $process = curl_init("http://192.168.10.46/pydio/api/default/ls/node+?dir=.");
        $username = "admin";
        $password = "123456";
        curl_setopt($process, CURLOPT_HEADER, 0);
        curl_setopt($process, CURLOPT_HTTPHEADER, array('Content-Type: application/xml', ""));
        curl_setopt($process, CURLOPT_USERPWD, $username.":".$password);
        curl_setopt($process, CURLOPT_RETURNTRANSFER, true);

        $return = curl_exec($process);
        curl_close($process);

        prn($return);
        $xml = simplexml_load_string($return);
        $tree = $xml->children();

        $children = $tree->children();

        foreach ($tree->tree as $file) {
            $file = get_object_vars($file);
            $attr = $file["@attributes"];
            $text = $attr["text"];
            print_r($text);
        }
    }

    public function test31Action()
    {
    }

    public function test4Action()
    {
        //        $aws = $this -> getServiceLocator()->  get('aws');
//        $client = $aws->get('S3');
//        $result = $client->listBuckets();
//        return $result;
    }

    public function formsAction()
    {
        $results  = [ ];
        $fclasses = [ ];
        foreach (glob("./module/Wepo/src/Wepo/Form/*[Form|Fieldset].php") as $filename) {
            $fname       = substr($filename, 28, -4);
            $fclasses[ ] = [ 'file' => $filename, 'class' => $fname ];
        }
        if (isset($_GET[ 'c' ]) && isset($fclasses[ $_GET[ 'c' ] ])) {
            $c        = (int) $_GET[ 'c' ];
            $fc       = $fclasses[ $c ];
            $filename = $fc[ 'file' ];
            $fname    = $fc[ 'class' ];
            $s        = "\\Wepo\\Form\\$fname";
            prn(class_exists("\\Wepo\\Form\\AdduserForm1"));
            $f = new $s();
            prn($f, $f->getConfig());
            prn(class_exists($s));
        }
        $results[ 'fclasses' ] = $fclasses;

        return $results;
    }
}
