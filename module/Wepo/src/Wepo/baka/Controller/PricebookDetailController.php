<?php

namespace Wepo\Controller;

use Wepo\Lib\WepoController;
use Wepo\Model\Table;
use Wepo\Model\Status;
use Zend\InputFilter\Factory as InputFactory;
use Zend\InputFilter\InputFilter;
use Wepo\Lib\AuthService as Auth;
use Zend\View\Model\JsonModel;

class PricebookDetailController extends WepoController
{
    public function indexAction()
    {
        return $this->redirect()->toRoute('pricebookdetail', array( 'action' => 'list' ));
    }

    protected function listAction()
    {
        $pricebookid = (string) $this->params()->fromRoute('pricebookid', 0);

        if ($pricebookid == 0) {
            return $this->redirect()->toRoute('pricebook');
        }
//        if ( $this -> getRequest() -> isXmlHttpRequest() )
//        {
//            $results = $this -> table( 'PricebookDetail' ) -> find( ['pricebook_id' => $pricebookid ] );
//            $results = new \Zend\View\Model\JsonModel($results);
//            $results -> setTemplate( '/' );
//            return $results;
//        }
        $results                              =
            $this->getListing('PricebookDetail', [ 'pricebook_id' => $pricebookid ]);
        $results[ 'params' ][ 'pricebookid' ] = $pricebookid;
        $results[ 'field_labels' ]            = $this->fields(array( 'table_id' => Table::PRICEBOOK ))[ 'labels' ];
        $results[ 'master' ]                  =
            $this->table('Pricebook')->findOne([ '_id' => $pricebookid ]);
        $results[ 'masterRoute' ]             = 'pricebook';
        $results[ 'details' ]                 = 'Product';
        $results[ 'currentRoute' ]            =
            $this->url()->fromRoute($results[ 'modelname' ], [ 'action' => 'list', 'pricebookid' => $pricebookid ]);
//        prn( $results, $results[ 'master' ]->getModelName() );

//        prn( $results['paginator']->getCurrentItems()->toArray());
        return $results;
    }

    public function addAction()
    {
        $permission  = $this->getPermission('data:PricebookDetail');
        $pricebookid = (string) $this->params()->fromRoute('pricebookid', 0);
        $form        = $this->form('PricebookDetailForm');
        $form->setRoute('pricebookdetail')->setAction('add')->setActionParams([
                                                                                       '_id'         => null,
                                                                                       'pricebookid' => $pricebookid,
                                                                                   ]);

        $pricebooks = $this->table('PricebookDetail')->gather([ 'pricebook_id' => $pricebookid ]);

        $hash                     =
            $this->trueTableHash('Product', '_id', 'name', [ 'status_id' => [ Status::NEW_, Status::NORMAL ] ]);
        $excludeLabelProductName  = array();
        $excludeLabelProductPrice = array();
        foreach ($pricebooks as $_pricebook) {
            $excludeLabelProductName[ ]  = (string) $_pricebook->product_id;
            $excludeLabelProductPrice[ ] = $_pricebook->product_price;
        }
        $price = $this->trueTableHash('Product', '_id', 'price', [ 'status_id' => [ Status::NEW_, Status::NORMAL ] ]);
        foreach ($hash as $_key => $_value) {
            foreach ($excludeLabelProductName as $_name) {
                if ($_key == $_name) {
                    unset($hash[ $_key ], $price[ $_key ]);
                }
            }
        }
        $form->getFieldsets()[ 'fields' ]->get('product_id')->setOptions(
                                         array(
                                             'label'         => 'Product',
                                             'value_options' => $hash,
                                         )
        );

        if ($this->getRequest()->isPost()) {
            $factory        = new InputFactory();
            $addInputFilter = new InputFilter();
            if ($this->getRequest()->getPost()->fields[ 'discount_type' ] == 'Direct Price Reduction') {
                $addInputFilter->add($factory->createInput(array(
                                                                 'name'       => 'discount',
                                                                 'required'   => true,
                                                                 'filters'    => array(
                                                                     array( 'name' => 'StripTags' ),
                                                                     array( 'name' => 'StringTrim' ),
                                                                 ),
                                                                 'validators' => array(
                                                                     [
                                                                         "name"    => "Regex",
                                                                         "options" => [
                                                                             "pattern"  => "/^([1-9]\\d*|0)((\\.)\\d{1,2})?$/",
                                                                             "messages" => [
                                                                                 "regexNotMatch" => "Input valid price",
                                                                             ],
                                                                         ],
                                                                     ],
                                                                 ),
                                                             )));
            } else {
                $addInputFilter->add($factory->createInput(array(
                                                                 'name'       => 'discount',
                                                                 'required'   => true,
                                                                 'filters'    => array(
                                                                     array( 'name' => 'StripTags' ),
                                                                     array( 'name' => 'StringTrim' ),
                                                                 ),
                                                                 'validators' => array(
                                                                     [
                                                                         'name'    => 'GreaterThan',
                                                                         'options' => array(
                                                                             'min'       => 0,
                                                                             'inclusive' => true,
                                                                             'messages'  => array( \Zend\Validator\GreaterThan::NOT_GREATER_INCLUSIVE => 'Input valid modifier' ),
                                                                         ),
                                                                     ],
                                                                 ),
                                                             )));
            }
            $form->addInputFilter($addInputFilter, 'fields');
        }

        $results = $this->add($form, 'PricebookDetail', $permission);
        if (is_array($results)) {
            $results[ 'pricebook_id' ] = $pricebookid;
            $results[ 'prices' ]       = $price;
            $back                      = $this->params()->fromQuery('back', $this->getRequest()->getPost('saurl',
                                                                                                           [ 'back' => 'home' ])[ 'back' ]);
            $form->getFieldsets()[ 'saurl' ]->get('back')->setValue($back);
            $results[ 'saurl' ]     = '?back='.$back;
            $results[ 'saurlback' ] = $this->getSaurlBack($back);
        } elseif ($results->getVariable('status')) {
            $redirect_action = $this->getRequest()->getPost()['redirect_action'];
            if ($redirect_action == 'save') {
                $results->setVariable('uri', $this->url()->fromRoute('pricebookdetail', [ 'action' => 'list', 'pricebookid' => $pricebookid ]));
            } elseif ($redirect_action == 'save_and_new') {
                $results->setVariable('uri', $this->url()->fromRoute('pricebookdetail', [ 'action' => 'add', 'pricebookid' => $pricebookid ]));
            }
        }

        return $results;
    }

    public function deleteAction()
    {
        return $results = $this->recycle('PricebookDetail');
    }

    public function rowscountAction()
    {
        $results = $this->RowsCount(Table::PRICEBOOK_DETAIL);

        return $this->redirect()->toUrl($results[ 'saurlback' ]);
    }

    public function editAction()
    {
        $permission  = $this->getPermission('data:PricebookDetail');
        $pricebookid = (string) $this->params()->fromRoute('pricebookid', 0);
        $id          = (string) $this->params()->fromRoute('id', 0);
        $form        = $this->form('EditPricebookDetailForm');
        $form->setRoute('pricebookdetail')->setAction('edit')->setActionParams([
                                                                                        'id'          => $id,
                                                                                        'pricebookid' => $pricebookid,
                                                                                    ]);
        $results = $this->edit($form, 'PricebookDetail', $permission);
        if (is_array($results)) {
            $pricebook                 = $this->table('PricebookDetail')->gather([ '_id' => $id ])->current();
            $results[ 'name' ]         = $pricebook->product;
            $results[ 'price' ]        = $pricebook->product_price;
            $results[ 'pricebook_id' ] = $pricebookid;
            $results[ 'final_price' ] = $pricebook->final_price;
            $back                      = $this->params()->fromQuery('back', $this->getRequest()->getPost('saurl',
                                                                                                           [ 'back' => 'home' ])[ 'back' ]);
            $form->getFieldsets()[ 'saurl' ]->get('back')->setValue($back);
            $results[ 'saurl' ]     = '?back='.$back;
//            $results[ 'saurlback' ] = $this->getSaurlBack( $back );
            $results['saurlback'] = $this->url()->fromRoute('pricebookdetail', [ 'action' => 'view', 'pricebookid' => $pricebookid, 'id' => $id ]);
        } elseif ($results->getVariable('status')) {
            $redirect_action = $this->getRequest()->getPost()['redirect_action'];
            if ($redirect_action == 'save') {
                $results->setVariable('uri', $this->url()->fromRoute('pricebookdetail', [ 'action' => 'list', 'pricebookid' => $pricebookid ]));
            } elseif ($redirect_action == 'save_and_new') {
                $results->setVariable('uri', $this->url()->fromRoute('pricebookdetail', [ 'action' => 'add', 'pricebookid' => $pricebookid ]));
            }
        }

        return $results;
    }

    public function viewAction()
    {
        $modelname    = 'PricebookDetail';
        $tableId              = Table:: getTableId($modelname);
        $permission   = $this->getPermission('data:'.$modelname);
        $id           = (string) $this->params()->fromRoute('id', 0);
        $pricebook_id = (string) $this->params()->fromRoute('pricebookid', 0);
        $_atts        = array( '_id' => $id, 'pricebook_id' => $pricebook_id );
//        if ( $permission == Auth::OWN )//add owner_id and uncommite
//        {
//            $_atts[ 'owner_id' ] = $this -> user() -> id();
//        }
        $model = $this->table($modelname)->gather($_atts)->current();
        if (!$model) {
            return $this->showerror('Data not found', $this->url()->fromRoute('dashboard'));
        }
//        $results = parent::view( 'PricebookDetail' );
        $results[ 'field_labels' ]   = $this->fields(array( 'table_id' => $tableId, 'target' => 'list' ))[ 'labels' ];
        $results[ 'model' ]          = $model;
        $results[ 'table_id' ]       = $tableId;
        $results[ 'params' ][ 'id' ] = $id;
        $results[ 'saurl' ]          = '?back='.$this->params()->fromQuery('back', 'home');
        $results[ 'saurlback' ]      = $this->getSaurlBack($this->params()->fromQuery('back', 'home'));
        $results[ 'user' ]           = $this->user();

        return $results;
    }
}
