<?php

namespace Wepo\Controller;

use Wepo\Lib\WepoController;
use Wepo\Model\Table;

class LeadController extends WepoController
{
    public function indexAction()
    {
        return $this->redirect()->toRoute('lead', array( 'action' => 'list' ));
    }

    public function listAction()
    {
        $modelView = $this->getModelViewServiceVerify()->get('Lead', 'list');
        $modelView->setParams($this->params());
        $modelView->process();
        $result = $modelView->getData();

        return $result;
    }

    public function recyclelistAction()
    {
        $modelView = $this->getModelViewServiceVerify()->get('Lead', 'recyclelist');
        $modelView->setParams($this->params());
        $modelView->process();
        $result = $modelView->getData();

        return $result;
    }

    public function deleteAction()
    {
        $modelView = $this->getModelViewServiceVerify()->get('Lead', 'delete');
        $modelView->setParams($this->params());
        $modelView->process();
        if ($modelView->hasRedirect()) {
            return $modelView->getRedirect();
        }
        $result = $modelView->getData();

        return $result;
    }

    public function cleanAction()
    {
        $modelView = $this->getModelViewServiceVerify()->get('Lead', 'clean');
        $modelView->setParams($this->params());
        $modelView->process();
        if ($modelView->hasRedirect()) {
            return $modelView->getRedirect();
        }
        $result = $modelView->getData();

        return $result;
    }

    public function restoreAction()
    {
        $modelView = $this->getModelViewServiceVerify()->get('Lead', 'restore');
        $modelView->setParams($this->params());
        $modelView->process();
        if ($modelView->hasRedirect()) {
            return $modelView->getRedirect();
        }
        $result = $modelView->getData();

        return $result;
    }

    public function addAction()
    {
        $modelView = $this->getModelViewServiceVerify()->get('Lead', 'add');
        $modelView->setParams($this->params());
        $modelView->process();
        if ($modelView->hasRedirect()) {
            return $modelView->getRedirect();
        }
        $result = $modelView->getData();

        return $result;
    }

    public function editAction()
    {
        $modelView = $this->getModelViewServiceVerify()->get('Lead', 'edit');
        $modelView->setParams($this->params());
        $modelView->process();
        if ($modelView->hasRedirect()) {
            return $modelView->getRedirect();
        }
        $result = $modelView->getData();

        return $result;
    }

    public function convertAction()
    {
        $modelView = $this->getModelViewServiceVerify()->get('Lead', 'convert');
        $modelView->setParams($this->params());
        $modelView->process();
        if ($modelView->hasRedirect()) {
            return $modelView->getRedirect();
        }
        $result = $modelView->getData();

        return $result;
    }

    public function viewAction()
    {
        $modelView = $this->getModelViewServiceVerify()->get('Lead', 'view');
        $modelView->setParams($this->params());
        $modelView->process();
        $result = $modelView->getData();

        return $result;
    }

    public function rowscountAction()
    {
        $results = $this->RowsCount(Table::LEAD);

        return $this->redirect()->toRoute('lead', $results);
    }
}
