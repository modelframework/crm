<?php

namespace Wepo\Controller;

use Wepo\Lib\WepoController;
use Wepo\Model\Table;
use Wepo\Model\Status;
use Wepo\Lib\AuthService as Auth;
use Zend\EventManager\EventManagerInterface;

class QuoteController extends WepoController
{
    public function setEventManager(EventManagerInterface $eventManager)
    {
        $dm = $this->getServiceLocator()->get('Wepo\Lib\DataMaster');

        return parent::setEventManager($eventManager);
    }

    public function indexAction()
    {
        return $this->redirect()->toRoute('quote', array( 'action' => 'list' ));
    }

    protected function listAction()
    {
        return $this->getListing('Quote');
    }

    public function addAction()
    {
        $permission = $this->getPermission('data:Quote');
        $form       = $this->form('QuoteForm');
        $form->setRoute('quote')->setAction('add')->setActionParams([ 'id' => null ]);
        if ($permission == Auth::ALL) {
            $form->getFieldsets()[ 'fields' ]->add(array(
                                                        'type'       => 'Zend\Form\Element\Select',
                                                        'name'       => 'owner_id',
                                                        'attributes' => array(
                                                            'id'       => 'iduser',
                                                            'disabled' => '',
                                                            'value'    => $this->user()->id(),
                                                        ),
                                                        'options'    => array(
                                                            'label'         => 'Owner',
                                                            'value_options' => $this->trueTableHash('User', '_id',
                                                                                                     [
                                                                                                         'fname',
                                                                                                         'lname'
                                                                                                     ], [
                                                                                                         'status_id' => [
                                                                                                             Status::NEW_,
                                                                                                             Status::NORMAL,
                                                                                                         ]
                                                                                                     ]),
                                                        ),
                                                    ));
            $form->addValidationField('fields', 'owner_id');
        }
        $form->getFieldsets()[ 'fields' ]->get('contact_id')->setOptions(
                                         array(
                                             'value_options' => $this->trueTableHash('Contact', '_id',
                                                                                      [ 'fname', 'lname' ], [
                                                         'status_id' => [
                                                             Status::NEW_, Status::NORMAL,
                                                         ]
                                                     ]),
                                         )
        );
        $form->getFieldsets()[ 'fields' ]->get('contact_id')->setValue($this->params()
                                                                               ->fromRoute('contact_id', null) ?:
                                                                              "0");
        $pricebookHash = $this->trueTableHash('Pricebook', '_id', 'pricebook', ['-status_id' => Status::DELETED]);

        $form->getFieldsets()[ 'fields' ]->get('pricebook_id')->setOptions(
            array(
                'value_options' => $pricebookHash,
            )
        );

        $results = $this->add($form, 'Quote', $permission);

        if (!is_array($results) && $results->getVariable('status')) {
            $redirect_action = $this->getRequest()->getPost()['redirect_action'];
            if ($redirect_action == 'save') {
                $results->setVariable('uri', $this->url()->fromRoute('quotedetail', [ 'action' => 'list', 'quoteid' => $results->getVariable('id') ]));
            }
        }

        return $results;
    }

    public function editAction()
    {
        $permission = $this->getPermission('data:Quote');
        $form       = $this->form('QuoteForm');
        $form->setRoute('quote')->setAction('edit')->setActionParams([
                                                                              'id' => (string) $this->params()
                                                                                                    ->fromRoute('id',
                                                                                                                 0),
                                                                          ]);
        if ($permission == Auth::ALL) {
            $form->getFieldsets()[ 'fields' ]->add(array(
                                                        'type'       => 'Zend\Form\Element\Select',
                                                        'name'       => 'owner_id',
                                                        'attributes' => array(
                                                            'id'       => 'iduser',
                                                            'disabled' => '',
                                                        ),
                                                        'options'    => array(
                                                            'label'         => 'Owner',
                                                            'value_options' => $this->trueTableHash('User', '_id',
                                                                                                     [
                                                                                                         'fname',
                                                                                                         'lname'
                                                                                                     ], [
                                                                                                         'status_id' => [
                                                                                                             Status::NEW_,
                                                                                                             Status::NORMAL,
                                                                                                         ]
                                                                                                     ]),
                                                        ),
                                                    ));
            $form->addValidationField('fields', 'owner_id');
        }
        $form->getFieldsets()[ 'fields' ]->get('pricebook_id')->setOptions(
            array(
                'value_options' => $this->trueTableHash('Pricebook', '_id', 'pricebook', ['-status_id' => Status::DELETED]),
            )
        );
        $form->getFieldsets()[ 'fields' ]->get('contact_id')->setOptions(
                                         array(
                                             'value_options' => $this->trueTableHash('Contact', '_id',
                                                                                      [ 'fname', 'lname' ], [
                                                         'status_id' => [
                                                             Status::NEW_, Status::NORMAL,
                                                         ]
                                                     ]),
                                         )
        );

//        $form->setAttribute('class','validate');
        $results = $this->edit($form, 'Quote', $permission);
//        exit;


        if (!is_array($results) && $results->getVariable('status')) {
            $redirect_action = $this->getRequest()->getPost()['redirect_action'];
            if ($redirect_action == 'save') {
                $results->setVariable('uri', $this->url()->fromRoute('quotedetail', [ 'action' => 'list', 'quoteid' => $results->getVariable('id') ]));
            }
        }

        return $results;
    }

    public function deleteAction()
    {
        return $this->recycle('Quote');
    }

    public function convertAction()
    {
        $permission = $this->getPermission('data:Order');
        $form       = $this->form('OrderForm');
        $form->setRoute('order')->setAction('add')->setActionParams([ 'id' => null ]);
        if ($permission == Auth::ALL) {
            $form->getFieldsets()[ 'fields' ]->add(array(
                'type'       => 'Zend\Form\Element\Select',
                'name'       => 'owner_id',
                'attributes' => array(
                    'id'       => 'iduser',
                    'disabled' => '',
                    'value'    => $this->user()->id(),
                ),
                'options'    => array(
                    'label'         => 'Owner',
                    'value_options' => $this->trueTableHash('User', '_id',
                            [
                                'fname',
                                'lname'
                            ], [
                                'status_id' => [
                                    Status::NEW_,
                                    Status::NORMAL,
                                ]
                            ]),
                ),
            ));
            $form->addValidationField('fields', 'owner_id');
        }

        $form->getFieldsets()[ 'fields' ]->get('pricebook_id')->setOptions(
            array(
                'value_options' => $this->trueTableHash('Pricebook', '_id', 'pricebook', ['-status_id' => Status::DELETED]),
            )
        );
        $form->getFieldsets()[ 'fields' ]->get('contact_id')->setOptions(
            array(
                'value_options' => $this->trueTableHash('Contact', '_id',
                        [ 'fname', 'lname' ], [
                            'status_id' => [
                                Status::NEW_, Status::NORMAL,
                            ]
                        ]),
            )
        );
        $form->setAttribute('class', 'validate');
        $results = $this->convert($form, 'Quote', 'Order', $permission);

        return $results;
    }

//    public function orderAction()
//    {
//        $request = $this->getRequest();
//        if ( $request->isPost() )
//        {
//            $id    = (string) $request->getPost( 'id', 0 );
//            $quote = $this->table( 'Quote' )->get( $id );
//            $this->trigger( 'convert', $quote );
//            $order_id = $this->table( 'Order' )->find( [ 'quote_id' => $id ], ['created_dtm'=>'desc'] )->current()->id();
//
//            return $this->refresh( 'Quote' . ' data was successfully converted', $this->url()->fromRoute( 'orderdetail',
//                                                                                                          array(
//                                                                                                              'action'  => 'list',
//                                                                                                              'orderid' => $order_id
//                                                                                                          ) ) );
//        }
//        $modelname  = 'Quote';
//        $permission = $this->getPermission( 'data:' . $modelname );
//        $id         = (string) $this->params()->fromRoute( 'id', 0 );
//        $_atts      = array( '_id' => $id );
//        if ( $permission == Auth::OWN )
//        {
//            $_atts[ 'owner_id' ] = $this->user()->id();
//        }
//        $model = $this->table( $modelname )->gather( $_atts )->current();
//        if ( !$model )
//        {
//            return $this->showerror( 'Data not found', $this->url()->fromRoute( 'dashboard' ) );
//        }
//        $results[ 'details' ] = $this->table( 'QuoteDetail' )->gather( array( 'quote_id' => $id ) );
//        $results[ 'details' ]->buffer();
//        $results[ 'detail_labels' ]  = $this->fields( array( 'table_id' => Table::QUOTE_DETAIL ) )[ 'labels' ];
//        $results[ 'field_labels' ]   = $this->fields( array( 'table_id' => Table::QUOTE ) )[ 'labels' ];
//        $results[ 'model' ]          = $model;
//        $results[ 'table_id' ]       = Table::QUOTE;
//        $results[ 'params' ][ 'id' ] = $id;
//        $results[ 'saurl' ]          = '?back=' . $this->generateLabel();
//        $results[ 'saurlback' ]      = $this->getSaurlBack( $this->params()->fromQuery( 'back', 'home' ) );
//        $results[ 'user' ]           = $this->user();
//
//        return $results;
//    }

    public function rowscountAction()
    {
        $results = $this->RowsCount(Table::QUOTE);

//        return $this->redirect()->toRoute( 'quote', $this->RowsCount( Table::QUOTE ) );

        return $this->redirect()->toUrl($results[ 'saurlback' ]);
    }

    public function viewAction()
    {
        return $this->redirect()->toRoute('quotedetail', array(
            'action' => 'list', 'quoteid' => $this->params()->fromRoute('id', 0),
        ));
    }
}
