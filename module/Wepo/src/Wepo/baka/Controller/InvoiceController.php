<?php

namespace Wepo\Controller;

use Wepo\Lib\WepoController;
use Wepo\Model\Table;
use Wepo\Model\Status;
use Zend\EventManager\EventManagerInterface;
use Wepo\Lib\AuthService as Auth;

class InvoiceController extends WepoController
{
    //    public function setEventManager( EventManagerInterface $eventManager )
//    {
//
//        $dm = $this -> getServiceLocator() -> get( 'Wepo\Lib\DataMaster' );
//        $eventManager -> attach( 'payment', array( $dm, 'dispatch' ), 100 );
//        return parent::setEventManager( $eventManager );
//    }

    public function indexAction()
    {
        return $this->redirect()->toRoute('invoice', array( 'action' => 'list' ));
    }

    protected function listAction()
    {
        return $this->getListing('Invoice');
    }

    public function deleteAction()
    {
        return $this->recycle('Invoice');
    }

    public function convertAction()
    {
        $modelname  = 'Invoice';
        $permission = $this->getPermission('data:'.$modelname);

        $settings = [
            'FromModel' => [
                'model' => 'Invoice',
                'table_id' => Table::INVOICE,
            ],
            'ToModel' => [
                'model' => 'Payment',
                'link' => 'invoice_id',
            ],
            'Detail' => [
                'model' => 'InvoiceDetail',
                'link' => 'invoice_id',
                'table_id' => Table::INVOICE_DETAIL,
            ],
        ];

        return $this->transfer($settings, $permission);
    }

    public function viewAction()
    {
        $modelname  = 'Invoice';
        $permission = $this->getPermission('data:'.$modelname);
        $id         = (string) $this->params()->fromRoute('id', 0);
        $_atts      = array( '_id' => $id );
        if ($permission == Auth::OWN) {
            $_atts[ 'owner_id' ] = $this->user()->id();
        }
        $model = $this->table($modelname)->gather($_atts)->current();
//        $this->trigger('presave',$model);
//        $this->table('Invoice')->save($model);
        if (!$model) {
            return $this->showerror('Data not found', $this->url()->fromRoute('dashboard'));
        }
        $results[ 'details' ]        = $this->table('InvoiceDetail')->gather(array( 'invoice_id' => $id ));
        $results[ 'details' ]->buffer();
        $results[ 'detail_labels' ]  = $this->fields(array( 'table_id' => Table::INVOICE_DETAIL, 'target' => 'list', 'visible' => 1 ), ['order' => 'asc'])[ 'labels' ];
//        prn($this->table('Field')->find(['table_id'=>Table::INVOICE_DETAIL, 'target'=>'list'],['order'=>'asc'])->toArray());
//        prn($results['detail_labels']);
        $results[ 'field_labels' ]   = $this->fields(array( 'table_id' => Table::INVOICE, 'target' => 'list' ))[ 'labels' ];
        $results[ 'model' ]          = $model;
        $results[ 'table_id' ]       = Table::INVOICE;
        $results[ 'params' ][ 'id' ] = $id;
        $results[ 'saurl' ]          = '?back='.$this->generateLabel();
        $results[ 'saurldoubleback' ]      = "?back=".$this->params()->fromQuery('back', 'home');
        $results[ 'saurlback' ]      = $this->getSaurlBack($this->params()->fromQuery('back', 'home'));
        $results[ 'user' ]           = $this->user();

        return $results;
    }

    public function viewpdfAction()
    {
        $modelname  = 'Invoice';
        $permission = $this->getPermission('data:'.$modelname);
        $id         = (string) $this->params()->fromRoute('id', 0);
        $_atts      = array( '_id' => $id );
        if ($permission == Auth::OWN) {
            $_atts[ 'owner_id' ] = $this->user()->id();
        }
        $model = $this->table($modelname)->gather($_atts)->current();
        if (!$model) {
            return $this->showerror('Data not found', $this->url()->fromRoute('dashboard'));
        }
        $results[ 'details' ]        = $this->table('InvoiceDetail')->gather(array( 'invoice_id' => $id ));
        $results[ 'details' ]->buffer();
        $results[ 'detail_labels' ]  = $this->fields(array( 'table_id' => Table::INVOICE_DETAIL, 'target' => 'list', 'visible' => 1 ), ['order' => 'asc'])[ 'labels' ];
        $results[ 'field_labels' ]   = $this->fields(array( 'table_id' => Table::INVOICE ))[ 'labels' ];
        $results[ 'model' ]          = $model;
        $results[ 'table_id' ]       = Table::INVOICE;
        $results[ 'params' ][ 'id' ] = $id;
        $results[ 'user' ]           = $this->user();

//        $results['infoFields'] = ['subject','description','raw_price','total_price','total_discount','created_dtm','order','status'];
        $results['infoFields'] = ['subject','description','total_price','created_dtm','order','status'];
        $results['detailFields'] = ['product_price','discount','qty','final_price','product'];
//        $service = $this ->  getServiceLocator() ->  get( 'Wepo\Lib\PDFService' );

//        return $service -> getPDFResponse( 'wepo\invoice\pdf.twig', $results);
//        return $service -> getViewModel( 'wepo\invoice\pdf.twig', $results);
//        echo $this ->  generatePDF( 'wepo\invoice\pdf.twig', 'showPDF', $results );
//        exit();
        return $this->generatePDF('wepo\invoice\pdf.twig', 'showPDF', $results);
    }

    public function rowscountAction()
    {
        return $this->redirect()->toRoute('invoice', $this->RowsCount(Table::INVOICE));
    }
}
