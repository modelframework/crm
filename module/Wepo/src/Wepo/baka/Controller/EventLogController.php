<?php

namespace Wepo\Controller;

use Wepo\Lib\WepoController;
use Wepo\Model\Table;

class EventLogController extends WepoController
{
    public function indexAction()
    {
        return $this->redirect()->toRoute('eventlog', array( 'action' => 'list' ));
    }

    protected function listAction()
    {
        return $this->getListing('EventLog');
    }
    public function rowscountAction()
    {
        return $this->redirect()->toRoute('eventlog', $this->RowsCount(Table::EVENT_LOG));
    }
}
