<?php

namespace Wepo\Controller;

use Wepo\Lib\WepoController;
use Wepo\Model\Table;
use Wepo\Lib\AuthService as Auth;
use Wepo\Model\Status;

class OrderController extends WepoController
{
    public function indexAction()
    {
        return $this->redirect()->toRoute('order', array( 'action' => 'list' ));
    }

    protected function listAction()
    {
        //        prn($this->table( 'Order' )->find( [ 'quote_id' => '53fc9f175f8060a414000012' ], ['created_dtm'=>'desc'] )->toArray());
//        exit;
        return $this->getListing('Order');
    }

    public function addAction()
    {
        $permission = $this->getPermission('data:Order');
        $form       = $this->form('OrderForm');
        $form->setRoute('order')->setAction('add')->setActionParams([ 'id' => null ]);
        if ($permission == Auth::ALL) {
            $form->getFieldsets()[ 'fields' ]->add(array(
                                                        'type'       => 'Zend\Form\Element\Select',
                                                        'name'       => 'owner_id',
                                                        'attributes' => array(
                                                            'id'       => 'iduser',
                                                            'disabled' => '',
                                                            'value'    => $this->user()->id(),
                                                        ),
                                                        'options'    => array(
                                                            'label'         => 'Owner',
                                                            'value_options' => $this->trueTableHash('User', '_id',
                                                                                                     [
                                                                                                         'fname',
                                                                                                         'lname'
                                                                                                     ], [
                                                                                                         'status_id' => [
                                                                                                             Status::NEW_,
                                                                                                             Status::NORMAL,
                                                                                                         ]
                                                                                                     ]),
                                                        ),
                                                    ));
            $form->addValidationField('fields', 'owner_id');
        }

        $form->getFieldsets()[ 'fields' ]->get('pricebook_id')->setOptions(
                                         array(
                                             'value_options' => $this->trueTableHash('Pricebook', '_id', 'pricebook', ['-status_id' => Status::DELETED]),
                                         )
        );
        $form->getFieldsets()[ 'fields' ]->get('contact_id')->setOptions(
                                         array(
                                             'value_options' => $this->trueTableHash('Contact', '_id',
                                                                                      [ 'fname', 'lname' ], [
                                                         'status_id' => [
                                                             Status::NEW_, Status::NORMAL,
                                                         ]
                                                     ]),
                                         )
        );
        $form->getFieldsets()[ 'fields' ]->get('contact_id')->setValue($this->params()
                                                                               ->fromRoute('contact_id', null) ?:
                                                                              "0");

        $results = $this->add($form, 'Order', $permission);

        if (!is_array($results)) {
            $redirect_action = $this->getRequest()->getPost()['redirect_action'];
            if ($redirect_action == 'save') {
                $results->setVariable('uri', $this->url()->fromRoute('orderdetail', [ 'action' => 'list', 'orderid' => $results->getVariable('id') ]));
            }
        }

        return $results;
    }

    public function editAction()
    {
        $permission = $this->getPermission('data:Order');
        $form       = $this->form('OrderForm');
        $form->setRoute('order')->setAction('edit')->setActionParams([
                                                                              'id' => $this->params()
                                                                                           ->fromRoute('id', 0),
                                                                          ]);
        if ($permission == Auth::ALL) {
            $form->getFieldsets()[ 'fields' ]->add(array(
                                                        'type'       => 'Zend\Form\Element\Select',
                                                        'name'       => 'owner_id',
                                                        'attributes' => array(
                                                            'id'       => 'iduser',
                                                            'disabled' => '',
                                                        ),
                                                        'options'    => array(
                                                            'label'         => 'Owner',
                                                            'value_options' => $this->trueTableHash('User', '_id',
                                                                                                     [
                                                                                                         'fname',
                                                                                                         'lname'
                                                                                                     ], [
                                                                                                         'status_id' => [
                                                                                                             Status::NEW_,
                                                                                                             Status::NORMAL,
                                                                                                         ]
                                                                                                     ]),
                                                        ),
                                                    ));
            $form->addValidationField('fields', 'owner_id');
        }
        $form->getFieldsets()[ 'fields' ]->get('pricebook_id')->setOptions(
            array(
                'value_options' => $this->trueTableHash('Pricebook', '_id', 'pricebook', ['-status_id' => Status::DELETED]),
            )
        );
        $form->getFieldsets()[ 'fields' ]->get('contact_id')->setOptions(
                                         array(
//                                             'label'         => 'Contact',
                                             'value_options' => $this->trueTableHash('Contact', '_id',
                                                                                      [ 'fname', 'lname' ], [
                                                         'status_id' => [
                                                             Status::NEW_, Status::NORMAL,
                                                         ]
                                                     ]),
                                         )
        );
//        $form->setAttribute( 'class', 'validate' );
        $results = $this->edit($form, 'Order', $permission);
//        exit;


        if (!is_array($results)) {
            $redirect_action = $this->getRequest()->getPost()['redirect_action'];
            if ($redirect_action == 'save') {
                $results->setVariable('uri', $this->url()->fromRoute('orderdetail', [ 'action' => 'list', 'orderid' => $results->getVariable('id') ]));
            }
        }

        return $results;
    }

    public function deleteAction()
    {
        //        prn($this->recycle( 'Order' ));
        return $this->recycle('Order');
    }

    public function convertAction()
    {
        $modelname  = 'Order';
        $permission = $this->getPermission('data:'.$modelname);

        $settings = [
            'FromModel' => [
                'model' => 'Order',
                'table_id' => Table::ORDER,
            ],
            'ToModel' => [
                'model' => 'Invoice',
                'link' => 'order_id',
            ],
            'Detail' => [
                'model' => 'OrderDetail',
                'link' => 'order_id',
                'table_id' => Table::ORDER_DETAIL,
            ],
        ];

        return $this->transfer($settings, $permission);
    }

    public function rowscountAction()
    {
        $results = $this->RowsCount(Table::ORDER);

//        return $this->redirect()->toRoute( 'order', $this->RowsCount( Table::ORDER ) );
        return $this->redirect()->toUrl($results[ 'saurlback' ]);
    }

    public function viewAction()
    {
        return $this->redirect()->toRoute('orderdetail', array(
            'action' => 'list', 'orderid' => $this->params()->fromRoute('id', 0),
        ));
    }
}
