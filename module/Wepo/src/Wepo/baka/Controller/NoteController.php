<?php

namespace Wepo\Controller;

use Wepo\Lib\WepoController;
use Wepo\Model\Table;

class NoteController extends WepoController
{
    public function addAction()
    {
        $permission = $this->getPermission('data:Note');
        $form = $this->form('NoteForm');
        $parent = $this->params()->fromRoute('parentid', 0);
        $table = $this->params()->fromRoute('tableid', 0);
        $form->setRoute('note')->setAction('add')->setActionParams([ 'id' => null, 'parentid' => $parent, 'tableid' => $table]);

        return $this->add($form, 'Note', $permission);
    }

    public function add($form, $transport, $permission = 2)
    {
        $model             = $this->model($transport);
        $results           = [ ];
        $results[ 'user' ] = $this->user();
        $form->setAttribute('action', $this->url()->fromRoute($form->getRoute(), [ 'action' => $form->getAction() ] +
                                                                                   $form->getActionParams()));
        $request = $this->getRequest();
        if ($request->isPost()) {
            $form->updateInputFilter($model->getInputFilter(), 'fields');
            $form->setData($request->getPost());
            if ($form->isValid()) {
                try {
                    $model_data = array();
                    foreach ($form->getData() as $_k => $_data) {
                        $model_data += is_array($_data) ? $_data : array( $_k => $_data );
                    }
                    $model->merge($model_data);
                    $this->trigger('presave', $model);
                    $model->postExchange();
                } catch (\Exception $ex) {
                    $results[ 'message' ] = 'Error.'.$ex->getMessage();
                }

                if (!isset($results[ 'message' ]) || !strlen($results[ 'message' ])) {
                    try {
                        $tr = $this->table($transport);
                        $tr->save($model);
                        $model->setId($tr->getLastInsertId());
                    } catch (\Exception $ex) {
                        $results[ 'message' ] = 'Invalid input data.'.$ex->getMessage();
                    }
                }
                if (!isset($results[ 'message' ]) || !strlen($results[ 'message' ])) {
                    $this->trigger('postsave', $model);

                    return $this->refresh($transport.' data was successfully add', $this->url()
                                                                                           ->fromRoute(strtolower(Table::getTransportName(($this->params()->fromRoute('tableid', 0)))),
                                                                                                        array( 'action' => 'view', 'id' =>  $this->params()->fromRoute('parentid', 0) )));
                }
            } else {
                $results[ 'message' ] = 'Invalid input data.';
            }
        }
        $form->getFieldsets()['button']->remove('submit_and_new');
        $form->prepare();
        $results[ 'form' ]       = $form;
        $results[ 'modelname' ]  = strtolower($model->getModelName());
        $results[ 'modelLabel' ]  = strtolower($model->getModelLabel());
        $results[ 'permission' ] = $permission;
        $results[ 'saurl' ]      = '?back='.$this->generateLabel();
        $results[ 'saurlback' ]  = $this->getSaurlBack($this->params()->fromQuery('back', 'home'));

        return $results;
    }

    public function deleteAction()
    {
        return $results = $this->recycle('Note');
    }

    public function recycle($modelName)
    {
        $permission              = $this->getPermission('data:'.$modelName);
        $request                 = $this->getRequest();
        $results[ 'modelname' ]  = strtolower($modelName);
        $results[ 'modellabel' ] =
            $this->table('Table')->findOne([ '_id' => Table::getTableId($modelName) ])->label;
        $results[ 'action' ]     = $this->params('action');
        $ids                     = $request->getPost('checkedid', null);
        $id = null;
        if (!is_array($ids)) {
            $id = $this->params()->fromRoute('id', 0);

            if ($id) {
                $ids = array( $id );
            } else {
                return $this->redirect()->toRoute($results[ 'modelname' ], [
                    'action' => $results[ 'action' ] == 'delete' ? 'list' : 'recyclelist'
                ]);
            }
        }
        $parent = $this->params()->fromRoute('parentid', 0);
        $table = $this->params()->fromRoute('tableid', 0);
        if ($parent && $table) {
            $results['parent'] = $parent;
            $results['table'] = $table;
            $results['route'] = strtolower(Table::getTransportName($table));
        }
        $results[ 'ids' ] = $ids;
        foreach ($ids as $id) {
            try {
                $results[ 'items' ][ $id ] = $this->table($modelName)->find([ '_id' => $id ])->current();
            } catch (\Exception $ex) {
                return $this->refresh($modelName.' data is invalid '.$ex->getMessage(), $this->url()
                                                                                                  ->fromRoute($results[ 'modelname' ],
                                                                                                               array( 'action' => 'list' )));
            }
            if ($permission != 1 && isset($results[ 'items' ][ $id ]->owner_id) &&
                 ($results[ 'items' ][ $id ]->owner_id != $this->user()->id())
            ) {
                return $this->showerror('You don\'t have access to this page.',
                                         $this->url()->fromRoute('dashboard'));
            }
        }
        if (array_key_exists('route', $results)) {
            $results[ 'nosaurlback' ]      = $this->url()->fromRoute($results['route'], [ 'action' => "view", 'id' => $results['parent'] ]);
        }
        if ($request->isPost()) {
            $delyes = $request->getPost('delyes', null);
            $delno  = $request->getPost('delno', null);
            if ($delyes !== null) {
                $this->trigger('prerecycle', $results[ 'items' ]);
                $this->trigger('recycle', $results[ 'items' ]);
                $this->trigger('postrecycle', $results[ 'items' ]);
                $url = $this->getSaurlBack($this->params()->fromQuery('back'));
                if (!isset($url)) {
                    $url = $this->url()->fromRoute($results[ 'modelname' ], [
                        'action' => $results[ 'action' ] == 'delete' ? 'list' : 'recyclelist'
                    ]);
                }

                if ($this->getRequest()->isXmlHttpRequest()) {
                    $results = [ 'ok' => 1, 'url' => $url ];

                    $jresults = new \Zend\View\Model\JsonModel($results);
                    $jresults->setTemplate('/');

                    return $jresults;
                } else {
                    return $this->refresh('Action has been completed successfully', $url);
                }
            }
            if ($delno !== null) {
                return $this->redirect()->toRoute($results[ 'modelname' ], [
                    'action' => $results[ 'action' ] == 'delete' ? 'list' : 'recyclelist'
                ]);
            }
        }
        $results[ 'permission' ] = $permission;
        $results[ 'user' ]       = $this->user();
        $results[ 'saurl' ]      = '?back='.$this->params()->fromQuery('back', 'home');

        return $results;
    }

    public function editAction()
    {
        $permission = $this->getPermission('data:Payment');
        $form       = $this->form('NoteForm');
        $parent = $this->params()->fromRoute('parentid', 0);
        $table = $this->params()->fromRoute('tableid', 0);
        $form->setRoute('note')->setAction('edit')->setActionParams([ 'id' => $this->params()->fromRoute('id', 0), 'parentid' => $parent, 'tableid' => $table ]);

        return $this->edit($form, 'Note', $permission);
    }

    public function edit($form, $transport, $permission = 2)
    {
        if (!$form->getActionParams('id')) {
            return $this->redirect()->toRoute($form->getRoute());
        }

        $form->setAttribute('action', $this->url()->fromRoute($form->getRoute(), [ 'action' => $form->getAction() ] +
                                                                                   $form->getActionParams()));
        $results           = [ ];
        $results[ 'user' ] = $this->user();
        try {
            $model    = $this->table($transport)->get($form->getActionParams('id'));
            $old_data = $model->split($form->getValidationGroup());

            //Это жесть конечно и забавно, но на время сойдет :)
//            $model_bind = array_diff( $model->toArray(), $model->split( $form->getValidationGroup() ) );
            $model_bind = $model->toArray();
            foreach ($model_bind as $_k => $_v) {
                //                $_wrs = explode( '_', $_k );
                if (substr($_k, -4) == '_dtm') {
                    $model->$_k = str_replace(' ', 'T', $_v);
                }
            }
            //Конец жести
        } catch (\Exception $ex) {
            throw $ex;

//            exit();

            return $this->redirect()->toRoute($form->getRoute(), array( 'action' => 'list' ));
        }
        if ($permission != 1 && isset($model->owner_id) && ($model->owner_id != $this->user()->id())) {
            return $this->showerror('You don\'t have access to this page.', $this->url()->fromRoute('dashboard'));
        }

        $request = $this->getRequest();
        if ($request->isPost()) {
            $form->addInputFilter($model->getInputFilter(), 'fields');
            $form->setData($request->getPost());
            if ($form->isValid()) {
                $model_data = array();

                foreach ($form->getData() as $_k => $_data) {
                    $model_data += is_array($_data) ? $_data : array( $_k => $_data );
                }
                $model->merge($model_data);
                $model->merge($old_data);
                $this->trigger('presave', $model);
                if (!isset($results[ 'message' ]) || !strlen($results[ 'message' ])) {
                    try {
                        $tr = $this->table($transport);
                        $tr->save($model);
                        $model->setId($tr->getLastInsertId());
                    } catch (\Exception $ex) {
                        $results[ 'message' ] = 'Invalid input data.'.$ex->getMessage();
                    }
                }
                if (!isset($results[ 'message' ]) || !strlen($results[ 'message' ])) {
                    $this->trigger('postsave', $model);
                    $url = $this->getBackUrl();
                    if ($url == null || $url == '/') {
                        $url = $this->url()->fromRoute(strtolower(Table::getTransportName(($this->params()->fromRoute('tableid', 0)))),
                                                                                                        array( 'action' => 'view', 'id' =>  $this->params()->fromRoute('parentid', 0) ));
                    }

                    return $this->refresh($transport.' data was successfully changed', $url);
                }
            }
        } else {
            $form->bind($model);
        }
        $form->getFieldsets()['button']->remove('submit_and_new');
        $form->prepare();
        $results[ 'form' ]       = $form;
        $results[ 'permission' ] = $permission;
        $results[ 'modelname' ]  = strtolower($model->getModelName());
        $results[ 'modelLabel' ]  = strtolower($model->getModelLabel());
        $results[ 'saurl' ]      = '?back='.$this->generateLabel();
        $results[ 'saurlback' ]  = $this->getSaurlBack($this->params()->fromQuery('back', 'home'));
        if (isset($form->getFieldsets()[ 'saurl' ])) {
            $form->getFieldsets()[ 'saurl' ]->get('back')->setValue($this->params()->fromQuery('back', 'home'));
        }

        return $results;
    }
}
