<?php

namespace Wepo\Controller;

use Wepo\Lib\WepoController;
use Wepo\Model\Table;
use Wepo\Model\Status;
use ModelFramework\AuthService\AuthService as Auth;

class ContactController extends WepoController
{
    public function indexAction()
    {
        return $this->redirect()->toRoute('contact', array( 'action' => 'list' ));
    }

    protected function listAction()
    {
        return $this->getListing('Contact', [
            'status_id' => [
                Status::NEW_, Status::NORMAL,
            ]
//            Status::CONVERTED, Status::DEAD
        ]);
    }

    protected function recyclelistAction()
    {
        $results = $this->getListing('Contact', [ 'status_id' => [ Status::DELETED ] ]);
        $results[ 'params' ][ 'action' ] = 'recyclelist';

        return $results;
    }

    public function deleteAction()
    {
        return $results = $this->recycle('Contact');
    }

    public function cleanAction()
    {
        return $results = $this->recycle('Contact');
    }

    public function restoreAction()
    {
        return $results = $this->recycle('Contact');
    }

    public function addAction()
    {
        $permission = $this->getPermission('data:Contact');
        $form       = $this->form('ContactForm');
        $form->setRoute('contact')->setAction('add')->setActionParams([ 'id' => null ]);
        if ($permission == Auth::ALL) {
            $form->getFieldsets()[ 'fields' ]->add(array(
                                                        'type'       => 'Zend\Form\Element\Select',
                                                        'name'       => 'owner_id',
                                                        'attributes' => array(
                                                            'id'       => 'iduser',
                                                            'disabled' => '',
                                                            'value'    => $this->user()->id(),
                                                        ),
                                                        'options'    => array(
                                                            'label'         => 'Owner',
                                                            'value_options' => $this->trueTableHash('User', '_id',
                                                                    ['fname', 'lname'], [
                                                                        'status_id' => [
                                                                            Status::NEW_, Status::NORMAL,
                                                                        ]
                                                                    ]),
                                                        ),
                                                    ));
            $form->addValidationField('fields', 'owner_id');
        }
        $clientHash = $this->truetableHash(
            'Client', '_id', 'name',
            [ 'status_id' => [ Status::NEW_, Status::NORMAL ] ],
            true, [ 'rrrrrrrrrrrrrrrrrrrrrrrr' => 'New account']
        );
        $doctorHash = $this->truetableHash(
            'Doctor', '_id', 'title',
            [ 'status_id' => [ Status::NEW_, Status::NORMAL ] ]
        );
        $form->getFieldsets()[ 'fields' ]->get('client_id')->setOptions(
                                         array(
                                             'label'         => 'Client',
                                             'value_options' => $clientHash,
                                         )
        );
        $form->getFieldsets()[ 'medical' ]->get('doctor_id')->setOptions(
                                         array(
                                             'label'         => 'Doctor',
                                             'value_options' => $doctorHash,
                                         )
        );
        $form->getFieldsets()[ 'fields' ]->get('consultant')->setValue($this->user()->title);

        return $this->add($form, 'Contact', $permission);
    }

    public function add($form, $transport, $permission = 2)
    {
        $model             = $this->model($transport);
        $results           = [ 'status' => false ];
        $request = $this->getRequest();

        if ($request->isXmlHttpRequest()) {
            if ($request->isPost()) {
                $form->updateInputFilter($model->getInputFilter(), 'fields');
                $form->setData($request->getPost());
                if ($form->isValid()) {
                    try {
                        $model_data = array();
                        foreach ($form->getData() as $_k => $_data) {
                            $model_data += is_array($_data) ? $_data : array( $_k => $_data );
                        }
                        $model->merge($model_data);
                        $model->postExchange();
                        $this->trigger('presave', $model);
                    } catch (\Exception $ex) {
                        $results[ 'message' ] = 'Error.'.$ex->getMessage();
                    }
                    if (!isset($results[ 'message' ]) || !strlen($results[ 'message' ])) {
                        try {
                            if ($model->client_id == "rrrrrrrrrrrrrrrrrrrrrrrr") {
                                $data             = $request->getPost('fields');
                                $model->client_id = null;
                                $transport3       = 'Client';
                                $client           = $this->model('Client');
                                $client->exchangeArray($model->toArray());
                                $client->name      = $data[ 'account' ];
                                $client->status_id = Status::NEW_;
                                $this->trigger('presave', $client);
                                $this->table($transport3)->save($client);
                                $model->client_id =
                                    $this->table($transport3)->find(array( 'changed_dtm' => $model->changed_dtm ))
                                        ->current()->id();
                            }
                            $this->trigger('presave', $model);
                            $tr = $this->table($transport);
                            $tr->save($model);
                            $model->setId($tr->getLastInsertId());
                        } catch (\Exception $ex) {
                            $results[ 'message' ] = 'Invalid input data.'.$ex->getMessage();
                        }
                    }
                    if (!isset($results[ 'message' ]) || !strlen($results[ 'message' ])) {
                        $this->trigger('postsave', $model);

                        $results['status'] = true;
                        $results['message'] = 'Data has been saved successfully!';

                        $routeMatch = $this->getServiceLocator()->get('Application')->getMvcEvent()->getRouteMatch();
                        $route = $routeMatch->getMatchedRouteName();
                        $id = $model->id();

                        $results['id'] = $id;
//                    $results['uri'] = $this->url()->fromRoute( $route , [ 'action'=>'view','id'=>$id ]);
                        $redirect_action = $request->getPost()['redirect_action'];
                        $redirect_action = $redirect_action == 'save_and_new' ? 'add' : 'list';
                        $results['uri'] = $this->url()->fromRoute($route, [ 'action' => $redirect_action ]);
                    }
                } else {
                    $results['field_messages'] = $form->getMessages();
                    $results[ 'message' ] = 'Invalid input data.';
                }
            }

            $results = new \Zend\View\Model\JsonModel($results);
            $results->setTemplate('/');
        } else {
            $results[ 'user' ] = $this->user();
            $form->setAttribute('action', $this->url()->fromRoute($form->getRoute(), [ 'action' => $form->getAction() ] +
                $form->getActionParams()));
            $form->getFieldsets()[ 'fields' ]->get('client_id')->setValue($this->params()
                ->fromRoute('client_id', null) ?:
                "0");
            $form->prepare();
            $results[ 'form' ]       = $form;
            $results[ 'modelname' ]  = strtolower($model->getModelName());
            $results[ 'modelLabel' ]  = $model->getModelLabel();
            $results[ 'permission' ] = $permission;
            $results[ 'saurl' ]      = '?back='.$this->generateLabel();
            $results[ 'saurlback' ]  = $this->getSaurlBack($this->params()->fromQuery('back', 'home'));
        }

        return $results;
    }

    public function editAction()
    {
        $permission = $this->getPermission('data:Contact');
        $form       = $this->form('ContactForm');
        $form->setRoute('contact')->setAction('edit')->setActionParams([
                                                                                'id' => (string) $this->params()
                                                                                                      ->fromRoute('id',
                                                                                                                   0),
                                                                            ]);
        if ($permission == Auth::ALL) {
            $form->getFieldsets()[ 'fields' ]->add(array(
                                                        'type'       => 'Zend\Form\Element\Select',
                                                        'name'       => 'owner_id',
                                                        'attributes' => array(
                                                            'id'       => 'iduser',
                                                            'disabled' => '',
                                                        ),
                                                        'options'    => array(
                                                            'label'         => 'Owner',
                                                            'value_options' => $this->trueTableHash('User', '_id',
                                                                    ['fname', 'lname'], [
                                                                        'status_id' => [
                                                                            Status::NEW_, Status::NORMAL,
                                                                        ]
                                                                    ]),
                                                        ),
                                                    ));
            $form->addValidationField('fields', 'owner_id');
        }
        $clientHash = $this->truetableHash(
            'Client', '_id', 'name',
            [ 'status_id' => [ Status::NEW_, Status::NORMAL ] ],
            true, [ 'rrrrrrrrrrrrrrrrrrrrrrrr' => 'New account']
        );

        $doctorHash = $this->truetableHash(
            'Doctor', '_id', 'title',
            [ 'status_id' => [ Status::NEW_, Status::NORMAL ] ]
        );
        $form->getFieldsets()[ 'fields' ]->get('client_id')->setOptions(
                                         array(
                                             'label'         => 'Client',
                                             //                'value_options' => $this -> tableHash( 'Client', '_id', 'name' ),
                                             'value_options' => $clientHash,
                                         )
        );
        $form->getFieldsets()[ 'medical' ]->get('doctor_id')->setOptions(
                                         array(
                                             'label'         => 'Doctor',
                                             'value_options' => $doctorHash,
                                         )
        );

        return $this->edit($form, 'Contact', $permission);
    }

    public function edit($form, $transport, $permission = 2)
    {
        $results           = [ 'status' => false ];
        $form->setAttribute('action', $this->url()->fromRoute($form->getRoute(), [ 'action' => $form->getAction() ] +
            $form->getActionParams()));
        try {
            $model    = $this->table($transport)->get($form->getActionParams('id'));
            $old_data = $model->split($form->getValidationGroup());
            //Это жесть конечно и забавно, но на время сойдет :)
            $model_bind = array_diff($model->toArray(), $model->split($form->getValidationGroup()));
            foreach ($model_bind as $_k => $_v) {
                $_wrs = explode('_', $_k);
                if (array_pop($_wrs) == 'dtm') {
                    $model->$_k = str_replace(' ', 'T', $_v);
                }
            }
            //Конец жести
        } catch (\Exception $ex) {
            return $this->redirect()->toRoute($form->getRoute(), array( 'action' => 'list' ));
        }

        $request = $this->getRequest();

        if ($request->isXmlHttpRequest()) {
            if ($request->isPost()) {
                $form->addInputFilter($model->getInputFilter(), 'fields');
                $form->setData($request->getPost());
                if ($form->isValid()) {
                    try {
                        $model_data = array();
                        foreach ($form->getData() as $_k => $_data) {
                            $model_data += is_array($_data) ? $_data : array( $_k => $_data );
                        }
                        $model->merge($model_data);
                        $model->merge($old_data);
                        $this->trigger('presave', $model);
                    } catch (\Exception $ex) {
                        $results[ 'message' ] = 'Error.'.$ex->getMessage();
                    }
                    if (!isset($results[ 'message' ]) || !strlen($results[ 'message' ])) {
                        try {
                            if ($model->client_id == "rrrrrrrrrrrrrrrrrrrrrrrr") {
                                $data             = $request->getPost('fields');
                                $model->client_id = null;
                                $transport3       = 'Client';
                                $client           = $this->model('Client');
                                $client->exchangeArray($model->toArray());
                                $client->name      = $data[ 'account' ];
                                $client->status_id = Status::NEW_;
                                $this->trigger('presave', $client);
                                $this->table($transport3)->save($client);
                                $model->client_id = $this->table($transport3)->getLastInsertId();
                                if (empty($model->client_id)) {
                                    $model->client_id =
                                        $this->table($transport3)->find(array( 'changed_dtm' => $model->changed_dtm ))
                                            ->current()->id();
                                }
                            }
                            $this->trigger('presave', $model);
                            $tr = $this->table($transport);
                            $tr->save($model);
                            $model->setId($tr->getLastInsertId());
                        } catch (\Exception $ex) {
                            $results[ 'message' ] = 'Invalid input data.'.$ex->getMessage();
                        }
                    }
                    if (!isset($results[ 'message' ]) || !strlen($results[ 'message' ])) {
                        $this->trigger('postsave', $model);

                        $results['status'] = true;
                        $results['message'] = 'Data has been saved successfully!';

                        $routeMatch = $this->getServiceLocator()->get('Application')->getMvcEvent()->getRouteMatch();
                        $route = $routeMatch->getMatchedRouteName();
                        $id = $model->id();

                        $results['id'] = $id;
//                    $results['uri'] = $this->url()->fromRoute( $route , [ 'action'=>'view','id'=>$id ]);
                        $redirect_action = $request->getPost()['redirect_action'];
                        $redirect_action = $redirect_action == 'save_and_new' ? 'add' : 'list';
//                        $results['uri'] = $this->url()->fromRoute( $route , [ 'action' => $redirect_action ]);
                        $url = $this->getBackUrl();
                        if ($url == null || $url == '/') {
                            $url = $this->getSaurlBack($this->params()->fromQuery('back', 'home'));
                        }
                        $results[ 'uri' ] = $url;
                    }
                } else {
                    $results['field_messages'] = $form->getMessages();
                    $results[ 'message' ] = 'Invalid input data.';
                }
            }

            $results = new \Zend\View\Model\JsonModel($results);
            $results->setTemplate('/');
        } else {
            if (!$form->getActionParams('id')) {
                return $this->redirect()->toRoute($form->getRoute());
            }
            $results[ 'user' ] = $this->user();
            if ($permission != 1 && isset($model->owner_id) && ($model->owner_id != $this->user()->id())) {
                return $this->showerror('You don\'t have access to this page.', $this->url()->fromRoute('dashboard'));
            }
            $form->bind($model);
            $form->getFieldsets()['button']->remove('submit_and_new');
            $form->prepare();
            $results[ 'form' ]       = $form;
            $results[ 'permission' ] = $permission;
            $results[ 'modelname' ]  = strtolower($model->getModelName());
            $results[ 'saurl' ]      = '?back='.$this->generateLabel();
            $results[ 'saurlback' ]  = $this->getSaurlBack($this->params()->fromQuery('back', 'home'));
            if (isset($form->getFieldsets()[ 'saurl' ])) {
                $form->getFieldsets()[ 'saurl' ]->get('back')->setValue($this->params()->fromQuery('back', 'home'));
            }
        }

        return $results;
    }

    public function viewAction()
    {
        return $this->view('Contact');
    }

    public function rowscountAction()
    {
        $results = $this->RowsCount(Table::CONTACT);

//        return $this->redirect()->toRoute( 'contact', $results );
        return $this->redirect()->toUrl($results['saurlback']);
    }
}
