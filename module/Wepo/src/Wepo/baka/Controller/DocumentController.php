<?php

namespace Wepo\Controller;

use Wepo\Lib\WepoController;
use Wepo\Model\Table;
use ModelFramework\AuthService\AuthService as Auth;
use Zend\InputFilter\Factory as InputFactory;
use Zend\InputFilter\InputFilter;
use Wepo\Model\Status;
use ModelFramework\FileService\FileService;

class DocumentController extends WepoController
{
    public function indexAction()
    {
        return $this->redirect()->toRoute('document', array('action' => 'list'));
    }

    protected function listAction()
    {
        return $this->getListing('Document', [
                    'status_id' => [
                        Status::NEW_, Status::NORMAL, Status::CONVERTED, Status::DEAD,
                    ]
                ]);
    }

    protected function recyclelistAction()
    {
        $results                         = $this->getListing('Document', [ 'status_id' => [ Status::DELETED ] ]);
        $results[ 'params' ][ 'action' ] = 'recyclelist';

        return $results;
    }

    public function restoreAction()
    {
        return $results = $this->recycle('Document');
    }

    public function addAction()
    {
        $permission = $this->getPermission('data:Document');
        $form = $this->form('DocumentForm');
        $tmp = $this->getParam('tableid', 0);
        $person = $this->getParam('id', 0);
        $model = $this->table('Lead')->get($person);
//        $person = $model->fname." ".$model->lname;
//        prn($model);exit();
        $form->setRoute('document')->setAction('add')->setActionParams([ 'id' => null]);

        $results = $this->add($form, 'Document', $permission);

        if (!empty($tmp)) {
            $results['target_id'] = $person;
            $results['table_id'] = strtolower(Table::getTransportName($this->getParam('tableid', 0)));
//        $attach_to =  strtolower(Table::getTransportName($this->getParam( 'tableid' , 0 )))."s_id";
//        $form -> getFieldsets()[ 'attach' ] -> get( $attach_to ) -> setValue( $person );
        }

        return $results;
    }

    public function viewAction()
    {
        return $results = $this->view('Document');
    }

    public function cleanAction()
    {
        $request                 = $this->getRequest();
        if ($request->isPost()) {
            $delyes = $request->getPost('delyes', null);
            if ($delyes !== null) {
                $ids                     = $request->getPost('checkedid', null);
                if (!is_array($ids)) {
                    $id = (string) $this->params()->fromRoute('id', 0);
                    if ($id) {
                        $ids = array( $id );
                    } else {
                        return $this->redirect()->toRoute($results[ 'modelname' ], [ 'action' => $results[ 'action' ] == 'delete' ? 'list' : 'recyclelist' ]);
                    }
                }
                foreach ($ids as $id) {
                    $model = $this->table('Document')->find(['_id' => $id])->current();
                    $filename = $model->document_real_name;
                    $fs = $this->getServiceLocator()->get('ModelFramework\FileService');
                    $fs->deleteFile($filename);
                }
            }
        }

        return $results = $this->recycle('Document');
    }

    public function deleteAction()
    {
        return $results = $this->recycle('Document');
    }

    public function editAction()
    {
        $permission = $this->getPermission('data:Document');
        $form = $this->form('EditDocumentForm');
//        $form = new EditDocumentForm;
        $form->getFieldsets()['fields']->add(array(
            'type' => 'Zend\Form\Element\Select',
            'name' => 'owner_id',
            'attributes' => array(
                'id' => 'iduser',
                'disabled' => '',
            ),
            'options' => array(
                'label' => 'Owner',
                'value_options' => $this->trueTableHash('User', '_id', ['fname', 'lname'], [
                    'status_id' => [
                        Status::NEW_,
                        Status::NORMAL,
                    ]
                ]),
            ),
        ));
        $form->addValidationField('fields', 'owner_id');
        $form->setRoute('document')->setAction('edit')->setActionParams([
            'id' => (string) $this->params()
                    ->fromRoute('id', 0),
        ]);

        return $this->edit($form, 'Document', $permission);
    }

    public function rowscountAction()
    {
        return $this->redirect()->toRoute('document', $this->RowsCount(Table::DOCUMENT));
    }

    public function add($form, $transport, $permission = 2)
    {
        $model = $this->model($transport);
        $results = [];
        $results['user'] = $this->user();
        $form->setAttribute('action', $this->url()->fromRoute($form->getRoute(), [ 'action' => $form->getAction()] +
                        $form->getActionParams()));

        if ($permission == Auth::ALL) {
            $form->getFieldsets()['fields']->add(array(
                'type' => 'Zend\Form\Element\Select',
                'name' => 'owner_id',
                'attributes' => array(
                    'id' => 'iduser',
                    'disabled' => '',
                    'value' => $this->user()->id(),
                ),
                'options' => array(
                    'label' => 'Owner',
                    'value_options' => $this->trueTableHash('User', '_id', ['fname', 'lname'], [
                        'status_id' => [
                            Status::NEW_, Status::NORMAL,
                        ]
                    ]),
                ),
            ));
            $form->addValidationField('fields', 'owner_id');
        }

        $factory        = new InputFactory();
        $addInputFilter = new InputFilter();

        $form->addInputFilter($addInputFilter, 'attach');

        $request = $this->getRequest();

        if ($request->isPost()) {
            $inputFilter = $model->getInputFilter();
            $form->addInputFilter($inputFilter, 'fields');

            $files = $request->getFiles();
            $fileinfo = $files->get('fields')['upload-file'];
            $fs = $this->getServiceLocator()->get('ModelFramework\FileService');
            $filename = $fs->saveFile($fileinfo['name'], $fileinfo['tmp_name']);
            $model->document_way = $filename;
            $model->document_real_name = basename($filename);
            $model->document_extension = $fs->getFileExtension($filename);
            $model->file_size = (string) (round((float) $fileinfo['size'] / 1048576, 2)).' MB';
            $post = array_merge_recursive(
                    $request->getPost()->toArray(), $fileinfo
            );
            $form->setData($post);
            if ($form->isValid()) {
                try {
                    $model_data = array();
                    foreach ($form->getData() as $_k => $_data) {
                        $model_data += is_array($_data) ? $_data : array($_k => $_data);
                    }
                    $model->merge($model_data);
                    $this->trigger('presave', $model);
                    $model->postExchange();
                } catch (\Exception $ex) {
                    $results['message'] = 'Error.'.$ex->getMessage();
                }
                if (!isset($results['message']) || !strlen($results['message'])) {
                    try {
                        $this->table($transport)->save($model);
                    } catch (\Exception $ex) {
                        $results['message'] = 'Invalid input data.'.$ex->getMessage();
                    }
                }
                if (!isset($results['message']) || !strlen($results['message'])) {
                    $this->trigger('postsave', $model);

                    return $this->refresh($transport.' data was successfully add', $this->url()
                                            ->fromRoute($form->getRoute(), array('action' => $form->getBackAction()) +
                                                    $form->getActionParams()));
                }
            } else {
                $results['message'] = 'Invalid input data.';
            }
        }

        $form->getFieldsets()['button']->remove('submit_and_new');

        $form->prepare();
        $results['form'] = $form;
        $results[ 'modelLabel' ]  = $model->getModelLabel();
        $results['modelname'] = strtolower($model->getModelName());
        $results['permission'] = $permission;
        $results['saurl'] = '?back='.$this->generateLabel();
        $results['saurlback'] = $this->getSaurlBack($this->params()->fromQuery('back', 'home'));
        $results['mainuser'] = $this->mainUser();

        return $results;
    }

    public function downloadAction()
    {
        try {
            $permission = $this->getPermission('data:Document');
        } catch (\Exception $ex) {
            return $this->showerror($ex->getMessage(), null, null, 'Permission denied');
        }
        $id = (string) $this->params()->fromRoute('id', 0);
        $_atts = array('_id' => $id);
        if ($permission == Auth::OWN) {
            $_atts['owner_id'] = $this->user()->id();
        }

        $model = $this->table('Document')->gather($_atts)->current();
        $filename = $model->document_real_name;
        $fs = $this->getServiceLocator()->get('ModelFramework\FileService');
        $response = $fs->downloadFile($filename);
        if (!$response) {
            return $this->showerror('File does not exist', $this->url()->fromRoute('document', ['action' => 'list']));
        }

        return $response;
    }

    public function getlinkAction()
    {
        $permission = $this->getPermission('data:Document');
        $id = (string) $this->params()->fromRoute('id', 0);
        $_atts = array('_id' => $id);
        if ($permission == Auth::OWN) {
            $_atts['owner_id'] = $this->user()->id();
        }
        $model = $this->table('Document')->gather($_atts)->current();
        $fs = $this->getServiceLocator()->get('ModelFramework\FileService');
        $filename = $model->document_real_name;
        $name = $model->document_name;
        $accesstype = 'private';
        $result = [
            'access' => $accesstype,
            'filename' => $filename,
            'name' => $name,
        ];

        return $result;
    }
}
