<?php

namespace Wepo\Controller;

use Wepo\Lib\WepoController;
use Wepo\Model\Table;
use ModelFramework\AuthService\AuthService as Auth;
use Zend\InputFilter\Factory as InputFactory;
use Zend\InputFilter\InputFilter;
use Wepo\Model\Status;

class ActivityController extends WepoController
{
    public function indexAction()
    {
        return $this->redirect()->toRoute('activity', array( 'action' => 'list' ));
    }

    protected function listAction()
    {
        return $this->getListing('Activity');
    }

    public function deleteAction()
    {
        return $this->recycle('Activity');
    }

    public function add($form, $modelname, $permission = 2)
    {
        $form->setRoute('activity')->setAction($this->params('action'))->setActionParams(
            [ 'id' => null, 'activity' => $this->params('activity') ]
        );
        if ($permission == Auth::ALL) {
            $form->getFieldsets()[ 'fields' ]->add(array(
                'type'       => 'Zend\Form\Element\Select',
                'name'       => 'owner_id',
                'attributes' => array(
                    'id'       => 'iduser',
                    'disabled' => '',
                    'value'    => $this->user()->id(),
                ),
                'options'    => array(
                    'label'         => 'Owner',
                    'value_options' => $this->trueTableHash('User', '_id', ['fname', 'lname'], ['status_id' => [ Status::NEW_, Status::NORMAL ] ]),
                ),
            ));
            $form->addValidationField('fields', 'owner_id');
        }
//        $form -> getFieldsets()[ 'fields' ] -> get( 'client_id' ) -> setOptions(
//            array(
//                'label'         => 'Client',
//                'value_options' => $this -> trueTableHash( 'Client', '_id', 'name', ['status_id'=>[  Status::NEW_,  Status::NORMAL]] ),
//            )
//        );

        $factory        = new InputFactory();
        $addInputFilter = new InputFilter();
        $addInputFilter->add($factory->createInput(array(
                'name'     => 'table_id',
                'required' => true,
                'filters'  => array(
                    array( 'name' => 'StripTags' ),
                    array( 'name' => 'StringTrim' ),
                ),
        )));
        $addInputFilter->add($factory->createInput(array(
                'name'       => 'target_id',
                'required'   => true,
                'filters'    => array(
                    array( 'name' => 'StripTags' ),
                    array( 'name' => 'StringTrim' ),
                ),
                'validators' => array(
                    array(
                        'name'    => 'regex', false,
                        'options' => array(
                            'pattern'  => '/^[1-9]/',
                            'messages' => array( \Zend\Validator\Regex::NOT_MATCH => 'select item' ),
                        ),
                    ),
                ),
        )));

        $form->addInputFilter($addInputFilter, 'additional');
        ////////////////////////////////////////////////
        $results = parent::add($form, $modelname, $permission);

        $tableid  = $form->getFieldsets()[ 'additional' ]->get('table_id')->getValue();
        if ($tableid == null) {
            $tableid =  $this->params()->fromRoute('tableid', null);
            if ($tableid == null) {
                $tableid =  Table::CONTACT;
            }
            $form->getFieldsets()[ 'additional' ]->get('table_id')->setValue($tableid);
        }

        $target_id  = $form->getFieldsets()[ 'additional' ]->get('target_id')->getValue();
        if ($target_id == null) {
            $target_id =  $this->params()->fromRoute('id', 0);
            $form->getFieldsets()[ 'additional' ]->get('target_id')->setValue($target_id);
        }

        $form->setRoute('activity')->setAction($this->params('action'))->setActionParams(
              [ 'id' => null, 'activity' => $this->params('activity'), 'tableid' => $tableid, 'id' => $target_id ]
        );

//        if ( $tableid == Table::LEAD )
//        {
//            $form -> getFieldsets()[ 'additional' ] -> get( 'target_id' ) -> setOptions(
//                array(
//                    'value_options' => $this -> trueTableHash( 'Lead', '_id', ['fname', 'lname'], ['status_id' => [ Status::NEW_, Status::NORMAL ] ] ),
//                    'label'         => 'Lead',
//                )
//            );
//        }
//        else
//        {
//            $form -> getFieldsets()[ 'additional' ] -> get( 'target_id' ) -> setOptions(
//                array(
//                    'value_options' => $this -> trueTableHash( 'Contact', '_id', ['fname', 'lname'], ['status_id' => [ Status::NEW_, Status::NORMAL ] ] ),
//                    'label'         => 'Contact',
//                )
//            );
//        }
        if (is_array($results)) {
            $results['mainuser'] = $this->mainUser();
            $results['target_id'] = $target_id;
            $results['table_id'] = $tableid;
//            prn($results);
        }

        return $results;
    }

    public function addAction()
    {
        $permission = $this->getPermission('data:Activity');
        $activity   = $this->params()->fromRoute('activity', null);
        if (!($activity == 'call' || $activity == 'task' || $activity == 'event')) {
            return $this->redirect()->toRoute('activity', array( 'action' => 'list' ));
        }
        $modelname = ucfirst($activity);
        $form      = $this->form($modelname.'Form');
        $result = $this->add($form, $modelname, $permission);

        if (is_array($result)) {
            $result['table_names'] = [Table::LEAD => 'Lead',Table::CONTACT => 'Contact'];
        } elseif (!is_array($result) && $result->getVariable('status')) {
            $redirect_action = $this->getRequest()->getPost()['redirect_action'];
            if ($redirect_action == 'save_and_new') {
                $result->setVariable('uri', $this->url()->fromRoute('activity', [ 'action' => 'add', 'activity' => $activity ]));
            }
        }

        return $result;
    }

    public function edit($form, $modelname, $permission = 2)
    {
        $id = (string) $this->params()->fromRoute('id', 0);
        try {
            $activity = $this->table($modelname)->get($id);
        } catch (\Exception $ex) {
            return $this->redirect()->toRoute('Activity', array( 'action' => 'list' ));
        }
        $form->setRoute('activity')->setAction($this->params('action'))->setActionParams([ 'id' => $id, 'activity' => $this->params('activity') ]);
        if ($permission == Auth::ALL) {
            $form->getFieldsets()[ 'fields' ]->add(array(
                'type'       => 'Zend\Form\Element\Select',
                'name'       => 'owner_id',
                'attributes' => array(
                    'id'       => 'iduser',
                    'disabled' => '',
                ),
                'options'    => array(
                    'label'         => 'Owner',
                    'value_options' => $this->trueTableHash('User', '_id', ['fname', 'lname'], ['status_id' => [ Status::NEW_, Status::NORMAL ] ]),
                ),
            ));
            $form->addValidationField('fields', 'owner_id');
        }

        ///////////////////////////
//        $factory        = new InputFactory();
//        $addInputFilter = new InputFilter();

//        $addInputFilter -> add( $factory -> createInput( array(
//                'name'     => 'table_id',
//                'required' => true,
//                'filters'  => array(
//                    array( 'name' => 'StripTags' ),
//                    array( 'name' => 'StringTrim' ),
//                ),
//        ) ) );
//        $addInputFilter -> add( $factory -> createInput( array(
//                'name'       => 'target_id',
//                'required'   => TRUE,
//                'filters'    => array(
//                    array( 'name' => 'StripTags' ),
//                    array( 'name' => 'StringTrim' ),
//                ),
//                'validators' => array(
//                    array(
//                        'name'    => 'GreaterThan',
//                        'options' => array(
//                            'min'       => 1,
//                            'inclusive' => true,
//                            'messages'  => array( \Zend\Validator\GreaterThan::NOT_GREATER_INCLUSIVE => 'select item' ),
//                        )
//                    )
//                ),
//        ) ) );

//        $form -> addInputFilter( $addInputFilter, 'additional' );
        ///////////////////////////////
        $results = parent::edit($form, $modelname, $permission);

        $tableid  = $this->table($modelname)->get($id)->toArray()[ 'table_id' ];
//        $targetid = $this -> table( $modelname ) -> get( $id ) -> toArray()[ 'target_id' ];

        $formTableId = $form->getFieldsets()[ 'additional' ]->get('table_id')->getValue();

        $tableid = $formTableId ?: $tableid;
        $form->getFieldsets()[ 'additional' ]->get('table_id')->setValue($tableid);

//        if ( $tableid == Table::LEAD )
//        {
//            $form -> getFieldsets()[ 'additional' ] -> get( 'target_id' ) -> setOptions(
//                array(
//                    'value_options' => $this -> trueTableHash( 'Lead', '_id', ['fname', 'lname'], ['status_id' => [ Status::NEW_, Status::NORMAL ] ] ),
//                    'label'         => 'Lead',
//                )
//            ) -> setValue( $targetid );
//        }
//        else
//        {
//            $form -> getFieldsets()[ 'additional' ] -> get( 'target_id' ) -> setOptions(
//                array(
//                    'value_options' => $this -> trueTableHash( 'Contact', '_id', ['fname', 'lname'], ['status_id' => [ Status::NEW_, Status::NORMAL ] ] ),
//                    'label'         => 'Contact',
//                )
//            ) -> setValue( $targetid );
//        }
        if (is_array($results)) {
            $results['mainuser'] = $this->mainUser();
            $results['modelid'] = $id;
        }
//        prn($results);
        return $results;
    }

    public function editAction()
    {
        $permission = $this->getPermission('data:Activity');
        $activity   = $this->params()->fromRoute('activity', null);
        if (!($activity == 'call' || $activity == 'task' || $activity == 'event')) {
            return $this->redirect()->toRoute('activity', array( 'action' => 'list' ));
        }
        $modelname = ucfirst($activity);
        $form      = $this->form($modelname.'Form');
        $result = $this->edit($form, $modelname, $permission);

        if (is_array($result)) {
            $result['table_names'] = [Table::LEAD => 'Lead',Table::CONTACT => 'Contact'];
        } elseif (!is_array($result) && $result->getVariable('status')) {
            $redirect_action = $this->getRequest()->getPost()['redirect_action'];
            if ($redirect_action == 'save_and_new') {
                $result->setVariable('uri', $this->url()->fromRoute('activity', [ 'action' => 'add', 'activity' => $activity ]));
            }
        }

        return $result;
    }

    public function viewAction()
    {
        $this->getPermission('data:Activity');
        $id       = (string) $this->params()->fromRoute('id', 0);
        $activity = $this->table('Activity')->get($id);

        return $this->view(Table::getTransportName((string) $activity->type_id));
    }

    public function rowscountAction()
    {
        $results = $this->RowsCount(Table::ACTIVITY);
//        return $this -> redirect() -> toRoute( 'activity', $results );
        return $this->redirect()->toUrl($results['saurlback']);
    }
}
