<?php

namespace Wepo\Controller;

use Wepo\Lib\WepoController;
use Wepo\Model\Table;
use Zend\EventManager\EventManagerInterface;
use ModelFramework\AuthService\AuthService as Auth;
use Wepo\Model\Status;

class QuoteDetailController extends WepoController
{
    public function setEventManager(EventManagerInterface $eventManager)
    {
        $dm = $this->getServiceLocator()->get('ModelFramework\LogicService');
        $eventManager->attach('prerecycle', array( $dm, 'dispatch' ), 100);
        $eventManager->attach('recycle', array( $dm, 'dispatch' ), 100);
        $eventManager->attach('postrecycle', array( $dm, 'dispatch' ), 100);

        return parent::setEventManager($eventManager);
    }

    public function indexAction()
    {
        return $this->redirect()->toRoute('quotedetail', array( 'action' => 'list' ));
    }

    protected function listAction()
    {
        $quoteid                          = (string) $this->params()->fromRoute('quoteid', 0);
        if ($quoteid == 0) {
            return $this->redirect()->toRoute('quote');
        }
        $results                          = $this->getListing('QuoteDetail', [ 'quote_id' => $quoteid ]);
        $results[ 'params' ][ 'quoteid' ] = $quoteid;
        $results[ 'field_labels' ]        = $this->fields([ 'table_id' => Table::QUOTE, 'target' => 'list' ])[ 'labels' ];
        $results[ 'master' ]               = $this->table('Quote')->findOne([ '_id' => $quoteid ]);
        $results[ 'currentRoute' ]        =
            $this->url()->fromRoute($results[ 'modelname' ], [ 'action' => 'list', 'quoteid' => $quoteid ]);

        $results[ 'masterRoute' ]         = 'quote';
        $results[ 'details' ] = 'Product';

        return $results;
    }

    public function addAction()
    {
        $permission = $this->getPermission('data:Quote');
        $quoteid    = (string) $this->params()->fromRoute('quoteid', 0);

        $globalPricebook = $this->table('Quote')->find([ '_id' => $quoteid ])->current()->pricebook_id;
        $globalPricebook =
            $this->table('Pricebook')->find([ '_id' => $globalPricebook, '-status_id' => [ Status::DELETED ] ])
                ->current() ? $globalPricebook : '0';
        $pricebookId     = $this->getRequest()->getPost('fields', null);
        $pricebookId     = is_null($pricebookId) ? $globalPricebook : $pricebookId[ 'pricebook_id' ];

        if ($this->getRequest()->isXmlHttpRequest() && ($this->getRequest()->getPost('action', null) == 'selectize')) {
            //set product hash
            $productsHash = [ '0' => 'Please Select ... ' ];
            if ($pricebookId == '0') {
                $includedProducts          = array();
                $quoteProducts             =
                    $this->table('QuoteDetail')->find([ 'quote_id' => $quoteid ])->toArray();
                $condition[ '-_id' ]       = [ ];
                $condition[ '-status_id' ] = Status::DELETED;
                foreach ($quoteProducts as $value) {
                    array_push($condition[ '-_id' ], $value[ 'product_id' ]);
                }
                $condition[ '-_id' ] = array_values(array_unique($condition[ '-_id' ]));

                //add global pricebook prices or default price
                $includedProducts = $this->table('Product')->find($condition);
                foreach ($includedProducts as $value) {
                    $productPrices[ $value->id() ] = $value->price;
                    $productsHash[ $value->id() ]  = $value->name;
                }
            } else {
                $quoteProducts       = $this->table('QuoteDetail')->find([ 'quote_id' => $quoteid ]);
                $deletedProducts     = $this->table('Product')->find([ 'status_id' => Status::DELETED ]);
                $condition[ '-_id' ] = [ ];
                foreach ($quoteProducts as $value) {
                    array_push($condition[ '-_id' ], (string) $value->product_id);
                }
                $var1 = $condition['-_id'];
                foreach ($deletedProducts as $value) {
                    array_push($condition[ '-_id' ], $value->id());
                }
                $condition[ '-_id' ] = array_values(array_unique($condition[ '-_id' ]));
//                $results = new \Zend\View\Model\JsonModel( [ 'pricebook_id' => $pricebookId,
//                    '-product_id'  => $condition[ '-_id' ] ] );
//                $results->setTemplate( '/' );
//
//                return $results;
                $pricebookDetails    = $this->table('PricebookDetail')->find([
                    'pricebook_id' => $pricebookId,
                    '-product_id'  => $condition[ '-_id' ],
                ])->toArray();
                $products[ '_id' ]   = [ ];
                foreach ($pricebookDetails as $key => $value) {
                    $products[ '_id' ][ $key ]                        = $value[ 'product_id' ];
                    $productPrices[ (string) $value[ 'product_id' ] ] = $value[ 'final_price' ];
                }
                ksort($productPrices);
                if ($products = $this->table('Product')->find($products)) {
                    foreach ($products as $value) {
                        $productsHash[ $value->id() ] = $value->name;
                    }
                }
                ksort($productsHash);
            }
            foreach ($productsHash as $key => $value) {
                $newProductHash[ ] = [ 'id' => $key, 'name' => $value ];
            }

            $results = new \Zend\View\Model\JsonModel([ 'names' => $newProductHash, 'prices' => $productPrices ]);
            $results->setTemplate('/');

            return $results;
        }

        $form = $this->form('QuoteDetailForm');
        $form->setRoute('quotedetail')->setAction('add')->setActionParams([
            'id' => null, 'quoteid' => $quoteid,
        ]);

        //set pricebook hash
        if ($permission == Auth::ALL) {
            $pricebookSet = $this->table('Pricebook')->find([ '-status_id' => Status::DELETED ]);
        } else {
            $pricebookSet = $this->table('Pricebook')->find([
                '-status_id' => Status::DELETED,
                'owner_id'   => $this->user()->id(),
            ]);
        }
        $pricebookHash = [ '0' => "No pricebook" ];
        foreach ($pricebookSet as $pricebook) {
            $pricebookHash[ $pricebook->id() ] = $pricebook->pricebook;
        }

        $form->getFieldsets()[ 'fields' ]->get('pricebook_id')->setOptions(
            array(
                'label'         => 'Pricebook',
                'value_options' => $pricebookHash,
            )
        )->setValue($globalPricebook);
//        $form->setAttribute('class','validate');
        $results = $this->add($form, 'QuoteDetail', $permission);
        if (is_array($results)) {
            $back = $this->params()
                ->fromQuery('back', $this->getRequest()->getPost('saurl', [ 'back' => 'home' ])[ 'back' ]);
            $form->getFieldsets()[ 'saurl' ]->get('back')->setValue($back);
            $results[ 'saurl' ]     = '?back='.$back;
            $results[ 'saurlback' ] = $this->getSaurlBack($back);
            $results[ 'quote_id' ]  = $quoteid;
//            $results[ 'prices' ]    = $productPrices;
        } elseif ($results->getVariable('status')) {
            $redirect_action = $this->getRequest()->getPost()['redirect_action'];
            if ($redirect_action == 'save') {
                $results->setVariable('uri', $this->url()->fromRoute('quotedetail', [ 'action' => 'list', 'quoteid' => $quoteid ]));
            } elseif ($redirect_action == 'save_and_new') {
                $results->setVariable('uri', $this->url()->fromRoute('quotedetail', [ 'action' => 'add', 'quoteid' => $quoteid ]));
            }
        }
//        exit;

        return $results;
    }

    public function editAction()
    {
        $permission = $this->getPermission('data:Quote');
        $quoteid    = (string) $this->params()->fromRoute('quoteid', 0);
        $id         = (string) $this->params()->fromRoute('id', 0);
        $form       = $this->form('QuoteDetailEditForm');
        $form->setRoute('quotedetail')->setAction('edit')->setActionParams([
            'id' => $id, 'quoteid' => $quoteid,
        ]);
        $results = $this->edit($form, 'QuoteDetail', $permission);

        $product_id                           = $this->table('QuoteDetail')->get($id)->toArray()[ 'product_id' ];
        $product                              = $this->table('Product')->get($product_id)->toArray();
        $removedPricebooks[ '-pricebook_id' ] = [ ];
        if ($permission == Auth::ALL) {
            $removedPricebooksSet = $this->table('Pricebook')->find([ 'status_id' => Status::DELETED ]);
        } else {
            $removedPricebooksSet  = $this->table('Pricebook')->find([ 'status_id' => Status::DELETED ]);
            $removedPricebooksSet1 = $this->table('Pricebook')->find([ '-owner_id' => $this->user()->id() ]);

            foreach ($removedPricebooksSet1 as $pricebook) {
                array_push($removedPricebooks[ '-pricebook_id' ], $pricebook->id());
            }
        }
        foreach ($removedPricebooksSet as $pricebook) {
            array_push($removedPricebooks[ '-pricebook_id' ], $pricebook->id());
        }

        $removedPricebooks[ 'product_id' ] = $product_id;
        $pricebooks_set                    = $this->table('PricebookDetail')->gather($removedPricebooks);
        $pricebooks                        = [ ];
        $prices                            = [ ];
        foreach ($pricebooks_set as $detail) {
            $pricebooks[ (string) $detail->pricebook_id ] = $detail->pricebook;
            $prices[ (string) $detail->pricebook_id ]     = $detail->result_price;
        }
        $pricebooks[ '0' ] = 'Please select ... ';
        $prices[ '0' ]     = $product[ 'price' ];

        $form->getFieldsets()[ 'fields' ]->get('pricebook_id')->setOptions(
            array(
                'label'         => 'Pricebook',
                'value_options' => $pricebooks,
            )
        );

        if (is_array($results)) {
            $results[ 'name' ]     = $product[ 'name' ];
            $results[ 'quote_id' ] = $quoteid;
            $results[ 'prices' ]   = $prices;
            $results[ 'saurlback' ] = $this->url()->fromRoute('quotedetail', [ 'action' => 'view', 'quoteid' => $quoteid, 'id' => $id ]);
        } elseif ($results->getVariable('status')) {
            $redirect_action = $this->getRequest()->getPost()['redirect_action'];
            if ($redirect_action == 'save') {
                $results->setVariable('uri', $this->url()->fromRoute('quotedetail', [ 'action' => 'list', 'quoteid' => $quoteid ]));
            } elseif ($redirect_action == 'save_and_new') {
                $results->setVariable('uri', $this->url()->fromRoute('quotedetail', [ 'action' => 'add', 'quoteid' => $quoteid ]));
            }
        }

        return $results;
    }

    public function deleteAction()
    {
        return $this->recycle('QuoteDetail');
    }

    public function viewAction()
    {
        $modelname  = 'QuoteDetail';
        $permission = $this->getPermission('data:'.$modelname);
        $id         = (string) $this->params()->fromRoute('id', 0);
        $quote_id   = (string) $this->params()->fromRoute('quoteid', 0);
        $_atts      = [ '_id' => $id, 'quote_id' => $quote_id ];
//        if ( $permission == Auth::OWN )// we should add owner_id in every created model and uncomment it.
//        {
//            $_atts[ 'owner_id' ] = $this -> user() -> id();
//        }
        $model = $this->table($modelname)->gather($_atts)->current();
        if (!$model) {
            return $this->showerror('Data not found', $this->url()->fromRoute('dashboard'));
        }
        $results[ 'field_labels' ]   = $this->fields([ 'table_id' => Table::QUOTE_DETAIL ])[ 'labels' ];
        $results[ 'model' ]          = $model;
        $results[ 'table_id' ]       = Table::QUOTE_DETAIL;
        $results[ 'params' ][ 'id' ] = $id;
        $results[ 'saurl' ]          = '?back='.$this->params()->fromQuery('back', 'home');
        $results[ 'saurlback' ]      = $this->getSaurlBack($this->params()->fromQuery('back', 'home'));
        $results[ 'user' ]           = $this->user();

//        $results = parent::view($modelname);
        return $results;
    }

    public function rowscountAction()
    {
        $results = $this->RowsCount(Table::QUOTE_DETAIL);

        return $this->redirect()->toUrl($results[ 'saurlback' ]);
    }
}
