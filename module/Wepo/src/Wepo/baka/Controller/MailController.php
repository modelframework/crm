<?php

namespace Wepo\Controller;

use Wepo\Lib\WepoController;
use Wepo\Model\Table;
use Zend\EventManager\EventManagerInterface;
use Wepo\Model\Status;
//////
use Zend\Mail\Message;

class MailController extends WepoController
{
    public function setEventManager(EventManagerInterface $eventManager)
    {
        return parent::setEventManager($eventManager);
    }

    public function indexAction()
    {
        return $this->redirect()->toRoute('mail', array( 'action' => 'list' ));
    }

    public function syncAction()
    {
        //        prn('done');
//        exit;
//        $settings = $this->checkMailSettingExist( 'sync', $this->user() );
        $count = 0;
//        /*
        try {
            $count = $this->syncMails();
        } catch (\Exception $ex) {
            //            throw $ex;
            $count = 0;
        }
        exit;
//        /*/
        $this->updateMailChains();
//        exit;
         /**/
//        return $this -> refresh( $count . ' mails was successfully add', $this -> url() -> fromRoute( 'mail', ['action' => 'list' ] ) );
    }

    public function sendAction()
    {
        $settings   = $this->checkMailSettingExist('send');
        $inReplyTo  = $this->getParam('inReplyTo', 0);
        $inReplyTo = $this->table('Mail')->find(['_id' => $inReplyTo ])->current();
        $to         = $this->getParam('to', 0);
        $to = $this->table('Email')->find(['_id' => $to ])->current();
        $title = '';
        $email = '';
        if (!empty($inReplyTo)) {
            $title = $inReplyTo->title;
            if (strpos(strtolower($title), 're:') !== false) {
                $title = 'Re:'.$title;
            }
            $email = $inReplyTo->from;
        }

        if (!empty($to)) {
            $email = empty($email) ? '' : $email.', ';
            $email .= $to->user_name.' <'.$to->email.'>';
        }

        if (!count($settings)) {
            $back_url       = '';
            $back_url_title = '';
            if (isset($_SERVER[ 'HTTP_REFERER' ])) {
                $back_url       = $_SERVER[ 'HTTP_REFERER' ];
                $back_url_title = explode('/', str_replace('http://'.$_SERVER[ 'HTTP_HOST' ].'/', '', $back_url))[ 0 ];
            }

            $renderer = $this->getServiceLocator()->get('ZfcTwigRenderer');
            echo $renderer->render('wepo/partial/error.twig', array( 'message' => 'You don\'t have settings to use sending functionality, tune it', 'tourl' => $back_url, 'tourl_name' => $back_url_title, 'seconds' => 1 ));
            exit();
        }

        $form       = $this->form('MailForm');
//        $form -> setRoute( 'mail' ) -> setAction( 'send' );
        $form->getFieldsets()[ 'fields' ]->get('to')->setValue($email);
        $form->getFieldsets()[ 'fields' ]->get('title')->setValue($title);
        $form->getFieldsets()[ 'button' ]->get('submit')->setValue('Send');
        $defaultSetting = $this->table('MailSendSetting')->findOne(['user_id' => $this->user()->id(), 'is_default' => 'true', 'status_id' => [ Status::NORMAL, Status::NEW_ ]]);
        $defaultSetting = isset($defaultSetting) ? $defaultSetting->id() : '0';
        $form->getFieldsets()[ 'fields' ]->get('setting_id')->setOptions(
            array(
                'value_options' => $this->trueTableHash('MailSetting', '_id', 'email', [ 'user_id' => $this->user()->id(), 'setting_protocol' => \Wepo\Model\MailSetting::sendProtocols(), 'status_id' => [ Status::NORMAL, Status::NEW_ ] ], empty($defaultSetting)),
            )
        )->setValue($defaultSetting);

        return $this->send($form, 'Mail');
    }

    public function send($form, $transport, $permission = 2)
    {
        $mail              = $this->model($transport);
        $results           = [ ];
        $results[ 'user' ] = $this->user();
        $request           = $this->getRequest();
        if ($request->isPost()) {
            $form->updateInputFilter($mail->getInputFilter(), 'fields');
            $form->setData($request->getPost());
            if ($form->isValid()) {
                try {
                    $setting    = $this->table('MailSetting')->get($form->getData()[ 'fields' ][ 'setting_id' ]);
                    $model_data = array();
                    foreach ($form->getData() as $_k => $_data) {
                        $model_data += is_array($_data) ? $_data : array( $_k => $_data );
                    }
                    $model_data ['header']['to'] = $model_data['to'];
                    $model_data ['header']['subject'] = $model_data['title'];
                    $mail->merge($model_data);

                    $inReplyTo = $this->getParam('inReplyTo', 0);
                    $inReplyTo = $this->table($transport)->find(['_id' => $inReplyTo ])->current();

                    if (!empty($inReplyTo)) {
                        $mail->inReplyTo = $inReplyTo->header_id;
                    }

                    $this->trigger('presend', $mail);
                    $mail->postExchange();
//                    prn($mail);
                    $sender = $this->mail($setting);
                    $sender->sendMail($mail);
//                    prn($mail);
                } catch (\Exception $ex) {
                    //                    throw $ex;
                    $results[ 'message' ] = 'Error.'.$ex->getMessage();
                }
                if (!isset($results[ 'message' ]) || !strlen($results[ 'message' ])) {
                    try {
                        $this->table($transport)->save($mail);
                    } catch (\Exception $ex) {
                        $results[ 'message' ] = 'Invalid input data.'.$ex->getMessage();
                    }
                }
                if (!isset($results[ 'message' ]) || !strlen($results[ 'message' ])) {
                    $this->trigger('postsave', $mail);
                    $this->updateMailChains();

                    return $this->refresh($transport.' mail was successfully send', $this->getSaurlBack($this->params()->fromQuery('back', 'home')), 100000);
                }
            } else {
                $results[ 'message' ] = 'Invalid input data.';
            }
        }
        $results[ 'form' ]       = $form;
        $results[ 'modelname' ]  = strtolower($mail->getModelName());
        $results[ 'permission' ] = $permission;
        $results[ 'saurl' ]      = '?back='.$this->params()->fromQuery('back', 'home');
        $results[ 'saurlback' ]  = $this->getSaurlBack($this->params()->fromQuery('back', 'home'));

        $form->setAttribute('action', $this->url()->fromRoute('mail', ['action' => 'send', 'inReplyTo' => $this->getParam('inReplyTo', 0) ]).$results[ 'saurl' ]);
        $form->prepare();

        return $results;
    }
}
