<?php

namespace Wepo\Controller;

use Wepo\Lib\WepoController;
use Wepo\Model\Table;
use Wepo\Model\Status;
use ModelFramework\AuthService\AuthService as Auth;

class PricebookController extends WepoController
{
    public function indexAction()
    {
        return $this->redirect()->toRoute('pricebook', array( 'action' => 'list' ));
    }

    protected function listAction()
    {
        return $this->getListing('Pricebook', [ 'status_id' => [ Status::NEW_, Status::NORMAL ] ]);
    }

    protected function recyclelistAction()
    {
        $results                         = $this->getListing('Pricebook', [ 'status_id' => [ Status::DELETED ] ]);
        $results[ 'params' ][ 'action' ] = 'recyclelist';

        return $results;
    }

    public function productAction()
    {
        $permission           = $this->getPermission('data:Pricebook');
        $product_id           = $this->params('product');
        $pricebookHash        = $this->trueTableHash('Pricebook', '_id', 'pricebook');
        $containingPricebooks = $this->table('PricebookDetail')->find([ 'product_id' => $product_id ]);
        foreach ($containingPricebooks as $pbDetail) {
            unset($pricebookHash[ (string) $pbDetail->pricebook_id ]);
        }

        $form = $this->form('PricebookProductForm');
        $form->setRoute('pricebook')->setAction('product')->setActionParams([
                                                                                     'product' => $product_id,
                                                                                     'back'    => $this->params()
                                                                                                       ->fromQuery('back',
                                                                                                                    'home'),
                                                                                 ]);
        $form->getFieldsets()[ 'fields' ]->get('pricebook_id')->setOptions(
                                         array(
                                             'value_options' => $pricebookHash,
                                         )
        );
        $form->getFieldsets()[ 'fields' ]->get('product_price')->setValue($this->table('Product')
                                                                                  ->get($product_id)->price);

        return $this->addProduct($form, 'PricebookDetail', $permission);
    }

    public function addAction()
    {
        $permission = $this->getPermission('data:Pricebook');
        $form       = $this->form('PricebookForm');
        $form->setRoute('pricebook')->setAction('add')->setActionParams([ 'id' => null ]);
        if ($permission == Auth::ALL) {
            $form->getFieldsets()[ 'fields' ]->add(array(
                                                        'type'       => 'Zend\Form\Element\Select',
                                                        'name'       => 'owner_id',
                                                        'attributes' => array(
                                                            'id'       => 'iduser',
                                                            'disabled' => '',
                                                            'value'    => $this->user()->id(),
                                                        ),
                                                        'options'    => array(
                                                            'label'         => 'Owner',
                                                            'value_options' => $this->trueTableHash('User', '_id',
                                                                    ['fname', 'lname'], [
                                                                        'status_id' => [
                                                                            Status::NEW_, Status::NORMAL,
                                                                        ]
                                                                    ]),
                                                        ),
                                                    ));
            $form->addValidationField('fields', 'owner_id');
        }

        return $this->add($form, 'Pricebook', $permission);
    }

//    public function add( $form, $transport, $permission = 2 )
//    {
//        $model             = $this->model( $transport );
//        $results           = [ ];
//        $results[ 'user' ] = $this->user();
//        $form->setAttribute( 'action', $this->url()->fromRoute( $form->getRoute(), [ 'action' => $form->getAction() ] +
//                                                                                   $form->getActionParams() ) );
//        $request = $this->getRequest();
//        if ( $request->isPost() )
//        {
//            $form->updateInputFilter( $model->getInputFilter(), 'fields' );
//            $form->setData( $request->getPost() );
//            if ( $form->isValid() )
//            {
//                try
//                {
//                    $model_data = array();
//                    foreach ( $form->getData() as $_k => $_data )
//                    {
//                        $model_data += is_array( $_data ) ? $_data : array( $_k => $_data );
//                    }
//                    $model->merge( $model_data );
//                    $this->trigger( 'presave', $model );
//                    $model->postExchange();
//                }
//                catch ( \Exception $ex )
//                {
//                    $results[ 'message' ] = 'Error.' . $ex->getMessage();
//                }
//                if ( !isset( $results[ 'message' ] ) || !strlen( $results[ 'message' ] ) )
//                {
//                    try
//                    {
//                        $tr = $this->table( $transport );
//                        $tr->save( $model );
//                        $model->setId( $tr->getLastInsertId() );
//                    }
//                    catch ( \Exception $ex )
//                    {
//                        $results[ 'message' ] = 'Invalid input data.' . $ex->getMessage();
//                    }
//                }
//                if ( !isset( $results[ 'message' ] ) || !strlen( $results[ 'message' ] ) )
//                {
//                    $this->trigger( 'postsave', $model );
//                    $url = $this->getBackUrl();
//                    if ( $url == null || $url == '/' )
//                    {
//                        $url = $this->url()
//                                    ->fromRoute( $form->getRoute(),
//                                                 [ 'action' => 'view', 'id' => $model -> id() ] );
//                    }
//
//                    return $this->refresh( $transport . ' data has been added successfully', $url );
//
//                }
//            }
//            else
//            {
//                $results[ 'message' ] = 'Invalid input data.';
//            }
//        }
//        $form->prepare();
//        $results[ 'form' ]       = $form;
//        $results[ 'modelname' ]  = strtolower( $model->getModelName() );
//        $results[ 'permission' ] = $permission;
//        $results[ 'saurl' ]      = '?back=' . $this->generateLabel();
//        $results[ 'saurlback' ]  = $this->getSaurlBack( $this->params()->fromQuery( 'back', 'home' ) );
//        if ( isset( $form->getFieldsets()[ 'saurl' ] ) )
//        {
//            $form->getFieldsets()[ 'saurl' ]->get( 'back' )->setValue( $this->params()->fromQuery( 'back', 'home' ) );
//        }
//
//        return $results;
//
//    }

    public function deleteAction()
    {
        return $results = $this->recycle('Pricebook');
    }

    public function cleanAction()
    {
        return $results = $this->recycle('Pricebook');
    }

    public function restoreAction()
    {
        return $results = $this->recycle('Pricebook');
    }

    public function viewAction()
    {
        //        return $this -> view( 'Pricebook' );
        return $this->redirect()->toRoute('pricebookdetail', array(
            'action' => 'list', 'pricebookid' => $this->params()->fromRoute('id', 0),
        ));
    }

    public function editAction()
    {
        $permission = $this->getPermission('data:Pricebook');
        $form       = $this->form('PricebookForm');
        $form->setRoute('pricebook')->setAction('edit')->setActionParams([
                                                                                  'id' => (string) $this->params()
                                                                                                        ->fromRoute('id',
                                                                                                                     0),
                                                                              ]);

        return $this->edit($form, 'Pricebook', $permission);
    }

    public function rowscountAction()
    {
        $results = $this->RowsCount(Table::PRICEBOOK);
//        return $this->redirect()->toRoute( 'pricebook', $this->RowsCount( Table::PRICEBOOK ) );
        return $this->redirect()->toUrl($results['saurlback']);
    }

    public function addProduct($form, $transport, $permission = 2)
    {
        $model             = $this->model($transport);
        $results           = [ ];
        $results[ 'user' ] = $this->user();
        $form->setAttribute('action', $this->url()->fromRoute($form->getRoute(), [ 'action' => $form->getAction() ] +
                                                                                   $form->getActionParams()));
        $request = $this->getRequest();
        if ($request->isPost()) {
            //            prn('heare');
//            prn($model->__get('_filters'));
//            exit();
            $form->updateInputFilter($model->getInputFilter(), 'fields');
            $form->setData($request->getPost());
            if ($form->isValid()) {
                try {
                    $model_data = array();
                    foreach ($form->getData() as $_k => $_data) {
                        $model_data += is_array($_data) ? $_data : array( $_k => $_data );
                    }
                    $model->merge($model_data);
                    $this->trigger('presave', $model);
                    $model->postExchange();
                } catch (\Exception $ex) {
                    $results[ 'message' ] = 'Error.'.$ex->getMessage();
                }
                if (!isset($results[ 'message' ]) || !strlen($results[ 'message' ])) {
                    try {
                        $this->table($transport)->save($model);
                    } catch (\Exception $ex) {
                        $results[ 'message' ] = 'Invalid input data.'.$ex->getMessage();
                    }
                }
                if (!isset($results[ 'message' ]) || !strlen($results[ 'message' ])) {
                    $this->trigger('postsave', $model);

                    return $this->refresh($transport.' data was successfully add',
                                           $this->getSaurlBack($this->params()->fromQuery('back', 'home')));
                }
            } else {
                $results[ 'message' ] = 'Invalid input data.';
            }
        }
        $form->prepare();
        $results[ 'form' ]       = $form;
        $results[ 'modelname' ]  = strtolower($model->getModelName());
        $results[ 'modelLabel' ]  = $model->getModelLabel();
        $results[ 'permission' ] = $permission;
        $results[ 'saurl' ]      = '?back='.$this->generateLabel();
        $results[ 'saurlback' ]  = $this->getSaurlBack($this->params()->fromQuery('back', 'home'));

        return $results;
    }
}
