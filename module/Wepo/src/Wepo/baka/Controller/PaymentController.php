<?php

namespace Wepo\Controller;

use Wepo\Lib\WepoController;
use Wepo\Model\Table;
use ModelFramework\AuthService\AuthService as Auth;
use Wepo\Model\Status;

class PaymentController extends WepoController
{
    public function indexAction()
    {
        return $this->redirect()->toRoute('payment', array( 'action' => 'list' ));
    }

    protected function listAction()
    {
        return $this->getListing('Payment');
    }

    public function addAction()
    {
        $permission = $this->getPermission('data:Payment');
        $form       = $this->form('PaymentForm');
        $form->setRoute('payment')->setAction('add')->setActionParams([ 'id' => null ]);
        if ($permission == Auth::ALL) {
            $form->getFieldsets()[ 'fields' ]->add(array(
                'type'       => 'Zend\Form\Element\Select',
                'name'       => 'owner_id',
                'attributes' => array(
                    'id'       => 'iduser',
                    'disabled' => '',
                    'value'         => $this->user()->id(),
                ),
                'options'    => array(
                    'label'         => 'Owner',
                    'value_options' => $this->trueTableHash('User', '_id', ['fname', 'lname'], ['status_id' => [  Status::NEW_,  Status::NORMAL]]),
                ),
            ));
            $form->addValidationField('fields', 'owner_id');
        }
        $form->getFieldsets()[ 'fields' ]->get('contact_id')->setOptions(
            [
                'value_options' => $this->trueTableHash('Contact', '_id', ['fname', 'lname'], ['status_id' => [  Status::NEW_,  Status::NORMAL]]),
            ]
        );
        $form->getFieldsets()[ 'fields' ]->get('contact_id')->setValue($this->params()->fromRoute('contact_id', null) ?: "0");

        return $this->add($form, 'Payment', $permission);
    }

    public function editAction()
    {
        $permission = $this->getPermission('data:Payment');
        $form       = $this->form('PaymentForm');
        $form->setRoute('payment')->setAction('edit')->setActionParams([ 'id' => $this->params()->fromRoute('id', 0) ]);
        if ($permission == Auth::ALL) {
            $form->getFieldsets()[ 'fields' ]->add(array(
                'type'       => 'Zend\Form\Element\Select',
                'name'       => 'owner_id',
                'attributes' => array(
                    'id'       => 'iduser',
                    'disabled' => '',
                ),
                'options'    => array(
                    'label'         => 'Owner',
                    'value_options' => $this->trueTableHash('User', '_id', ['fname', 'lname'], ['status_id' => [  Status::NEW_,  Status::NORMAL]]),
                ),
            ));
            $form->addValidationField('fields', 'owner_id');
        }
        $form->getFieldsets()[ 'fields' ]->get('contact_id')->setOptions(
            array(
                'label'         => 'Contact',
                'value_options' => $this->trueTableHash('Contact', '_id', ['fname', 'lname'], ['status_id' => [  Status::NEW_,  Status::NORMAL]]),
            )
        );

        return $this->edit($form, 'Payment', $permission);
    }

    public function viewAction()
    {
        return $this->view('Payment');
    }

    public function deleteAction()
    {
        return $this->recycle('Payment');
    }

    public function rowscountAction()
    {
        $results = $this->RowsCount(Table::PAYMENT);
//        return $this -> redirect() -> toRoute( 'payment', $this -> RowsCount( Table::PAYMENT ) );
        return $this->redirect()->toUrl($results['saurlback']);
    }
}
