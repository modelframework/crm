<?php

namespace Wepo\Controller;

use Wepo\Lib\WepoController;
use Wepo\Model\Table;
use Wepo\Model\Status;
use ModelFramework\AuthService\AuthService as Auth;

class ProductController extends WepoController
{
    public function indexAction()
    {
        return $this->redirect()->toRoute('product', array( 'action' => 'list' ));
    }

    protected function listAction()
    {
        return $this->getListing('Product', [
            'status_id' => [
                Status::NEW_, Status::NORMAL, Status::CONVERTED, Status::DEAD,
            ]
        ]);
    }

    protected function recyclelistAction()
    {
        $results                         = $this->getListing('product', [ 'status_id' => [ Status::DELETED ] ]);
        $results[ 'params' ][ 'action' ] = 'recyclelist';

        return $results;
    }

    public function deleteAction()
    {
        $permission = $this->getPermission('data:Product');
        $request    = $this->getRequest();
        $ids        = $request->getPost('checkedid', null);

        if (!is_array($ids)) {
            $id = $this->params()->fromRoute('id', 0);
            if ($id) {
                $ids = array( $id );
            } else {
                return $this->redirect()->toRoute('product');
            }
        }
        foreach ($ids as $id) {
            $owner = $this->table('Product')->get($id)->owner_id;
            if ($owner != $this->user()->id()) {
                $owner = false;
                break;
            }
        }
        if (($permission != Auth::ALL) && !$owner) {
            return $this->showerror('You don\'t have permission to delete that product. You don\'t own it.',
                                     $this->url()->fromRoute('product'));
        } else {
            return $results = $this->recycle('Product');
        }
    }

    public function cleanAction()
    {
        return $results = $this->recycle('Product');
    }

    public function restoreAction()
    {
        return $results = $this->recycle('Product');
    }

    public function addAction()
    {
        $permission = $this->getPermission('data:Product');
        $form       = $this->form('ProductForm');
        $form->setRoute('product')->setAction('add')->setActionParams([ 'id' => null ]);
        if ($permission == Auth::ALL) {
            $form->getFieldsets()[ 'fields' ]->add(array(
                                                        'type'       => 'Zend\Form\Element\Select',
                                                        'name'       => 'owner_id',
                                                        'attributes' => array(
                                                            'id'       => 'iduser',
                                                            'disabled' => '',
                                                            'value'    => $this->user()->id(),
                                                        ),
                                                        'options'    => array(
                                                            'label'         => 'Owner',
                                                            'value_options' => $this->trueTableHash('User', '_id',
                                                                    ['fname', 'lname'], [
                                                                        'status_id' => [
                                                                            Status::NEW_, Status::NORMAL,
                                                                        ]
                                                                    ]),
                                                        ),
                                                    ));
            $form->addValidationField('fields', 'owner_id');
        }

        return $this->add($form, 'Product', $permission);
    }

    public function editAction()
    {
        $permission = $this->getPermission('data:Product');
        $form       = $this->form('ProductForm');
        $form->setRoute('product')->setAction('edit')->setActionParams([
                                                                                'id' => (string) $this->params()
                                                                                                      ->fromRoute('id',
                                                                                                                   0),
                                                                            ]);
        if ($permission == Auth::ALL) {
            $form->getFieldsets()[ 'fields' ]->add(array(
                                                        'type'       => 'Zend\Form\Element\Select',
                                                        'name'       => 'owner_id',
                                                        'attributes' => array(
                                                            'id'       => 'iduser',
                                                            'disabled' => '',
                                                        ),
                                                        'options'    => array(
                                                            'label'         => 'Owner',
                                                            'value_options' => $this->trueTableHash('User', '_id',
                                                                    ['fname', 'lname'], [
                                                                        'status_id' => [
                                                                            Status::NEW_, Status::NORMAL,
                                                                        ]
                                                                    ]),
                                                        ),
                                                    ));
            $form->addValidationField('fields', 'owner_id');
        }

        return $this->edit($form, 'Product', $permission);
    }

    public function viewAction()
    {
        return $this->view('Product');
    }

    public function rowscountAction()
    {
        $results = $this->RowsCount(Table::PRODUCT);
//        return $this->redirect()->toRoute( 'product', $this->RowsCount( Table::PRODUCT ) );
        return $this->redirect()->toUrl($results['saurlback']);
    }
}
