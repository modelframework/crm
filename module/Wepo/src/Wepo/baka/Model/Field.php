<?php

namespace Wepo\Model;

use Zend\InputFilter\Factory as InputFactory;
use Zend\InputFilter\InputFilter;
use Wepo\Lib\WepoMongoModel;

class Field extends WepoMongoModel
{
    //    public $table_name = 'field';

//    const TABLE_NAME = 'field';

    protected $_fields = [
        '_id'      => [ 'type' => 'pk', 'datatype' => 'string', 'default' => 0 ],
        'table_id' => [ 'type' => 'field', 'datatype' => 'string', 'default' => 0 ],
        'field'    => [ 'type' => 'field', 'datatype' => 'string', 'default' => '' ],
        'visible'  => [ 'type' => 'field', 'datatype' => 'int', 'default' => 1 ],
        'order'    => [ 'type' => 'field', 'datatype' => 'int', 'default' => 0 ],
        'label'    => [ 'type' => 'field', 'datatype' => 'string', 'default' => '' ],
        'target'   => [ 'type' => 'field', 'datatype' => 'string', 'default' => '' ],
        'alias'    => [ 'type' => 'field', 'datatype' => 'string', 'default' => null ],
    ];
    protected $inputFilter;

    public function getInputFilter()
    {
        if (!$this->inputFilter) {
            $inputFilter = new InputFilter();
            $factory     = new InputFactory();

            $inputFilter->add($factory->createInput(array(
                    'name'     => 'id',
                    'required' => true,
                    'filters'  => array(
                        array( 'name' => 'Int' ),
                    ),
            )));

            $inputFilter->add($factory->createInput(array(
                    'name'       => 'table_id',
                    'required'   => true,
                    'filters'    => array(
                        array( 'name' => 'Int' ),
                    ),
                    'validators' => array(
                        array(
                            'name'    => 'InArray',
                            'options' => array(
                                'haystack' => array(),
                            ),
                        ),
                    ),
            )));

            $inputFilter->add($factory->createInput(array(
                    'name'       => 'visible',
                    'required'   => true,
                    'filters'    => array(
                        array( 'name' => 'Int' ),
                    ),
                    'validators' => array(
                        array(
                            'name'    => 'InArray',
                            'options' => array(
                                'haystack' => array( 0, 1 ),
                            ),
                        ),
                    ),
            )));

            $inputFilter->add($factory->createInput(array(
                    'name'       => 'order',
                    'required'   => false,
                    'filters'    => array(
                        array( 'name' => 'Int' ),
                    ),
                    'validators' => array(
                        array(
                            'name'    => 'Digits',
                            'options' => array(),
                        ),
                        array(
                            'name'    => 'GreaterThan',
                            'options' => array( 'min' => 1 ),
                        ),
                    ),
            )));

            $inputFilter->add($factory->createInput(array(
                    'name'       => 'field',
                    'required'   => true,
                    'filters'    => array(
                        array( 'name' => 'StripTags' ),
                        array( 'name' => 'StringTrim' ),
                    ),
                    'validators' => array(
                        array(
                            'name'    => 'StringLength',
                            'options' => array(
                                'encoding' => 'UTF-8',
                                'min'      => 1,
                                'max'      => 100,
                            ),
                        ),
                    ),
            )));
            $inputFilter->add($factory->createInput(array(
                    'name'       => 'label',
                    'required'   => false,
                    'filters'    => array(
                        array( 'name' => 'StripTags' ),
                        array( 'name' => 'StringTrim' ),
                    ),
                    'validators' => array(
                        array(
                            'name'    => 'StringLength',
                            'options' => array(
                                'encoding' => 'UTF-8',
                                'min'      => 0,
                                'max'      => 100,
                            ),
                        ),
                    ),
            )));
            $this->inputFilter = $inputFilter;
        }

        return $this->inputFilter;
    }
}
