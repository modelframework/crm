<?php

namespace Wepo\Model;

use Wepo\Lib\WepoMongoModel;

class Permission extends WepoMongoModel
{
    public $adapterName = 'wepo_company';
    public $table_name  = 'Permission';
    protected $_fields  = [
        '_id'     => [ 'type' => 'pk', 'datatype' => 'string', 'default' => '' ],
        'model'   => [ 'type' => 'field', 'datatype' => 'string', 'default' => '' ],
        'role_id' => [ 'type' => 'field', 'datatype' => 'string', 'default' => '' ],
        'user_id' => [ 'type' => 'source', 'datatype' => 'string', 'default' => '', 'alias' => 'user_login', 'label' => 'User' ],
        'user_login' => [ 'type' => 'alias', 'datatype' => 'string', 'default' => '', 'source' => 'user_id', 'label' => 'User' ],
        'role_id' => [ 'type' => 'source', 'datatype' => 'string', 'default' => '', 'alias' => 'role_role', 'label' => 'Role' ],
        'role_role' => [ 'type' => 'alias', 'datatype' => 'string', 'default' => '', 'source' => 'role_id', 'label' => 'Role' ],
        'fields'  => [ 'type' => 'field', 'datatype' => 'array', 'default' => [ ] ],
    ];
}
