<?php

namespace Wepo\Model;

use Zend\InputFilter\Factory as InputFactory;
use Zend\InputFilter\InputFilter;
use Wepo\Lib\WepoMongoModel;

class Activity extends WepoMongoModel
{
    public $table_name = 'activity';

    const TABLE_NAME = 'activity';

    protected $_fields = [
        '_id'         => [ 'type' => 'pk', 'datatype' => 'string', 'default' => 0 ],
        'type_id'     => [ 'type' => 'source', 'datatype' => 'string', 'default' => '', 'alias' => 'type' ],
        'owner_id'    => [ 'type' => 'source', 'datatype' => 'string', 'default' => '', 'alias' => 'owner' ],
        "changer_id"  => [ "type" => "source", "datatype" => "string", "default" => "", "alias" => "changer" ],
        "changed_dtm" => [ "type" => "field", "datatype" => "string", "default" => "" ],
        'table_id'    => [ 'type' => 'source', 'datatype' => 'string', 'default' => '', 'alias' => 'table' ],
        'target_id'   => [ 'type' => 'source', 'datatype' => 'string', 'default' => '' ],
        'subject'     => [ 'type' => 'field', 'datatype' => 'string', 'default' => '' ],
        'created_dtm' => [ 'type' => 'field', 'datatype' => 'string', 'default' => '' ],
        'remind_dtm'  => [ 'type' => 'field', 'datatype' => 'string', 'default' => '' ],
        'target'      => [ 'type' => 'alias', 'datatype' => 'string', 'default' => '', 'source' => 'target_id' ],
        'type'        => [ 'type' => 'alias', 'datatype' => 'string', 'default' => '', 'source' => 'type_id' ],
        'owner'       => [ 'type' => 'alias', 'datatype' => 'string', 'default' => '', 'source' => 'owner_id' ],
        'table'       => [ 'type' => 'alias', 'datatype' => 'string', 'default' => '', 'source' => 'table_id' ],
        'ownerfname'  => [ 'type' => 'alias', 'datatype' => 'string', 'default' => '', 'source' => 'owner_id' ],
        'ownerlname'  => [ 'type' => 'alias', 'datatype' => 'string', 'default' => '', 'source' => 'owner_id' ],
        'changerfname'  => [ 'type' => 'alias', 'datatype' => 'string', 'default' => '', 'source' => 'changer_id' ],
        'changerlname'  => [ 'type' => 'alias', 'datatype' => 'string', 'default' => '', 'source' => 'changer_id' ],
        'title'        => [ 'type' => 'alias', 'datatype' => 'string', 'default' => '' ],
    ];
    protected $_joins = [
        [ 'model' => 'Table', 'on' => [ 'type_id' => '_id' ], 'fields' => [ 'type' => 'label' ] ],
        [ 'model'  => 'User', 'on' => [ 'owner_id' => '_id' ],
          'fields' => [ 'ownerfname' => 'fname', 'ownerlname' => 'lname' ]
        ],
        [ 'model'  => 'User', 'on' => [ 'changer_id' => '_id' ],
          'fields' => [ 'changerfname' => 'fname', 'changerlname' => 'lname' ]
        ],
        [ 'model' => 'Table', 'on' => [ 'table_id' => '_id' ], 'fields' => [ 'table' => 'table' ] ],
    ];
    protected $inputFilter;

    public function toWidgetArray()
    {
        return array(
            'type_id'    => $this->type_id,
            'subject'    => $this->subject,
            'remind_dtm' => $this->remind_dtm,
        );
    }

    public function getInputFilter()
    {
        if (!$this->inputFilter) {
            $inputFilter = new InputFilter();
            $factory     = new InputFactory();

            $inputFilter->add($factory->createInput(array(
                                                          'name'     => 'type_id',
                                                          'required' => false,
                                                          'filters'  => array(
                                                              array( 'name' => 'StripTags' ),
                                                              array( 'name' => 'StringTrim' ),
                                                          ),
                                                      )));
            $inputFilter->add($factory->createInput(array(
                                                          'name'     => 'owner_id',
                                                          'required' => false,
                                                          'filters'  => array(
                                                              array( 'name' => 'StripTags' ),
                                                              array( 'name' => 'StringTrim' ),
                                                          ),
                                                      )));
            $inputFilter->add($factory->createInput(array(
                                                          'name'     => 'table_id',
                                                          'required' => false,
                                                          'filters'  => array(
                                                              array( 'name' => 'StripTags' ),
                                                              array( 'name' => 'StringTrim' ),
                                                          ),
                                                      )));
            $inputFilter->add($factory->createInput(array(
                                                          'name'     => 'target_id',
                                                          'required' => true,
                                                          'filters'  => array(
                                                              array( 'name' => 'StripTags' ),
                                                              array( 'name' => 'StringTrim' ),
                                                          ),
//                                                          'validators' => array(
//                                                              array(
//                                                                  'name'    => 'GreaterThan',
//                                                                  'options' => array(
//                                                                      'min'       => 1,
//                                                                      'inclusive' => true,
//                                                                      'messages'  => array( \Zend\Validator\GreaterThan::NOT_GREATER_INCLUSIVE => 'select item' ),
//                                                                  )
//                                                              )
//                                                          ),
                                                      )));
            $inputFilter->add($factory->createInput(array(
                                                          'name'       => 'subject',
                                                          'required'   => false,
                                                          'filters'    => array(
                                                              array( 'name' => 'StripTags' ),
                                                              array( 'name' => 'StringTrim' ),
                                                          ),
                                                          'validators' => array(
                                                              array(
                                                                  'name'    => 'StringLength',
                                                                  'options' => array(
                                                                      'encoding' => 'UTF-8',
                                                                      'min'      => 3,
                                                                      'max'      => 100,
                                                                  ),
                                                              ),
                                                          ),
                                                      )));
            $inputFilter->add($factory->createInput(array(
                                                          'name'       => 'created_dtm',
                                                          'required'   => false,
                                                          'filters'    => array(
                                                              array( 'name' => 'StripTags' ),
                                                              array( 'name' => 'StringTrim' ),
                                                          ),
                                                          'validators' => array(
                                                              array(
                                                                  'name'    => 'StringLength',
                                                                  'options' => array(
                                                                      'encoding' => 'UTF-8',
                                                                      'min'      => 3,
                                                                      'max'      => 100,
                                                                  ),
                                                              ),
                                                          ),
                                                      )));
            $inputFilter->add($factory->createInput(array(
                                                          'name'       => 'remind_dtm',
                                                          'required'   => false,
                                                          'filters'    => array(
                                                              array( 'name' => 'StripTags' ),
                                                              array( 'name' => 'StringTrim' ),
                                                          ),
                                                          'validators' => array(
                                                              array(
                                                                  'name'    => 'StringLength',
                                                                  'options' => array(
                                                                      'encoding' => 'UTF-8',
                                                                      'min'      => 3,
                                                                      'max'      => 100,
                                                                  ),
                                                              ),
                                                          ),
                                                      )));
            $this->inputFilter = $inputFilter;
        }

        return $this->inputFilter;
    }

//    public function setInputFilter(InputFilterInterface $inputFilter)
//    {
//        $this ->
//    }

    public function getName()
    {
        return $this->subject;
    }
}
