<?php

namespace Wepo\Model;

use Zend\InputFilter\Factory as InputFactory;
use Zend\InputFilter\InputFilter;
use Wepo\Lib\WepoMongoModel;

class EventLog extends WepoMongoModel
{
    public $table_name = 'event_log';

    const TABLE_NAME = 'event_log';

    protected $_fields    = [
         '_id'         => [ 'type' => 'pk', 'datatype' => 'string', 'default' => '' ],
        'id'          => [ 'type' => 'field', 'datatype' => 'string', 'default' => '' ],
        'created_dtm'   => [ 'type'     => 'field', 'datatype' => 'string', 'default'  => '' ],
        'executor_id' => [ 'type'     => 'source', 'datatype' => 'string', 'default'  => '', 'alias'   => 'executor' ],
        'table_id'    => [ 'type'     => 'source', 'datatype' => 'string', 'default'  => '', 'alias'   => 'table' ],
        'target_id'   => [ 'type'     => 'field', 'datatype' => 'string', 'default'  => '' ],
        'event_id'    => [ 'type'     => 'source', 'datatype' => 'int', 'default'  => 0,  'alias'   => 'event' ],

        'executor'    => [ 'type'     => 'alias', 'datatype' => 'string', 'default'  => '', 'source'   => 'executor_id' ],
        'executorfname' => [ 'type' => 'alias', 'datatype' => 'string', 'default' => '', 'source' => 'executor_id'],
        'executorlname' => [ 'type' => 'alias', 'datatype' => 'string', 'default' => '', 'source' => 'executor_id'],
        'table'       => [ 'type'     => 'alias', 'datatype' => 'string', 'default'  => '', 'source'   => 'executor_id'],
        'event'       => [ 'type'     => 'alias', 'datatype' => 'string', 'default'  => '', 'source' => 'event_id' ],
        'target'       => [ 'type'     => 'field', 'datatype' => 'string', 'default'  => ''],
    ];
    protected $_joins     = [
        [ 'model'  => 'User', 'on'     => [ 'executor_id' => '_id' ], 'fields' => [ 'executor' => 'login', 'executorfname' => 'fname', 'executorlname' => 'lname' ] ],
        [ 'model'  => 'Table', 'on'     => [ 'table_id' => '_id' ], 'fields' => [ 'table' => 'label' ] ],
        [ 'model'  => 'EventType', 'on'     => [ 'event_id' => 'id' ], 'fields' => [ 'event' => 'type' ] ],
    ];
    protected $_inputFilter;

    public static function ids()
    {
        return array( self::NEW_, self::NORMAL, self::CONVERTED, self::DEAD );
    }

    public function getInputFilter()
    {
        if (!$this->inputFilter) {
            $inputFilter = new InputFilter();
            $factory     = new InputFactory();
            $inputFilter->add($factory->createInput(array(
                    'name'       => 'id',
                    'required'   => true,
//                    'filters'    => array(
//                        array( 'name' => 'Int' ),
//                    ),
                    'validators' => array(
                        array(
                            'name'    => 'Digits',
                            'options' => array(
                            ),
                        ),
//                        array(
//                            'name'    => 'InArray',
//                            'options' => array(
//                                'hastack' => [self::ids()],
//                            ),
//                        ),
                    ),
            )));
            $this->inputFilter = $inputFilter;
        }

        return $this->inputFilter;
    }
}
