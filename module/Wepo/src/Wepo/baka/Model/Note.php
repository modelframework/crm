<?php

namespace Wepo\Model;

use Zend\InputFilter\Factory as InputFactory;
use Zend\InputFilter\InputFilter;
use Wepo\Lib\WepoMongoModel;

class Note extends WepoMongoModel
{
    public $table_name = 'note';

    const TABLE_NAME = 'note';

    protected $_fields = [
        '_id' => [ 'type' => 'pk', 'datatype' => 'string', 'default' => ''],
        'owner_id' => [ 'type' => 'source', 'datatype' => 'string', 'default' => '', 'alias' => 'owner'],
        'changer_id' => ['type' => 'source', 'datatype' => 'string', 'default' => '', 'alias' => 'changer'],
        'title' => ['type' => 'field', 'datatype' => 'string', 'default' => ''],
        'content' => ['type' => 'field', 'datatype' => 'string', 'default' => ''],
        'table_id' => ['type' => 'field', 'datatype' => 'string', 'default' => ''],
        'parent_id' => ['type' => 'field', 'datatype' => 'string', 'default' => ''],
        'created_dtm' => [ 'type' => 'field', 'datatype' => 'string', 'default' => ''],
        'changed_dtm' => [ 'type' => 'field', 'datatype' => 'string', 'default' => ''],
        'status_id' => [ 'type' => 'source', 'datatype' => 'string', 'default' => '', 'alias' => 'status'],
        'status' => [ 'type' => 'alias', 'datatype' => 'string', 'default' => '', 'source' => 'status_id'],
        'owner' => ['type' => 'alias', 'datatype' => 'string', 'default' => '', 'source' => 'owner_id'],
        'changer' => ['type' => 'alias', 'datatype' => 'string', 'default' => '', 'source' => 'changer_id'],
        'ownerfname' => [ 'type' => 'alias', 'datatype' => 'string', 'default' => '', 'source' => 'owner_id'],
        'ownerlname' => [ 'type' => 'alias', 'datatype' => 'string', 'default' => '', 'source' => 'owner_id'],
        'changerfname' => [ 'type' => 'alias', 'datatype' => 'string', 'default' => '', 'source' => 'changer_id'],
        'changerlname' => [ 'type' => 'alias', 'datatype' => 'string', 'default' => '', 'source' => 'changer_id'],
    ];
    protected $_joins = [
        [
            'model' => 'User', 'on' => [ 'owner_id' => '_id'], 'fields' => [ 'ownerfname' => 'fname', 'ownerlname' => 'lname'],
        ],
        [ 'model' => 'Status', 'on' => [ 'status_id' => '_id'], 'fields' => [ 'status' => 'status']],
        [
            'model' => 'User', 'on' => [ 'changer_id' => '_id'], 'fields' => [ 'changerfname' => 'fname', 'changerlname' => 'lname']
        ],
    ];
    protected $inputFilter;

    public function getInputFilter()
    {
        if (!$this->inputFilter) {
            $inputFilter = new InputFilter();
            $factory = new InputFactory();

            $inputFilter->add($factory->createInput(array(
                        'name' => 'owner_id',
                        'required' => true,
            )));

            $inputFilter->add($factory->createInput(array(
                        'name' => 'title',
                        'required' => false,
                        'validators' => array(
                            array(
                                'name' => 'StringLength',
                                'options' => array(
                                    'encoding' => 'UTF-8',
                                    'max' => 150,
                                ),
                            ),
                        ),
            )));
            $inputFilter->add($factory->createInput(array(
                        'name' => 'content',
                        'required' => true,
                        'validators' => array(
                            array(
                                'name' => 'StringLength',
                                'options' => array(
                                    'encoding' => 'UTF-8',
                                    'min' => 3,
                                    'max' => 500,
                                ),
                            ),
                        ),
            )));
            $this->inputFilter = $inputFilter;
        }

        return $this->inputFilter;
    }

    public function getName()
    {
        return $this->title;
    }

    public function getModelLabel()
    {
        return 'Note';
    }
}
