<?php

namespace Wepo\Model;

use Zend\InputFilter\InputFilter;
use Wepo\Lib\WepoMongoModel;

class Email extends WepoMongoModel
{
    public $table_name = 'email';

    static $link_rules = [
//        'User'=>[
//            'name' => ['lname', 'fname'],
//            'email' => 'email',
//            'priority' => 4,
//        ],
        'Contact' => [
            'name' => [ 'fname', 'lname' ],
            'email' => 'email',
            'priority' => 3,
        ],
        'Client' => [
            'name' => ['name'],
            'email' => 'email',
            'priority' => 2,
        ],
        'Lead' => [
            'name' => [ 'fname', 'lname' ],
            'email' => 'email',
            'priority' => 1,
        ],
    ];

    const TABLE_NAME = 'email';

    protected $_fields = [
        '_id'               => [ 'type' => 'pk', 'datatype' => 'string', 'default' => 0 ],
        'email'             => [ 'type' => 'field', 'datatype' => 'string', 'default' => '' ],
        'user_name'         => [ 'type' => 'field', 'datatype' => 'string', 'default' => '' ],
        'user_name_source'  => [ 'type' => 'field', 'datatype' => 'string', 'default' => '' ],
//        'models'            => [ 'type' => 'field', 'datatype' => 'array', 'default' => '' ],
    ];
    protected $_joins  = [];
    protected $inputFilter;

    public function getName()
    {
        return $this->subject;
    }

    public static function getLinkRules()
    {
        return self::$link_rules;
    }
}
