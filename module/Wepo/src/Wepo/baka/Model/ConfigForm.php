<?php

namespace Wepo\Model;

use Wepo\Lib\WepoMongoModel;

class ConfigForm extends WepoMongoModel
{
    public $adapterName = 'wepo_company';
    public $table_name  = 'config_form';

    const TABLE_NAME = 'config_form';

    protected $_fields      = [
        '_id'             => [ 'type' => 'pk', 'datatype' => 'string', 'default' => '' ],
        'name'            => [ 'type' => 'field', 'datatype' => 'string', 'default' => '' ],
        'group'           => [ 'type' => 'field', 'datatype' => 'string', 'default' => '' ],
        'type'            => [ 'type' => 'field', 'datatype' => 'string', 'default' => '' ],
        'options'         => [ 'type' => 'field', 'datatype' => 'array', 'default' => [ ] ],
        'attributes'      => [ 'type' => 'field', 'datatype' => 'array', 'default' => [ ] ],
        'fieldsets'       => [ 'type' => 'field', 'datatype' => 'array', 'default' => [ ] ],
        'elements'        => [ 'type' => 'field', 'datatype' => 'array', 'default' => [ ] ],
        'validationGroup' => [ 'type' => 'field', 'datatype' => 'array', 'default' => [ ] ],
//        'adapter'         => [ 'type' => 'field', 'datatype' => 'string', 'default' => '' ],
//        'table'           => [ 'type' => 'field', 'datatype' => 'string', 'default' => '' ],
//        'gateway' => [ 'type' => 'field', 'datatype' => 'string', 'default' => '' ],
    ];
    public $_id             = '';
    public $name            = '';
    public $group           = '';
    public $type            = '';
    public $options         = [ ];
    public $attributes      = [ ];
    public $fieldsets       = [ ];
    public $elements        = [ ];
    public $validationGroup = [ ];
}
