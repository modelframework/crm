<?php

namespace Wepo\Model;

use Zend\InputFilter\Factory as InputFactory;
use Zend\InputFilter\InputFilter;
use Wepo\Lib\WepoMongoModel;

class Mail extends WepoMongoModel
{
    public $table_name = 'mail';

    const inbox      = 'inbox';
    const outbox     = 'outbox';
    const TABLE_NAME = 'mail';

    protected $_fields = [
        '_id'          => [ 'type' => 'pk', 'datatype' => 'string', 'default' => '' ],
        'protocol_ids' => [ 'type' => 'field', 'datatype' => 'array', 'default' => '' ],
        //headers
        'header_id'    => [ 'type' => 'field', 'datatype' => 'string', 'default' => '' ],
        'content_type' => [ 'type' => 'field', 'datatype' => 'string', 'default' => '' ],
        'from'         => [ 'type' => 'field', 'datatype' => 'string', 'default' => '' ],
        'view_from'    => [ 'type' => 'field', 'datatype' => 'array', 'default' => [] ],
        'to'           => [ 'type' => 'field', 'datatype' => 'string', 'default' => '' ],
        'view_to'      => [ 'type' => 'field', 'datatype' => 'array', 'default' => [] ],
        'inReplyTo'    => [ 'type' => 'field', 'datatype' => 'string', 'default' => 0 ],
        'type'         => [ 'type' => 'field', 'datatype' => 'string', 'default' => '' ],
        'title'        => [ 'type' => 'field', 'datatype' => 'string', 'default' => '' ],
        'created_dtm'  => [ 'type' => 'field', 'datatype' => 'string', 'default' => '' ],
        //end headers
        'text'         => [ 'type' => 'field', 'datatype' => 'string', 'default' => '' ],
        'size'         => [ 'type' => 'field', 'datatype' => 'int', 'default' => 0 ],
        'email_link'   => [ 'type' => 'source', 'datatype' => 'array', 'default' => [] ],
        'chain_id'     => [ 'type' => 'field', 'datatype' => 'string', 'default' => '' ],
        'status_id'    => [ 'type' => 'source', 'datatype' => 'string', 'default' => '', 'alias' => 'status' ],
        'owner_id'     => [ 'type' => 'source', 'datatype' => 'string', 'default' => '', 'alias' => 'owner' ],
        'owner'        => [ 'type' => 'alias', 'datatype' => 'string', 'default' => '', 'source' => 'owner_id' ],
        'status'       => [ 'type' => 'alias', 'datatype' => 'string', 'default' => '', 'source' => 'status_id' ],
        /////////////
        'ownerfname' => [ 'type' => 'alias', 'datatype' => 'string', 'default' => '', 'source' => 'owner_id'],
        'ownerlname' => [ 'type' => 'alias', 'datatype' => 'string', 'default' => '', 'source' => 'owner_id'],
    ];
    protected $_joins  = [
//        [ 'model' => 'MailStatus', 'on' => [ 'mail_status_id' => '_id' ], 'fields' => [ 'mail_status' => 'status' ] ],
        [ 'model' => 'MailSetting', 'on' => [ 'mail_setting_id' => '_id' ], 'fields' => [ 'email' => 'email' ] ],
        [ 'model' => 'User', 'on' => [ 'owner_id' => '_id' ], 'fields' => [ 'owner' => 'login', 'ownerfname' => 'fname', 'ownerlname' => 'lname' ] ],
        [ 'model' => 'Status', 'on' => [ 'status_id' => '_id' ], 'fields' => [ 'status' => 'status' ] ],
    ];
    public $_unique    = [ 'header_id' ];
    protected $inputFilter;

    public function getInputFilter()
    {
        if (!$this->inputFilter) {
            $inputFilter = new InputFilter();
            $factory     = new InputFactory();

//            $inputFilter -> add( $factory -> createInput( array(
//                    'name'     => 'unique_header_id',
//                    'required' => false,
//                    'filters'  => array(
//                        array( 'name' => 'Int' ),
//                    ),
//            ) ) );
//            $inputFilter -> add( $factory -> createInput( array(
//                    'name'     => 'content_type',
//                    'required' => false,
//                    'filters'  => array(
//                        array( 'name' => 'StripTags' ),
//                        array( 'name' => 'StringTrim' ),
//                    ),
//            ) ) );
//            $inputFilter -> add( $factory -> createInput( array(
//                    'name'     => 'from',
//                    'required' => false,
//                    'filters'  => array(
//                        array( 'name' => 'StripTags' ),
//                        array( 'name' => 'StringTrim' ),
//                    ),
//            ) ) );
            $inputFilter->add($factory->createInput(array(
                    'name'     => 'to',
                    'required' => false,
                    'filters'  => array(
                        array( 'name' => 'StringTrim' ),
                    ),
            )));
            $inputFilter->add($factory->createInput(array(
                    'name'     => 'text',
                    'required' => false,
                    'filters'  => array(
                        array( 'name' => 'StripTags' ),
                        array( 'name' => 'StringTrim' ),
                    ),
            )));
//            $inputFilter -> add( $factory -> createInput( array(
//                    'name'     => 'mail_status_id',
//                    'required' => false,
//                    'filters'  => array(
//                        array( 'name' => 'StripTags' ),
//                        array( 'name' => 'StringTrim' ),
//                    ),
//            ) ) );
//            $inputFilter -> add( $factory -> createInput( array(
//                    'name'     => 'mail_conditions_id',
//                    'required' => false,
//                    'filters'  => array(
//                        array( 'name' => 'StripTags' ),
//                        array( 'name' => 'StringTrim' ),
//                    ),
//            ) ) );
            $inputFilter->add($factory->createInput(array(
                    'name'     => 'title',
                    'required' => false,
                    'filters'  => array(
                        array( 'name' => 'StripTags' ),
                        array( 'name' => 'StringTrim' ),
                    ),
            )));
//            $inputFilter -> add( $factory -> createInput( array(
//                    'name'     => 'path',
//                    'required' => false,
//                    'filters'  => array(
//                        array(
//                            'name'    => 'RealPath',
//                            'options' => array(
//                                'exists' => false,
//                            )
//                        ),
//                        array( 'name' => 'StripTags' ),
//                        array( 'name' => 'StringTrim' ),
//                    ),
//            ) ) );
//            $inputFilter -> add( $factory -> createInput( array(
//                    'name'     => 'size',
//                    'required' => false,
//                    'filters'  => array(
//                        array( 'name' => 'Int' ),
//                    ),
//            ) ) );
            $inputFilter->add($factory->createInput(array(
                    'name'     => 'owner_id',
                    'required' => false,
                    'filters'  => array(
                        array( 'name' => 'StripTags' ),
                        array( 'name' => 'StringTrim' ),
                    ),
            )));
            $this->inputFilter = $inputFilter;
        }

        return $this->inputFilter;
    }

//    public function setInputFilter(InputFilterInterface $inputFilter)
//    {
//        $this ->
//    }

    public function getName()
    {
        return $this->title.' - '.$this->from;
    }
}
