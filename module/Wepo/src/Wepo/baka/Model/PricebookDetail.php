<?php

namespace Wepo\Model;

use Zend\InputFilter\Factory as InputFactory;
use Zend\InputFilter\InputFilter;
use Wepo\Lib\WepoMongoModel;
use Wepo\Form\Validator\Float;

class PricebookDetail extends WepoMongoModel
{
    public $table_name = 'pricebook_detail';

    const TABLE_NAME = 'pricebook_detail';

    protected $_fields = [
        '_id'           => [ 'type' => 'pk', 'datatype' => 'string', 'default' => 0 ],
        'pricebook_id'  => [ 'type' => 'source', 'datatype' => 'string', 'default' => 0, 'alias' => 'pricebook' ],
        'product_id'    => [ 'type' => 'source', 'datatype' => 'string', 'default' => 0, 'alias' => 'product' ],

    //product price formation fields
        'discount_type' => [ 'type' => 'field', 'datatype' => 'string', 'default' => '' ],//updated
        'discount'       => [ 'type' => 'field', 'datatype' => 'float', 'default' => 0 ],//updated
//        'price_type'    => [ 'type' => 'field', 'datatype' => 'string', 'default' => '' ],
//        'discoun'      => [ 'type' => 'field', 'datatype' => 'float', 'default' => 0 ],
    //product price formation fields

        'final_price'  => [ 'type' => 'field', 'datatype' => 'float', 'default' => 0 ],
//        'result_price'  => [ 'type' => 'field', 'datatype' => 'float', 'default' => 0 ],
        'pricebook'     => [ 'type' => 'alias', 'datatype' => 'string', 'default' => '', 'source' => 'pricebook_id' ],
        'product_price' => [ 'type' => 'alias', 'datatype' => 'string', 'default' => '', 'source' => 'product_id' ],
        'product'       => [ 'type' => 'alias', 'datatype' => 'string', 'default' => '', 'source' => 'product_id' ],
        'title'         => [ 'type' => 'alias', 'datatype' => 'string', 'default' => '' ],
        'status'        => [ 'type' => 'alias', 'datatype' => 'string', 'default' => 'normal' ],
    ];
    protected $_joins = [
        [ 'model' => 'Pricebook', 'on' => [ 'pricebook_id' => '_id' ], 'fields' => [ 'pricebook' => 'pricebook' ] ],
        [
            'model'  => 'Product', 'on' => [ 'product_id' => '_id' ],
            'fields' => [ 'product' => 'name', 'product_price' => 'price' ]
        ],
    ];
    protected $_unique = [ [ 'pricebook_id', 'product_id' ] ];
    protected $inputFilter;

    public function getInputFilter()
    {
        if (!$this->inputFilter) {
            $inputFilter = new InputFilter();
            $factory     = new InputFactory();
            $inputFilter->add($factory->createInput(array(
                                                          'name'       => 'pricebook_id',
                                                          'required'   => true,
                                                          'filters'    => array(
                                                              array( 'name' => 'StripTags' ),
                                                              array( 'name' => 'StringTrim' ),
                                                          ),
                                                          'validators' => array(
                                                              array(
                                                                  'name'    => 'GreaterThan',
                                                                  'options' => array(
                                                                      'min'       => 1,
                                                                      'inclusive' => true,
                                                                      'messages'  => array( \Zend\Validator\GreaterThan::NOT_GREATER_INCLUSIVE => 'select item' ),
                                                                  ),
                                                              ),
                                                          ),
                                                      )));
            $inputFilter->add($factory->createInput(array(
                                                          'name'     => 'product_id',
                                                          'required' => true,
                                                          'filters'  => array(
                                                              array( 'name' => 'StripTags' ),
                                                              array( 'name' => 'StringTrim' ),
                                                          ),
                                                      )));
            $inputFilter->add($factory->createInput(array(
                                                          'name'     => 'discount',
                                                          'required' => true,
                                                          'filters'  => array(
                                                              array( 'name' => 'StripTags' ),
                                                              array( 'name' => 'StringTrim' ),
                                                          ),
                                                      )));
            $inputFilter->add($factory->createInput(array(
                                                          'name'       => 'discount_type',
                                                          'required'   => false,
                                                          'filters'    => array(
                                                              array( 'name' => 'StripTags' ),
                                                              array( 'name' => 'StringTrim' ),
                                                          ),
                                                          'validators' => array(
                                                              array(
                                                                  'name'    => 'InArray',
                                                                  'options' => array(
                                                                      'haystack' => [
                                                                          'Direct Price Reduction',
                                                                          '% of Price',
                                                                      ],
                                                                  ),
                                                              ),
                                                          ),
                                                      )));
            $inputFilter->add($factory->createInput(array(
                                                          'name'       => 'product_price',
                                                          'required'   => true,
                                                          'filters'    => array(
                                                              array( 'name' => 'StripTags' ),
                                                              array( 'name' => 'StringTrim' ),
                                                          ),
                                                          'validators' => array(
                                                              [
                                                                  "name"    => "Regex",
                                                                  "options" => [
                                                                      "pattern"  => "/^([1-9]\\d*|0)((\\.|\\,)\\d{1,2})?$/",
                                                                      "messages" => [
                                                                          "regexNotMatch" => "Input valid price",
                                                                      ],
                                                                  ],
                                                              ],
                                                          ),
                                                      )));
            $this->inputFilter = $inputFilter;
        }

        return $this->inputFilter;
    }

    public function getName()
    {
        return 'Product '.$this->product;
    }
}
