<?php

namespace Wepo\Model\Logic;

use ModelFramework\LogicService\DataLogic;
use Wepo\Lib\GatewayService;
use Wepo\Model\Status;

class Contact extends DataLogic
{
    protected $_rules     = [
        'add'  => [
            'owner_id'    => [ 'type'  => 'function', 'value' => 'setOwner' ],
            'created_dtm' => [ 'type'   => 'function', 'value'  => 'setDate', 'params' => 'Y-m-d H:i:s' ],
            'changer_id'  => [ 'type'  => 'function', 'value' => 'setUser' ],
            'changed_dtm' => [ 'type'   => 'function', 'value'  => 'setDate', 'params' => 'Y-m-d H:i:s' ],
            'email'       => [ 'type'   => 'function', 'value'  => 'updateEmails' ],
            'email_id'    => [ 'type'   => 'function', 'value'  => 'setEmailId' ],
            'age'         => [ 'type' => 'function', 'value' => 'setAge', 'params' => "birth_date" ],
//            'status_id'   => [ 'type'  => 'const', 'value' => Status::NEW_ ],
            'status_id'   => [ 'type' => 'function', 'value' => 'newItem' ],
            'title'       => [ 'type' => 'function', 'value' => 'setTitle', 'params' => [ 'fname', 'lname' ] ],
            'owner'       => [ 'type' => 'function', 'value' => 'setTitle', 'params' => [ 'ownerfname', 'ownerlname' ] ],
            'changer'       => [ 'type' => 'function', 'value' => 'setTitle', 'params' => [ 'changerfname', 'changerlname' ] ],

        ],
        'edit' => [
            'owner_id'    => [ 'type'  => 'function', 'value' => 'setOwner' ],
            'changer_id'  => [ 'type'  => 'function', 'value' => 'setUser' ],
            'changed_dtm' => [ 'type'   => 'function', 'value'  => 'setDate', 'params' => 'Y-m-d H:i:s' ],
            'email'       => [ 'type'   => 'function', 'value'  => 'updateEmails' ],
            'age'         => [ 'type' => 'function', 'value' => 'setAge', 'params' => "birth_date" ],
            'email_id'    => [ 'type'   => 'function', 'value'  => 'setEmailId' ],
//            'status_id'   => [ 'type'  => 'const', 'value' => Status::NORMAL ],
            'title'       => [ 'type' => 'function', 'value' => 'setTitle', 'params' => [ 'fname', 'lname' ] ],
            'owner'       => [ 'type' => 'function', 'value' => 'setTitle', 'params' => [ 'ownerfname', 'ownerlname' ] ],
            'changer'       => [ 'type' => 'function', 'value' => 'setTitle', 'params' => [ 'changerfname', 'changerlname' ] ],

        ],
        'convert_from_lead' => [
            'owner_id'    => [ 'type'  => 'function', 'value' => 'setOwner' ],
            'changer_id'  => [ 'type'  => 'function', 'value' => 'setUser' ],
            'changed_dtm' => [ 'type'   => 'function', 'value'  => 'setDate', 'params' => 'Y-m-d H:i:s' ],
            'email'       => [ 'type'   => 'function', 'value'  => 'updateEmails' ],
            'email_id'    => [ 'type'   => 'function', 'value'  => 'setEmailId' ],
            'title'       => [ 'type' => 'function', 'value' => 'setTitle', 'params' => [ 'fname', 'lname' ] ],
            'owner'       => [ 'type' => 'function', 'value' => 'setTitle', 'params' => [ 'ownerfname', 'ownerlname' ] ],
            'changer'     => [ 'type' => 'function', 'value' => 'setTitle', 'params' => [ 'changerfname', 'changerlname' ] ],
            'client_id'   => [ 'type'  => 'function', 'value' => 'setClient' ],
        ],
        'view' => [
            'status_id' => [ 'type' => 'function', 'value' => 'looked' ],
            'age'         => [ 'type' => 'function', 'value' => 'setAge', 'params' => "birth_date" ],
            'owner'       => [ 'type' => 'function', 'value' => 'setTitle', 'params' => [ 'ownerfname', 'ownerlname' ] ],
            'changer'       => [ 'type' => 'function', 'value' => 'setTitle', 'params' => [ 'changerfname', 'changerlname' ] ],

            //            'title'       => [ 'type' => 'function', 'value' => 'setTitle', 'params' => [ 'fname', 'lname' ] ],
        ],
    ];
    private $_transport = null;

    public function __construct(GatewayService $transport)
    {
        $this->_transport = $transport;
        parent::__construct($transport);
    }

    public function newItem($model, $key)
    {
        $modelname = $model->getModelName();
        $model->$key = Status::NEW_;
        $id = $model->owner_id;
        $user   = $this->_transport->getGateway('User')->find(array( '_id' => $id ))->current();
        $newItems = $user->newitems;

        $newItems[$modelname] = $newItems[$modelname]+ 0.5;
        $this->_transport->getGateway('User')->update([ 'newitems' => $newItems ], [ '_id' => $id ]);

        return $model->$key;
    }

    public function presave($event)
    {
        parent::presave($event);
        $this->setAvatar();
    }

    public function recycle($event)
    {
        $this->setEvent($event);
        $ids = [ ];
        if (!is_array($this->getEventObjects())) {
            $modelname = $this->getEventObjects()->getModelName();
            $ids[]     = $this->getEventObjects()->id();
            $this->looked($this->getEventObjects(), 'status_id');
        } else {
            $models = $this->getEventObjects();
            foreach ($models as $model) {
                $ids[] = $model->id();
                $this->looked($model, 'status_id');
            }
            $modelname = reset($models)->getModelName();
        }

        switch ($this->getAction()) {
            case 'restore':
                $this->_transport->getGateway($modelname)->update([ 'status' => Status::getLabel(Status::NORMAL), 'status_id' => Status::NORMAL ], [ '_id' => $ids ]);
                break;
            case 'delete':
                $this->_transport->getGateway($modelname)->update([ 'status' => Status::getLabel(Status::DELETED), 'status_id' => Status::DELETED ], [ '_id' => $ids ]);
                break;
            case 'clean':
                $this->_transport->getGateway($modelname)->delete([ '_id' => $ids ]);
                break;
        }
    }

    public function setAvatar()
    {
        $user = $this->getEventObjects();
        $request = $this->getController()->getRequest();
        $filename = false;
        if ($request->isPost()) {
            $files    = $request->getFiles();
            $fileinfo = $files->get('fields')[ 'avatar-file' ];
            $fs       = $this->getController()->getServiceLocator()->get('\Wepo\Lib\FileService');
            $filename = $fs->saveFile($fileinfo[ 'name' ], $fileinfo[ 'tmp_name' ], true, 'Leads');
        }
        if ($filename) {
            $user->avatar = basename($filename);
        }
    }

    public function setClient($model, $key)
    {
        $controller = $this->getController();
        $data = $controller->getRequest()->getPost()['fields'];
//        prn($data, $model, $key, $model->$key);
        if (empty($model->$key)) {
            $client = $controller->model('Client');
            $client->exchangeArray($model->data());
            $client->name      = $data[ 'account' ];
            $gw = $controller->table('Client');
            $controller->trigger('presave', $client);
            $gw->save($client);
            $clientId = $gw->getLastInsertId();
            $model->$key = empty($clientId) ?
                $this->table('Client')->find(array( 'changed_dtm' => $model->changed_dtm ))->current()->id() :
                $clientId;
        }

        return $model->$key;
    }
}
