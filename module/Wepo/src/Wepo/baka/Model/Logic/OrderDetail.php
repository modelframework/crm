<?php

namespace Wepo\Model\Logic;

use Wepo\Lib\GatewayService;
use Wepo\Lib\DataLogic;

class OrderDetail extends DataLogic
{
    protected $_rules   = [
        'add'  => [
            'order_id'    => [ 'type' => 'function', 'value' => 'setOrder' ],
            'final_price' => [
                'type' => 'function',
                'value' => 'countPrice',
                'params' => [ 'dsc' => 'discount', 'dsc_type' => 'discount_type', 'prd_price' => 'product_price', 'qty' => 'qty' ],
            ],
//            'final_price' => [ 'type' => 'function', 'value' => 'countPrice' ],
            'title'       => [ 'type' => 'function', 'value' => 'setTitle', 'params' => [ 'product' ] ],
        ],
        'edit' => [
            'final_price' => [
                'type' => 'function',
                'value' => 'countPrice',
                'params' => [ 'dsc' => 'discount', 'dsc_type' => 'discount_type', 'prd_price' => 'product_price', 'qty' => 'qty' ],
            ],
//            'final_price' => [ 'type' => 'function', 'value' => 'countPrice' ],
            'title'       => [ 'type' => 'function', 'value' => 'setTitle', 'params' => [ 'product' ] ],
        ],
        'order'  => [
            'title'       => [ 'type' => 'function', 'value' => 'setTitle', 'params' => [ 'product' ] ],
        ],
    ];
    private $_transport = null;

    public function __construct(GatewayService $transport)
    {
        $this->_transport = $transport;
        parent::__construct($transport);
    }

    public function presave($event)
    {
        parent::presave($event);
        //create recalculation
    }

    public function postsave($event)
    {
        $this->setEvent($event);
        $order                = $this->_transport->getGateway('Order')->get($this->getModel()->order_id);
        $this->getController()->trigger('presave', $order);
//        $order -> total_price = $this -> getTotalPrice( $order );
//        $order -> raw_price   = $this -> getRawPrice( $order );
        $this->_transport->getGateway('Order')->save($order);
    }

    public function recycle($event)
    {
        $this->setEvent($event);
        $ids = [ ];
        if (!is_array($this->getModel())) {
            $modelname = $this->getModel()->getModelName();
            $ids[]    = $this->getModel()->id();
        } else {
            $models = $this->getModel();
            foreach ($models as $model) {
                $ids[] = $model->id();
            }
            $modelname = reset($models)->getModelName();
        }
        $this->_transport->getGateway($modelname)->delete([ '_id' => $ids ]);
    }

    public function postrecycle($event)
    {
        $this->setEvent($event);
//        parent::postrecycle( $event );
        $models               = $this->getModel();
        $model                = reset($models);
        $order                = $this->_transport->getGateway('Order')->get($model->order_id);
        $this->getController()->trigger('presave', $order);
//        $order -> total_price = $this -> getTotalPrice( $order );
//        $order -> raw_price   = $this -> getRawPrice( $order );
        $this->_transport->getGateway('Order')->save($order);
    }

    protected function setOrder($model, $key)
    {
        return $model->$key = $this->getController()->params()->fromRoute('orderid', 0);
    }

//    protected function countPrice( $model, $key )
//    {
//        $resultPrice   = ($model -> discount >= 0 && $model -> discount <= 100 ) ? ($model -> product_price) * 0.01 * (100 - $model -> discount) * $model -> qty : $model -> product_price * $model -> qty;
////        return $model -> $key = number_format( $resultPrice, 2, '.', '' );
//        return $model -> $key = $resultPrice;
//    }

    protected function getTotalPrice($order)
    {
        $order_details = $this->_transport->getGateway('OrderDetail')->find(array( 'order_id' => $order->id() ));
        $total_price   = 0;
        foreach ($order_details as $_data) {
            $total_price += $_data->final_price;
        }

        if ($order->price_type == 'fixed') {
            $order->total_price = $order->modifier;
        } else {
            $order->total_price = ($total_price / 100) * ($order->modifier);
        }

        return $order->total_price;
    }

    protected function getRawPrice($order)
    {
        $order_details = $this->_transport->getGateway('OrderDetail')->find(array( 'order_id' => $order->id() ));
        $total_price   = 0;
        foreach ($order_details as $_data) {
            $total_price += $_data->final_price;
        }
        $order->raw_price = $total_price;

        return $order->raw_price;
    }
}
