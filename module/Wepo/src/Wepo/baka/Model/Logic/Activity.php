<?php

namespace Wepo\Model\Logic;

use ModelFramework\LogicService\DataLogic;
use Wepo\Lib\GatewayService;
use Wepo\Model\Table;

class Activity extends DataLogic
{
    protected $_rules   = [
        'add'  => [
        ],
        'edit' => [
        ],
    ];
    private $_transport = null;

    public function __construct(GatewayService $transport)
    {
        $this->_transport = $transport;
        parent::__construct($transport);
    }

    public function prerecycle($event)
    {
        parent::prerecycle($event);
        $this->recycleChain();
    }

    public function recycle($event)
    {
        $this->setEvent($event);
        $ids = [ ];
        if (!is_array($this->getEventObjects())) {
            $modelname = $this->getEventObjects->getModelName();
            $ids[]     = $this->getEventObjects->id();
        } else {
            $models = $this->getEventObjects();
            foreach ($models as $model) {
                $ids[] = $model->id();
            }
            $modelname = reset($models)->getModelName();
        }
//        prn_exit($ids);
        $this->_transport->getGateway($modelname)->delete([ '_id' => $ids ]);
    }

    public function recycleChain()
    {
        //        $models = $this -> getEventObjects();
//        foreach ( $models as $_model )
//        {
//            $this -> _transport -> getGateway( Table::getTransportName( $_model -> type_id ) ) -> delete( [ '_id' => $_model -> id ] );
//        }
    }

    public function setTarget($model, $key)
    {
        if (empty($model->$key)) {
            $model->$key = $this->_transport->getGateway($model->table)->findOne(['_id' => $model->target_id ]);
            prn_exit($model->$key);
        }
//        return $model -> $key;
    }
}
