<?php

namespace Wepo\Model\Logic;

use Wepo\Lib\GatewayService;
use Wepo\Lib\DataLogic;
use Wepo\Model\Status;

class Order extends DataLogic
{
    protected $_rules   = [
        'add'  => [
            'owner_id'    => [ 'type' => 'function', 'value' => 'setOwner' ],
//            'total_price' => [ 'type' => 'function', 'value' => 'getTotalPrice' ],
            'raw_price'   => [
                'type' => 'function',
                'value' => 'countDetailsSumPrice',
                'params' => ['dtl_model' => 'OrderDetail','dtl_link_field' => 'order_id','dtl_price_field' => 'final_price'],
            ],
            'total_price' => [
                'type' => 'function', 'value' => 'countPrice', 'params' => [ 'dsc' => 'modifier', 'dsc_type' => 'price_type', 'prd_price' => 'raw_price' ],
            ],
            'tax'         => [
                'type' => 'function',
                'value' => 'setTaxesAndAdjustment',
                'params' => [ 'ttl_field' => 'total_price','txs_fields' => [ 'vat', 'sales_tax' ], 'adjustment' => 'adjustment' ],
            ],
            'status_id'   => [ 'type' => 'const', 'value' => Status::NEW_ ],
            'created_dtm' => [ 'type' => 'function', 'value' => 'setDate', 'params' => 'Y-m-d H:i:s' ],
            'changer_id'  => [ 'type' => 'function', 'value' => 'setUser' ],
            'changed_dtm' => [ 'type' => 'function', 'value' => 'setDate', 'params' => 'Y-m-d H:i:s' ],
            'title'       => [ 'type' => 'function', 'value' => 'setTitle', 'params' => [ 'subject' ] ],
            'contact'     => [
                'type' => 'function', 'value' => 'setTitle', 'params' => [ 'contactfname', 'contactlname' ],
            ],
        ],
        'edit' => [
//            'total_price' => [ 'type' => 'function', 'value' => 'getTotalPrice' ],
            'raw_price'   => [
                'type' => 'function',
                'value' => 'countDetailsSumPrice',
                'params' => ['dtl_model' => 'OrderDetail', 'dtl_link_field' => 'order_id', 'dtl_price_field' => 'final_price'],
            ],
            'total_price' => [
                'type' => 'function', 'value' => 'countPrice', 'params' => [ 'dsc' => 'modifier', 'dsc_type' => 'price_type', 'prd_price' => 'raw_price' ],
            ],
            'tax'         => [
                'type' => 'function',
                'value' => 'setTaxesAndAdjustment',
                'params' => [ 'ttl_field' => 'total_price','txs_fields' => [ 'vat', 'sales_tax' ], 'adjustment' => 'adjustment' ],
            ],
            'owner_id'    => [ 'type' => 'function', 'value' => 'setOwner' ],
            'status_id'   => [ 'type' => 'const', 'value' => Status::NORMAL ],
            'changer_id'  => [ 'type' => 'function', 'value' => 'setUser' ],
            'changed_dtm' => [ 'type' => 'function', 'value' => 'setDate', 'params' => 'Y-m-d H:i:s' ],
            'title'       => [ 'type' => 'function', 'value' => 'setTitle', 'params' => [ 'subject' ] ],
            'contact'     => [
                'type' => 'function', 'value' => 'setTitle', 'params' => [ 'contactfname', 'contactlname' ],
            ],
        ],
        'delete' => [
            'raw_price'   => [
                'type' => 'function',
                'value' => 'countDetailsSumPrice',
                'params' => [ 'dtl_model' => 'OrderDetail', 'dtl_link_field' => 'order_id', 'dtl_price_field' => 'final_price' ],
            ],
            'total_price' => [
                'type' => 'function', 'value' => 'countPrice', 'params' => [ 'dsc' => 'modifier', 'dsc_type' => 'price_type', 'prd_price' => 'raw_price' ],
            ],
            'tax'         => [
                'type' => 'function',
                'value' => 'setTaxesAndAdjustment',
                'params' => [ 'ttl_field' => 'total_price','txs_fields' => [ 'vat', 'sales_tax' ], 'adjustment' => 'adjustment' ],
            ],
        ],
        // work when converting order from quote
        'convert_from_quote' => [
            'owner_id'    => [ 'type' => 'function', 'value' => 'setOwner' ],
            'status_id'   => [ 'type' => 'const', 'value' => Status::NEW_ ],
            'created_dtm' => [ 'type' => 'function', 'value' => 'setDate', 'params' => 'Y-m-d H:i:s' ],
            'changer_id'  => [ 'type' => 'function', 'value' => 'setUser' ],
            'changed_dtm' => [ 'type' => 'function', 'value' => 'setDate', 'params' => 'Y-m-d H:i:s' ],
            'title'       => [ 'type' => 'function', 'value' => 'setTitle', 'params' => [ 'subject' ] ],
            'contact'     => [
                'type' => 'function', 'value' => 'setTitle', 'params' => [ 'contactfname', 'contactlname' ],
            ],
        ],
        // should work on convert to invoice
        'convert_to_invoice' => [
            'owner_id'    => [ 'type' => 'function', 'value' => 'setOwner' ],
            'status_id'   => [ 'type' => 'const', 'value' => Status::PAID ],
            'changer_id'  => [ 'type' => 'function', 'value' => 'setUser' ],
            'changed_dtm' => [ 'type' => 'function', 'value' => 'setDate', 'params' => 'Y-m-d H:i:s' ],
        ],
    ];

    protected $_exchange_converted_fields = [
        'from_model' => [
        ],
        'to_model' => [
            '_id' => 'order_id',
            'adjustment' => 'total_adjustment',
        ],
    ];

    private $_transport = null;

    public function __construct(GatewayService $transport)
    {
        $this->_transport = $transport;
        parent::__construct($transport);
    }

    public function presave($event)
    {
        parent::presave($event);
    }

    public function prerecycle($event)
    {
        parent::prerecycle($event);
        $this->recycleChain();
    }

    public function recycle($event)
    {
        $this->setEvent($event);
        $ids = [ ];
        if (!is_array($this->getModel())) {
            $modelname = $this->getModel()->getModelName();
            $ids[]    = $this->getModel()->id();
        } else {
            $models = $this->getModel();
            foreach ($models as $model) {
                $ids[] = $model->id();
            }
            $modelname = reset($models)->getModelName();
        }
        $this->_transport->getGateway('Invoice')->update([ 'order' => null, 'order_id' => '' ], [ 'order_id' => $ids ]);
        $this->_transport->getGateway($modelname)->delete([ '_id' => $ids ]);
    }

    public function convert($event)
    {
        parent::convert($event);

        $od_gw = $this->_transport->getGateway('OrderDetail');
        $id_gw = $this->_transport->getGateway('InvoiceDetail');

        $order               = $this->getModel();
        $order_details       =
            $od_gw->find(array( 'order_id' => $order->id() ));
        $invoice               = $this->getParam('to_model');

        $invoice_id = $invoice->id();

        foreach ($order_details as $order_detail) {
            $invoice_detail                = $this->_transport->getModel('InvoiceDetail');
            $invoice_detail->exchangeArray($order_detail->data());
            $invoice_detail->invoice_id      = $invoice_id;
            $this->getController()->trigger('presave', $invoice_detail);
            $id_gw->save($invoice_detail);
        }
    }

    protected function setTotalDiscount($model, $key, $params = [])
    {
        $toModel = $this->getEvent()->getParams()['to_model'];

        return $model->$key;
    }

    public function recycleChain()
    {
        $models    = $this->getModel();
        $model_ids = [ ];
        foreach ($models as $_model) {
            $model_ids[] = $_model->id();
        }
        $this->_transport->getGateway('OrderDetail')->delete([ 'order_id' => $model_ids ]);
    }
}
