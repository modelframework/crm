<?php

namespace Wepo\Model\Logic;

use Wepo\Lib\GatewayService;
use Wepo\Lib\DataLogic;
use Wepo\Model\Table;
use Wepo\Model\Status;

class MailSetting extends DataLogic
{
    protected $_rules   = [
        'addSend'  => [
            'owner_id'   => [ 'type' => 'function', 'value' => 'setOwner' ],
            'status_id'  => [ 'type' => 'const', 'value' => Status::NEW_ ],
            'is_default' => [ 'type' => 'function', 'value' => 'setDefault' ],
            'title'      => [ 'type' => 'function', 'value' => 'setTitle', 'params' => [ 'user', 'email' ] ],
        ],
        'addReceive'  => [
            'owner_id'   => [ 'type' => 'function', 'value' => 'setOwner' ],
            'status_id'  => [ 'type' => 'const', 'value' => Status::NEW_ ],
            'title'      => [ 'type' => 'function', 'value' => 'setTitle', 'params' => [ 'user', 'email' ] ],
        ],
        'edit' => [
            'owner_id'   => [ 'type' => 'function', 'value' => 'setOwner' ],
            'status_id'  => [ 'type' => 'const', 'value' => Status::NORMAL ],
            'is_default' => [ 'type' => 'function', 'value' => 'setDefault' ],
            'title'      => [ 'type' => 'function', 'value' => 'setTitle', 'params' => [ 'user', 'email' ] ],
        ],
    ];
    private $_transport = null;

    public function __construct(GatewayService $transport)
    {
        $this->_transport = $transport;
        parent::__construct($transport);
    }

    public function setDefault($model, $key)
    {
        if (isset($model->$key) && $model->$key) {
            $defaultSetting = $this->getController()->table('MailSendSetting')->findOne(['user_id' => $this->getController()->user()->id(), 'is_default' => 'true']);
//            prn($defaultSetting);
//            exit();
            if ($defaultSetting) {
                $defaultSetting->$key = 'false';
                $this->getController()->table('MailSendSetting')->save($defaultSetting);
            }

            return $model->$key;
        }

        return;
    }

    public function postrecycle($event)
    {
        parent::postrecycle($event);
        if ($this->getAction() == 'clean') {
            $ids = [ ];
            if (!is_array($this->getModel())) {
                $modelname = $this->getModel()->getModelName();
                $ids[]     = $this->getModel()->id();
            } else {
                $models = $this->getModel();
                foreach ($models as $model) {
                    $ids[] = $model->id();
                }
                $modelname = reset($models)->getModelName();
            }
            foreach ($ids as $id) {
                $this->_transport->getGateway('Mail')->update(['mail_setting_id' => 0 ], ['mail_setting_id' => $id ]);
            }
        }
    }
}
