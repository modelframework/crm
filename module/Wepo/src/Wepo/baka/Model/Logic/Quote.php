<?php

namespace Wepo\Model\Logic;

use Wepo\Lib\GatewayService;
use Wepo\Lib\DataLogic;
use Wepo\Model\Status;

class Quote extends DataLogic
{
    protected $_rules = [
        'add'  => [
            'owner_id'    => [ 'type' => 'function', 'value' => 'setOwner' ],
            'raw_price'   => [
                'type' => 'function',
                'value' => 'countDetailsSumPrice',
                'params' => ['dtl_model' => 'QuoteDetail','dtl_link_field' => 'quote_id','dtl_price_field' => 'final_price'],
            ],
            'total_price' => [
                'type' => 'function', 'value' => 'countPrice', 'params' => [ 'dsc' => 'modifier', 'dsc_type' => 'price_type', 'prd_price' => 'raw_price' ],
            ],
            'tax'         => [
                'type' => 'function',
                'value' => 'setTaxesAndAdjustment',
                'params' => [ 'ttl_field' => 'total_price','txs_fields' => [ 'vat', 'sales_tax' ], 'adjustment' => 'adjustment' ],
            ],
            'status_id'   => [ 'type' => 'const', 'value' => Status::NEW_ ],
            'created_dtm' => [ 'type' => 'function', 'value' => 'setDate', 'params' => 'Y-m-d H:i:s' ],
            'changer_id'  => [ 'type' => 'function', 'value' => 'setUser' ],
            'changed_dtm' => [ 'type' => 'function', 'value' => 'setDate', 'params' => 'Y-m-d H:i:s' ],
            'title'       => [ 'type' => 'function', 'value' => 'setTitle', 'params' => [ 'subject' ] ],
            'contact'     => [
                'type' => 'function', 'value' => 'setTitle', 'params' => [ 'contactfname', 'contactlname' ],
            ],
        ],
        'edit' => [
            'raw_price'   => [
                'type' => 'function',
                'value' => 'countDetailsSumPrice',
                'params' => ['dtl_model' => 'QuoteDetail','dtl_link_field' => 'quote_id','dtl_price_field' => 'final_price'],
            ],
            'total_price' => [
                'type' => 'function', 'value' => 'countPrice', 'params' => [ 'dsc' => 'modifier', 'dsc_type' => 'price_type', 'prd_price' => 'raw_price' ],
            ],
            'tax'         => [
                'type' => 'function',
                'value' => 'setTaxesAndAdjustment',
                'params' => [ 'ttl_field' => 'total_price','txs_fields' => [ 'vat', 'sales_tax' ], 'adjustment' => 'adjustment' ],
            ],
            'owner_id'    => [ 'type' => 'function', 'value' => 'setOwner' ],
            'status_id'   => [ 'type' => 'const', 'value' => Status::NORMAL ],
            'changer_id'  => [ 'type' => 'function', 'value' => 'setUser' ],
            'changed_dtm' => [ 'type' => 'function', 'value' => 'setDate', 'params' => 'Y-m-d H:i:s' ],
            'title'       => [ 'type' => 'function', 'value' => 'setTitle', 'params' => [ 'subject' ] ],
            'contact'     => [
                'type' => 'function', 'value' => 'setTitle', 'params' => [ 'contactfname', 'contactlname' ],
            ],
        ],
        'delete' => [
            'raw_price'   => [
                'type' => 'function',
                'value' => 'countDetailsSumPrice',
                'params' => ['dtl_model' => 'QuoteDetail','dtl_link_field' => 'quote_id','dtl_price_field' => 'final_price'],
            ],
            'total_price' => [
                'type' => 'function', 'value' => 'countPrice', 'params' => [ 'dsc' => 'modifier', 'dsc_type' => 'price_type', 'prd_price' => 'raw_price' ],
            ],
            'tax'         => [
                'type' => 'function',
                'value' => 'setTaxesAndAdjustment',
                'params' => [ 'ttl_field' => 'total_price','txs_fields' => [ 'vat', 'sales_tax' ], 'adjustment' => 'adjustment' ],
            ],
        ],
    ];
    private $_transport = null;

    protected $_exchange_converted_fields = [
        'from_model' => [
        ],
        'to_model' => [
            '_id' => 'quote_id',
        ],
    ];

    public function __construct(GatewayService $transport)
    {
        $this->_transport = $transport;
        parent::__construct($transport);
    }

    public function prerecycle($event)
    {
        parent::prerecycle($event);
        $this->recycleChain();
    }

    public function recycle($event)
    {
        $this->setEvent($event);
        $ids = [ ];
        if (!is_array($this->getModel())) {
            $modelname = $this->getModel()->getModelName();
            $ids[ ]    = $this->getModel()->id();
        } else {
            $models = $this->getModel();
            foreach ($models as $model) {
                $ids[ ] = $model->id();
            }
            $modelname = reset($models)->getModelName();
        }
        $this->_transport->getGateway('Order')->update([ 'quote' => null, 'quote_id' => '' ],
                                                          [ 'quote_id' => $ids ]);
        $this->_transport->getGateway($modelname)->delete([ '_id' => $ids ]);
    }

    public function convert($event)
    {
        parent::convert($event);

        $od_gw = $this->_transport->getGateway('OrderDetail');
        $qd_gw = $this->_transport->getGateway('QuoteDetail');

        $quote               = $this->getModel();
        $quote_details       =
            $qd_gw->find(array( 'quote_id' => $quote->id() ));
        $order               = $this->getParam('to_model');

        $order_id = $order->id();

        foreach ($quote_details as $quote_detail) {
            $order_detail                = $this->_transport->getModel('OrderDetail');
            $order_detail->exchangeArray($quote_detail->data());
            $order_detail->order_id      = $order_id;
            $this->getController()->trigger('presave', $order_detail);
            $od_gw->save($order_detail);
        }
    }

    public function recycleChain()
    {
        $models    = $this->getModel();
        $model_ids = [ ];
        foreach ($models as $_model) {
            $model_ids[ ] = $_model->id();
        }
        $this->_transport->getGateway('QuoteDetail')->delete([ 'quote_id' => $model_ids ]);
    }
}
