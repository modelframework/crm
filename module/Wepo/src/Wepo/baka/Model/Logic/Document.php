<?php

namespace Wepo\Model\Logic;

use Wepo\Lib\GatewayService;
use Wepo\Lib\DataLogic;
use Wepo\Model\Status;

class Document extends DataLogic
{
    protected $_rules = [
        'add'  => [
            'owner_id'    => [ 'type' => 'function', 'value' => 'setOwner' ],
            'status_id'   => [ 'type' => 'const', 'value' => Status::NEW_ ],
            'created_dtm' => [ 'type' => 'function', 'value' => 'setDate', 'params' => 'Y-m-d H:i:s' ],
            'changer_id'  => [ 'type' => 'function', 'value' => 'setUser' ],
            'changed_dtm' => [ 'type' => 'function', 'value' => 'setDate', 'params' => 'Y-m-d H:i:s' ],
            'target_id'   => [ 'type' => 'function', 'value' => 'setTarget' ],
            'title'       => [ 'type' => 'function', 'value' => 'setTitle', 'params' => [ 'document_name' ] ],
            'owner'       => [ 'type' => 'function', 'value' => 'setTitle', 'params' => [ 'ownerfname', 'ownerlname' ] ],
            'changer'       => [ 'type' => 'function', 'value' => 'setTitle', 'params' => [ 'changerfname', 'changerlname' ] ],
        ],
        'edit' => [
            'owner_id'    => [ 'type' => 'function', 'value' => 'setOwner' ],
            'status_id'   => [ 'type' => 'const', 'value' => Status::NORMAL ],
            'changer_id'  => [ 'type' => 'function', 'value' => 'setUser' ],
            'changed_dtm' => [ 'type' => 'function', 'value' => 'setDate', 'params' => 'Y-m-d H:i:s' ],
            'target_id'   => [ 'type' => 'function', 'value' => 'setTarget' ],
            'title'       => [ 'type' => 'function', 'value' => 'setTitle', 'params' => [ 'document_name' ] ],
            'owner'       => [ 'type' => 'function', 'value' => 'setTitle', 'params' => [ 'ownerfname', 'ownerlname' ] ],
            'changer'       => [ 'type' => 'function', 'value' => 'setTitle', 'params' => [ 'changerfname', 'changerlname' ] ],
        ],
        'view' => [
            'owner'       => [ 'type' => 'function', 'value' => 'setTitle', 'params' => [ 'ownerfname', 'ownerlname' ] ],
            'changer'       => [ 'type' => 'function', 'value' => 'setTitle', 'params' => [ 'changerfname', 'changerlname' ] ],
        ],
    ];
    private $_transport = null;

    public function __construct(GatewayService $transport)
    {
        $this->_transport = $transport;
        parent::__construct($transport);
    }

    public function presave($event)
    {
        parent::presave($event);
        $this->setAttachments();
    }

    public function setTarget($model, $key)
    {
        if (!empty($model->$key)) {
            $this->fillJoinsConvert($model);
            $gw            = ucfirst($model->table);
            $target        = $this->_transport->getGateway($gw)->findOne([ '_id' => $model->target_id ]);
            $model->target = $target->fname." ".$target->lname;
        }

        return $model->target;
    }

    public function setAttachments()
    {
        $document = $this->getModel();
        $request  = $this->getController()->getRequest();
        if ($request->isPost()) {
            $post            = $request->getPost();
            $attach_to       = $document->attach_to;
            $attach_to_names = $document->attach_to_names;
            if (!empty($post[ 'attach' ][ 'leads_id' ])) {
                $leads      = explode(',', $post[ 'attach' ][ 'leads_id' ]);
                $leadsnames = [ ];
                foreach ($leads as $_lead) {
                    $tmp           = $this->_transport->getGateway('Lead')->findOne([ '_id' => $_lead ]);
                    $leadsnames[ ] = $tmp->fname." ".$tmp->lname;
                }
            } else {
                $leads      = $attach_to[ 'Lead' ];
                $leadsnames = $attach_to_names[ 'Lead' ];
            }
            if (!empty($post[ 'attach' ][ 'contacts_id' ])) {
                $contacts      = explode(',', $post[ 'attach' ][ 'contacts_id' ]);
                $contactsnames = [ ];
                foreach ($contacts as $_contact) {
                    $tmp              = $this->_transport->getGateway('Contact')->findOne([ '_id' => $_contact ]);
                    $contactsnames[ ] = $tmp->fname." ".$tmp->lname;
                }
            } else {
                $contacts      = $attach_to[ 'Contact' ];
                $contactsnames = $attach_to_names[ 'Patient' ];
            }
            if (!empty($post[ 'attach' ][ 'users_id' ])) {
                $users      = explode(',', $post[ 'attach' ][ 'users_id' ]);
                $usersnames = [ ];
                foreach ($users as $_user) {
                    $tmp           = $this->_transport->getGateway('User')->findOne([ '_id' => $_user ]);
                    $usersnames[ ] = $tmp->fname." ".$tmp->lname;
                }
            } else {
                $users      = $attach_to[ 'User' ];
                $usersnames = $attach_to_names[ 'User' ];
            }

            $attach_to[ 'Lead' ]          = $leads;
            $attach_to[ 'Contact' ]       = $contacts;
            $attach_to[ 'User' ]          = $users;
            $attach_to_names[ 'Lead' ]    = $leadsnames;
            $attach_to_names[ 'Patient' ] = $contactsnames;
            $attach_to_names[ 'User' ]    = $usersnames;
            $document->attach_to          = $attach_to;
            $document->attach_to_names    = $attach_to_names;
//            prn($document);  exit();
        }
    }
}
