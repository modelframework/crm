<?php

namespace Wepo\Model\Logic;

use Wepo\Lib\GatewayService;
use Wepo\Lib\DataLogic;
use Wepo\Model\Status;

class Product extends DataLogic
{
    protected $_rules = [
        'add'  => [
            'changer_id'  => [ 'type' => 'function', 'value' => 'setUser' ],
            'owner_id'    => [ 'type' => 'function', 'value' => 'setOwner' ],
            'changed_dtm' => [ 'type' => 'function', 'value' => 'setDate', 'params' => 'Y-m-d H:i:s' ],
            'created_dtm' => [ 'type' => 'function', 'value' => 'setDate', 'params' => 'Y-m-d H:i:s' ],
            'status_id'   => [ 'type' => 'function', 'value' => 'newItem' ],
            'title'       => [ 'type' => 'function', 'value' => 'setTitle', 'params' => [ 'name' ] ],
            'owner'       => [ 'type' => 'function', 'value' => 'setTitle', 'params' => [ 'ownerfname', 'ownerlname' ] ],
            'changer'       => [ 'type' => 'function', 'value' => 'setTitle', 'params' => [ 'changerfname', 'changerlname' ] ],
        ],
        'edit' => [
            'changer_id'  => [ 'type' => 'function', 'value' => 'setUser' ],
            'changed_dtm' => [ 'type' => 'function', 'value' => 'setDate', 'params' => 'Y-m-d H:i:s' ],
            'owner_id'    => [ 'type' => 'function', 'value' => 'setOwner' ],
            'title'       => [ 'type' => 'function', 'value' => 'setTitle', 'params' => [ 'name' ] ],
            'owner'       => [ 'type' => 'function', 'value' => 'setTitle', 'params' => [ 'ownerfname', 'ownerlname' ] ],
            'changer'       => [ 'type' => 'function', 'value' => 'setTitle', 'params' => [ 'changerfname', 'changerlname' ] ],
        ],
        'view' => [
            'status_id' => [ 'type' => 'function', 'value' => 'looked' ],
            'owner'       => [ 'type' => 'function', 'value' => 'setTitle', 'params' => [ 'ownerfname', 'ownerlname' ] ],
            'changer'       => [ 'type' => 'function', 'value' => 'setTitle', 'params' => [ 'changerfname', 'changerlname' ] ],
        ],
    ];
    private $_transport = null;

    public function __construct(GatewayService $transport)
    {
        $this->_transport = $transport;
        parent::__construct($transport);
    }

    public function recycle($event)
    {
        $this->setEvent($event);
        $ids = [ ];
        if (!is_array($this->getModel())) {
            $modelname = $this->getModel()->getModelName();
            $ids[ ]    = $this->getModel()->id();
            $this->looked($this->getModel(), 'status_id');
        } else {
            $models = $this->getModel();
            foreach ($models as $model) {
                $ids[ ] = $model->id();
                $this->looked($model, 'status_id');
            }
            $modelname = reset($models)->getModelName();
        }

        switch ($this->getAction()) {
            case 'restore':
                $this->_transport->getGateway($modelname)->update([ 'status'    => Status::getLabel(Status::NORMAL),
                                                                       'status_id' => Status::NORMAL,
                                                                     ], [ '_id' => $ids ]);
                break;
            case 'delete':
                $this->_transport->getGateway($modelname)->update([ 'status'    => Status::getLabel(Status::DELETED),
                                                                       'status_id' => Status::DELETED,
                                                                     ], [ '_id' => $ids ]);
                break;
            case 'clean':
                $this->_transport->getGateway($modelname)->delete([ '_id' => $ids ]);
                break;
        }
    }
}
