<?php

namespace Wepo\Model\Logic;

use ModelFramework\LogicService\DataLogic;
use Wepo\Lib\GatewayService;

class PricebookDetail extends DataLogic
{
    protected $_rules   = [
        'product' => [
            'pricebook_id'  => [ 'type' => 'function', 'value' => 'setPricebook' ],
            'product_id'    => [ 'type' => 'function', 'value' => 'setProductId' ],
            'product_price' => [ 'type' => 'function', 'value' => 'setProductPrice' ],
            'final_price' => [ 'type' => 'function', 'value' => 'countPrice', 'params' => [ 'dsc' => 'discount', 'dsc_type' => 'discount_type', 'prd_price' => 'product_price' ] ],
//            'result_price'  => [ 'type' => 'function', 'value' => 'countPrice', 'params' => [ 'dsc'=>'modifier', 'dsc_type'=>'price_type', 'prd_price'=>'product_price' ] ],
            'title'       => [ 'type' => 'function', 'value' => 'setTitle', 'params' => [ 'product' ] ],
        ],
        'add'     => [
            'pricebook_id' => [ 'type' => 'function', 'value' => 'setPricebook' ],
            'final_price' => [ 'type' => 'function', 'value' => 'countPrice', 'params' => [ 'dsc' => 'discount', 'dsc_type' => 'discount_type', 'prd_price' => 'product_price' ] ],
//            'result_price' => [ 'type' => 'function', 'value' => 'countPrice', 'params' => [ 'dsc'=>'modifier', 'dsc_type'=>'price_type', 'prd_price'=>'product_price' ] ],
            'title'       => [ 'type' => 'function', 'value' => 'setTitle', 'params' => [ 'product' ] ],
        ],
        'edit'    => [
//            'result_price' => [ 'type' => 'function', 'value' => 'countPrice', 'params' => [ 'dsc'=>'modifier', 'dsc_type'=>'price_type', 'prd_price'=>'product_price' ] ],
            'final_price' => [ 'type' => 'function', 'value' => 'countPrice', 'params' => [ 'dsc' => 'discount', 'dsc_type' => 'discount_type', 'prd_price' => 'product_price' ] ],
            'title'       => [ 'type' => 'function', 'value' => 'setTitle', 'params' => [ 'product' ] ],
        ],
    ];
    private $_transport = null;

    public function __construct(GatewayService $transport)
    {
        $this->_transport = $transport;
        parent::__construct($transport);
    }

    public function presave($event)
    {
        parent::presave($event);
    }

//    public function postsave( $event )
//    {
//        $this -> setEvent( $event );
//        $pricebookdetail                 = $this -> _transport -> getGateway( 'PricebookDetail' ) -> get( $this -> getEventObjects() -> id );
//        $pricebookdetail -> result_price = $this -> getResultPrice( $pricebookdetail );
//        $this -> _transport -> getGateway( 'PricebookDetail' ) -> save( $pricebookdetail );
//    }

    public function recycle($event)
    {
        $this->setEvent($event);
        $ids = [ ];
        if (!is_array($this->getEventObjects())) {
            $modelname = $this->getEventObjects()->getModelName();
            $ids[]     = $this->getEventObjects()->id();
        } else {
            $models = $this->getEventObjects();
            foreach ($models as $model) {
                $ids[] = $model->id();
            }
            $modelname = reset($models)->getModelName();
        }
        $this->_transport->getGateway($modelname)->delete([ '_id' => $ids ]);
    }

    public function postrecycle($event)
    {
        $this->setEvent($event);
    }

    protected function setPricebook($model, $key)
    {
        return $model->$key = isset($model->$key) && $model->$key ? $model->$key : $this->getController()->params()->fromRoute('pricebookid', 0);
    }

    protected function setProductId($model, $key)
    {
        $model->$key = $this->getController()->params('product');
    }

    protected function setProductPrice($model, $key)
    {
        $products_price = $this->_transport->getGateway('Product')->find(array( '_id' => $model->product_id ));
        $model->$key   = round($products_price->current()->price, 2);
    }

//    protected function getResultPrice( $pricebookdetail )
//    {
//        $products_price = $this -> _transport -> get( 'Product' ) -> find( array( 'id' => $pricebookdetail -> product_id ) );
//        $result_price   = $products_price -> price;
//        if ( $pricebookdetail -> price_type == 'percent' )
//        {
//            $pricebookdetail -> result_price = ($result_price) / 100 * (100 - $pricebookdetail -> modifier);
//        }
//        else
//        {
//            $pricebookdetail -> result_price = $pricebookdetail -> modifier;
//        }
//        return $pricebookdetail -> result_price;
//    }
}
