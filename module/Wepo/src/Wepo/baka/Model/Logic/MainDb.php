<?php

namespace Wepo\Model\Logic;

use ModelFramework\LogicService\DataLogic;

class MainDb extends DataLogic
{
    public static $adapter = 'wepo_main';
}
