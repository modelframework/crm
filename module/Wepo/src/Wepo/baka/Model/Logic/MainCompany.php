<?php

namespace Wepo\Model\Logic;

use ModelFramework\LogicService\DataLogic;
use Wepo\Lib\GatewayService;
use Wepo\Model\Status;

class MainCompany extends DataLogic
{
    public static $adapter = 'wepo_main';
    protected $_rules = [
        'signup' => [
//            'status_id'   => [ 'type' => 'const', 'value' => Status::NEW_ ],
            'status_id'   => [ 'type' => 'const', 'value' => '1' ],
            'created_dtm' => [ 'type' => 'function', 'value' => 'setDate', 'params' => 'Y-m-d H:i:s' ],
        ],
    ];

    public function __construct(GatewayService $transport)
    {
        $this->_transport = $transport;
        parent::__construct($transport);
    }

    public function install($event)
    {
        $this->setEvent($event);
        $models = $this->getEventObjects();
        if (is_array($models) && count($models) == 2) {
            $mainUser    = array_pop($models);
            $mainCompany = array_pop($models);
        } else {
            throw new Exception('wrong params array count');
        }
//        $mainCompany -> status_id = Status::INSTALL;
        $mainCompany->status_id = '8';
        $this->_transport->getGateway('MainCompany')->save($mainCompany);
        $mainCompanyInstall       = $this->_transport->getGateway('MainCompany')->findOne($mainCompany->toArray());
        if ($mainCompanyInstall != null) {
            $mainUser->company_id = $mainCompanyInstall->_id;
            $dbs                    = $this->_transport->getGateway('MainDb')->find([ 'status_id' => '0' ], [ ], 1);
            if ($dbs->count() > 0) {
                $db                              = $dbs->current();
                $db->company_id                = $mainCompanyInstall->_id;
                $db->status_id                 = '1';
                $this->_transport->getGateway('MainDb')->save($db);
                $mainCompanyInstall->status_id = '2';
            } else {
                //                $mainCompanyInstall -> status_id = Status::NEW_;
                $mainCompanyInstall->status_id = '1';
            }
            $this->_transport->getGateway('MainCompany')->save($mainCompanyInstall);
        }
    }
}
