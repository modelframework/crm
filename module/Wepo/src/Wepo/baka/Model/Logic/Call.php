<?php

namespace Wepo\Model\Logic;

use Wepo\Lib\GatewayService;
use Wepo\Lib\DataLogic;
use Wepo\Model\Table;
use Wepo\Model\Activity as ModelActivity;

class Call extends DataLogic
{
    protected $_rules = [
        'add'  => [
            'owner_id'  => [ 'type' => 'function', 'value' => 'setOwner' ],
            'created_dtm' => [ 'type' => 'function', 'value' => 'setDate', 'params' => 'Y-m-d H:i:s' ],
            'target_id' => [ 'type' => 'function', 'value' => 'setTarget' ],
            'title'     => [ 'type' => 'function', 'value' => 'setTitle', 'params' => [ 'subject' ] ],
            'changer_id'  => [ 'type' => 'function', 'value' => 'setUser' ],
            'changed_dtm' => [ 'type' => 'function', 'value' => 'setDate', 'params' => 'Y-m-d H:i:s' ],
            'owner'       => [ 'type' => 'function', 'value' => 'setTitle', 'params' => [ 'ownerfname', 'ownerlname' ] ],
            'changer'       => [ 'type' => 'function', 'value' => 'setTitle', 'params' => [ 'changerfname', 'changerlname' ] ],
            'type_id'       => [ 'type' => 'const', 'value' => Table::CALL ],
        ],
        'edit' => [
            'owner_id'  => [ 'type' => 'function', 'value' => 'setOwner' ],
            'created_dtm' => [ 'type' => 'function', 'value' => 'setDate', 'params' => 'Y-m-d H:i:s' ],
            'target_id' => [ 'type' => 'function', 'value' => 'setTarget' ],
            'title'     => [ 'type' => 'function', 'value' => 'setTitle', 'params' => [ 'subject' ] ],
            'changer_id'  => [ 'type' => 'function', 'value' => 'setUser' ],
            'changed_dtm' => [ 'type' => 'function', 'value' => 'setDate', 'params' => 'Y-m-d H:i:s' ],
            'owner'       => [ 'type' => 'function', 'value' => 'setTitle', 'params' => [ 'ownerfname', 'ownerlname' ] ],
            'changer'       => [ 'type' => 'function', 'value' => 'setTitle', 'params' => [ 'changerfname', 'changerlname' ] ],
            'type_id'       => [ 'type' => 'const', 'value' => Table::CALL ],
        ],
    ];
    private $_transport = null;

    public function __construct(GatewayService $transport)
    {
        $this->_transport = $transport;
        parent::__construct($transport);
    }

    public function presave($event)
    {
        parent::presave($event);
    }

    public function presaveBooBoo($event)
    {
        $this->setEvent($event)->forge();
        $this->getModel()->type_id = Table::CALL;
        $this->fillJoins();
        $this->setDate($this->getModel(), 'created_dtm', 'Y-m-d H:i:s');
//        $this -> activity();
    }

//    private function activity()
//    {
//        if ( substr( $this -> getAction(), 0, 4 ) == 'edit' )
//        {
//            $call              = $this -> getModel();
//            $frompost          = $this -> getController() -> params() -> fromPost()[ 'additional' ];
//            $call -> table_id  = $frompost[ 'table_id' ];
//            $call -> target_id = $frompost[ 'target_id' ];
//            $activity          = $this -> _transport -> getGateway( 'Activity' ) -> get( $call -> id() );
//            $activity -> merge( $call -> toActivity() );
//            $this -> _transport -> getGateway( 'Activity' ) -> save( $activity );
//        }
//        elseif ( substr( $this -> getAction(), 0, 3 ) == 'add' )
//        {
//            $call                = $this -> getModel();
//            $frompost            = $this -> getController() -> params() -> fromPost()[ 'additional' ];
////            var_dump($frompost[ 'table_id' ]);
////            exit();
//            $call -> table_id    = (string) $frompost[ 'table_id' ];
//            $call -> target_id   = (string) $frompost[ 'target_id' ];
//            $activity            = New ModelActivity();
//            $activity -> exchangeArray( $call -> toActivity() );
////            prn($activity);
////            exit();
//            $activity -> type_id = Table::CALL;
//            $this -> setDate( $activity, 'created_dtm', 'Y-m-d H:i:s' );
//            $this -> _transport -> getGateway( 'Activity' ) -> save( $activity );
//            $activity_id         = $this -> _transport -> getGateway( 'Activity' ) -> find( $activity -> toArray() ) -> current() -> id();
//            $call -> _id         = $activity_id;
//        }
//    }
    public function setTarget($model, $key)
    {
        if (!empty($model->$key)) {
            $this->fillJoinsConvert($model);
            $gw = Table::getTransportName($model->table_id);
//            $gw = substr($model->table, 0, -1);
            $target        = $this->_transport->getGateway($gw)->findOne([ '_id' => $model->target_id ]);
            $model->target = $target->fname." ".$target->lname;
        }

        return $model->target;
    }
}
