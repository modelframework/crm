<?php

namespace Wepo\Model\Logic;

use ModelFramework\LogicService\DataLogic;
use Wepo\Lib\GatewayService;
use Wepo\Model\Status;

class ModelConfiguration extends DataLogic
{
    public static $adapter = 'wepo_company_mongo';

    protected $_rules     = [
        'add'  => [
            'owner_id'    => [ 'type'  => 'function', 'value' => 'setOwner' ],
            'created_dtm' => [ 'type'   => 'function', 'value'  => 'setDate', 'params' => 'Y-m-d H:i:s' ],
            'changer_id'  => [ 'type'  => 'function', 'value' => 'setUser' ],
            'changed_dtm' => [ 'type'   => 'function', 'value'  => 'setDate', 'params' => 'Y-m-d H:i:s' ],
            'status_id'   => [ 'type'  => 'const', 'value' => Status::NEW_ ],
        ],
        'edit' => [
            'owner_id'    => [ 'type'  => 'function', 'value' => 'setOwner' ],
            'changer_id'  => [ 'type'  => 'function', 'value' => 'setUser' ],
            'changed_dtm' => [ 'type'   => 'function', 'value'  => 'setDate', 'params' => 'Y-m-d H:i:s' ],
            'status_id'   => [ 'type'  => 'const', 'value' => Status::NORMAL ],
        ],
    ];
    private $_transport = null;

    public function __construct(GatewayService $transport)
    {
        $this->_transport = $transport;
        parent::__construct($transport);
    }

    public function presave($event)
    {
        parent::presave($event);
    }
}
