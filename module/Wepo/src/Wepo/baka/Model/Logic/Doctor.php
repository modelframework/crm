<?php

namespace Wepo\Model\Logic;

use Wepo\Lib\GatewayService;
use Wepo\Lib\DataLogic;
use Wepo\Model\Status;

class Doctor extends DataLogic
{
    protected $_rules = [
        'add'  => [
            'owner_id'    => [ 'type' => 'function', 'value' => 'setOwner' ],
            'status_id'   => [ 'type' => 'const', 'value' => Status::NEW_ ],
            'created_dtm' => [ 'type' => 'function', 'value' => 'setDate', 'params' => 'Y-m-d H:i:s' ],
            'changer_id'  => [ 'type' => 'function', 'value' => 'setUser' ],
            'dea'         => [ 'type' => 'function', 'value' => 'setDea' ],
            'changed_dtm' => [ 'type' => 'function', 'value' => 'setDate', 'params' => 'Y-m-d H:i:s' ],
            'title'       => [ 'type' => 'function', 'value' => 'setTitle', 'params' => [ 'name' ] ],
            'owner'       => [ 'type' => 'function', 'value' => 'setTitle', 'params' => [ 'ownerfname', 'ownerlname' ] ],
            'changer'       => [ 'type' => 'function', 'value' => 'setTitle', 'params' => [ 'changerfname', 'changerlname' ] ],
        ],
        'edit' => [
            'owner_id'    => [ 'type' => 'function', 'value' => 'setOwner' ],
            'status_id'   => [ 'type' => 'const', 'value' => Status::NORMAL ],
            'changer_id'  => [ 'type' => 'function', 'value' => 'setUser' ],
            'changed_dtm' => [ 'type' => 'function', 'value' => 'setDate', 'params' => 'Y-m-d H:i:s' ],
            'title'       => [ 'type' => 'function', 'value' => 'setTitle', 'params' => [ 'name' ] ],
            'dea'         => [ 'type' => 'function', 'value' => 'setDea' ],
            'owner'       => [ 'type' => 'function', 'value' => 'setTitle', 'params' => [ 'ownerfname', 'ownerlname' ] ],
            'changer'       => [ 'type' => 'function', 'value' => 'setTitle', 'params' => [ 'changerfname', 'changerlname' ] ],
        ],
        'view' => [
            'owner'       => [ 'type' => 'function', 'value' => 'setTitle', 'params' => [ 'ownerfname', 'ownerlname' ] ],
            'changer'       => [ 'type' => 'function', 'value' => 'setTitle', 'params' => [ 'changerfname', 'changerlname' ] ],
        ],
    ];
    private $_transport = null;

    public function __construct(GatewayService $transport)
    {
        $this->_transport = $transport;
        parent::__construct($transport);
    }

    public function setDea($model, $key)
    {
        if (!$model->$key) {
            $model->$key = 'None';
        }

        return $model->$key;
    }
}
