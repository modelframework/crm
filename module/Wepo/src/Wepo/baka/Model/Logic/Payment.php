<?php

namespace Wepo\Model\Logic;

use ModelFramework\LogicService\DataLogic;
use Wepo\Lib\GatewayService;

class Payment extends DataLogic
{
    protected $_rules     = [
        'add'  => [
            'owner_id'    => [ 'type'  => 'function', 'value' => 'setOwner' ],
            'invoice_id'  => [ 'type'  => 'const', 'value' => null ],
            'created_dtm' => [ 'type'   => 'function', 'value'  => 'setDate', 'params' => 'Y-m-d H:i:s' ],
            'title'       => [ 'type' => 'function', 'value' => 'setTitle', 'params' => [ 'description' ] ],
            'contact'     => [
                'type' => 'function', 'value' => 'setTitle', 'params' => [ 'contactfname', 'contactlname' ],
            ],
        ],
        'edit' => [
            'owner_id'    => [ 'type'  => 'function', 'value' => 'setOwner' ],
            'title'       => [ 'type' => 'function', 'value' => 'setTitle', 'params' => [ 'description' ] ],
            'contact'     => [
                'type' => 'function', 'value' => 'setTitle', 'params' => [ 'contactfname', 'contactlname' ],
            ],
//            'invoice_id' => [ 'type'  => 'const', 'value' => null ],
        ],
        'payment' => [
            'owner_id'      => [ 'type'  => 'function', 'value' => 'setOwner' ],
            'created_dtm'   => [ 'type'   => 'function', 'value'  => 'setDate', 'params' => 'Y-m-d H:i:s' ],
            'performed_dtm' => [ 'type'   => 'function', 'value'  => 'setDate', 'params' => 'Y-m-d H:i:s' ],
            'title'         => [ 'type' => 'function', 'value' => 'setTitle', 'params' => [ 'description' ] ],
            'contact'     => [
                'type' => 'function', 'value' => 'setTitle', 'params' => [ 'contactfname', 'contactlname' ],
            ],
        ],
    ];
    private $_transport = null;

    public function __construct(GatewayService $transport)
    {
        $this->_transport = $transport;
        parent::__construct($transport);
    }

    public function presave($event)
    {
        parent::presave($event);
    }

    public function recycle($event)
    {
        $this->setEvent($event);
        $ids = [ ];
        if (!is_array($this->getEventObjects())) {
            $modelname = $this->getEventObjects()->getModelName();
            $ids[ ]    = $this->getEventObjects()->id();
        } else {
            $models = $this->getEventObjects();
            foreach ($models as $model) {
                $ids[ ] = $model->id();
            }
            $modelname = reset($models)->getModelName();
        }
        $this->_transport->getGateway($modelname)->delete([ '_id' => $ids ]);
    }
}
