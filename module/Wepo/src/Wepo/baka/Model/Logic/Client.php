<?php

namespace Wepo\Model\Logic;

use Wepo\Lib\GatewayService;
use Wepo\Lib\DataLogic;
use Wepo\Model\Status;

class Client extends DataLogic
{
    protected $_rules = [
        'add'     => [
            'owner_id'    => [ 'type' => 'function', 'value' => 'setOwner' ],
            'created_dtm' => [ 'type' => 'function', 'value' => 'setDate', 'params' => 'Y-m-d H:i:s' ],
            'changer_id'  => [ 'type' => 'function', 'value' => 'setUser' ],
            'changed_dtm' => [ 'type' => 'function', 'value' => 'setDate', 'params' => 'Y-m-d H:i:s' ],
            'email'       => [ 'type' => 'function', 'value' => 'updateEmails' ],
            'email_id'    => [ 'type' => 'function', 'value' => 'setEmailId' ],
            'status_id'   => [ 'type' => 'const', 'value' => Status::NEW_ ],
            'title'       => [ 'type' => 'function', 'value' => 'setTitle', 'params' => [ 'name' ] ],
            'owner'       => [ 'type' => 'function', 'value' => 'setTitle', 'params' => [ 'ownerfname', 'ownerlname' ] ],
            'changer'       => [ 'type' => 'function', 'value' => 'setTitle', 'params' => [ 'changerfname', 'changerlname' ] ],
        ],
        'edit'    => [
            'owner_id'    => [ 'type' => 'function', 'value' => 'setOwner' ],
            'changer_id'  => [ 'type' => 'function', 'value' => 'setUser' ],
            'changed_dtm' => [ 'type' => 'function', 'value' => 'setDate', 'params' => 'Y-m-d H:i:s' ],
            'email'       => [ 'type' => 'function', 'value' => 'updateEmails' ],
            'email_id'    => [ 'type' => 'function', 'value' => 'setEmailId' ],
            'status_id'   => [ 'type' => 'const', 'value' => Status::NORMAL ],
            'title'       => [ 'type' => 'function', 'value' => 'setTitle', 'params' => [ 'name' ] ],
            'owner'       => [ 'type' => 'function', 'value' => 'setTitle', 'params' => [ 'ownerfname', 'ownerlname' ] ],
            'changer'       => [ 'type' => 'function', 'value' => 'setTitle', 'params' => [ 'changerfname', 'changerlname' ] ],
        ],
        'convert' => [
            'owner_id'    => [ 'type' => 'function', 'value' => 'setOwner' ],
            'changer_id'  => [ 'type' => 'function', 'value' => 'setUser' ],
            'changed_dtm' => [ 'type' => 'function', 'value' => 'setDate', 'params' => 'Y-m-d H:i:s' ],
            'email'       => [ 'type' => 'function', 'value' => 'updateEmails' ],
            'email_id'    => [ 'type' => 'function', 'value' => 'setEmailId' ],
            'status_id'   => [ 'type' => 'const', 'value' => Status::NEW_ ],
            'title'       => [ 'type' => 'function', 'value' => 'setTitle', 'params' => [ 'name' ] ],
            'owner'       => [ 'type' => 'function', 'value' => 'setTitle', 'params' => [ 'ownerfname', 'ownerlname' ] ],
            'changer'       => [ 'type' => 'function', 'value' => 'setTitle', 'params' => [ 'changerfname', 'changerlname' ] ],
        ],
        'view' => [
            'owner'       => [ 'type' => 'function', 'value' => 'setTitle', 'params' => [ 'ownerfname', 'ownerlname' ] ],
            'changer'       => [ 'type' => 'function', 'value' => 'setTitle', 'params' => [ 'changerfname', 'changerlname' ] ],
        ],

    ];
    private $_transport = null;

    public function __construct(GatewayService $transport)
    {
        $this->_transport = $transport;
        parent::__construct($transport);
    }

    public function presave($event)
    {
        parent::presave($event);
        $this->setAvatar();
    }

    public function setAvatar()
    {
        $user     = $this->getModel();
        $request  = $this->getController()->getRequest();
        $filename = false;
        if ($request->isPost()) {
            $files    = $request->getFiles();
            $fileinfo = $files->get('fields')[ 'avatar-file' ];
            $fs       = $this->getController()->getServiceLocator()->get('\Wepo\Lib\FileService');
            $filename = $fs->saveFile($fileinfo[ 'name' ], $fileinfo[ 'tmp_name' ], true, 'Clients');
        }
        if ($filename) {
            $user->avatar = basename($filename);
        }
    }
}
