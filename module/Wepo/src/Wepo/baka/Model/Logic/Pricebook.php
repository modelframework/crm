<?php

namespace Wepo\Model\Logic;

use ModelFramework\LogicService\DataLogic;
use Wepo\Lib\GatewayService;
use Wepo\Model\Status;

class Pricebook extends DataLogic
{
    protected $_rules   = [
        'add'  => [
            'owner_id'    => [ 'type' => 'function', 'value' => 'setOwner' ],
            'changer_id'  => [ 'type' => 'function', 'value' => 'setUser' ],
            'status_id'   => [ 'type' => 'const', 'value' => Status::NEW_ ],
            'created_dtm' => [ 'type' => 'function', 'value' => 'setDate', 'params' => 'Y-m-d H:i:s' ],
            'changed_dtm' => [ 'type' => 'function', 'value' => 'setDate', 'params' => 'Y-m-d H:i:s' ],
            'title'       => [ 'type' => 'function', 'value' => 'setTitle', 'params' => [ 'pricebook' ] ],
            'owner'       => [ 'type' => 'function', 'value' => 'setTitle', 'params' => [ 'ownerfname', 'ownerlname' ] ],
            'changer'       => [ 'type' => 'function', 'value' => 'setTitle', 'params' => [ 'changerfname', 'changerlname' ] ],
        ],
        'edit' => [
            'owner_id'    => [ 'type' => 'function', 'value' => 'setOwner' ],
            'status_id'   => [ 'type' => 'const', 'value' => Status::NORMAL ],
            'changer_id'  => [ 'type' => 'function', 'value' => 'setUser' ],
            'changed_dtm' => [ 'type' => 'function', 'value' => 'setDate', 'params' => 'Y-m-d H:i:s' ],
            'title'       => [ 'type' => 'function', 'value' => 'setTitle', 'params' => [ 'pricebook' ] ],
            'owner'       => [ 'type' => 'function', 'value' => 'setTitle', 'params' => [ 'ownerfname', 'ownerlname' ] ],
            'changer'       => [ 'type' => 'function', 'value' => 'setTitle', 'params' => [ 'changerfname', 'changerlname' ] ],
        ],
        'view' => [
            'owner'       => [ 'type' => 'function', 'value' => 'setTitle', 'params' => [ 'ownerfname', 'ownerlname' ] ],
            'changer'       => [ 'type' => 'function', 'value' => 'setTitle', 'params' => [ 'changerfname', 'changerlname' ] ],
        ],
    ];
    private $_transport = null;

    public function __construct(GatewayService $transport)
    {
        $this->_transport = $transport;
        parent::__construct($transport);
    }

    public function prerecycle($event)
    {
        $this->setEvent($event);
        $ids = [ ];
        if (!is_array($this->getEventObjects())) {
            $modelname = $this->getEventObjects()->getModelName();
            $ids[]     = $this->getEventObjects()->id();
        } else {
            $models = $this->getEventObjects();
            foreach ($models as $model) {
                $ids[] = $model->id();
            }
            $modelname = reset($models)->getModelName();
        }
        switch ($this->getAction()) {
            case 'restore':
                $this->_transport->getGateway($modelname)->update([ 'status_id' => Status::NORMAL ], [ '_id' => $ids ]);
                break;
            case 'delete':
                $this->_transport->getGateway($modelname)->update([ 'status_id' => Status::DELETED ], [ '_id' => $ids ]);
                break;
            case 'clean':
                $this->recycleChain();
                $this->_transport->getGateway($modelname)->delete([ '_id' => $ids ]);
                break;
        }
    }

    public function recycleChain()
    {
        $models    = $this->getEventObjects();
        $model_ids = [ ];
        foreach ($models as $_model) {
            $model_ids[] = $_model->id();
        }
        $this->_transport->getGateway('PricebookDetail')->delete([ 'pricebook_id' => $model_ids ]);
        $tr         = $this->_transport->getGateway('Order');
        $orders_set = $tr->find([ 'pricebook_id' => $model_ids ]);
        foreach ($orders_set as $order) {
            $order->pricebook_id = 0;
            $tr->save($order);
        }
        $tr              = $this->_transport->getGateway('OrderDetail');
        $orderdetail_set = $tr->find([ 'pricebook_id' => $model_ids ]);
        foreach ($orderdetail_set as $orderdetail) {
            $orderdetail->pricebook_id = 0;
            $tr->save($orderdetail);
        }
    }
}
