<?php

namespace Wepo\Model\Logic;

use ModelFramework\LogicService\DataLogic;
use Wepo\Lib\GatewayService;
use Wepo\Model\Status;

class User extends DataLogic
{
    protected $_rules   = [
        'add'  => [
            'ip'          => [ 'type' => 'function', 'value' => 'setClientIp' ],
            'created_dtm' => [ 'type' => 'function', 'value' => 'setDate', 'params' => 'Y-m-d H:i:s' ],
            'changer_id'  => [ 'type' => 'function', 'value' => 'setUser' ],
            'changed_dtm' => [ 'type' => 'function', 'value' => 'setDate', 'params' => 'Y-m-d H:i:s' ],
            'status_id'   => [ 'type' => 'const', 'value' => Status::NEW_ ],
            'title'       => [ 'type' => 'function', 'value' => 'setTitle', 'params' => [ 'fname', 'lname' ] ],
//            'email'       => [ 'type' => 'function', 'value' => 'updateEmails' ],
        ],
        'edit' => [
            'changer_id'  => [ 'type' => 'function', 'value' => 'setUser' ],
            'changed_dtm' => [ 'type' => 'function', 'value' => 'setDate', 'params' => 'Y-m-d H:i:s' ],
            'status_id'   => [ 'type' => 'const', 'value' => Status::NORMAL ],
            'title'       => [ 'type' => 'function', 'value' => 'setTitle', 'params' => [ 'fname', 'lname' ] ],
//            'email'       => [ 'type' => 'function', 'value' => 'updateEmails' ],
        ],
    ];
    private $_transport = null;

    public function __construct(GatewayService $transport)
    {
        $this->_transport = $transport;
        parent::__construct($transport);
    }

    public function postsave($event)
    {
        $this->setEvent($event);
        $this->saveMainUser();
        $this->saveLog();
    }

    public function presave($event)
    {
        parent::presave($event);
        $this->setAvatar();
//        $this -> saveMainUserId();
    }

    public function recycle($event)
    {
        $this->setEvent($event);
        $ids = [ ];
        $selfdel = false;
        if (!is_array($this->getEventObjects())) {
            $modelname = $this->getEventObjects()->getModelName();
            $ids[]     = $this->getEventObjects()->id();
            $mids[]    = $this->getEventObjects()->main_id;
            if ($model->id() == $this->getController()->User()->id()) {
                $selfdel = true;
            }
        } else {
            $models = $this->getEventObjects();
            foreach ($models as $model) {
                $ids[]  = $model->id();
                $mids[] = (string) $model->main_id;
                if ($model->id() == $this->getController()->User()->id()) {
                    $selfdel = true;
                }
            }
            $modelname = reset($models)->getModelName();
        }

        switch ($this->getAction()) {
            case 'restore':
                $this->_transport->getGateway($modelname)->update([ 'status' => Status::getLabel(Status::NORMAL), 'status_id' => Status::NORMAL ], [ '_id' => $ids ]);
                $this->_transport->getGateway('MainUser')->update([ 'status' => Status::getLabel(Status::NORMAL), 'status_id' => Status::NORMAL ], [ '_id' => $mids ]);
                break;
            case 'delete':
                $this->_transport->getGateway($modelname)->update([ 'status' => Status::getLabel(Status::DELETED), 'status_id' => Status::DELETED ], [ '_id' => $ids ]);
                $this->_transport->getGateway('MainUser')->update([ 'status' => Status::getLabel(Status::DELETED), 'status_id' => Status::DELETED ], [ '_id' => $mids ]);
                if ($selfdel) {
                    $this->getController()->User($this->getController()->model('User'));
                    $this->getController()->MainUser($this->getController()->model('MainUser'));
                    $this->getController()->refresh('Index', null);
                }
                break;
            case 'clean':
                $this->_transport->getGateway($modelname)->delete([ '_id' => $ids ]);
                $this->_transport->getGateway('MainUser')->delete([ '_id' => $mids ]);
                if ($selfdel) {
                    $this->getController()->User($this->getController()->model('User'));
                    $this->getController()->MainUser($this->getController()->model('MainUser'));
                    $this->getController()->refresh('Index', null);
                }
                break;
        }
    }

    public function saveMainUser()
    {
        $user                   = $this->getEventObjects();
        $mainUser               = $this->getModel('MainUser');
        $mainUser->exchange($user->toArray());
        $mainUser->company_id = $this->getController()->MainUser()->company_id;
//        $mainUser -> postExchange();
        try {
            $mainUser->_id = $user->main_id;
            $this->_transport->getGateway('MainUser')->save($mainUser);
        } catch (Exception $ex) {
            throw new \Exception($ex->getMessage());
        }
        $savedMainUser   = $this->_transport->getGateway('MainUser')->findOne([ 'login' => $mainUser->login ]);
        $this->_transport->getGateway('User')->update([ 'main_id' => $savedMainUser->id()], [ 'login' => $user->login ]);
    }

    public function passwordChange($event)
    {
        $this->setEvent($event);
        $user                 = $this->getEventObjects();
        $mainUser             = $this->getController()->MainUser();
        $mainUser->password = $user->password;
        try {
            $this->_transport->getGateway('MainUser')->save($mainUser);
        } catch (Exception $ex) {
            throw new \Exception($ex->getMessage());
        }
    }
    public function saveMainUserId()
    {
        $user                   = $this->getEventObjects();
        $mainUser               = $this->getModel('MainUser');

        $savedMainUser   = $this->_transport->getGateway('MainUser')->findOne([ 'login' => $mainUser->login ]);
        if ($savedMainUser == null) {
            try {
                $mainUser->_id = $user->main_id;
                $this->_transport->getGateway('MainUser')->save($mainUser);
                $savedMainUser   = $this->_transport->getGateway('MainUser')->findOne([ 'login' => $mainUser->login ]);
            } catch (Exception $ex) {
                throw new \Exception($ex->getMessage());
            }
        }
        $user->main_id = $savedMainUser->id();
    }
}
