<?php

namespace Wepo\Model\Logic;

use Wepo\Lib\GatewayService;
use Wepo\Lib\DataLogic;
use Wepo\Model\Status;

class Note extends DataLogic
{
    protected $_rules = [
        'add'     => [
            'owner_id'    => [ 'type' => 'function', 'value' => 'setOwner' ],
            'created_dtm' => [ 'type' => 'function', 'value' => 'setDate', 'params' => 'Y-m-d H:i:s' ],
            'changer_id'  => [ 'type' => 'function', 'value' => 'setUser' ],
            'changed_dtm' => [ 'type' => 'function', 'value' => 'setDate', 'params' => 'Y-m-d H:i:s' ],
            'status_id'   => [ 'type'  => 'const', 'value' => Status::NEW_ ],
            'status_id'   => [ 'type' => 'function', 'value' => 'newItem' ],
            'owner'       => [ 'type' => 'function', 'value' => 'setTitle', 'params' => [ 'ownerfname', 'ownerlname' ] ],
            'changer'     => [ 'type' => 'function', 'value' => 'setTitle', 'params' => [ 'changerfname', 'changerlname' ] ],
            'table_id'     => [ 'type' => 'function', 'value' => 'setTable'],
            'parent_id'     => [ 'type' => 'function', 'value' => 'setParent'],
        ],
        'edit'    => [
            'owner_id'    => [ 'type' => 'function', 'value' => 'setOwner' ],
            'changer_id'  => [ 'type' => 'function', 'value' => 'setUser' ],
            'changed_dtm' => [ 'type' => 'function', 'value' => 'setDate', 'params' => 'Y-m-d H:i:s' ],
            'owner'       => [ 'type' => 'function', 'value' => 'setTitle', 'params' => [ 'ownerfname', 'ownerlname' ] ],
            'changer'     => [ 'type' => 'function', 'value' => 'setTitle', 'params' => [ 'changerfname', 'changerlname' ] ],
        ],
    ];
    private $_transport = null;

    public function __construct(GatewayService $transport)
    {
        $this->_transport = $transport;
        parent::__construct($transport);
    }

    public function setTable($model, $key)
    {
        return $model->$key = $this->getController()->params()->fromRoute('tableid', 0);
    }
    public function setParent($model, $key)
    {
        return $model->$key = $this->getController()->params()->fromRoute('parentid', 0);
    }

    public function recycle($event)
    {
        $this->setEvent($event);
        $ids = [ ];
        if (!is_array($this->getModel())) {
            $modelname = $this->getModel()->getModelName();
            $ids[ ]    = $this->getModel()->id();
        } else {
            $models = $this->getModel();
            foreach ($models as $model) {
                $ids[ ] = $model->id();
            }
            $modelname = reset($models)->getModelName();
        }
        $this->_transport->getGateway($modelname)->delete([ '_id' => $ids ]);
    }
}
