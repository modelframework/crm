<?php

namespace Wepo\Model\Logic;

use ModelFramework\LogicService\DataLogic;

class MailChain extends DataLogic
{
    protected $_rules   = [
        'send'  => [
            'date'         => [ 'type' => 'function', 'value' => 'setDate' ],
            'owner_id'     => [ 'type' => 'function', 'value' => 'setOwner' ],
        ],
        'sync' => [
            'date'         => [ 'type' => 'function', 'value' => 'setDate' ],
            'owner_id'     => [ 'type' => 'function', 'value' => 'setOwner' ],
        ],
    ];

    public function presend($event)
    {
        $this->setEvent($event);
        $this->forge();
    }
}
