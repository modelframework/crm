<?php

namespace Wepo\Model\Logic;

use Wepo\Lib\GatewayService;
use Wepo\Lib\DataLogic;

class QuoteDetail extends DataLogic
{
    protected $_rules   = [
        'add'  => [
            'quote_id'    => [ 'type' => 'function', 'value' => 'setQuote' ],
            'final_price' => [
                'type' => 'function',
                'value' => 'countPrice',
                'params' => [ 'dsc' => 'discount', 'dsc_type' => 'discount_type', 'prd_price' => 'product_price', 'qty' => 'qty' ],
            ],
            'title'       => [ 'type' => 'function', 'value' => 'setTitle', 'params' => [ 'product' ] ],
        ],
        'edit' => [
            'final_price' => [
                'type' => 'function',
                'value' => 'countPrice',
                'params' => [ 'dsc' => 'discount', 'dsc_type' => 'discount_type', 'prd_price' => 'product_price', 'qty' => 'qty' ],
            ],
            'title'       => [ 'type' => 'function', 'value' => 'setTitle', 'params' => [ 'product' ] ],
        ],
    ];
    private $_transport = null;

    public function __construct(GatewayService $transport)
    {
        $this->_transport = $transport;
        parent::__construct($transport);
    }

    public function presave($event)
    {
        parent::presave($event);
    }

    public function postsave($event)
    {
        $this->setEvent($event);
        $quote                = $this->_transport->getGateway('Quote')->get($this->getModel()->quote_id);
        $this->getController()->trigger('presave', $quote);
//        $quote -> total_price = $this -> getTotalPrice( $quote );
//        $quote -> raw_price   = $this -> getRawPrice( $quote );
        $this->_transport->getGateway('Quote')->save($quote);
    }

    public function recycle($event)
    {
        $this->setEvent($event);
        $ids = [ ];
        if (!is_array($this->getModel())) {
            $modelname = $this->getModel()->getModelName();
            $ids[]     = $this->getModel()->id();
        } else {
            $models = $this->getModel();
            foreach ($models as $model) {
                $ids[] = $model->id();
            }
            $modelname = reset($models)->getModelName();
        }
        $this->_transport->getGateway($modelname)->delete([ '_id' => $ids ]);
    }

    public function postrecycle($event)
    {
        $this->setEvent($event);
//        parent::postrecycle( $event );
        $models               = $this->getModel();
        $model                = reset($models);
        $quote                = $this->_transport->getGateway('Quote')->get($model->quote_id);
        $this->getController()->trigger('presave', $quote);
//        $quote -> total_price = $this -> getTotalPrice( $quote );
//        $quote -> raw_price   = $this -> getRawPrice( $quote );
        $this->_transport->getGateway('Quote')->save($quote);
    }

    protected function setQuote($model, $key)
    {
        return $model->$key = $this->getController()->params()->fromRoute('quoteid', 0);
    }

//    protected function getTotalPrice( $quote )
//    {
//        $quote_details = $this -> _transport -> getGateway( 'QuoteDetail' ) -> find( array( 'quote_id' => $quote -> id() ) );
//        $total_price   = 0;
//        foreach ( $quote_details as $_data )
//        {
//            $total_price += $_data -> final_price;
//        }
//
//        if ( $quote -> price_type == 'fixed' )
//        {
//            $quote -> total_price = $quote -> modifier;
//        }
//        else
//        {
//            $quote -> total_price = ($total_price / 100) * ($quote -> modifier);
//        }
//        return $quote -> total_price;
//    }
}
