<?php

namespace Wepo\Model\Logic;

use Wepo\Lib\GatewayService;
use Wepo\Lib\DataLogic;
use Wepo\Model\Table;
use Wepo\Model\Activity as ModelActivity;

class Event extends DataLogic
{
    protected $_rules   = [
        'add'  => [
            'owner_id'  => [ 'type' => 'function', 'value' => 'setOwner' ],
            'created_dtm' => [ 'type' => 'function', 'value' => 'setDate', 'params' => 'Y-m-d H:i:s' ],
            'target_id' => [ 'type' => 'function', 'value' => 'setTarget' ],
            'title'     => [ 'type' => 'function', 'value' => 'setTitle', 'params' => [ 'subject' ] ],
            'changer_id'  => [ 'type' => 'function', 'value' => 'setUser' ],
            'changed_dtm' => [ 'type' => 'function', 'value' => 'setDate', 'params' => 'Y-m-d H:i:s' ],
            'owner'       => [ 'type' => 'function', 'value' => 'setTitle', 'params' => [ 'ownerfname', 'ownerlname' ] ],
            'changer'       => [ 'type' => 'function', 'value' => 'setTitle', 'params' => [ 'changerfname', 'changerlname' ] ],
            'type_id'       => [ 'type' => 'const', 'value' => Table::EVENT ],
        ],
        'edit' => [
            'owner_id'  => [ 'type' => 'function', 'value' => 'setOwner' ],
            'created_dtm' => [ 'type' => 'function', 'value' => 'setDate', 'params' => 'Y-m-d H:i:s' ],
            'target_id' => [ 'type' => 'function', 'value' => 'setTarget' ],
            'title'     => [ 'type' => 'function', 'value' => 'setTitle', 'params' => [ 'subject' ] ],
            'changer_id'  => [ 'type' => 'function', 'value' => 'setUser' ],
            'changed_dtm' => [ 'type' => 'function', 'value' => 'setDate', 'params' => 'Y-m-d H:i:s' ],
            'owner'       => [ 'type' => 'function', 'value' => 'setTitle', 'params' => [ 'ownerfname', 'ownerlname' ] ],
            'changer'       => [ 'type' => 'function', 'value' => 'setTitle', 'params' => [ 'changerfname', 'changerlname' ] ],
            'type_id'       => [ 'type' => 'const', 'value' => Table::EVENT ],
        ],
    ];
    private $_transport = null;

    public function __construct(GatewayService $transport)
    {
        $this->_transport = $transport;
        parent::__construct($transport);
    }

    public function presave($event)
    {
        parent::presave($event);
    }

//    private function activity()
//    {
//        if ( substr( $this -> getAction(), 0, 4 ) == 'edit' )
//        {
//            $event              = $this -> getModel();
//            $frompost           = $this -> getController() -> params() -> fromPost()['additional'];
//            $event -> table_id  = $frompost[ 'table_id' ];
//            $event -> target_id = $frompost[ 'target_id' ];
//            $activity           = $this -> _transport -> getGateway( 'Activity' ) -> get( $event -> id );
//            $activity -> merge( $event -> toActivity() );
//            $this -> _transport -> getGateway( 'Activity' ) -> save( $activity );
//        }
//        elseif ( substr( $this -> getAction(), 0, 3 ) == 'add' )
//        {
//            $event               = $this -> getModel();
//            $frompost          = $this -> getController() -> params() -> fromPost()['additional'];
//            $event -> table_id  = $frompost[ 'table_id' ];
//            $event -> target_id = $frompost[ 'target_id' ];
//            $activity            = new ModelActivity();
//            $activity -> exchangeArray( $event -> toActivity() );
//            $activity -> type_id = Table::EVENT;
//            $this -> setDate( $activity, 'created_dtm', 'Y-m-d H:i:s' );
//            $this -> _transport -> getGateway( 'Activity' ) -> save( $activity );
//            $activity_id         = $this -> _transport -> getGateway( 'Activity' ) -> find( $activity -> toArray() ) -> current() -> id;
//            $event -> id         = $activity_id;
//        }
//    }
//    public function setTarget( $model, $key )
//    {
//        if ( !empty( $model -> $key ) )
//        {
//            $this -> fillJoinsConvert( $model );
//            $gw             = substr( $model -> table, 0, -1 );
//            $target         = $this -> _transport -> getGateway( $gw ) -> findOne( ['_id' => $model -> target_id ] );
//            $model-> target = $target -> fname." ".$target->lname;
//        }
//        return $model -> target;
//    }

    public function setTarget($model, $key)
    {
        if (!empty($model->$key)) {
            $this->fillJoinsConvert($model);
            $gw = Table::getTransportName($model->table_id);
//            $gw = substr($model->table, 0, -1);
            $target        = $this->_transport->getGateway($gw)->findOne([ '_id' => $model->target_id ]);
            $model->target = $target->fname." ".$target->lname;
        }

        return $model->target;
    }
}
