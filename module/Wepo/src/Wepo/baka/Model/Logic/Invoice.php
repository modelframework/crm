<?php

namespace Wepo\Model\Logic;

use Wepo\Lib\GatewayService;
use Wepo\Lib\DataLogic;
use Wepo\Model\Status;

class Invoice extends DataLogic
{
    protected $_rules     = [
        'view'  => [
                'title'       => [ 'type' => 'function', 'value' => 'setTitle', 'params' => [ 'subject' ] ],
                'contact'     => [
                    'type' => 'function', 'value' => 'setTitle', 'params' => [ 'contactfname', 'contactlname' ],
                ],
            ],
        'convert_from_order' => [
            'owner_id'    => [ 'type' => 'function', 'value' => 'setOwner' ],
            'status_id'   => [ 'type' => 'const', 'value' => Status::NEW_ ],
            'created_dtm' => [ 'type' => 'function', 'value' => 'setDate', 'params' => 'Y-m-d H:i:s' ],
            'changer_id'  => [ 'type' => 'function', 'value' => 'setUser' ],
            'changed_dtm' => [ 'type' => 'function', 'value' => 'setDate', 'params' => 'Y-m-d H:i:s' ],
            'title'       => [ 'type' => 'function', 'value' => 'setTitle', 'params' => [ 'subject' ] ],
            'contact'     => [
                'type' => 'function', 'value' => 'setTitle', 'params' => [ 'contactfname', 'contactlname' ],
            ],
            'total_discount' => [ 'type' => 'function', 'value' => 'setTotalDiscount', 'params' => [ 'dsc' => 'modifier', 'dsc_type' => 'price_type', 'prd_price' => 'raw_price' ] ],
            'total_tax' => [ 'type' => 'function', 'value' => 'setTotalTax', 'params' => [ 'ttl_field' => 'total_price', 'txs_fields' => [ 'vat', 'sales_tax' ] ] ],
        ],
//        'view'=>[
//            'title'       => [ 'type' => 'function', 'value' => 'setTitle', 'params' => [ 'subject' ] ],
//            'contact'     => [
//                'type' => 'function', 'value' => 'setTitle', 'params' => [ 'contactfname', 'contactlname' ]
//            ]
//        ]
     ];

    protected $_exchange_converted_fields = [
        'from_model' => [
        ],
        'to_model' => [
            '_id' => 'invoice_id',
            'total_price' => 'sum',
        ],
    ];

    private $_transport = null;

    public function __construct(GatewayService $transport)
    {
        $this->_transport = $transport;
        parent::__construct($transport);
    }

    public function prerecycle($event)
    {
        parent::prerecycle($event);
        $this->recycleChain();
    }

    public function recycle($event)
    {
        $this->setEvent($event);
        $ids = [ ];
        if (!is_array($this->getModel())) {
            $modelname = $this->getModel()->getModelName();
            $ids[]    = $this->getModel()->id();
        } else {
            $models = $this->getModel();
            foreach ($models as $model) {
                $ids[] = $model->id();
            }
            $modelname = reset($models)->getModelName();
        }
        $this->_transport->getGateway('Payment')->update([ 'invoice' => null, 'invoice_id' => '' ], [ 'invoice_id' => $ids ]);
        $this->_transport->getGateway($modelname)->delete([ '_id' => $ids ]);
    }

//    public function convert($event)
//    {
//        parent::convert($event);
//
////        $id_gw = $this->_transport->getGateway('InvoiceDetail');
////        $pd_gw = $this->_transport->getGateway('PaymentDetail');
////
////        $invoice               = $this->getModel();
////        $invoice_details       =
////            $id_gw->find( array( 'invoice_id' => $invoice->id() ) );
////        $payment               = $this->getParam('to_model');
////
////        $payment_id = $payment->id();
////
////        foreach ( $invoice_details as $invoice_detail )
////        {
////            $payment_detail                = $this->_transport->getModel( 'PaymentDetail' );
////            $payment_detail->exchangeArray($payment_detail->data());
////            $payment_detail->payment_id      = $payment_id;
////            $this->getController()->trigger('presave', $payment_detail);
////            $pd_gw->save( $payment_detail );
////        }
//    }


    protected function setTotalDiscount($model, $key, $params = [])
    {
        $modelFrom = $this->getParam('from_model');
        $qty = isset($params['qty']) ? $modelFrom->$params['qty'] : 1;
        if (isset($params['dsc']) && isset($params['dsc_type']) && isset($params['prd_price'])) {
            $dsc = $params['dsc'];
            $dsc_type = $params['dsc_type'];
            $prd_price = $params['prd_price'];
        } else {
            throw new \Exception('discount, product price and discount type doesn\'t set as params in countPrice logic method');
        }
//        prn($model,$dsc_type,$prd_price,$dsc,$qty);

        if ($modelFrom->$dsc_type == '% of Price') {
            $model->$key = $modelFrom->$prd_price - max([ $modelFrom->$prd_price * $qty * $modelFrom->$dsc/100, 0 ]);
        } else {
            //            prn('heare');
//            prn($model -> $prd_price,$model->$dsc,$qty);
            $model->$key = max([ $modelFrom->$dsc, 0 ]);
        }

//        prn($model->$key);
//        exit;

        return $model->$key;
    }

    protected function setTotalTax($model, $key, $params = [])
    {
        $modelFrom = $this->getParam('from_model');
        if (isset($params['txs_fields']) && isset($params['txs_fields'])) {
            $taxes = 0;
            foreach ($params['txs_fields'] as $taxField) {
                $taxes += $modelFrom->$taxField;
            }
            $taxes = $modelFrom->$params['ttl_field']*$taxes/100;
            $model->$key = $taxes;

            return $model->$key;
        } else {
            throw new \Exception('txs_fields or ttl_fields doesn\'t set');
        }
    }

    public function recycleChain()
    {
        $models    = $this->getModel();
        $model_ids = [ ];
        foreach ($models as $_model) {
            $model_ids[] = $_model->id();
        }
        $this->_transport->getGateway('InvoiceDetail')->delete([ 'invoice_id' => $model_ids ]);
    }
}
