<?php

namespace Wepo\Model\Logic;

use Wepo\Lib\GatewayService;
use Wepo\Lib\DataLogic;
use Wepo\Model\Table;
use Wepo\Model\Status;
use Wepo\Model\Mail as MailModel;

class Mail extends DataLogic
{
    protected $_rules   = [
        'add'  => [
            'owner_id'    => [ 'type' => 'function', 'value' => 'setOwner' ],
            'status_id'   => [ 'type' => 'const', 'value' => Status::NORMAL ],
            'email_link'  => [ 'type' => 'function', 'value' => 'setEmailsLinks' ],
            'created_dtm' => [ 'type' => 'function', 'value' => 'setDate' ],
            'view_to'     => [ 'type' => 'function', 'value' => 'setEmailList'],
            'view_from'   => [ 'type' => 'function', 'value' => 'setEmailList'],
        ],
        'sync' => [
            'owner_id'    => [ 'type' => 'function', 'value' => 'setOwner' ],
            'status_id'   => [ 'type' => 'function', 'value' => 'setStatus' ],
            'email_link'  => [ 'type' => 'function', 'value' => 'setEmailsLinks' ],
            'created_dtm' => [ 'type' => 'function', 'value' => 'setDate' ],
            'view_to'     => [ 'type' => 'function', 'value' => 'setEmailList'],
            'view_from'   => [ 'type' => 'function', 'value' => 'setEmailList'],
        ],
        'edit' => [
//            'owner_id'  => [ 'type' => 'function', 'value' => 'setOwner' ],
            'status_id'  => [ 'type' => 'const', 'value' => Status::NORMAL ],
            'email_link' => [ 'type' => 'function', 'value' => 'setEmailsLinks' ],
        ],
    ];
    private $_transport = null;

    public function __construct(GatewayService $transport)
    {
        $this->_transport = $transport;
        parent::__construct($transport);
    }

    public function presend($event)
    {
        $model      = $event->getParams()[ 'model' ];
//        $contact_id = $event -> getParams()[ 'data' ][ 'fields' ][ 'contact_id' ];
//        $contact    = $this -> _transport -> getGateway( 'Contact' ) -> find( ['_id' => $contact_id ] ) -> current();

        $user    = $event->getTarget()->user()->id();
        $setting = $this->_transport->getGateway('MailSetting')->find(['user_id' => $user ])->current();

        $model->from            = $setting->email;
//        $model -> to              = $contact -> email;
        $model->mail_setting_id = $setting->id();
        $model->type            = MailModel::outbox;
        $model->size            = strlen($model->text.$model->title);
    }

    protected function setStatus($model, $key)
    {
        if (empty($model->$key)) {
            if ($model->type == 'inbox') {
                $model->$key = Status::NEW_;
            } else {
                $model->$key = Status::NORMAL;
            }
        }

        return $model->$key;
    }

    protected function setEmailsLinks($model, $key)
    {
        if (empty($model->$key)) {
            $mailLinkKey = $model->type == 'inbox' ? 'from' : 'to';

            preg_match_all('/\b[A-Z0-9._%+-]+@(?:[A-Z0-9-]+\.)+[A-Z]{2,6}\b/i', $model->$mailLinkKey, $matches, PREG_PATTERN_ORDER);

            $emails = $matches[ 0 ];

            $emails = $this->getController()->table('Email')->find([ 'email' => $emails ]);

            $email_ids = [];

            foreach ($emails as $email) {
                $email_ids[] = $email->id();
            }

            $model->$key = $email_ids;
        }

        return $model->$key;
    }

    public function setEmailList($model, $key)
    {
        $target = $key == 'view_to' ? 'to' : 'from';
        $emails = $model->$target;

        preg_match_all('/\b[A-Z0-9._%+-]+@(?:[A-Z0-9-]+\.)+[A-Z]{2,6}\b/i', $emails, $matches, PREG_PATTERN_ORDER);

        $emails = $matches[0];
        array_walk(
            $emails,
            function (&$value, $key) {
                $email = explode('@', $value);
                $email[0] = strtolower($email[0]);
                $value = trim(implode('@', $email));
            }
        );

        $emails_to_remove = [];
        $known_emails = $this->_transport->getGateway('Email')->find(['email' => $emails]);
        foreach ($known_emails as $email_obj) {
            $source = $email_obj->user_name_source;
            $source_id = Table::getTableId($source);
            $email_id = $email_obj->id();
            $id = $this->_transport->getGateway($source)->findOne(['email_id' => $email_id]);
            if (isset($id)) {
                $emails_to_remove[] = $id->email;
                $id = $id->id();
                $accepted_emails[$source_id][$id] = $email_obj->user_name;
            }
        }
        $accepted_emails['Other'] = array_filter($emails, function ($value) use ($emails_to_remove) {return !in_array($value, $emails_to_remove);});
        if (!count($accepted_emails['Other'])) {
            unset($accepted_emails['Other']);
        }
        $model->$key = $accepted_emails;

        return $model->$key;
    }

    public function prerecycle($event)
    {
        $this->setEvent($event);
        $ids = [ ];
        if (!is_array($this->getModel())) {
            $modelname = $this->getModel()->getModelName();
            $ids[]     = $this->getModel()->id();
        } else {
            $models = $this->getModel();
            foreach ($models as $model) {
                $ids[] = $model->id();
            }
            $modelname = reset($models)->getModelName();
        }
        switch ($this->getAction()) {
            case 'restore':
                $this->_transport->getGateway($modelname)->update([ 'status_id' => Status::NORMAL ], [ '_id' => $ids ]);
                break;
            case 'delete':
                $this->_transport->getGateway($modelname)->update([ 'status_id' => Status::DELETED ], [ '_id' => $ids ]);
                break;
        }
    }
}
