<?php

namespace Wepo\Model\Logic;

use ModelFramework\LogicService\DataLogic;
use Wepo\Lib\WepoTransport;

class ConfigModel extends DataLogic
{
    public static $adapter = 'wepo_company_mongo';

    protected $_rules     = [
        'add'  => [],
        'edit' => [],
    ];
    private $_transport = null;

    public function __construct(WepoTransport $transport)
    {
        $this->_transport = $transport;
        parent::__construct($transport);
    }

    public function presave($event)
    {
        parent::presave($event);
    }
}
