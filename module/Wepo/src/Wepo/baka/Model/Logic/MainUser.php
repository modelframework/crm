<?php

namespace Wepo\Model\Logic;

use ModelFramework\LogicService\DataLogic;
use Wepo\Model\User;
use Wepo\Model\Status;
use Wepo\Model\Role;

class MainUser extends DataLogic
{
    public static $adapter = 'wepo_main';
    protected $_rules = [
        'signup' => [
            'role_id'     => [ 'type' => 'const', 'value' => Role::SENIOR ],
            'status_id'   => [ 'type' => 'const', 'value' => Status::NEW_ ],
            'created_dtm' => [ 'type' => 'function', 'value' => 'setDate', 'params' => 'Y-m-d H:i:s' ],
        ],
    ];

    public function signin($event)
    {
        //        prn($event);
//        exit();

        $this->setEvent($event);
        $mainUser = $this->getEventObjects();
        $company  = $this->getGateway('MainCompany')->findOne([ '_id' => $mainUser->company_id ]);

        # :TODO: add helpful message about missing company or missing local user account
        $dbs = $this->getGateway('MainDb')->find([ 'company_id' => $company->_id ]);
        if ($dbs->count() > 0) {
            $db         = $dbs->current();
            $connection =
                $this->getController()->getServiceLocator()->get('wepo_company')->getDriver()->getConnection();
            $connection->setConnectionParameters($db->toArray());
            $user = $this->getGateway('User')->findOne([ 'main_id' => $mainUser->_id ]);
            $this->getController()->User($user);
        } else {
            throw new \Exception('Could not connect to db');
        }
    }

    public function signup($event)
    {
        $this->setEvent($event);
        $mainUser = $this->getEventObjects();
        $this->getGateway('MainUser')->save($mainUser);
        $mainUserInstall = $this->getGateway('MainUser')->findOne($mainUser->toArray());
        if ($mainUserInstall != null) {
            //            $mainCompany = $this-> getGateway( 'MainCompany' ) -> get( $mainUserInstall -> company_id );
            $dbs =
                $this->getGateway('MainDb')->find([ 'company_id' => $mainUserInstall->company_id ]);
            if ($dbs->count() > 0) {
                $db         = $dbs->current();
                $connection =
                    $this->getController()->getServiceLocator()->get('wepo_company')->getDriver()->getConnection();
                $connection->setConnectionParameters($db->getParamsArray());
                $user = new User();
                $user->exchangeArray($mainUserInstall->toArray());
                $user->main_id = $mainUserInstall->_id;
                $this->getGateway('User')->save($user);
            }
        }
    }
}
