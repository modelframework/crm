<?php

namespace Wepo\Model\Logic;

use ModelFramework\LogicService\DataLogic;
use Wepo\Model\Status;

class Lead extends DataLogic
{
    protected $_rules = [
        'add'     => [
            'owner_id'    => [ 'type' => 'function', 'value' => 'setOwner' ],
            'created_dtm' => [ 'type' => 'function', 'value' => 'setDate', 'params' => 'Y-m-d H:i:s' ],
            'changer_id'  => [ 'type' => 'function', 'value' => 'setUser' ],
            'changed_dtm' => [ 'type' => 'function', 'value' => 'setDate', 'params' => 'Y-m-d H:i:s' ],
            'email'       => [ 'type' => 'function', 'value' => 'updateEmails' ],
            'email_id'    => [ 'type' => 'function', 'value' => 'setEmailId' ],
            'title'       => [ 'type' => 'function', 'value' => 'setTitle', 'params' => [ 'fname', 'lname' ] ],
            'age'         => [ 'type' => 'function', 'value' => 'setAge', 'params' => "birth_date" ],
            //            'status_id'   => [ 'type'  => 'const', 'value' => Status::NEW_ ],
            'status_id'   => [ 'type' => 'function', 'value' => 'newItem' ],
            'owner'       => [ 'type' => 'function', 'value' => 'setTitle', 'params' => [ 'ownerfname', 'ownerlname' ] ],
            'changer'     => [ 'type' => 'function', 'value' => 'setTitle', 'params' => [ 'changerfname', 'changerlname' ] ],
        ],
        'edit'    => [
            'owner_id'    => [ 'type' => 'function', 'value' => 'setOwner' ],
            'age'         => [ 'type' => 'function', 'value' => 'setAge', 'params' => 'birth_date' ],
            'changer_id'  => [ 'type' => 'function', 'value' => 'setUser' ],
            'changed_dtm' => [ 'type' => 'function', 'value' => 'setDate', 'params' => 'Y-m-d H:i:s' ],
            'email'       => [ 'type' => 'function', 'value' => 'updateEmails' ],
            'email_id'    => [ 'type' => 'function', 'value' => 'setEmailId' ],
            'title'       => [ 'type' => 'function', 'value' => 'setTitle', 'params' => [ 'fname', 'lname' ] ],
            'owner'       => [ 'type' => 'function', 'value' => 'setTitle', 'params' => [ 'ownerfname', 'ownerlname' ] ],
            'changer'     => [ 'type' => 'function', 'value' => 'setTitle', 'params' => [ 'changerfname', 'changerlname' ] ],
            //            'status_id'   => [ 'type'  => 'const', 'value' => Status::NORMAL ],
        ],
        'view'    => [
            'status_id' => [ 'type' => 'function', 'value' => 'looked' ],
            'age'         => [ 'type' => 'function', 'value' => 'setAge', 'params' => 'birth_date' ],
            'owner'       => [ 'type' => 'function', 'value' => 'setTitle', 'params' => [ 'ownerfname', 'ownerlname' ] ],
            'changer'       => [ 'type' => 'function', 'value' => 'setTitle', 'params' => [ 'changerfname', 'changerlname' ] ],
        ],
        'convert_to_contact' => [
            'status_id' => [ 'type' => 'function', 'value' => 'converted' ],
        ],
        //        'delete' =>[
        //            'status_id' => [ 'type' => 'function', 'value' => 'looked' ],
        //        ]
    ];

    protected $_exchange_converted_fields = [
        'from_model' => [
            'contact_id' => '_id',
        ],
        'to_model' => [
        ],
    ];

    private $_transport = null;

    public function presave($event)
    {
        parent::presave($event);
//        $this->setAvatar();
    }

    public function postsave($event)
    {
        //        $this->checkAttach();
//        $this->checkAсtivity();
//        $this->checkNote();
        parent::postsave($event);
    }

    public function converted($model, $key)
    {
        $this->looked($model, $key);
        $model->$key = Status::CONVERTED;

        return $model->$key;
    }

    public function recycle($event)
    {
        $this->setEvent($event);
        $ids = [ ];
        if (!is_array($this->getModel())) {
            $modelname = $this->getModel()->getModelName();
            $ids[ ]    = $this->getModel()->id();
            $this->looked($this->getModel(), 'status_id');
        } else {
            $models = $this->getModel();
            foreach ($models as $model) {
                $ids[ ] = $model->id();
                $this->looked($model, 'status_id');
            }
            $modelname = reset($models)->getModelName();
        }

        switch ($this->getAction()) {
            case 'restore':
                $this->_transport->getGateway($modelname)->update([ 'status'    => Status::getLabel(Status::NORMAL),
                                                                       'status_id' => Status::NORMAL,
                                                                     ], [ '_id' => $ids ]);
                break;
            case 'delete':
                $this->_transport->getGateway($modelname)->update([ 'status'    => Status::getLabel(Status::DELETED),
                                                                       'status_id' => Status::DELETED,
                                                                     ], [ '_id' => $ids ]);
                break;
            case 'clean':
                $this->_transport->getGateway($modelname)->delete([ '_id' => $ids ]);
                break;
        }
    }

    public function convert($event)
    {
        parent::convert($event);
        $this->checkAttach();
        $this->checkAсtivity();
        $this->checkNote();
    }

    public function setAvatar()
    {
        $user     = $this->getModel();
        $request  = $this->getController()->getRequest();
        $filename = false;
        if ($request->isPost()) {
            $files    = $request->getFiles();
            $fileinfo = $files->get('fields')[ 'avatar-file' ];
            $fs       = $this->getController()->getServiceLocator()->get('\Wepo\Lib\FileService');
            $filename = $fs->saveFile($fileinfo[ 'name' ], $fileinfo[ 'tmp_name' ], true, 'Leads');
        }
        if ($filename) {
            $user->avatar = basename($filename);
        }
    }

    public function checkAttach()
    {
        $lead  = $this->getModel();
        $id = $lead->id();
        $title = $lead->fname." ".$lead->lname;
        if (!empty($lead->contact_id)) {
            $cont  = $this->_transport->getGateway('Contact')->find([ '_id' => $lead->contact_id ])->current();
        } else {
            $cont  = $this->_transport->getGateway('Contact')->find([ 'created_dtm' => $lead->changed_dtm ])->current();
        }
        $docs        = $this->_transport->getGateway('Document')->find(['attach_to.Lead' => [ $id ]]);
        if (!empty($docs) && !empty($cont)) {
            foreach ($docs as $_doc) {
                $newAtt = $_doc->attach_to;
                $newAtt['Contact'][] = $cont->id();
                $this->_transport->getGateway('Document')->update([ 'attach_to' => $newAtt ], ['attach_to.Lead' => [ $id ]]);
                $newAtt = $_doc->attach_to_names;
                $newAtt[ucfirst($cont->getModelLabel())][] = $title;
                $this->_transport->getGateway('Document')->update([ 'attach_to_names' => $newAtt ], ['attach_to.Lead' => [ $id ]]);
            }
        }
    }

    public function checkAсtivity()
    {
        $lead  = $this->getModel();
        $target_id = $lead->id();
        $target = $lead->fname." ".$lead->lname;
        if (!empty($lead->contact_id)) {
            $cont  = $this->_transport->getGateway('Contact')->find([ '_id' => $lead->contact_id ])->current();
        } else {
            $cont  = $this->_transport->getGateway('Contact')->find([ 'created_dtm' => $lead->changed_dtm ])->current();
        }
        $activs        = $this->_transport->getGateway('Activity')->find(['target_id' =>  $target_id, 'table_id' => \Wepo\Model\Table::LEAD ]);
        if (!empty($activs) && !empty($cont)) {
            $this->_transport->getGateway('Activity')->update([ 'table' => ucfirst($cont->getModelLabel()), 'target' => $target, 'table_id' => \Wepo\Model\Table::CONTACT, 'target_id' => $cont->id() ], ['target_id' =>  $target_id, 'table_id' => \Wepo\Model\Table::LEAD ]);
        }
    }

    public function checkNote()
    {
        $lead  = $this->getModel();
        $target_id = $lead->id();
        if (!empty($lead->contact_id)) {
            $cont  = $this->_transport->getGateway('Contact')->find([ '_id' => $lead->contact_id ])->current();
        } else {
            $cont  = $this->_transport->getGateway('Contact')->find([ 'created_dtm' => $lead->changed_dtm ])->current();
        }
        $notes        = $this->_transport->getGateway('Note')->find(['parent_id' =>  $target_id, 'table_id' => \Wepo\Model\Table::LEAD ]);
        if (!empty($notes) && !empty($cont)) {
            $this->_transport->getGateway('Note')->update([ 'table_id' => \Wepo\Model\Table::CONTACT, 'parent_id' => $cont->id() ], ['parent_id' =>  $target_id, 'table_id' => \Wepo\Model\Table::LEAD ]);
        }
    }
}
