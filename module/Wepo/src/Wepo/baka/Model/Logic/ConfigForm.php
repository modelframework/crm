<?php

namespace Wepo\Model\Logic;

use ModelFramework\LogicService\DataLogic;
use Wepo\Lib\GatewayService;

class ConfigForm extends DataLogic
{
    public static $adapter = 'wepo_company_mongo';

    protected $_rules     = [
        'add'  => [],
        'edit' => [],
    ];
    private $_transport = null;

    public function __construct(GatewayService $transport)
    {
        $this->_transport = $transport;
        parent::__construct($transport);
    }

    public function presave($event)
    {
        parent::presave($event);
    }
}
