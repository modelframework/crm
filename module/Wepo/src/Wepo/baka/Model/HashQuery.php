<?php

namespace Wepo\Model;

use Zend\InputFilter\Factory as InputFactory;
use Zend\InputFilter\InputFilter;
use Wepo\Lib\WepoModel;

class HashQuery extends WepoModel
{
    public $table_name = 'HashQuery';

    const TABLE_NAME = 'HashQuery';

    protected $_fields = [
        'id'    => [ 'type'     => 'pk', 'datatype'    => 'string', 'default' => 0 ],
        'hash' => [ 'type'     => 'field', 'datatype'    => 'string', 'default' => '' ],
        'md5tag' => [ 'type'     => 'field', 'datatype'    => 'string', 'default' => '' ],
        'params'   => [ 'type'     => 'field', 'datatype'    => 'array', 'default' => [] ],
    ];

    protected $inputFilter;

    public function getInputFilter()
    {
        if (!$this->inputFilter) {
            $inputFilter = new InputFilter();
            $factory     = new InputFactory();

            $this->inputFilter = $inputFilter;
        }

        return $this->inputFilter;
    }
}
