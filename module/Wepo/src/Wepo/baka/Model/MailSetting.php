<?php

namespace Wepo\Model;

use Zend\InputFilter\InputFilter;
use Wepo\Lib\WepoMongoModel;

class MailSetting extends WepoMongoModel
{
    public $table_name            = 'mail_setting';
    private static $securityTypes = ['none', 'ssl', 'tls' ];
    private static $protocolTypes = [
        'receive' => ['Imap', 'Pop3' ],
        'send'    => [ 'SMTP' ],
    ];

    const TABLE_NAME = 'mail_setting';

    protected $_fields = [
        '_id'              => [ 'type' => 'pk', 'datatype' => 'string', 'default' => '' ],
        'email'            => [ 'type' => 'field', 'datatype' => 'string', 'default' => '' ],
        'setting_user'     => [ 'type' => 'field', 'datatype' => 'string', 'default' => '' ],
        'setting_protocol' => [ 'type' => 'field', 'datatype' => 'string', 'default' => '' ],
        'setting_host'     => [ 'type' => 'field', 'datatype' => 'string', 'default' => '' ],
        'setting_port'     => [ 'type' => 'field', 'datatype' => 'int', 'default' => 0 ],
        'setting_security' => [ 'type' => 'field', 'datatype' => 'string', 'default' => '' ],
        'pass'             => [ 'type' => 'field', 'datatype' => 'string', 'default' => '' ],
        'type'             => [ 'type' => 'field', 'datatype' => 'string', 'default' => '' ],
//        'additional_settings' => [ 'type' => 'field', 'datatype' => 'array', 'default' => '' ],
        'owner_id'         => [ 'type' => 'source', 'datatype' => 'string', 'default' => '' ],
        'user_id'          => [ 'type' => 'source', 'datatype' => 'string', 'default' => '' ],
        'status_id'        => [ 'type' => 'source', 'datatype' => 'string', 'default' => '' ],
        'owner'            => [ 'type' => 'alias', 'datatype' => 'string', 'default' => '', 'source' => 'owner_id' ],
        'user'             => [ 'type' => 'alias', 'datatype' => 'string', 'default' => '', 'source' => 'user_id' ],
        'status'           => [ 'type' => 'alias', 'datatype' => 'string', 'default' => '', 'source' => 'status_id' ],
        'title'        => [ 'type' => 'alias', 'datatype' => 'string', 'default' => '' ],
        ////////////////////
        'ownerfname'       => [ 'type' => 'alias', 'datatype' => 'string', 'default' => '', 'source' => 'owner_id'],
        'ownerlname'       => [ 'type' => 'alias', 'datatype' => 'string', 'default' => '', 'source' => 'owner_id'],
    ];
    protected $_joins  = [
        [ 'model' => 'User', 'on' => [ 'owner_id' => '_id' ], 'fields' => [ 'owner' => 'login', 'ownerfname' => 'fname', 'ownerlname' => 'lname' ] ],
        [ 'model' => 'User', 'on' => [ 'user_id' => '_id' ], 'fields' => [ 'user' => 'login' ] ],
        [ 'model' => 'Status', 'on' => [ 'status_id' => '_id' ], 'fields' => [ 'status' => 'status' ] ],
    ];

    protected $_unique = [
        ['email','setting_protocol'],
    ];

    protected $inputFilter;

    public static function securityTypes()
    {
        $ret = [ ];
        foreach (self :: $securityTypes as $value) {
            $ret[ $value ] = $value;
        }

        return $ret;
    }

    public static function protocolTypes()
    {
        $res = [ ];
        foreach (self :: $protocolTypes as $types) {
            $res = array_merge($res, $types);
        }
        $res = array_combine($res, $res);

        return $res;
    }

    public static function receiveProtocols()
    {
        return self :: $protocolTypes[ 'receive' ];
    }

    public static function sendProtocols()
    {
        return self:: $protocolTypes[ 'send' ];
    }

    public function getProtocolType()
    {
        if (!isset($this->setting_protocol)) {
            throw new \Exception('setting is configuret yet');
        }
        foreach (self::$protocolTypes as $type => $protocols) {
            if (in_array($this->setting_protocol, $protocols)) {
                return ucfirst($type);
            }
        }
        throw new \Exception('setting protocol didn\'t registered in types');
    }

    public function getProtocolSetting()
    {
        if (!isset($this->setting_protocol) || (!in_array($this->setting_protocol, self::receiveProtocols()))) {
            throw new Exception('selected mail setting is uncomplete or can\'t be used to fetch mails');
        }
        $res = $this->toArray();

        return array(
            'host'     => $res[ 'setting_host' ],
            'user'     => $res[ 'setting_user' ],
            'password' => $res[ 'pass' ],
            'ssl'      => $res[ 'setting_security' ],
            'port'     => $res[ 'setting_port' ],
        );
    }

    public function getSMTPSetting()
    {
        if (!isset($this->setting_protocol) || (!in_array($this->setting_protocol, self::sendProtocols()))) {
            throw new \Exception('selected mail setting is uncomplete or can\'t be used to send mails');
        }
        $res = $this->toArray();

        return array(
            'name'              => 'Wepo',
            'host'              => $res[ 'setting_host' ],
            'port'              => $res[ 'setting_port' ],
            'connection_class'  => 'login',
            'connection_config' => array(
                'ssl'      => $res[ 'setting_security' ],
                'username' => $res[ 'setting_user' ],
                'password' => $res[ 'pass' ],
            ),
        );
    }

//    public function setInputFilter(InputFilterInterface $inputFilter)
//    {
//        $this ->
//    }

    public function getName()
    {
        return $this->user.'  -  '.$this->email;
    }
}
