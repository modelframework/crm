<?php

namespace Wepo\Model;

use Zend\InputFilter\Factory as InputFactory;
use Zend\InputFilter\InputFilter;
use Wepo\Lib\WepoMongoModel;

class UserMongo extends WepoMongoModel
{
    public $table_name = 'user';
    public $adapterName = 'wepo_company_mongo';

    const TABLE_NAME = 'user';

    protected $_fields = [
        '_id'         => [ 'type' => 'pk', 'datatype' => 'string', 'default' => '' ],
        'id'          => [ 'type' => 'field', 'datatype' => 'int', 'default' => 0 ],
        'main_id'     => [ 'type' => 'field', 'datatype' => 'int', 'default' => null ],
        'fname'       => [ 'type' => 'field', 'datatype' => 'string', 'default' => '' ],
        'lname'       => [ 'type' => 'field', 'datatype' => 'string', 'default' => '' ],
        'role_id'     => [ 'type' => 'source', 'datatype' => 'int', 'default' => null, 'alias' => 'role' ],
        'login'       => [ 'type' => 'field', 'datatype' => 'string', 'default' => '' ],
        'password'    => [ 'type' => 'field', 'datatype' => 'string', 'default' => '' ],
        'ip'          => [ 'type' => 'field', 'datatype' => 'string', 'default' => '' ],
        'birth_date'  => [ 'type' => 'field', 'datatype' => 'string', 'default' => '' ],
        'changer_id'  => [ 'type' => 'source', 'datatype' => 'int', 'default' => null, 'alias' => 'changer' ],
        'changed_dtm' => [ 'type' => 'field', 'datatype' => 'string', 'default' => '' ],
        'created_dtm' => [ 'type' => 'field', 'datatype' => 'string', 'default' => '' ],
        'status_id'   => [ 'type' => 'source', 'datatype' => 'int', 'default' => 1, 'alias' => 'status' ],
        'role'        => [ 'type' => 'alias', 'datatype' => 'string', 'default' => '', 'source' => 'role_id' ],
        'changer'     => [ 'type' => 'alias', 'datatype' => 'string', 'default' => '', 'source' => 'changer_id' ],
        'status'      => [ 'type' => 'alias', 'datatype' => 'string', 'default' => '', 'source' => 'status_id' ],
    ];
    protected $_joins  = [
        [ 'model' => 'Role', 'on' => [ 'role_id' => 'id' ], 'fields' => [ 'role' => 'role' ] ],
        [ 'model' => 'User', 'on' => [ 'changer_id' => 'id' ], 'fields' => [ 'changer' => 'login' ] ],
        [ 'model' => 'Status', 'on' => [ 'status_id' => 'id' ], 'fields' => [ 'status' => 'status' ] ],
    ];
    protected $_unique = [ 'login' ];

    public function getInputFilter()
    {
        if (!$this->inputFilter) {
            $inputFilter         = new InputFilter();
            $factory             = new InputFactory();
            $inputFilter->add($factory->createInput(array(
                    'name'     => 'id',
                    'required' => true,
                    'filters'  => array(
                        array( 'name' => 'Int' ),
                    ),
            )));
            $inputFilter->add($factory->createInput(array(
                    'name'     => 'role_id',
                    'required' => false,
                    'filters'  => array(
                        array( 'name' => 'Int' ),
                    ),
            )));
            $inputFilter->add($factory->createInput(array(
                    'name'       => 'fname',
                    'required'   => true,
                    'filters'    => array(
                        array( 'name' => 'StripTags' ),
                        array( 'name' => 'StringTrim' ),
                    ),
                    'validators' => array(
                        array(
                            'name'    => 'StringLength',
                            'options' => array(
                                'encoding' => 'UTF-8',
                                'min'      => 1,
                                'max'      => 100,
                            ),
                        ),
                    ),
            )));
            $inputFilter->add($factory->createInput(array(
                    'name'       => 'lname',
                    'required'   => true,
                    'filters'    => array(
                        array( 'name' => 'StripTags' ),
                        array( 'name' => 'StringTrim' ),
                    ),
                    'validators' => array(
                        array(
                            'name'    => 'StringLength',
                            'options' => array(
                                'encoding' => 'UTF-8',
                                'min'      => 1,
                                'max'      => 100,
                            ),
                        ),
                    ),
            )));
            $inputFilter->add($factory->createInput(array(
                    'name'       => 'login',
                    'required'   => true,
                    'filters'    => array(
                        array( 'name' => 'StripTags' ),
                        array( 'name' => 'StringTrim' ),
                    ),
                    'validators' => array(
                        array(
                            'name'    => 'StringLength',
                            'options' => array(
                                'encoding' => 'UTF-8',
                                'min'      => 3,
                                'max'      => 100,
                            ),
                        ),
                    ),
            )));
            $inputFilter->add($factory->createInput(array(
                    'name'       => 'password',
                    'required'   => true,
                    'filters'    => array(
                        array( 'name' => 'StripTags' ),
                        array( 'name' => 'StringTrim' ),
                    ),
                    'validators' => array(
                        array(
                            'name'    => 'StringLength',
                            'options' => array(
                                'encoding' => 'UTF-8',
                                'min'      => 3,
                                'max'      => 100,
                            ),
                        ),
                    ),
            )));
            $inputFilter->add($factory->createInput(array(
                    'name'       => 'birth_date',
                    'required'   => false,
                    'filters'    => array(
                        array( 'name' => 'StripTags' ),
                        array( 'name' => 'StringTrim' ),
                    ),
                    'validators' => array(
                        array(
                            'name'    => 'StringLength',
                            'options' => array(
                                'encoding' => 'UTF-8',
                                'min'      => 10,
                                'max'      => 20,
                            ),
                        ),
                        array(
                            'name'    => 'Date',
                            'options' => array(
                                'format' => 'Y-m-d',
                            ),
                        ),
                        array(
                            'name'    => 'Between',
                            'options' => array(
                                'min' => '1940-01-01',
                                'max' => date('Y-m-d', time()),
                            ),
                        ),
                    ),
            )));
            $this->inputFilter = $inputFilter;
        }

        return $this->inputFilter;
    }

    public function getName()
    {
        return $this->login;
    }

    public function getRole()
    {
        return $this->role_id;
    }

    public function postExchange()
    {
        $this->password = md5($this->password);

        return parent::postExchange();
    }
}
