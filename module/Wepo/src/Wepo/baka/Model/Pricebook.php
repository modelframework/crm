<?php

namespace Wepo\Model;

use Zend\InputFilter\Factory as InputFactory;
use Zend\InputFilter\InputFilter;
use Wepo\Lib\WepoMongoModel;

class Pricebook extends WepoMongoModel
{
    public $table_name = 'pricebook';

    const TABLE_NAME = 'pricebook';

    protected $_fields = [
        '_id'          => [ 'type' => 'pk', 'datatype' => 'string', 'default' => '' ],
        'pricebook'    => [ 'type' => 'field', 'datatype' => 'string', 'default' => '' ],
        'owner_id'     => [ 'type' => 'source', 'datatype' => 'string', 'default' => 0, 'alias' => 'owner' ],
        'changer_id'   => [ 'type' => 'source', 'datatype' => 'string', 'default' => 0, 'alias' => 'changer' ],
        'status_id'    => [ 'type' => 'source', 'datatype' => 'string', 'default' => 0, 'alias' => 'status' ],
        'created_dtm'  => [ 'type' => 'field', 'datatype' => 'string', 'default' => '' ],
        'description'  => [ 'type' => 'field', 'datatype' => 'string', 'default' => '' ],
        'changed_dtm'  => [ 'type' => 'field', 'datatype' => 'string', 'default' => '' ],
        'ownerfname'   => [ 'type' => 'alias', 'datatype' => 'string', 'default' => '', 'source' => 'owner_id' ],
        'ownerlname'   => [ 'type' => 'alias', 'datatype' => 'string', 'default' => '', 'source' => 'owner_id' ],
        'changerfname' => [ 'type' => 'alias', 'datatype' => 'string', 'default' => '', 'source' => 'changer_id' ],
        'changerlname' => [ 'type' => 'alias', 'datatype' => 'string', 'default' => '', 'source' => 'changer_id' ],
        'owner'        => [ 'type' => 'alias', 'datatype' => 'string', 'default' => '', 'source' => 'owner_id' ],
        'changer'      => [ 'type' => 'alias', 'datatype' => 'string', 'default' => '', 'source' => 'changer_id' ],
        'status'       => [ 'type' => 'alias', 'datatype' => 'string', 'default' => '', 'source' => 'status_id' ],
        'title'        => [ 'type' => 'alias', 'datatype' => 'string', 'default' => '' ],
    ];
    protected $_joins = [
        [ 'model' => 'Status', 'on' => [ 'status_id' => '_id' ], 'fields' => [ 'status' => 'status' ] ],
        [
            'model'  => 'User', 'on' => [ 'owner_id' => '_id' ],
            'fields' => [ 'ownerfname' => 'fname', 'ownerlname' => 'lname' ]
        ],
        [
            'model'  => 'User', 'on' => [ 'changer_id' => '_id' ],
            'fields' => [ 'changerfname' => 'fname', 'changerlname' => 'lname' ]
        ],
    ];
    protected $_unique = [ 'pricebook' ];
    protected $inputFilter;

    public function getInputFilter()
    {
        if (!$this->inputFilter) {
            $inputFilter = new InputFilter();
            $factory     = new InputFactory();
            $inputFilter->add($factory->createInput(array(
                                                          'name'     => 'owner_id',
                                                          'required' => false,
                                                          'filters'  => array(
                                                              array( 'name' => 'StripTags' ),
                                                              array( 'name' => 'StringTrim' ),
                                                          ),
                                                      )));
            $inputFilter->add($factory->createInput(array(
                                                          'name'       => 'description',
                                                          'required'   => false,
                                                          'filters'    => array(
                                                              array( 'name' => 'StripTags' ),
                                                              array( 'name' => 'StringTrim' ),
                                                          ),
                                                          'validators' => array(
                                                              array(
                                                                  'name'    => 'StringLength',
                                                                  'options' => array(
                                                                      'encoding' => 'UTF-8',
                                                                      'min'      => 3,
                                                                      'max'      => 100,
                                                                  ),
                                                              ),
                                                          ),
                                                      )));
            $inputFilter->add($factory->createInput(array(
                                                          'name'       => 'pricebook',
                                                          'required'   => true,
                                                          'filters'    => array(
                                                              array( 'name' => 'StripTags' ),
                                                              array( 'name' => 'StringTrim' ),
                                                          ),
                                                          'validators' => array(
                                                              array(
                                                                  'name'    => 'StringLength',
                                                                  'options' => array(
                                                                      'encoding' => 'UTF-8',
                                                                      'min'      => 3,
                                                                      'max'      => 100,
                                                                  ),
                                                              ),
                                                          ),
                                                      )));
            $this->inputFilter = $inputFilter;
        }

        return $this->inputFilter;
    }

    public function getName()
    {
        return $this->pricebook;
    }
}
