<?php

namespace Wepo\Model;

use Zend\InputFilter\Factory as InputFactory;
use Zend\InputFilter\InputFilter;
use Wepo\Lib\WepoMongoModel;

class InvoiceDetail extends WepoMongoModel
{
    public $table_name = 'invoice_detail';

    const TABLE_NAME = 'invoice_detail';

    protected $_fields = [
        '_id'           => [ 'type' => 'pk', 'datatype' => 'string', 'default' => 0 ],
        'invoice_id'    => [ 'type' => 'source', 'datatype' => 'string', 'default' => 0, 'alias' => 'invoice' ],
        'product_id'    => [ 'type' => 'source', 'datatype' => 'string', 'default' => 0, 'alias' => 'product' ],
        'product_price' => [ 'type' => 'field', 'datatype' => 'string', 'default' => 0 ],
        'discount'      => [ 'type' => 'field', 'datatype' => 'string', 'default' => 0 ],
        'discount_type'      => [ 'type' => 'field', 'datatype' => 'string', 'default' => 0 ],
        'qty'           => [ 'type' => 'field', 'datatype' => 'int', 'default' => 0 ],
        'final_price'   => [ 'type' => 'field', 'datatype' => 'string', 'default' => 0 ],
        'pricebook_id'  => [ 'type' => 'source', 'datatype' => 'string', 'default' => 0, 'alias' => 'pricebook' ],

        'pricebook' => [ 'type' => 'alias', 'datatype' => 'string', 'default' => '', 'source' => 'pricebook_id' ],
        'invoice'   => [ 'type' => 'alias', 'datatype' => 'string', 'default' => '', 'source' => 'invoice_id' ],
        'product'   => [ 'type' => 'alias', 'datatype' => 'string', 'default' => '', 'source' => 'product_id' ],
    ];
    protected $_joins  = [
        [ 'model' => 'Invoice', 'on' => [ 'invoice_id' => '_id' ], 'fields' => [ 'invoce' => 'subject' ] ],
        [ 'model' => 'Product', 'on' => [ 'product_id' => '_id' ], 'fields' => [ 'product' => 'name' ] ],
        [ 'model' => 'Pricebook', 'on' => [ 'pricebook_id' => '_id' ], 'fields' => [ 'pricebook' => 'pricebook' ] ],
    ];
    protected $inputFilter;

    public function toInvoice($data)
    {
        $this->product_id    = (isset($data[ 'product_id' ])) ? $data[ 'product_id' ] : 0;
        $this->product_price = (isset($data[ 'product_price' ])) ? $data[ 'product_price' ] : 0;
        $this->discount      = (isset($data[ 'discount' ])) ? $data[ 'discount' ] : null;
        $this->qty           = (isset($data[ 'qty' ])) ? $data[ 'qty' ] : null;
        $this->final_price   = (isset($data[ 'final_price' ])) ? $data[ 'final_price' ] : null;
    }

    public function getInputFilter()
    {
        if (!$this->inputFilter) {
            $inputFilter         = new InputFilter();
            $factory             = new InputFactory();
            $inputFilter->add($factory->createInput(array(
                    'name'       => 'invoice_id',
                    'required'   => false,
                    'filters'    => array(
                        array( 'name' => 'StripTags' ),
                        array( 'name' => 'StringTrim' ),
                    ),
                    'validators' => array(
                        array(
                            'name'    => 'StringLength',
                            'options' => array(
                                'encoding' => 'UTF-8',
                                'min'      => 24,
                                'max'      => 24,
                            ),
                        ),
                    ),
            )));
            $inputFilter->add($factory->createInput(array(
                    'name'       => 'product_id',
                    'required'   => false,
                    'filters'    => array(
                        array( 'name' => 'StripTags' ),
                        array( 'name' => 'StringTrim' ),
                    ),
                    'validators' => array(
                        array(
                            'name'    => 'StringLength',
                            'options' => array(
                                'encoding' => 'UTF-8',
                                'min'      => 24,
                                'max'      => 24,
                            ),
                        ),
                    ),
            )));
            $inputFilter->add($factory->createInput(array(
                    'name'     => 'product_price',
                    'required' => false,
            )));
            $inputFilter->add($factory->createInput(array(
                    'name'     => 'discount',
                    'required' => false,
            )));
            $inputFilter->add($factory->createInput(array(
                    'name'     => 'qty',
                    'required' => false,
                    'filters'  => array(
                        array( 'name' => 'Int' ),
                    ),
            )));
            $inputFilter->add($factory->createInput(array(
                    'name'     => 'final_price',
                    'required' => false,
            )));
            $this->inputFilter = $inputFilter;
        }

        return $this->inputFilter;
    }

    public function getName()
    {
        return $this->order_id.' '.$this->product_id;
    }
}
