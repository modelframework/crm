<?php

namespace Wepo\Model;

use Zend\InputFilter\Factory as InputFactory;
use Zend\InputFilter\InputFilter;
use Wepo\Lib\WepoMongoModel;

class MailChain extends WepoMongoModel
{
    public $table_name = 'mail_chain';

    const TABLE_NAME = 'mail_chian';

    protected $_fields = [
        '_id'         => [ 'type' => 'pk', 'datatype' => 'string', 'default' => '' ],
        //headers
        'title'       => [ 'type' => 'field', 'datatype' => 'string', 'default' => '' ],
        'date'        => [ 'type' => 'field', 'datatype' => 'string', 'default' => '' ],
        'modifyer_id' => [ 'type' => 'field', 'datatype' => 'string', 'default' => '' ],
        //end headers
        'status_id'   => [ 'type' => 'source', 'datatype' => 'string', 'default' => '', 'alias' => 'status' ],
        'owner_id'    => [ 'type' => 'source', 'datatype' => 'string', 'default' => '', 'alias' => 'owner' ],
        'owner'       => [ 'type' => 'alias', 'datatype' => 'string', 'default' => '', 'source' => 'owner_id' ],
        'status'      => [ 'type' => 'alias', 'datatype' => 'string', 'default' => '', 'source' => 'status_id' ],
    ];
    protected $_joins  = [
        [ 'model' => 'User', 'on' => [ 'owner_id' => '_id' ], 'fields' => [ 'owner' => 'login' ] ],
        [ 'model' => 'Status', 'on' => [ 'status_id' => '_id' ], 'fields' => [ 'status' => 'status' ] ],
    ];
    protected $inputFilter;

    public function getInputFilter()
    {
        if (!$this->inputFilter) {
            $inputFilter = new InputFilter();
            $factory     = new InputFactory();

            $inputFilter->add($factory->createInput(array(
                    'name'     => 'title',
                    'required' => false,
                    'filters'  => array(
                        array( 'name' => 'StripTags' ),
                        array( 'name' => 'StringTrim' ),
                    ),
            )));

            $inputFilter->add($factory->createInput(array(
                    'name'     => 'owner_id',
                    'required' => false,
                    'filters'  => array(
                        array( 'name' => 'StripTags' ),
                        array( 'name' => 'StringTrim' ),
                    ),
            )));
            $this->inputFilter = $inputFilter;
        }

        return $this->inputFilter;
    }

    public function getName()
    {
        return $this->title.' - '.$this->from;
    }
}
