<?php

namespace Wepo\Model;

use Wepo\Lib\WepoMongoModel;
use Zend\InputFilter\Factory as InputFactory;
use Zend\InputFilter\InputFilter;

class Contact extends WepoMongoModel
{
    public $table_name = 'contact';
    public $adapterName = 'wepo_company';
    const TABLE_NAME = 'contact';

    protected $_fields = [
        '_id'          => [ 'type' => 'pk', 'datatype' => 'string', 'default' => '' ],
        'flink'        => [ 'type' => 'source', 'datatype' => 'string', 'default' => '' ],
        'tlink'        => [ 'type' => 'source', 'datatype' => 'string', 'default' => '' ],
        'llink'        => [ 'type' => 'source', 'datatype' => 'string', 'default' => '' ],
        'glink'        => [ 'type' => 'source', 'datatype' => 'string', 'default' => '' ],
        'client_id'    => [ 'type' => 'source', 'datatype' => 'string', 'default' => '', 'alias' => 'client' ],
        'lead_source'  => [ 'type' => 'field', 'datatype' => 'string', 'default' => '' ],
        'age'          => [ 'type' => 'field', 'datatype' => 'string', 'default' => '' ],
        'height'       => [ 'type' => 'field', 'datatype' => 'string', 'default' => '' ],
        'weight'       => [ 'type' => 'field', 'datatype' => 'string', 'default' => '' ],
        'allergies'    => [ 'type' => 'field', 'datatype' => 'string', 'default' => '' ],
        'consultant'   => [ 'type' => 'field', 'datatype' => 'string', 'default' => '' ],
        'gender'       => [ 'type' => 'field', 'datatype' => 'string', 'default' => '' ],
        'social_security' => [ 'type' => 'field', 'datatype' => 'string', 'default' => '' ],
        'drivers_license' => [ 'type' => 'field', 'datatype' => 'string', 'default' => '' ],
        'physician_exam'  => [ 'type' => 'field', 'datatype' => 'string', 'default' => '' ],
        'medical_history' => [ 'type' => 'field', 'datatype' => 'string', 'default' => '' ],
        'last_lab'     => [ 'type' => 'field', 'datatype' => 'string', 'default' => '' ],
        'payment_option'  => [ 'type' => 'field', 'datatype' => 'string', 'default' => '' ],
        'card_type'    => [ 'type' => 'field', 'datatype' => 'string', 'default' => '' ],
        'card_number'  => [ 'type' => 'field', 'datatype' => 'string', 'default' => '' ],
        'exp_date'     => [ 'type' => 'field', 'datatype' => 'string', 'default' => '' ],
        'sec_code'     => [ 'type' => 'field', 'datatype' => 'string', 'default' => '' ],
        'card_type_2'  => [ 'type' => 'field', 'datatype' => 'string', 'default' => '' ],
        'card_number_2' => [ 'type' => 'field', 'datatype' => 'string', 'default' => '' ],
        'exp_date_2'   => [ 'type' => 'field', 'datatype' => 'string', 'default' => '' ],
        'sec_code_2'   => [ 'type' => 'field', 'datatype' => 'string', 'default' => '' ],
        'doctor_id'    => [ 'type' => 'source', 'datatype' => 'string', 'default' => '', 'alias' => 'doctor' ],
        'fname'        => [ 'type' => 'field', 'datatype' => 'string', 'default' => '' ],
        'lname'        => [ 'type' => 'field', 'datatype' => 'string', 'default' => '' ],
        'phone'        => [ 'type' => 'field', 'datatype' => 'string', 'default' => '' ],
        'mobile'       => [ 'type' => 'field', 'datatype' => 'string', 'default' => '' ],
        'email'        => [ 'type' => 'field', 'datatype' => 'string', 'default' => '' ],
        'second_email' => [ 'type' => 'field', 'datatype' => 'string', 'default' => '' ],
        'prefix'       => [ 'type' => 'field', 'datatype' => 'string', 'default' => '' ],
        'email_id'     => [ 'type' => 'source', 'datatype' => 'string', 'default' => '' ],
        'birth_date'   => [ 'type' => 'field', 'datatype' => 'string', 'default' => '' ],
        'avatar'       => [ 'type' => 'field', 'datatype' => 'string', 'default' => 'Contact.jpg' ],
        'created_dtm'  => [ 'type' => 'field', 'datatype' => 'string', 'default' => '' ],
        'address'      => [ 'type' => 'field', 'datatype' => 'string', 'default' => '' ],
        'changer_id'   => [ 'type' => 'source', 'datatype' => 'string', 'default' => '', 'alias' => 'changer' ],
        'changer'      => [ 'type' => 'alias', 'datatype' => 'string', 'default' => '', 'source' => 'changer_id' ],
        'changerfname' => [ 'type' => 'alias', 'datatype' => 'string', 'default' => '', 'source' => 'changer_id' ],
        'changerlname' => [ 'type' => 'alias', 'datatype' => 'string', 'default' => '', 'source' => 'changer_id' ],
        'doctor'       => [ 'type' => 'alias', 'datatype' => 'string', 'default' => '', 'source' => 'doctor_id' ],
        'changed_dtm'  => [ 'type' => 'field', 'datatype' => 'string', 'default' => '' ],
        'lastactiv_dtm' => [ 'type' => 'field', 'datatype' => 'string', 'default' => '' ],
        'status_id'    => [
            'type'  => 'source', 'datatype' => 'string', 'default' => '5295fdf7c5b9f222acd3c74b',
            'alias' => 'status',
        ],
        'owner_id'     => [ 'type' => 'source', 'datatype' => 'string', 'default' => '', 'alias' => 'owner' ],
        'owner'        => [ 'type' => 'alias', 'datatype' => 'string', 'default' => '', 'source' => 'owner_id' ],
        'ownerfname'   => [ 'type' => 'alias', 'datatype' => 'string', 'default' => '', 'source' => 'owner_id' ],
        'ownerlname'   => [ 'type' => 'alias', 'datatype' => 'string', 'default' => '', 'source' => 'owner_id' ],
        'client'       => [ 'type' => 'alias', 'datatype' => 'string', 'default' => '', 'source' => 'client_id' ],
        'status'       => [ 'type' => 'alias', 'datatype' => 'string', 'default' => '', 'source' => 'status_id' ],
        'country'      => [ 'type' => 'address', 'datatype' => 'string', 'default' => '' ],
        'zip'          => [ 'type' => 'address', 'datatype' => 'string', 'default' => '' ],
        'state'        => [ 'type' => 'address', 'datatype' => 'string', 'default' => '' ],
        'city'         => [ 'type' => 'address', 'datatype' => 'string', 'default' => '' ],
        'address'      => [ 'type' => 'address', 'datatype' => 'string', 'default' => '' ],
        'description'  => [ 'type' => 'description', 'datatype' => 'string', 'default' => '' ],
        'title'        => [ 'type' => 'alias', 'datatype' => 'string', 'default' => '' ],
    ];
    protected $_joins = [
        [
            'model'  => 'User', 'on' => [ 'owner_id' => '_id' ],
            'fields' => [ 'ownerfname' => 'fname', 'ownerlname' => 'lname' ],
        ],
        [ 'model' => 'Client', 'on' => [ 'client_id' => '_id' ], 'fields' => [ 'client' => 'name' ] ],
        [
            'model'  => 'User', 'on' => [ 'changer_id' => '_id' ],
            'fields' => [ 'changerfname' => 'fname', 'changerlname' => 'lname' ]
        ],
        [ 'model' => 'Doctor', 'on' => [ 'doctor_id' => '_id' ], 'fields' => [ 'doctor' => 'title' ] ],
        [ 'model' => 'Status', 'on' => [ 'status_id' => '_id' ], 'fields' => [ 'status' => 'status' ] ],
    ];
    protected $inputFilter;

    public function getInputFilter()
    {
        if (!$this->inputFilter) {
            $inputFilter = new InputFilter();
            $factory     = new InputFactory();

            $inputFilter->add($factory->createInput(array(
                                                          'name'     => 'id',
                                                          'required' => true,
                                                          'filters'  => array(
                                                              array( 'name' => 'Int' ),
                                                          ),
                                                      )));
            $inputFilter->add($factory->createInput(array(
                                                          'name'       => 'flink',
                                                          'required'   => false,
                                                          'filters'    => array(
                                                              array( 'name' => 'StripTags' ),
                                                              array( 'name' => 'StringTrim' ),
                                                          ),
                                                          'validators' => array(
                                                              array(
                                                                  'name'    => 'StringLength',
                                                                  'options' => array(
                                                                      'encoding' => 'UTF-8',
                                                                      'min'      => 3,
                                                                      'max'      => 100,
                                                                  ),
                                                              ),
                                                          ),
                                                      )));
            $inputFilter->add($factory->createInput(array(
                                                          'name'       => 'tlink',
                                                          'required'   => false,
                                                          'filters'    => array(
                                                              array( 'name' => 'StripTags' ),
                                                              array( 'name' => 'StringTrim' ),
                                                          ),
                                                          'validators' => array(
                                                              array(
                                                                  'name'    => 'StringLength',
                                                                  'options' => array(
                                                                      'encoding' => 'UTF-8',
                                                                      'min'      => 3,
                                                                      'max'      => 100,
                                                                  ),
                                                              ),
                                                          ),
                                                      )));
            $inputFilter->add($factory->createInput(array(
                                                          'name'       => 'llink',
                                                          'required'   => false,
                                                          'filters'    => array(
                                                              array( 'name' => 'StripTags' ),
                                                              array( 'name' => 'StringTrim' ),
                                                          ),
                                                          'validators' => array(
                                                              array(
                                                                  'name'    => 'StringLength',
                                                                  'options' => array(
                                                                      'encoding' => 'UTF-8',
                                                                      'min'      => 3,
                                                                      'max'      => 100,
                                                                  ),
                                                              ),
                                                          ),
                                                      )));
            $inputFilter->add($factory->createInput(array(
                                                          'name'     => 'owner_id',
                                                          'required' => true,
                                                          'filters'    => array(
                                                              array( 'name' => 'StripTags' ),
                                                              array( 'name' => 'StringTrim' ),
                                                          ),
                                                          'validators' => array(
                                                              array(
                                                                  'name'    => 'StringLength',
                                                                  'options' => array(
                                                                      'encoding' => 'UTF-8',
                                                                      'min'      => 24,
                                                                      'max'      => 24,
                                                                  ),
                                                              ),
                                                          ),
                                                      )));
            $inputFilter->add($factory->createInput(array(
                                                          'name'     => 'client_id',
                                                          'required' => true,
                                                          'filters'    => array(
                                                              array( 'name' => 'StripTags' ),
                                                              array( 'name' => 'StringTrim' ),
                                                          ),
//                                                          'validators' => array(
//                                                              array(
//                                                                  'name'    => 'StringLength',
//                                                                  'options' => array(
//                                                                      'encoding' => 'UTF-8',
//                                                                      'min'      => 24,
//                                                                      'max'      => 24,
//                                                                  ),
//                                                              ),
//                                                          ),
                                                      )));
            $inputFilter->add($factory->createInput(array(
                                                          'name'     => 'doctor_id',
                                                          'required' => true,
                                                          'filters'    => array(
                                                              array( 'name' => 'StripTags' ),
                                                              array( 'name' => 'StringTrim' ),
                                                          ),
//                                                          'validators' => array(
//                                                              array(
//                                                                  'name'    => 'StringLength',
//                                                                  'options' => array(
//                                                                      'encoding' => 'UTF-8',
//                                                                      'min'      => 24,
//                                                                      'max'      => 24,
//                                                                  ),
//                                                              ),
//                                                          ),
                                                      )));

            $inputFilter->add($factory->createInput(array(
                                                          'name'       => 'fname',
                                                          'required'   => true,
                                                          'filters'    => array(
                                                              array( 'name' => 'StripTags' ),
                                                              array( 'name' => 'StringTrim' ),
                                                          ),
                                                          'validators' => array(
                                                              array(
                                                                  'name'    => 'StringLength',
                                                                  'options' => array(
                                                                      'encoding' => 'UTF-8',
                                                                      'min'      => 1,
                                                                      'max'      => 100,
                                                                  ),
                                                              ),
                                                          ),
                                                      )));

            $inputFilter->add($factory->createInput(array(
                                                          'name'       => 'lname',
                                                          'required'   => true,
                                                          'filters'    => array(
                                                              array( 'name' => 'StripTags' ),
                                                              array( 'name' => 'StringTrim' ),
                                                          ),
                                                          'validators' => array(
                                                              array(
                                                                  'name'    => 'StringLength',
                                                                  'options' => array(
                                                                      'encoding' => 'UTF-8',
                                                                      'min'      => 1,
                                                                      'max'      => 100,
                                                                  ),
                                                              ),
                                                          ),
                                                      )));
            $inputFilter->add($factory->createInput(array(
                                                          'name'       => 'login',
                                                          'required'   => true,
                                                          'filters'    => array(
                                                              array( 'name' => 'StripTags' ),
                                                              array( 'name' => 'StringTrim' ),
                                                          ),
                                                          'validators' => array(
                                                              array(
                                                                  'name'    => 'StringLength',
                                                                  'options' => array(
                                                                      'encoding' => 'UTF-8',
                                                                      'min'      => 3,
                                                                      'max'      => 100,
                                                                  ),
                                                              ),
                                                          ),
                                                      )));
            $inputFilter->add($factory->createInput(array(
                                                          'name'       => 'phone',
                                                          'required'   => true,
                                                          'filters'    => array(
                                                              array( 'name' => 'StripTags' ),
                                                              array( 'name' => 'StringTrim' ),
                                                          ),
                                                          'validators' => array(
                                                              array(
                                                                  'name'    => 'StringLength',
                                                                  'options' => array(
                                                                      'encoding' => 'UTF-8',
                                                                      'min'      => 5,
                                                                      'max'      => 100,
                                                                  ),
                                                              ),
                                                          ),
                                                      )));
            $inputFilter->add($factory->createInput(array(
                                                          'name'       => 'mobile',
                                                          'required'   => false,
                                                          'filters'    => array(
                                                              array( 'name' => 'StripTags' ),
                                                              array( 'name' => 'StringTrim' ),
                                                          ),
                                                          'validators' => array(
                                                              array(
                                                                  'name'    => 'StringLength',
                                                                  'options' => array(
                                                                      'encoding' => 'UTF-8',
                                                                      'min'      => 8,
                                                                      'max'      => 100,
                                                                  ),
                                                              ),
                                                          ),
                                                      )));
            $inputFilter->add($factory->createInput(array(
                                                          'name'       => 'email',
                                                          'required'   => false,
                                                          'filters'    => array(
                                                              array( 'name' => 'StripTags' ),
                                                              array( 'name' => 'StringTrim' ),
                                                          ),
                                                          'validators' => array(
                                                              array(
                                                                  'name'    => 'StringLength',
                                                                  'options' => array(
                                                                      'encoding' => 'UTF-8',
                                                                      'min'      => 6,
                                                                      'max'      => 100,
                                                                  ),
                                                              ),
                                                          ),
                                                      )));
            $inputFilter->add($factory->createInput(array(
                                                          'name'       => 'birth_date',
                                                          'required'   => false,
                                                          'filters'    => array(
                                                              array( 'name' => 'StripTags' ),
                                                              array( 'name' => 'StringTrim' ),
                                                          ),
                                                          'validators' => array(
                                                              array(
                                                                  'name'    => 'StringLength',
                                                                  'options' => array(
                                                                      'encoding' => 'UTF-8',
                                                                      'min'      => 10,
                                                                      'max'      => 20,
                                                                  ),
                                                              ),
                                                              array(
                                                                  'name'    => 'Date',
                                                                  'options' => array(
                                                                      'format' => 'Y-m-d',
                                                                  ),
                                                              ),
                                                              array(
                                                                  'name'    => 'Between',
                                                                  'options' => array(
                                                                      'min' => '1960-01-01',
                                                                      'max' => date('Y-m-d', time()),
                                                                  ),
                                                              ),
                                                          ),
                                                      )));
            $inputFilter->add($factory->createInput(array(
                                                          'name'       => 'exp_date',
                                                          'required'   => false,
                                                          'filters'    => array(
                                                              array( 'name' => 'StripTags' ),
                                                              array( 'name' => 'StringTrim' ),
                                                          ),
                                                          'validators' => array(
                                                              array(
                                                                  'name'    => 'StringLength',
                                                                  'options' => array(
                                                                      'encoding' => 'UTF-8',
                                                                      'min'      => 10,
                                                                      'max'      => 20,
                                                                  ),
                                                              ),
                                                              array(
                                                                  'name'    => 'Date',
                                                                  'options' => array(
                                                                      'format' => 'Y-m-d',
                                                                  ),
                                                              ),
                                                          ),
                                                      )));
            $inputFilter->add($factory->createInput(array(
                                                          'name'       => 'last_lab',
                                                          'required'   => false,
                                                          'filters'    => array(
                                                              array( 'name' => 'StripTags' ),
                                                              array( 'name' => 'StringTrim' ),
                                                          ),
                                                          'validators' => array(
                                                              array(
                                                                  'name'    => 'StringLength',
                                                                  'options' => array(
                                                                      'encoding' => 'UTF-8',
                                                                      'min'      => 10,
                                                                      'max'      => 20,
                                                                  ),
                                                              ),
                                                              array(
                                                                  'name'    => 'Date',
                                                                  'options' => array(
                                                                      'format' => 'Y-m-d',
                                                                  ),
                                                              ),
                                                          ),
                                                      )));
            $inputFilter->add($factory->createInput(array(
                                                          'name'       => 'physician_exam',
                                                          'required'   => false,
                                                          'filters'    => array(
                                                              array( 'name' => 'StripTags' ),
                                                              array( 'name' => 'StringTrim' ),
                                                          ),
                                                          'validators' => array(
                                                              array(
                                                                  'name'    => 'StringLength',
                                                                  'options' => array(
                                                                      'encoding' => 'UTF-8',
                                                                      'min'      => 10,
                                                                      'max'      => 20,
                                                                  ),
                                                              ),
                                                              array(
                                                                  'name'    => 'Date',
                                                                  'options' => array(
                                                                      'format' => 'Y-m-d',
                                                                  ),
                                                              ),
                                                          ),
                                                      )));
            $inputFilter->add($factory->createInput(array(
                                                          'name'       => 'medical_history',
                                                          'required'   => false,
                                                          'filters'    => array(
                                                              array( 'name' => 'StripTags' ),
                                                              array( 'name' => 'StringTrim' ),
                                                          ),
                                                          'validators' => array(
                                                              array(
                                                                  'name'    => 'StringLength',
                                                                  'options' => array(
                                                                      'encoding' => 'UTF-8',
                                                                      'min'      => 10,
                                                                      'max'      => 20,
                                                                  ),
                                                              ),
                                                              array(
                                                                  'name'    => 'Date',
                                                                  'options' => array(
                                                                      'format' => 'Y-m-d',
                                                                  ),
                                                              ),
                                                          ),
                                                      )));
            $inputFilter->add($factory->createInput(array(
                                                          'name'       => 'gender',
                                                          'required'   => false,
                                                          'filters'    => array(
                                                              array( 'name' => 'StripTags' ),
                                                              array( 'name' => 'StringTrim' ),
                                                          ),
                                                      )));
            $inputFilter->add($factory->createInput(array(
                                                          'name'       => 'lead_source',
                                                          'required'   => false,
                                                          'filters'    => array(
                                                              array( 'name' => 'StripTags' ),
                                                              array( 'name' => 'StringTrim' ),
                                                          ),
                                                      )));
            $inputFilter->add($factory->createInput(array(
                                                          'name'       => 'social_security',
                                                          'required'   => false,
                                                          'filters'    => array(
                                                              array( 'name' => 'StripTags' ),
                                                              array( 'name' => 'StringTrim' ),
                                                          ),
                                                      )));
            $inputFilter->add($factory->createInput(array(
                                                          'name'       => 'drivers_license',
                                                          'required'   => false,
                                                          'filters'    => array(
                                                              array( 'name' => 'StripTags' ),
                                                              array( 'name' => 'StringTrim' ),
                                                          ),
                                                      )));
            $inputFilter->add($factory->createInput(array(
                                                          'name'       => 'height',
                                                          'required'   => false,
                                                          'filters'    => array(
                                                              array( 'name' => 'StripTags' ),
                                                              array( 'name' => 'StringTrim' ),
                                                          ),
                                                      )));
            $inputFilter->add($factory->createInput(array(
                                                          'name'       => 'weight',
                                                          'required'   => false,
                                                          'filters'    => array(
                                                              array( 'name' => 'StripTags' ),
                                                              array( 'name' => 'StringTrim' ),
                                                          ),
                                                      )));
            $inputFilter->add($factory->createInput(array(
                                                          'name'       => 'allergies',
                                                          'required'   => false,
                                                          'filters'    => array(
                                                              array( 'name' => 'StripTags' ),
                                                              array( 'name' => 'StringTrim' ),
                                                          ),
                                                          'validators' => array(
                                                              array(
                                                                  'name'    => 'StringLength',
                                                                  'options' => array(
                                                                      'encoding' => 'UTF-8',
                                                                      'min'      => 3,
                                                                      'max'      => 100,
                                                                  ),
                                                              ),
                                                          ),
                                                      )));
            $inputFilter->add($factory->createInput(array(
                                                          'name'       => 'consultant',
                                                          'required'   => false,
                                                          'filters'    => array(
                                                              array( 'name' => 'StripTags' ),
                                                              array( 'name' => 'StringTrim' ),
                                                          ),
                                                          'validators' => array(
                                                              array(
                                                                  'name'    => 'StringLength',
                                                                  'options' => array(
                                                                      'encoding' => 'UTF-8',
                                                                      'min'      => 3,
                                                                      'max'      => 100,
                                                                  ),
                                                              ),
                                                          ),
                                                      )));
            $inputFilter->add($factory->createInput(array(
                                                          'name'       => 'card_number',
                                                          'required'   => false,
                                                          'filters'    => array(
                                                              array( 'name' => 'StripTags' ),
                                                              array( 'name' => 'StringTrim' ),
                                                          ),
                                                          'validators' => array(
                                                              array(
                                                                  'name'    => 'CreditCard',
                                                              ),
                                                          ),
                                                      )));
            $inputFilter->add($factory->createInput(array(
                                                          'name'       => 'card_type',
                                                          'required'   => false,
                                                          'filters'    => array(
                                                              array( 'name' => 'StripTags' ),
                                                              array( 'name' => 'StringTrim' ),
                                                          ),
                                                      )));
            $inputFilter->add($factory->createInput(array(
                                                          'name'       => 'sec_code',
                                                          'required'   => false,
                                                          'filters'    => array(
                                                              array( 'name' => 'StripTags' ),
                                                              array( 'name' => 'StringTrim' ),
                                                          ),
                                                      )));
            $inputFilter->add($factory->createInput(array(
                                                          'name'       => 'payment_option',
                                                          'required'   => false,
                                                          'filters'    => array(
                                                              array( 'name' => 'StripTags' ),
                                                              array( 'name' => 'StringTrim' ),
                                                          ),
                                                          'validators' => array(
                                                              array(
                                                                  'name'    => 'StringLength',
                                                                  'options' => array(
                                                                      'encoding' => 'UTF-8',
                                                                      'min'      => 6,
                                                                      'max'      => 100,
                                                                  ),
                                                              ),
                                                          ),
                                                      )));
            $inputFilter->add($factory->createInput(array(
                                                          'name'       => 'address',
                                                          'required'   => false,
                                                          'filters'    => array(
                                                              array( 'name' => 'StripTags' ),
                                                              array( 'name' => 'StringTrim' ),
                                                          ),
                                                          'validators' => array(
                                                              array(
                                                                  'name'    => 'StringLength',
                                                                  'options' => array(
                                                                      'encoding' => 'UTF-8',
                                                                      'min'      => 6,
                                                                      'max'      => 100,
                                                                  ),
                                                              ),
                                                          ),
                                                      )));
            $inputFilter->add($factory->createInput(array(
                                                          'name'       => 'country',
                                                          'required'   => false,
                                                          'filters'    => array(
                                                              array( 'name' => 'StripTags' ),
                                                              array( 'name' => 'StringTrim' ),
                                                          ),
                                                          'validators' => array(
                                                              array(
                                                                  'name'    => 'StringLength',
                                                                  'options' => array(
                                                                      'encoding' => 'UTF-8',
                                                                      'min'      => 1,
                                                                      'max'      => 100,
                                                                  ),
                                                              ),
                                                          ),
                                                      )));
            $inputFilter->add($factory->createInput(array(
                                                          'name'       => 'zip',
                                                          'required'   => false,
                                                          'filters'    => array(
                                                              array( 'name' => 'StripTags' ),
                                                              array( 'name' => 'StringTrim' ),
                                                          ),
                                                          'validators' => array(
                                                              array(
                                                                  'name'    => 'StringLength',
                                                                  'options' => array(
                                                                      'encoding' => 'UTF-8',
                                                                      'min'      => 1,
                                                                      'max'      => 100,
                                                                  ),
                                                              ),
                                                          ),
                                                      )));
            $inputFilter->add($factory->createInput(array(
                                                          'name'       => 'state',
                                                          'required'   => false,
                                                          'filters'    => array(
                                                              array( 'name' => 'StripTags' ),
                                                              array( 'name' => 'StringTrim' ),
                                                          ),
                                                          'validators' => array(
                                                              array(
                                                                  'name'    => 'StringLength',
                                                                  'options' => array(
                                                                      'encoding' => 'UTF-8',
                                                                      'min'      => 1,
                                                                      'max'      => 100,
                                                                  ),
                                                              ),
                                                          ),
                                                      )));
            $inputFilter->add($factory->createInput(array(
                                                          'name'       => 'city',
                                                          'required'   => false,
                                                          'filters'    => array(
                                                              array( 'name' => 'StripTags' ),
                                                              array( 'name' => 'StringTrim' ),
                                                          ),
                                                          'validators' => array(
                                                              array(
                                                                  'name'    => 'StringLength',
                                                                  'options' => array(
                                                                      'encoding' => 'UTF-8',
                                                                      'min'      => 1,
                                                                      'max'      => 100,
                                                                  ),
                                                              ),
                                                          ),
                                                      )));
            $inputFilter->add($factory->createInput(array(
                                                          'name'       => 'address',
                                                          'required'   => false,
                                                          'filters'    => array(
                                                              array( 'name' => 'StripTags' ),
                                                              array( 'name' => 'StringTrim' ),
                                                          ),
                                                          'validators' => array(
                                                              array(
                                                                  'name'    => 'StringLength',
                                                                  'options' => array(
                                                                      'encoding' => 'UTF-8',
                                                                      'min'      => 1,
                                                                      'max'      => 100,
                                                                  ),
                                                              ),
                                                          ),
                                                      )));
            $inputFilter->add($factory->createInput(array(
                                                          'name'       => 'description',
                                                          'required'   => false,
                                                          'filters'    => array(
                                                              array( 'name' => 'StripTags' ),
                                                              array( 'name' => 'StringTrim' ),
                                                          ),
                                                          'validators' => array(
                                                              array(
                                                                  'name'    => 'StringLength',
                                                                  'options' => array(
                                                                      'encoding' => 'UTF-8',
                                                                      'min'      => 1,
                                                                  ),
                                                              ),
                                                          ),
                                                      )));
            $this->inputFilter = $inputFilter;
        }

        return $this->inputFilter;
    }

    public function getModelLabel()
    {
        return 'Patient';
    }
}
