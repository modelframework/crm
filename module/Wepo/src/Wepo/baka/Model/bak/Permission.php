<?php

namespace Wepo\Model;

use Zend\InputFilter\Factory as InputFactory;
use Zend\InputFilter\InputFilter;
use Wepo\Lib\WepoModel;

class Permission extends WepoModel
{
    public $table_name = 'permission';

    const TABLE_NAME = 'permission';

    protected $_fields = [
        'id'       => [ 'type'     => 'pk', 'datatype'    => 'int', 'default' => 0 ],
        'role_id'  => [ 'type'     => 'field', 'datatype'    => 'int', 'default' => 0 ],
        'table_id' => [ 'type'     => 'field', 'datatype'    => 'int', 'default' => 0 ],
        'value'    => [ 'type'     => 'field', 'datatype'    => 'int', 'default' => 0 ],
    ];
    protected $inputFilter;

    public function getInputFilter()
    {
        if (!$this->inputFilter) {
            $inputFilter = new InputFilter();
            $factory     = new InputFactory();

            $inputFilter->add(
                $factory->createInput(array(
                    'name'       => 'id',
                    'required'   => true,
                    'filters'    => array(
                        array( 'name' => 'Int' ),
                    ),
                    'validators' => array(
                        array(
                            'name'    => 'Digits',
                            'options' => array( ),
                        ),
                        array(
                            'name'    => 'GreaterThan',
                            'options' => array(
                                'min' => 0,
                            ),
                        ),
                    ),
            )));
            $this->inputFilter = $inputFilter;
        }

        return $this->inputFilter;
    }
}
