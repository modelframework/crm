<?php

namespace Wepo\Model;

use Zend\InputFilter\Factory as InputFactory;
use Zend\InputFilter\InputFilter;
use Wepo\Lib\WepoModel;

class Order extends WepoModel
{
    public $table_name = 'order';

    const TABLE_NAME = 'order';

    protected $_fields = [
        'id'             => [ 'type'     => 'pk', 'datatype' => 'int', 'default'  => 0 ],
        'quote_id'       => [ 'type'     => 'field', 'datatype' => 'int', 'default'  => 0 ],
        'owner_id'       => [ 'type'     => 'source', 'datatype' => 'int', 'default'  => 0, 'alias'    => 'owner' ],
        'contact_id'     => [ 'type'     => 'source', 'datatype' => 'int', 'default'  => 0, 'alias'    => 'contact' ],
        'subject'        => [ 'type'     => 'field', 'datatype' => 'string', 'default'  => '' ],
        'description'    => [ 'type'     => 'field', 'datatype' => 'string', 'default'  => '' ],
        'total_price'    => [ 'type'     => 'field', 'datatype' => 'int', 'default'  => 0 ],
        'total_discount' => [ 'type'     => 'field', 'datatype' => 'int', 'default'  => 0 ],
        'status_id'      => [ 'type'     => 'source', 'datatype' => 'int', 'default'  => 0, 'alias'    => 'status' ],
        'created_dtm'    => [ 'type'     => 'field', 'datatype' => 'string', 'default'  => '' ],
        'changer_id'     => [ 'type'     => 'source', 'datatype' => 'int', 'default'  => 0, 'alias'    => 'changer' ],
        'changed_dtm'    => [ 'type'     => 'field', 'datatype' => 'string', 'default'  => '' ],

        'owner'          => [ 'type'     => 'alias', 'datatype' => 'string', 'default'  => '', 'source'   => 'owner_id' ],
        'contact'        => [ 'type'     => 'alias', 'datatype' => 'string', 'default'  => '', 'source'   => 'contact_id' ],
        'changer'        => [ 'type'     => 'alias', 'datatype' => 'string', 'default'  => '', 'source'   => 'changer_id' ],
        'status'         => [ 'type'     => 'alias', 'datatype' => 'string', 'default'  => '', 'source'   => 'status_id' ],
    ];
    protected $_joins  = [
        [ 'model'  => 'User', 'on'     => [ 'owner_id' => 'id' ], 'fields' => [ 'owner' => 'login' ] ],
        [ 'model'  => 'Contact', 'on'     => [ 'contact_id' => 'id' ], 'fields' => [ 'contact' => 'login' ] ],
        [ 'model'  => 'User', 'on'     => [ 'changer_id' => 'id' ], 'fields' => [ 'changer' => 'login' ] ],
        [ 'model'  => 'Status', 'on'     => [ 'status_id' => 'id' ], 'fields' => [ 'status' => 'status' ] ],
    ];
    protected $inputFilter;

    public function getInputFilter()
    {
        if (!$this->inputFilter) {
            $inputFilter         = new InputFilter();
            $factory             = new InputFactory();
            $inputFilter->add($factory->createInput(array(
                    'name'     => 'owner_id',
                    'required' => false,
                    'filters'  => array(
                        array( 'name' => 'Int' ),
                    ),
            )));
            $inputFilter->add($factory->createInput(array(
                    'name'     => 'contact_id',
                    'required' => true,
                    'filters'  => array(
                        array( 'name' => 'Int' ),
                    ),
            )));
            $inputFilter->add($factory->createInput(array(
                    'name'       => 'subject',
                    'required'   => false,
                    'filters'    => array(
                        array( 'name' => 'StripTags' ),
                        array( 'name' => 'StringTrim' ),
                    ),
                    'validators' => array(
                        array(
                            'name'    => 'StringLength',
                            'options' => array(
                                'encoding' => 'UTF-8',
                                'min'      => 3,
                                'max'      => 100,
                            ),
                        ),
                    ),
            )));
            $inputFilter->add($factory->createInput(array(
                    'name'       => 'description',
                    'required'   => false,
                    'filters'    => array(
                        array( 'name' => 'StripTags' ),
                        array( 'name' => 'StringTrim' ),
                    ),
                    'validators' => array(
                        array(
                            'name'    => 'StringLength',
                            'options' => array(
                                'encoding' => 'UTF-8',
                                'min'      => 3,
                                'max'      => 100,
                            ),
                        ),
                    ),
            )));
            $inputFilter->add($factory->createInput(array(
                    'name'     => 'total_discount',
                    'required' => false,
                    'filters'  => array(
                        array( 'name' => 'Int' ),
                    ),
            )));
            $this->inputFilter = $inputFilter;
        }

        return $this->inputFilter;
    }

    public function getName()
    {
        return $this->subject;
    }
}
