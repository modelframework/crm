<?php

namespace Wepo\Model;

use Zend\InputFilter\Factory as InputFactory;
use Zend\InputFilter\InputFilter;
use Wepo\Lib\WepoModel;

class QuoteDetail extends WepoModel
{
    public $table_name = 'quote_detail';

    const TABLE_NAME = 'quote_detail';

    protected $_fields = [
        'id'            => [ 'type'     => 'pk', 'datatype' => 'int', 'default'  => 0 ],
        'quote_id'      => [ 'type'     => 'source', 'datatype' => 'int', 'default'  => 0, 'alias'    => 'quote' ],
        'product_id'    => [ 'type'     => 'source', 'datatype' => 'int', 'default'  => 0, 'alias'    => 'product' ],
        'product_price' => [ 'type'     => 'field', 'datatype' => 'int', 'default'  => 0 ],
        'discount'      => [ 'type'     => 'field', 'datatype' => 'int', 'default'  => 0 ],
        'qty'           => [ 'type'     => 'field', 'datatype' => 'int', 'default'  => 0 ],
        'final_price'   => [ 'type'     => 'field', 'datatype' => 'int', 'default'  => 0 ],

        'quote'         => [ 'type'     => 'alias', 'datatype' => 'string', 'default'  => '', 'source'   => 'quote_id' ],
        'product'       => [ 'type'     => 'alias', 'datatype' => 'string', 'default'  => '', 'source'   => 'product_id' ],
    ];
    protected $_joins  = [
        [ 'model'  => 'Quote', 'on'     => [ 'quote_id' => 'id' ], 'fields' => [ 'quote' => 'subject' ] ],
        [ 'model'  => 'Product', 'on'     => [ 'product_id' => 'id' ], 'fields' => [ 'product' => 'name' ] ],
    ];
    protected $inputFilter;

    public function getInputFilter()
    {
        if (!$this->inputFilter) {
            $inputFilter         = new InputFilter();
            $factory             = new InputFactory();
            $inputFilter->add($factory->createInput(array(
                    'name'     => 'quote_id',
                    'required' => false,
                    'filters'  => array(
                        array( 'name' => 'Int' ),
                    ),
            )));
            $inputFilter->add($factory->createInput(array(
                    'name'     => 'product_id',
                    'required' => false,
                    'filters'  => array(
                        array( 'name' => 'Int' ),
                    ),
            )));
            $inputFilter->add($factory->createInput(array(
                    'name'     => 'product_price',
                    'required' => false,
                    'filters'  => array(
                        array( 'name' => 'Int' ),
                    ),
            )));
            $inputFilter->add($factory->createInput(array(
                    'name'     => 'discount',
                    'required' => false,
                    'filters'  => array(
                        array( 'name' => 'Int' ),
                    ),
            )));
            $inputFilter->add($factory->createInput(array(
                    'name'     => 'qty',
                    'required' => false,
                    'filters'  => array(
                        array( 'name' => 'Int' ),
                    ),
            )));
            $inputFilter->add($factory->createInput(array(
                    'name'     => 'final_price',
                    'required' => false,
                    'filters'  => array(
                        array( 'name' => 'Int' ),
                    ),
            )));
            $this->inputFilter = $inputFilter;
        }

        return $this->inputFilter;
    }

    public function getName()
    {
        return 'Product id '.$this->product_id;
    }
}
