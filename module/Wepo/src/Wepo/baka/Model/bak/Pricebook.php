<?php

namespace Wepo\Model;

use Zend\InputFilter\Factory as InputFactory;
use Zend\InputFilter\InputFilter;
use Wepo\Lib\WepoModel;

class Pricebook extends WepoModel
{
    public $table_name = 'pricebook';

    const TABLE_NAME = 'pricebook';

    protected $_fields = [
        'id'          => [ 'type' => 'pk', 'datatype' => 'int', 'default' => 0 ],
        'pricebook'   => ['type' => 'field', 'datatype' => 'string', 'default' => '' ],
        'owner_id'    => ['type' => 'source', 'datatype' => 'int', 'default' => 0, 'alias' => 'owner' ],
        'changer_id'  => [ 'type' => 'source', 'datatype' => 'int', 'default' => 0, 'alias' => 'changer' ],
        'status_id'   => [ 'type' => 'source', 'datatype' => 'int', 'default' => 0, 'alias' => 'status' ],
        'created_dtm' => [ 'type' => 'field', 'datatype' => 'string', 'default' => '' ],
        'description' => [ 'type' => 'field', 'datatype' => 'string', 'default' => '' ],
        'changed_dtm' => [ 'type' => 'field', 'datatype' => 'string', 'default' => '' ],
////////////////////////////////////////////////////////////////////////////////////////////////////////////
        'owner'       => [ 'type' => 'alias', 'datatype' => 'string', 'default' => '', 'source' => 'owner_id' ],
        'changer'     => [ 'type' => 'alias', 'datatype' => 'string', 'default' => '', 'source' => 'changer_id' ],
        'status'      => [ 'type' => 'alias', 'datatype' => 'string', 'default' => '', 'source' => 'status_id' ],
    ];
    protected $_joins  = [
        [ 'model' => 'Status', 'on' => [ 'status_id' => 'id' ], 'fields' => [ 'status' => 'status' ] ],
        [ 'model' => 'User', 'on' => [ 'owner_id' => 'id' ], 'fields' => [ 'owner' => 'login' ] ],
        [ 'model' => 'User', 'on' => [ 'changer_id' => 'id' ], 'fields' => [ 'changer' => 'login' ] ],
    ];
    protected $_unique = [ 'pricebook' ];
    protected $inputFilter;

    public function getInputFilter()
    {
        if (!$this->inputFilter) {
            $inputFilter         = new InputFilter();
            $factory             = new InputFactory();
            $inputFilter->add($factory->createInput(array(
                    'name'     => 'owner_id',
                    'required' => false,
                    'filters'  => array(
                        array( 'name' => 'Int' ),
                    ),
            )));
            $inputFilter->add($factory->createInput(array(
                    'name'       => 'description',
                    'required'   => false,
                    'filters'    => array(
                        array( 'name' => 'StripTags' ),
                        array( 'name' => 'StringTrim' ),
                    ),
                    'validators' => array(
                        array(
                            'name'    => 'StringLength',
                            'options' => array(
                                'encoding' => 'UTF-8',
                                'min'      => 3,
                                'max'      => 100,
                            ),
                        ),
                    ),
            )));
            $inputFilter->add($factory->createInput(array(
                    'name'       => 'pricebook',
                    'required'   => true,
                    'filters'    => array(
                        array( 'name' => 'StripTags' ),
                        array( 'name' => 'StringTrim' ),
                    ),
                    'validators' => array( array(
                            'name'    => 'StringLength',
                            'options' => array(
                                'encoding' => 'UTF-8',
                                'min'      => 3,
                                'max'      => 100,
                            ),
                        ),
                    ),
            )));
            $this->inputFilter = $inputFilter;
        }

        return $this->inputFilter;
    }

    public function getName()
    {
        return $this->pricebook;
    }
}
