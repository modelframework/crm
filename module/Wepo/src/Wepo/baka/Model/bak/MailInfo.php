<?php

namespace Wepo\Model;

use Zend\InputFilter\Factory as InputFactory;
use Zend\InputFilter\InputFilter;
use Wepo\Lib\WepoModel;

class MailInfo extends WepoModel
{
    public $table_name = 'mail_info';

    const TABLE_NAME = 'mail_info';
    const READ       = 2;
    const NEWMAIL    = 1;

    protected $_fields = [
        'id'           => [ 'type'     => 'pk', 'datatype' => 'int', 'default'  => 0 ],
        'mail_id'      => [ 'type'     => 'source', 'datatype' => 'int', 'default'  => 0, 'alias'    => 'mail' ],
        'mail_dtm'     => [ 'type'     => 'field', 'datatype' => 'string', 'default'  => '' ],
        'message_id'   => [ 'type'     => 'field', 'datatype' => 'string', 'default'  => '' ],
        'in_reply_to'  => [ 'type'     => 'field', 'datatype' => 'string', 'default'  => '' ],
        'type_id'      => [ 'type'     => 'field', 'datatype' => 'int', 'default'  => 0 ],
        'contact_type' => [ 'type'     => 'source', 'datatype' => 'int', 'default'  => 0, 'alias'    => 'contacttype' ],
        'contact_id'   => [ 'type'     => 'field', 'datatype' => 'int', 'default'  => 0 ],
        'user_id'      => [ 'type'     => 'source', 'datatype' => 'int', 'default'  => 0, 'alias'    => 'user' ],
        'owner_id'     => [ 'type'     => 'source', 'datatype' => 'int', 'default'  => 0, 'alias'    => 'owner' ],
        'status_id'    => [ 'type'     => 'source', 'datatype' => 'int', 'default'  => 0, 'alias'    => 'status' ],
        'group_id'     => [ 'type'     => 'field', 'datatype' => 'int', 'default'  => 0 ],

        'mail'         => [ 'type'     => 'alias', 'datatype' => 'string', 'default'  => '', 'source'   => 'mail_id' ],
        'contacttype'  => [ 'type'     => 'alias', 'datatype' => 'string', 'default'  => '', 'source'   => 'contact_type' ],
        'user'         => [ 'type'     => 'alias', 'datatype' => 'string', 'default'  => '', 'source'   => 'user_id' ],
        'owner'        => [ 'type'     => 'alias', 'datatype' => 'string', 'default'  => '', 'source'   => 'owner_id' ],
        'status'       => [ 'type'     => 'alias', 'datatype' => 'string', 'default'  => '', 'source'   => 'status_id' ],
    ];
    protected $_joins  = [
        [ 'model'  => 'MailText', 'on'     => [ 'mail_id' => 'id' ], 'fields' => [ 'mail' => 'subject' ] ],
        [ 'model'  => 'Table', 'on'     => [ 'contact_type' => 'id' ], 'fields' => [ 'contacttype' => 'table' ] ],
        [ 'model'  => 'User', 'on'     => [ 'user_id' => 'id' ], 'fields' => [ 'user' => 'login' ] ],
        [ 'model'  => 'User', 'on'     => [ 'owner_id' => 'id' ], 'fields' => [ 'owner' => 'login' ] ],
        [ 'model'  => 'Status', 'on'     => [ 'status_id' => 'id' ], 'fields' => [ 'status' => 'status' ] ],
    ];
    protected $inputFilter;

    public function getInputFilter()
    {
        if (!$this->inputFilter) {
            $inputFilter         = new InputFilter();
            $factory             = new InputFactory();
            $inputFilter->add($factory->createInput(array(
                    'name'     => 'mail_id',
                    'required' => false,
                    'filters'  => array(
                        array( 'name' => 'Int' ),
                    ),
            )));
            $inputFilter->add($factory->createInput(array(
                    'name'       => 'mail_dtm',
                    'required'   => false,
                    'filters'    => array(
                        array( 'name' => 'StripTags' ),
                        array( 'name' => 'StringTrim' ),
                    ),
                    'validators' => array(
                        array(
                            'name'    => 'StringLength',
                            'options' => array(
                                'encoding' => 'UTF-8',
                                'min'      => 10,
                                'max'      => 20,
                            ),
                        ),
                        array(
                            'name'    => 'Date',
                            'options' => array(
                                'format' => 'Y-m-d',
                            ),
                        ),
                        array(
                            'name'    => 'Between',
                            'options' => array(
                                'min' => '1960-01-01',
                                'max' => date('Y-m-d', time()),
                            ),
                        ),
                    ),
            )));
            $inputFilter->add($factory->createInput(array(
                    'name'     => 'type_id',
                    'required' => false,
                    'filters'  => array(
                        array( 'name' => 'Int' ),
                    ),
            )));
            $inputFilter->add($factory->createInput(array(
                    'name'     => 'message_id',
                    'required' => false,
                    'filters'  => array(
                        array( 'name' => 'Int' ),
                    ),
            )));
            $inputFilter->add($factory->createInput(array(
                    'name'     => 'in_reply_to',
                    'required' => false,
                    'filters'  => array(
                        array( 'name' => 'Int' ),
                    ),
            )));
            $inputFilter->add($factory->createInput(array(
                    'name'     => 'contact_type',
                    'required' => false,
                    'filters'  => array(
                        array( 'name' => 'Int' ),
                    ),
            )));
            $inputFilter->add($factory->createInput(array(
                    'name'     => 'contact_id',
                    'required' => false,
                    'filters'  => array(
                        array( 'name' => 'Int' ),
                    ),
            )));
            $inputFilter->add($factory->createInput(array(
                    'name'     => 'user_id',
                    'required' => false,
                    'filters'  => array(
                        array( 'name' => 'Int' ),
                    ),
            )));
            $inputFilter->add($factory->createInput(array(
                    'name'     => 'owner_id',
                    'required' => false,
                    'filters'  => array(
                        array( 'name' => 'Int' ),
                    ),
            )));
            $inputFilter->add($factory->createInput(array(
                    'name'     => 'status_id',
                    'required' => false,
                    'filters'  => array(
                        array( 'name' => 'Int' ),
                    ),
            )));
            $inputFilter->add($factory->createInput(array(
                    'name'     => 'group_id',
                    'required' => false,
                    'filters'  => array(
                        array( 'name' => 'Int' ),
                    ),
            )));
            $this->inputFilter = $inputFilter;
        }

        return $this->inputFilter;
    }

    public function getName()
    {
        return $this->subject;
    }
}
