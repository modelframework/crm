<?php

namespace Wepo\Model;

use Zend\InputFilter\Factory as InputFactory;
use Zend\InputFilter\InputFilter;
use Wepo\Lib\WepoModel;

class Payment extends WepoModel
{
    public $table_name = 'payment';

    const TABLE_NAME = 'payment';

    protected $_fields = [
        'id'            => [ 'type'     => 'pk', 'datatype' => 'int', 'default'  => 0 ],
        'owner_id'      => [ 'type'     => 'source', 'datatype' => 'int', 'default'  => 0, 'alias'    => 'owner' ],
        'contact_id'    => [ 'type'     => 'source', 'datatype' => 'int', 'default'  => 0, 'alias'    => 'contact' ],
        'sum'           => [ 'type'     => 'field', 'datatype' => 'int', 'default'  => 0 ],
        'performed_dtm' => [ 'type'     => 'field', 'datatype' => 'string', 'default'  => '' ],
        'created_dtm'   => [ 'type'     => 'field', 'datatype' => 'string', 'default'  => '' ],
        'invoice_id'    => [ 'type'     => 'source', 'datatype' => 'int', 'default'  => 0, 'alias'    => 'invoice' ],
        'description'   => [ 'type'     => 'field', 'datatype' => 'string', 'default'  => '' ],
        'owner'   => [ 'type'     => 'alias', 'datatype' => 'string', 'default'  => '', 'source'   => 'owner_id' ],
        'contact' => [ 'type'     => 'alias', 'datatype' => 'string', 'default'  => '', 'source'   => 'contact_id' ],
        'invoice' => [ 'type'     => 'alias', 'datatype' => 'string', 'default'  => '', 'source'   => 'invoice_id' ],
    ];
    protected $_joins  = [
        [ 'model'  => 'User', 'on'     => [ 'owner_id' => 'id' ], 'fields' => [ 'owner' => 'login' ] ],
        [ 'model'  => 'Contact', 'on'     => [ 'contact_id' => 'id' ], 'fields' => [ 'contact' => 'lname' ] ],
        [ 'model'  => 'Invoice', 'on'     => [ 'invoice_id' => 'id' ], 'fields' => [ 'invoice' => 'subject' ] ],
    ];
    protected $inputFilter;

    public function getInputFilter()
    {
        if (!$this->inputFilter) {
            $inputFilter         = new InputFilter();
            $factory             = new InputFactory();
            $inputFilter->add($factory->createInput(array(
                    'name'     => 'owner_id',
                    'required' => false,
                    'filters'  => array(
                        array( 'name' => 'Int' ),
                    ),
            )));
            $inputFilter->add($factory->createInput(array(
                    'name'     => 'client_id',
                    'required' => true,
                    'filters'  => array(
                        array( 'name' => 'Int' ),
                    ),
            )));
            $inputFilter->add($factory->createInput(array(
                    'name'     => 'contact_id',
                    'required' => true,
                    'filters'  => array(
                        array( 'name' => 'Int' ),
                    ),
            )));
            $inputFilter->add($factory->createInput(array(
                    'name'     => 'sum',
                    'required' => true,
                    'filters'  => array(
                        array( 'name' => 'Int' ),
                    ),
            )));
            $inputFilter->add($factory->createInput(array(
                    'name'     => 'performed_dtm',
                    'required' => true,
                    'filters'  => array(
                        array( 'name' => 'StripTags' ),
                        array( 'name' => 'StringTrim' ),
                    ),
            )));
            $inputFilter->add($factory->createInput(array(
                    'name'     => 'invoice_id',
                    'required' => false,
                    'filters'  => array(
                        array( 'name' => 'Int' ),
                    ),
            )));
            $inputFilter->add($factory->createInput(array(
                    'name'       => 'description',
                    'required'   => false,
                    'filters'    => array(
                        array( 'name' => 'StripTags' ),
                        array( 'name' => 'StringTrim' ),
                    ),
                    'validators' => array(
                        array(
                            'name'    => 'StringLength',
                            'options' => array(
                                'encoding' => 'UTF-8',
                                'min'      => 3,
                                'max'      => 100,
                            ),
                        ),
                    ),
            )));
            $this->inputFilter = $inputFilter;
        }

        return $this->inputFilter;
    }

    public function getName()
    {
        return $this->description;
    }
}
