<?php

namespace Wepo\Model;

use Zend\InputFilter\Factory as InputFactory;
use Zend\InputFilter\InputFilter;
use Wepo\Lib\WepoModel;

class Lead1 extends WepoModel
{
    public $table_name = 'lead';

    const TABLE_NAME = 'lead';

    protected $_fields    = [
        'id'          => [ 'type'     => 'pk', 'datatype' => 'int', 'default'  => 0 ],
        'owner_id'    => [ 'type'     => 'source', 'datatype' => 'int', 'default'  => 0, '', 'alias'   => 'owner' ],
        'fname'       => [ 'type'     => 'field', 'datatype' => 'string', 'default'  => '' ],
        'lname'       => [ 'type'     => 'field', 'datatype' => 'string', 'default'  => '' ],
        'login'       => [ 'type'     => 'field', 'datatype' => 'string', 'default'  => '' ],
        'phone'       => [ 'type'     => 'field', 'datatype' => 'string', 'default'  => '' ],
        'mobile'      => [ 'type'     => 'field', 'datatype' => 'string', 'default'  => '' ],
        'email'       => [ 'type'     => 'field', 'datatype' => 'string', 'default'  => '' ],
        'birth_date'  => [ 'type'     => 'field', 'datatype' => 'string', 'default'  => '' ],
        'changer_id'  => [ 'type'     => 'source', 'datatype' => 'int', 'default'  => 0, 'alias'   => 'changer' ],
        'changed_dtm' => [ 'type'     => 'field', 'datatype' => 'string', 'default'  => '' ],
        'created_dtm' => [ 'type'     => 'field', 'datatype' => 'string', 'default'  => '' ],
        'status_id'   => [ 'type'     => 'source', 'datatype' => 'int', 'default'  => 1, 'alias'   => 'status' ],

        'owner'       => [ 'type'     => 'alias', 'datatype' => 'string', 'default'  => '', 'source'   => 'owner_id' ],
        'changer'     => [ 'type'     => 'alias', 'datatype' => 'string', 'default'  => '', 'source'   => 'changer_id' ],
        'status'      => [ 'type'     => 'alias', 'datatype' => 'string', 'default'  => '', 'source'   => 'status_id' ],
    ];
    protected $_joins     = [
        [ 'model'  => 'User', 'on'     => [ 'owner_id' => 'id' ], 'fields' => [ 'owner' => 'login' ] ],
        [ 'model'  => 'User', 'on'     => [ 'changer_id' => 'id' ], 'fields' => [ 'changer' => 'login' ] ],
        [ 'model'  => 'Status', 'on'     => [ 'status_id' => 'id' ], 'fields' => [ 'status' => 'status' ] ],
    ];
    protected $inputFilter;

    public function getInputFilter()
    {
        if (!$this->inputFilter) {
            $inputFilter = new InputFilter();
            $factory     = new InputFactory();

            $inputFilter->add($factory->createInput(array(
                    'name'     => 'id',
                    'required' => true,
                    'filters'  => array(
                        array( 'name' => 'Int' ),
                    ),
            )));
            $inputFilter->add($factory->createInput(array(
                    'name'     => 'owner_id',
                    'required' => true,
                    'filters'  => array(
                        array( 'name' => 'Int' ),
                    ),
            )));

            $inputFilter->add($factory->createInput(array(
                    'name'       => 'fname',
                    'required'   => true,
                    'filters'    => array(
                        array( 'name' => 'StripTags' ),
                        array( 'name' => 'StringTrim' ),
                    ),
                    'validators' => array(
                        array(
                            'name'    => 'StringLength',
                            'options' => array(
                                'encoding' => 'UTF-8',
                                'min'      => 1,
                                'max'      => 100,
                            ),
                        ),
                    ),
            )));

            $inputFilter->add($factory->createInput(array(
                    'name'       => 'lname',
                    'required'   => true,
                    'filters'    => array(
                        array( 'name' => 'StripTags' ),
                        array( 'name' => 'StringTrim' ),
                    ),
                    'validators' => array(
                        array(
                            'name'    => 'StringLength',
                            'options' => array(
                                'encoding' => 'UTF-8',
                                'min'      => 1,
                                'max'      => 100,
                            ),
                        ),
                    ),
            )));
            $inputFilter->add($factory->createInput(array(
                    'name'       => 'login',
                    'required'   => true,
                    'filters'    => array(
                        array( 'name' => 'StripTags' ),
                        array( 'name' => 'StringTrim' ),
                    ),
                    'validators' => array(
                        array(
                            'name'    => 'StringLength',
                            'options' => array(
                                'encoding' => 'UTF-8',
                                'min'      => 3,
                                'max'      => 100,
                            ),
                        ),
                    ),
                    'attributes' => array(
                        'required' => 'required',
                    ),
            )));
            $inputFilter->add($factory->createInput(array(
                    'name'       => 'phone',
                    'required'   => true,
                    'filters'    => array(
                        array( 'name' => 'StripTags' ),
                        array( 'name' => 'StringTrim' ),
                    ),
                    'validators' => array(
                        array(
                            'name'    => 'StringLength',
                            'options' => array(
                                'encoding' => 'UTF-8',
                                'min'      => 5,
                                'max'      => 100,
                            ),
                        ),
                    ),
            )));
            $inputFilter->add($factory->createInput(array(
                    'name'       => 'mobile',
                    'required'   => false,
                    'filters'    => array(
                        array( 'name' => 'StripTags' ),
                        array( 'name' => 'StringTrim' ),
                    ),
                    'validators' => array(
                        array(
                            'name'    => 'StringLength',
                            'options' => array(
                                'encoding' => 'UTF-8',
                                'min'      => 8,
                                'max'      => 100,
                            ),
                        ),
                    ),
            )));
            $inputFilter->add($factory->createInput(array(
                    'name'       => 'email',
                    'required'   => false,
                    'filters'    => array(
                        array( 'name' => 'StripTags' ),
                        array( 'name' => 'StringTrim' ),
                    ),
                    'validators' => array(
                        array(
                            'name'    => 'StringLength',
                            'options' => array(
                                'encoding' => 'UTF-8',
                                'min'      => 6,
                                'max'      => 100,
                            ),
                        ),
                    ),
            )));
            $inputFilter->add($factory->createInput(array(
                    'name'       => 'birth_date',
                    'required'   => false,
                    'filters'    => array(
                        array( 'name' => 'StripTags' ),
                        array( 'name' => 'StringTrim' ),
                    ),
                    'validators' => array(
                        array(
                            'name'    => 'StringLength',
                            'options' => array(
                                'encoding' => 'UTF-8',
                                'min'      => 10,
                                'max'      => 20,
                            ),
                        ),
                        array(
                            'name'    => 'Date',
                            'options' => array(
                                'format' => 'Y-m-d',
                            ),
                        ),
                        array(
                            'name'    => 'Between',
                            'options' => array(
                                'min' => '1940-01-01',
                                'max' => date('Y-m-d', time()),
                            ),
                        ),
                    ),
            )));
            $this->inputFilter = $inputFilter;
        }

        return $this->inputFilter;
    }

    public function getName()
    {
        return $this->fname.' '.$this->lname;
    }
}
