<?php

namespace Wepo\Model;

use Zend\InputFilter\Factory as InputFactory;
use Zend\InputFilter\InputFilter;
use Wepo\Lib\WepoModel;

class Contact extends WepoModel
{
    public $table_name = 'contact';

    const TABLE_NAME = 'contact';

    protected $_fields = [
        'id'          => [ 'type'     => 'pk', 'datatype' => 'int', 'default'  => 0 ],
        'owner_id'    => [ 'type'     => 'source', 'datatype' => 'int', 'default'  => 0, 'alias'   => 'owner' ],
        'client_id'   => [ 'type'     => 'source', 'datatype' => 'int', 'default'  => 0, 'alias'   => 'client' ],
        'fname'       => [ 'type'     => 'field', 'datatype' => 'string', 'default'  => '' ],
        'lname'       => [ 'type'     => 'field', 'datatype' => 'string', 'default'  => '' ],
        'login'       => [ 'type'     => 'field', 'datatype' => 'string', 'default'  => '' ],
        'phone'       => [ 'type'     => 'field', 'datatype' => 'string', 'default'  => '' ],
        'mobile'      => [ 'type'     => 'field', 'datatype' => 'string', 'default'  => '' ],
        'email'       => [ 'type'     => 'field', 'datatype' => 'string', 'default'  => '' ],
        'birth_date'  => [ 'type'     => 'field', 'datatype' => 'string', 'default'  => '' ],
        'created_dtm' => [ 'type'     => 'field', 'datatype' => 'string', 'default'  => '' ],
        'address'     => [ 'type'     => 'field', 'datatype' => 'string', 'default'  => '' ],
        'changer_id'  => [ 'type'     => 'source', 'datatype' => 'int', 'default'  => 0, 'alias'   => 'changer' ],
        'changed_dtm' => [ 'type'     => 'field', 'datatype' => 'string', 'default'  => '' ],
        'status_id'   => [ 'type'     => 'source', 'datatype' => 'int', 'default'  => 0, 'alias'   => 'status' ],

        'owner'       => [ 'type'     => 'alias', 'datatype' => 'string', 'default'  => '', 'source'   => 'owner_id' ],
        'client'      => [ 'type'     => 'alias', 'datatype' => 'string', 'default'  => '', 'source'   => 'client_id' ],
        'changer'     => [ 'type'     => 'alias', 'datatype' => 'string', 'default'  => '', 'source'   => 'changer_id' ],
        'status'      => [ 'type'     => 'alias', 'datatype' => 'string', 'default'  => '', 'source'   => 'status_id' ],
    ];
    protected $_joins     = [
        [ 'model'  => 'User', 'on'     => [ 'owner_id' => 'id' ], 'fields' => [ 'owner' => 'login' ] ],
        [ 'model'  => 'Client', 'on'     => [ 'client_id' => 'id' ], 'fields' => [ 'client' => 'name' ] ],
        [ 'model'  => 'User', 'on'     => [ 'changer_id' => 'id' ], 'fields' => [ 'changer' => 'login' ] ],
        [ 'model'  => 'Status', 'on'     => [ 'status_id' => 'id' ], 'fields' => [ 'status' => 'status' ] ],
    ];
    protected $inputFilter;

    public function toclientArray()
    {
        return array(
            'owner_id'    => $this->owner_id,
            'name'        => $this->lname,
            'phone'       => $this->phone,
            'email'       => $this->email,
            'created_dtm' => $this->created_drm,
            'changer_id'  => $this->changer_id,
            'changed_dtm' => $this->changed_dtm,
        );
    }

    public function getInputFilter()
    {
        if (!$this->inputFilter) {
            $inputFilter = new InputFilter();
            $factory     = new InputFactory();

            $inputFilter->add($factory->createInput(array(
                    'name'     => 'id',
                    'required' => true,
                    'filters'  => array(
                        array( 'name' => 'Int' ),
                    ),
            )));
            $inputFilter->add($factory->createInput(array(
                    'name'     => 'owner_id',
                    'required' => true,
                    'filters'  => array(
                        array( 'name' => 'Int' ),
                    ),
            )));
            $inputFilter->add($factory->createInput(array(
                    'name'     => 'client_id',
                    'required' => true,
                    'filters'  => array(
                        array( 'name' => 'Int' ),
                    ),
            )));

            $inputFilter->add($factory->createInput(array(
                    'name'       => 'fname',
                    'required'   => true,
                    'filters'    => array(
                        array( 'name' => 'StripTags' ),
                        array( 'name' => 'StringTrim' ),
                    ),
                    'validators' => array(
                        array(
                            'name'    => 'StringLength',
                            'options' => array(
                                'encoding' => 'UTF-8',
                                'min'      => 1,
                                'max'      => 100,
                            ),
                        ),
                    ),
            )));

            $inputFilter->add($factory->createInput(array(
                    'name'       => 'lname',
                    'required'   => true,
                    'filters'    => array(
                        array( 'name' => 'StripTags' ),
                        array( 'name' => 'StringTrim' ),
                    ),
                    'validators' => array(
                        array(
                            'name'    => 'StringLength',
                            'options' => array(
                                'encoding' => 'UTF-8',
                                'min'      => 1,
                                'max'      => 100,
                            ),
                        ),
                    ),
            )));
            $inputFilter->add($factory->createInput(array(
                    'name'       => 'login',
                    'required'   => true,
                    'filters'    => array(
                        array( 'name' => 'StripTags' ),
                        array( 'name' => 'StringTrim' ),
                    ),
                    'validators' => array(
                        array(
                            'name'    => 'StringLength',
                            'options' => array(
                                'encoding' => 'UTF-8',
                                'min'      => 3,
                                'max'      => 100,
                            ),
                        ),
                    ),
            )));
            $inputFilter->add($factory->createInput(array(
                    'name'       => 'phone',
                    'required'   => true,
                    'filters'    => array(
                        array( 'name' => 'StripTags' ),
                        array( 'name' => 'StringTrim' ),
                    ),
                    'validators' => array(
                        array(
                            'name'    => 'StringLength',
                            'options' => array(
                                'encoding' => 'UTF-8',
                                'min'      => 5,
                                'max'      => 100,
                            ),
                        ),
                    ),
            )));
            $inputFilter->add($factory->createInput(array(
                    'name'       => 'mobile',
                    'required'   => false,
                    'filters'    => array(
                        array( 'name' => 'StripTags' ),
                        array( 'name' => 'StringTrim' ),
                    ),
                    'validators' => array(
                        array(
                            'name'    => 'StringLength',
                            'options' => array(
                                'encoding' => 'UTF-8',
                                'min'      => 8,
                                'max'      => 100,
                            ),
                        ),
                    ),
            )));
            $inputFilter->add($factory->createInput(array(
                    'name'       => 'email',
                    'required'   => false,
                    'filters'    => array(
                        array( 'name' => 'StripTags' ),
                        array( 'name' => 'StringTrim' ),
                    ),
                    'validators' => array(
                        array(
                            'name'    => 'StringLength',
                            'options' => array(
                                'encoding' => 'UTF-8',
                                'min'      => 6,
                                'max'      => 100,
                            ),
                        ),
                    ),
            )));
            $inputFilter->add($factory->createInput(array(
                    'name'       => 'birth_date',
                    'required'   => false,
                    'filters'    => array(
                        array( 'name' => 'StripTags' ),
                        array( 'name' => 'StringTrim' ),
                    ),
                    'validators' => array(
                        array(
                            'name'    => 'StringLength',
                            'options' => array(
                                'encoding' => 'UTF-8',
                                'min'      => 10,
                                'max'      => 20,
                            ),
                        ),
                        array(
                            'name'    => 'Date',
                            'options' => array(
                                'format' => 'Y-m-d',
                            ),
                        ),
                        array(
                            'name'    => 'Between',
                            'options' => array(
                                'min' => '1960-01-01',
                                'max' => date('Y-m-d', time()),
                            ),
                        ),
                    ),
            )));
            $inputFilter->add($factory->createInput(array(
                    'name'       => 'address',
                    'required'   => false,
                    'filters'    => array(
                        array( 'name' => 'StripTags' ),
                        array( 'name' => 'StringTrim' ),
                    ),
                    'validators' => array(
                        array(
                            'name'    => 'StringLength',
                            'options' => array(
                                'encoding' => 'UTF-8',
                                'min'      => 6,
                                'max'      => 100,
                            ),
                        ),
                    ),
            )));
            $this->inputFilter = $inputFilter;
        }

        return $this->inputFilter;
    }

    public function getName()
    {
        return $this->fname.' '.$this->lname;
    }
}
