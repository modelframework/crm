<?php

namespace Wepo\Model;

use Zend\InputFilter\Factory as InputFactory;
use Zend\InputFilter\InputFilter;
use Zend\InputFilter\InputFilterInterface;

class MailPrefs extends \Wepo\Lib\WepoPdoModel
{
    public $table_name = 'mail_prefs';

    const TABLE_NAME = 'mail_prefs';

    protected $_fields = [
        'id'        => [ 'type'     => 'pk', 'datatype' => 'string', 'default'  => '' ],
        'owner_id'  => [ 'type'     => 'field', 'datatype' => 'int', 'default'  => 0 ],
        'mail_name' => [ 'type'     => 'field', 'datatype' => 'string', 'default'  => '' ],
        'type_id'   => [ 'type'     => 'field', 'datatype' => 'int', 'default'  => 0 ],
        'prefs'     => [ 'type'     => 'field', 'datatype' => 'string', 'default'  => '' ],
    ];
    protected $inputFilter;

    public function exchangeArray($data)
    {
        $this->id        = (isset($data[ 'id' ])) ? $data[ 'id' ] : 0;
        $this->owner_id  = (isset($data[ 'owner_id' ])) ? $data[ 'owner_id' ] : 0;
        $this->mail_name = (isset($data[ 'mail_name' ])) ? $data[ 'mail_name' ] : 0;
        $this->type      = (isset($data[ 'type' ])) ? $data[ 'type' ] : 0;
        $this->prefs     = (isset($data[ 'prefs' ])) ? $data[ 'prefs' ] : null;
    }

    public function setInputFilter(InputFilterInterface $inputFilter)
    {
        throw new \Exception("Not used");
    }

    public function getInputFilter()
    {
        if (!$this->inputFilter) {
            $inputFilter         = new InputFilter();
            $factory             = new InputFactory();
            $inputFilter->add($factory->createInput(array(
                    'name'     => 'owner_id',
                    'required' => true,
                    'filters'  => array(
                        array( 'name' => 'Int' ),
                    ),
            )));
            $inputFilter->add($factory->createInput(array(
                    'name'       => 'mail_name',
                    'required'   => true,
                    'filters'    => array(
                        array( 'name' => 'StripTags' ),
                        array( 'name' => 'StringTrim' ),
                    ),
                    'validators' => array(
                        array(
                            'name'    => 'StringLength',
                            'options' => array(
                                'encoding' => 'UTF-8',
                                'min'      => 1,
                                'max'      => 250,
                            ),
                        ),
                    ),
            )));
            $inputFilter->add($factory->createInput(array(
                    'name'       => 'type',
                    'required'   => true,
                    'filters'    => array(
                        array( 'name' => 'StripTags' ),
                        array( 'name' => 'StringTrim' ),
                    ),
                    'validators' => array(
                        array(
                            'name'    => 'StringLength',
                            'options' => array(
                                'encoding' => 'UTF-8',
                            ),
                        ),
                    ),
            )));
            $inputFilter->add($factory->createInput(array(
                    'name'       => 'prefs',
                    'required'   => true,
                    'filters'    => array(
                        array( 'name' => 'StripTags' ),
                        array( 'name' => 'StringTrim' ),
                    ),
                    'validators' => array(
                        array(
                            'name'    => 'StringLength',
                            'options' => array(
                                'encoding' => 'UTF-8',
                            ),
                        ),
                    ),
            )));
            $this->inputFilter = $inputFilter;
        }

        return $this->inputFilter;
    }

    public function getName()
    {
        return $this->subject;
    }
}
