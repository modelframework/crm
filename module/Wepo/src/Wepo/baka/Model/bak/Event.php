<?php

namespace Wepo\Model;

use Zend\InputFilter\Factory as InputFactory;
use Zend\InputFilter\InputFilter;
use Wepo\Lib\WepoModel;

class Event extends WepoModel
{
    public $table_name = 'activity_event';

    const TABLE_NAME = 'activity_event';

    protected $_fields = [
        'id'                 => [ 'type'     => 'pk', 'datatype' => 'int', 'default'  => 0 ],
        'subject'            => [ 'type'     => 'field', 'datatype' => 'string', 'default'  => '' ],
        'start_dtm'          => [ 'type'     => 'field', 'datatype' => 'string', 'default'  => '' ],
        'end_dtm'            => [ 'type'     => 'field', 'datatype' => 'string', 'default'  => '' ],
        'owner_id'           => [ 'type'     => 'source', 'datatype' => 'int', 'default'  => 0, 'alias'    => 'owner' ],
        'remind_dtm'         => [ 'type'     => 'field', 'datatype' => 'string', 'default'  => '' ],
        'r_when'             => [ 'type'     => 'field', 'datatype' => 'string', 'default'  => '' ],
        'r_repeat'           => [ 'type'     => 'field', 'datatype' => 'string', 'default'  => '' ],
        'r_alert'            => [ 'type'     => 'field', 'datatype' => 'string', 'default'  => '' ],
        'rec_startdate'      => [ 'type'     => 'field', 'datatype' => 'string', 'default'  => '' ],
        'rec_enddate'        => [ 'type'     => 'field', 'datatype' => 'string', 'default'  => '' ],
        'invites'            => [ 'type'     => 'field', 'datatype' => 'string', 'default'  => '' ],
        'venue'              => [ 'type'     => 'field', 'datatype' => 'string', 'default'  => '' ],
        'table_id'           => [ 'type'     => 'source', 'datatype' => 'int', 'default'  => 0, 'alias'    => 'table' ],
        'target_id'          => [ 'type'     => 'field', 'datatype' => 'int', 'default'  => 0 ],
        'client_id'          => [ 'type'     => 'source', 'datatype' => 'int', 'default'  => 0, 'alias'    => 'client' ],
        'notification_email' => [ 'type'     => 'field', 'datatype' => 'int', 'default'  => 0 ],
        'description'        => [ 'type'     => 'field', 'datatype' => 'string', 'default'  => '' ],
        'recurring'          => [ 'type'     => 'field', 'datatype' => 'int', 'default'  => 0 ],

        'owner'              => [ 'type'     => 'alias', 'datatype' => 'string', 'default'  => '', 'source'   => 'owner_id' ],
        'client'             => [ 'type'     => 'alias', 'datatype' => 'string', 'default'  => '', 'source'   => 'client_id' ],
        'table'              => [ 'type'     => 'alias', 'datatype' => 'string', 'default'  => '', 'source'   => 'table_id' ],
    ];
    protected $_joins  = [
        [ 'model'  => 'User', 'on'     => [ 'owner_id' => 'id' ], 'fields' => [ 'owner' => 'login' ] ],
        [ 'model'  => 'Client', 'on'     => [ 'client_id' => 'id' ], 'fields' => [ 'client' => 'name' ] ],
        [ 'model'  => 'Table', 'on'     => [ 'table_id' => 'id' ], 'fields' => [ 'table' => 'label' ] ],
    ];
    protected $inputFilter;

    public function toActivity()
    {
        return array(
            'id'         => $this->id,
            'subject'    => $this->subject,
            'table_id'   => $this->table_id,
            'target_id'  => $this->target_id,
            'client_id'  => $this->client_id,
            'owner_id'   => $this->owner_id,
            'remind_dtm' => $this->remind_dtm,
        );
    }

    public function getInputFilter()
    {
        if (!$this->inputFilter) {
            $inputFilter = new InputFilter();
            $factory     = new InputFactory();
            $inputFilter->add($factory->createInput(array(
                    'name'       => 'subject',
                    'required'   => true,
                    'filters'    => array(
                        array( 'name' => 'StripTags' ),
                        array( 'name' => 'StringTrim' ),
                    ),
                    'validators' => array(
                        array(
                            'name'    => 'StringLength',
                            'options' => array(
                                'encoding' => 'UTF-8',
                                'min'      => 3,
                                'max'      => 100,
                            ),
                        ),
                    ),
            )));
            $inputFilter->add($factory->createInput(array(
                    'name'     => 'start_dtm',
                    'required' => false,
//                    'filters' => array(
//                        array( 'name' => 'Int' ),
//                    ),
            )));
            $inputFilter->add($factory->createInput(array(
                    'name'     => 'end_dtm',
                    'required' => false,
//                    'filters' => array(
//                        array( 'name' => 'Int' ),
//                    ),
            )));
            $inputFilter->add($factory->createInput(array(
                    'name'     => 'owner_id',
                    'required' => false,
                    'filters'  => array(
                        array( 'name' => 'Int' ),
                    ),
            )));
            $inputFilter->add($factory->createInput(array(
                    'name'     => 'remind_dtm',
                    'required' => false,
                    'filters'  => array(
                        array( 'name' => 'StripTags' ),
                        array( 'name' => 'StringTrim' ),
                    ),
//                    'validators' => array(
//                        array(
//                            'name'    => 'StringLength',
//                            'options' => array(
//                                'encoding' => 'UTF-8',
//                                'min'      => 0,
//                                'max'      => 20,
//                            ),
//                        ),
//                        array(
//                            'name'    => 'Date',
//                            'options' => array(
//                                'format' => 'Y-m-d',
//                            ),
//                        ),
//                        array(
//                            'name'    => 'Between',
//                            'options' => array(
//                                'min' => '1960-01-01',
//                                'max' => date( 'Y-m-d', time() )
//                            ),
//                        ),
//                    ),
            )));
            $inputFilter->add($factory->createInput(array(
                    'name'     => 'r_when',
                    'required' => false,
//                    'filters' => array(
//                        array( 'name' => 'Int' ),
//                    ),
            )));
            $inputFilter->add($factory->createInput(array(
                    'name'     => 'r_repeat',
                    'required' => false,
//                    'filters' => array(
//                        array( 'name' => 'Int' ),
//                    ),
            )));
            $inputFilter->add($factory->createInput(array(
                    'name'     => 'r_alert',
                    'required' => false,
//                    'filters' => array(
//                        array( 'name' => 'Int' ),
//                    ),
            )));
            $inputFilter->add($factory->createInput(array(
                    'name'     => 'rec_startdate',
                    'required' => false,
//                    'filters' => array(
//                        array( 'name' => 'Int' ),
//                    ),
            )));
            $inputFilter->add($factory->createInput(array(
                    'name'     => 'rec_enddate',
                    'required' => false,
//                    'filters' => array(
//                        array( 'name' => 'Int' ),
//                    ),
            )));
            $inputFilter->add($factory->createInput(array(
                    'name'     => 'venue',
                    'required' => false,
//                    'filters' => array(
//                        array( 'name' => 'Int' ),
//                    ),
            )));
            $inputFilter->add($factory->createInput(array(
                    'name'     => 'table_id',
                    'required' => false,
//                    'filters' => array(
//                        array( 'name' => 'Int' ),
//                    ),
            )));
            $inputFilter->add($factory->createInput(array(
                    'name'     => 'invites',
                    'required' => false,
//                    'filters' => array(
//                        array( 'name' => 'Int' ),
//                    ),
            )));
            $inputFilter->add($factory->createInput(array(
                    'name'     => 'target_id',
                    'required' => false,
//                    'filters' => array(
//                        array( 'name' => 'Int' ),
//                    ),
            )));
            $inputFilter->add($factory->createInput(array(
                    'name'     => 'client_id',
                    'required' => false,
                    'filters'  => array(
                        array( 'name' => 'Int' ),
                    ),
            )));
            $inputFilter->add($factory->createInput(array(
                    'name'     => 'notification_email',
                    'required' => false,
//                    'filters' => array(
//                        array( 'name' => 'Int' ),
//                    ),
            )));
            $inputFilter->add($factory->createInput(array(
                    'name'       => 'description',
                    'required'   => false,
                    'filters'    => array(
                        array( 'name' => 'StripTags' ),
                        array( 'name' => 'StringTrim' ),
                    ),
                    'validators' => array(
                        array(
                            'name'    => 'StringLength',
                            'options' => array(
                                'encoding' => 'UTF-8',
                                'min'      => 3,
                                'max'      => 100,
                            ),
                        ),
                    ),
            )));
            $inputFilter->add($factory->createInput(array(
                    'name'     => 'recurring',
                    'required' => false,
//                    'filters' => array(
//                        array( 'name' => 'Int' ),
//                    ),
            )));
            $this->inputFilter = $inputFilter;
        }

        return $this->inputFilter;
    }

    public function getName()
    {
        return $this->name;
    }
}
