<?php

namespace Wepo\Model;

use Zend\InputFilter\Factory as InputFactory;
use Zend\InputFilter\InputFilter;
use Wepo\Lib\WepoModel;

class OrderDetail extends WepoModel
{
    public $table_name = 'order_detail';

    const TABLE_NAME = 'order_detail';

    protected $_fields = [
        'id'            => [ 'type' => 'pk', 'datatype' => 'int', 'default' => 0 ],
        'order_id'      => [ 'type' => 'source', 'datatype' => 'int', 'default' => 0, 'alias' => 'order' ],
        'product_id'    => [ 'type' => 'source', 'datatype' => 'int', 'default' => 0, 'alias' => 'product' ],
        'pricebook_id'  => [ 'type' => 'source', 'datatype' => 'int', 'default' => 0, 'alias' => 'pricebook' ],
        'product_price' => [ 'type' => 'field', 'datatype' => 'int', 'default' => 0 ],
        'discount'      => [ 'type' => 'field', 'datatype' => 'int', 'default' => 0 ],
        'qty'           => [ 'type' => 'field', 'datatype' => 'int', 'default' => 0 ],
        'final_price'   => [ 'type' => 'field', 'datatype' => 'int', 'default' => 0 ],
        'order'     => [ 'type' => 'alias', 'datatype' => 'string', 'default' => '', 'source' => 'order_id' ],
        'product'   => [ 'type' => 'alias', 'datatype' => 'string', 'default' => '', 'source' => 'product_id' ],
        'pricebook' => [ 'type' => 'alias', 'datatype' => 'string', 'default' => '', 'source' => 'pricebook_id' ],
    ];
    protected $_joins  = [
        [ 'model' => 'Order', 'on' => [ 'order_id' => 'id' ], 'fields' => [ 'order' => 'subject' ] ],
        [ 'model' => 'Product', 'on' => [ 'product_id' => 'id' ], 'fields' => [ 'product' => 'name' ] ],
        [ 'model' => 'Pricebook', 'on' => [ 'pricebook_id' => 'id' ], 'fields' => [ 'pricebook' => 'pricebook' ] ],
    ];
    protected $inputFilter;

    public function getInputFilter()
    {
        if (!$this->inputFilter) {
            $inputFilter         = new InputFilter();
            $factory             = new InputFactory();
            $inputFilter->add($factory->createInput(array(
                    'name'     => 'order_id',
                    'required' => false,
                    'filters'  => array(
                        array( 'name' => 'Int' ),
                    ),
            )));
            $inputFilter->add($factory->createInput(array(
                    'name'       => 'product_id',
                    'required'   => true,
                    'filters'    => array(
                        array( 'name' => 'Int' ),
                    ),
                    'validators' => array(
                        array(
                            'name'    => 'GreaterThan',
                            'options' => array(
                                'min'       => 1,
                                'inclusive' => true,
                                'messages'  => array( \Zend\Validator\GreaterThan::NOT_GREATER_INCLUSIVE => 'select item' ),
                            ),
                        ),
                    ),
            )));
            $inputFilter->add($factory->createInput(array(
                    'name'     => 'product_price',
                    'required' => false,
                    'filters'  => array(
                        array( 'name' => 'Int' ),
                    ),
            )));
            $inputFilter->add($factory->createInput(array(
                    'name'     => 'discount',
                    'required' => false,
                    'filters'  => array(
                        array( 'name' => 'Int' ),
                    ),
            )));
            $inputFilter->add($factory->createInput(array(
                    'name'       => 'qty',
                    'required'   => false,
//                    'filters'  => array(
//                        array( 'name' => 'Int' ),
//                    ),
                    'validators' => array(
                        array( 'name' => 'Digits' ),
                    ),
            )));
            $inputFilter->add($factory->createInput(array(
                    'name'     => 'final_price',
                    'required' => false,
                    'filters'  => array(
                        array( 'name' => 'Int' ),
                    ),
            )));
            $this->inputFilter = $inputFilter;
        }

        return $this->inputFilter;
    }

    public function getName()
    {
        return 'Product id '.$this->product_id;
    }
}
