<?php

namespace Wepo\Model;

use Zend\InputFilter\Factory as InputFactory;
use Zend\InputFilter\InputFilter;
use Wepo\Lib\WepoModel;

class Product extends WepoModel
{
    public $table_name = 'product';

    const TABLE_NAME = 'product';

    protected $_fields = [
        'id'          => [ 'type'     => 'pk', 'datatype' => 'int', 'default'  => 0 ],
        'name'        => [ 'type'     => 'field', 'datatype' => 'string', 'default'  => '' ],
        'price'       => [ 'type'     => 'field', 'datatype' => 'string', 'default'  => '' ],
        'description' => [ 'type'     => 'field', 'datatype' => 'string', 'default'  => '' ],
        'changer_id'  => [ 'type'     => 'source', 'datatype' => 'int', 'default'  => 0, 'alias'   => 'changer' ],
        'changed_dtm' => [ 'type'     => 'field', 'datatype' => 'string', 'default'  => '' ],
        'status_id'   => [ 'type'     => 'source', 'datatype' => 'int', 'default'  => 0, 'alias'   => 'status' ],

        'changer'     => [ 'type'     => 'alias', 'datatype' => 'string', 'default'  => '', 'source'   => 'changer_id' ],
        'status'      => [ 'type'     => 'alias', 'datatype' => 'string', 'default'  => '', 'source'   => 'status_id' ],
    ];
    protected $_joins     = [
        [ 'model'  => 'User', 'on'     => [ 'changer_id' => 'id' ], 'fields' => [ 'changer' => 'login' ] ],
        [ 'model'  => 'Status', 'on'     => [ 'status_id' => 'id' ], 'fields' => [ 'status' => 'status' ] ],
    ];
    protected $inputFilter;

    public function getInputFilter()
    {
        if (!$this->inputFilter) {
            $inputFilter = new InputFilter();
            $factory     = new InputFactory();

            $inputFilter->add($factory->createInput(array(
                    'name'     => 'id',
                    'required' => true,
                    'filters'  => array(
                        array( 'name' => 'Int' ),
                    ),
            )));
            $inputFilter->add($factory->createInput(array(
                    'name'     => 'owner_id',
                    'required' => true,
                    'filters'  => array(
                        array( 'name' => 'Int' ),
                    ),
            )));
            $inputFilter->add($factory->createInput(array(
                    'name'       => 'name',
                    'required'   => true,
                    'filters'    => array(
                        array( 'name' => 'StripTags' ),
                        array( 'name' => 'StringTrim' ),
                    ),
                    'validators' => array(
                        array(
                            'name'    => 'StringLength',
                            'options' => array(
                                'encoding' => 'UTF-8',
                                'min'      => 3,
                                'max'      => 100,
                            ),
                        ),
                    ),
            )));
            $inputFilter->add($factory->createInput(array(
                    'name'       => 'price',
                    'required'   => true,
                    'filters'    => array(
                        array( 'name' => 'StripTags' ),
                        array( 'name' => 'StringTrim' ),
                    ),
                    'validators' => array(
                        array(
                            'name'    => 'StringLength',
                            'options' => array(
                                'encoding' => 'UTF-8',
                                'min'      => 1,
                                'max'      => 100000,
                            ),
                        ),
                    ),
            )));
            $inputFilter->add($factory->createInput(array(
                    'name'       => 'description',
                    'required'   => true,
                    'filters'    => array(
                        array( 'name' => 'StripTags' ),
                        array( 'name' => 'StringTrim' ),
                    ),
                    'validators' => array(
                        array(
                            'name'    => 'StringLength',
                            'options' => array(
                                'encoding' => 'UTF-8',
                                'min'      => 3,
                                'max'      => 100,
                            ),
                        ),
                    ),
            )));
            $this->inputFilter = $inputFilter;
        }

        return $this->inputFilter;
    }

    public function getName()
    {
        return $this->name;
    }
}
