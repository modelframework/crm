<?php

namespace Wepo\Model;

use Zend\InputFilter\Factory as InputFactory;
use Zend\InputFilter\InputFilter;
use Wepo\Lib\WepoModel;
use Wepo\Form\Validator\Float;

class PricebookDetail extends WepoModel
{
    public $table_name = 'pricebook_detail';

    const TABLE_NAME = 'pricebook_detail';

    protected $_fields = [
        'id'            => ['type' => 'pk', 'datatype' => 'int', 'default' => 0 ],
        'pricebook_id'  => ['type' => 'source', 'datatype' => 'int', 'default' => 0, 'alias' => 'pricebook' ],
        'product_id'    => ['type' => 'source', 'datatype' => 'int', 'default' => 0, 'alias' => 'product' ],
        'price_type'    => ['type' => 'field', 'datatype' => 'string', 'default' => '' ],
        'modifier'      => ['type' => 'field', 'datatype' => 'float', 'default' => 0 ],
        'result_price'  => ['type' => 'field', 'datatype' => 'float', 'default' => 0 ],
        ////////////////////////////////////////////////////////////////////
        'pricebook'     => ['type' => 'alias', 'datatype' => 'string', 'default' => '', 'source' => 'pricebook_id' ],
        'product_price' => ['type' => 'alias', 'datatype' => 'string', 'default' => '', 'source' => 'pricebook_id' ],
        'product'       => ['type' => 'alias', 'datatype' => 'string', 'default' => '', 'source' => 'product_id' ],
    ];
    protected $_joins  = [
        ['model' => 'Pricebook', 'on' => ['pricebook_id' => 'id' ], 'fields' => ['pricebook' => 'pricebook' ] ],
        ['model' => 'Product', 'on' => ['product_id' => 'id' ], 'fields' => ['product' => 'name', 'product_price' => 'price' ] ],
    ];
    protected $_unique = [[ 'pricebook_id', 'product_id' ] ];
    protected $inputFilter;

    public function getInputFilter()
    {
        if (!$this->inputFilter) {
            $inputFilter         = new InputFilter();
            $factory             = new InputFactory();
            $inputFilter->add($factory->createInput(array(
                    'name'     => 'pricebook_id',
                    'required' => true,
                    'filters'  => array(
                        array( 'name' => 'Int' ),
                    ),
            )));
            $inputFilter->add($factory->createInput(array(
                    'name'     => 'product_id',
                    'required' => true,
                    'filters'  => array(
                        array( 'name' => 'Int' ),
                    ),
            )));
            $inputFilter->add($factory->createInput(array(
                    'name'       => 'modifier',
                    'required'   => true,
//                    'filters'  => array(
//                        array( 'name' => 'Int' ),
//                    ),
                    'validators' => array(
                        new Float(),
                        array( 'name'    => 'GreaterThan',
                            'options' => array(
                                'min'       => 0,
                                'inclusive' => true,
                            ), ),
                    ),
            )));
            $inputFilter->add($factory->createInput(array(
                    'name'       => 'price_type',
                    'required'   => false,
                    'filters'    => array(
                        array( 'name' => 'StripTags' ),
                        array( 'name' => 'StringTrim' ),
                    ),
                    'validators' => array(
                        array(
                            'name'    => 'InArray',
                            'options' => array(
                                'haystack' => ['static', 'percent' ],
                            ),
                        ),
                    ),
            )));
            $inputFilter->add($factory->createInput(array(
                    'name'       => 'product_price',
                    'required'   => true,
//                    'filters'  => array(
//                        array( 'name' => 'Int' ),
//                    ),
                    'validators' => array(
                        new Float(),
                        array( 'name'    => 'GreaterThan',
                            'options' => array(
                                'min'       => 0,
                                'inclusive' => true,
                            ), ),
                    ),
            )));
            $this->inputFilter = $inputFilter;
        }

        return $this->inputFilter;
    }

    public function getName()
    {
        return 'Product '.$this->product;
    }
}
