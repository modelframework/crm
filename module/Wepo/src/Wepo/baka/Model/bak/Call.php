<?php

namespace Wepo\Model;

use Zend\InputFilter\Factory as InputFactory;
use Zend\InputFilter\InputFilter;
use Wepo\Lib\WepoModel;

class Call extends WepoModel
{
    public $table_name = 'activity_call';

    const TABLE_NAME = 'activity_call';

    protected $_fields = [
        'id'             => [ 'type'     => 'pk', 'datatype' => 'int', 'default'  => 0 ],
        'subject'        => [ 'type'     => 'field', 'datatype' => 'string', 'default'  => '' ],
        'call_type'      => [ 'type'     => 'field', 'datatype' => 'int', 'default'  => 0 ],
        'call_purpose'   => [ 'type'     => 'field', 'datatype' => 'string', 'default'  => '' ],
        'table_id'       => [ 'type'     => 'source', 'datatype' => 'int', 'default'  => 0, 'alias'    => 'table' ],
        'target_id'      => [ 'type'     => 'field', 'datatype' => 'int', 'default'  => 0 ],
        'client_id'      => [ 'type'     => 'source', 'datatype' => 'int', 'default'  => 0, 'alias'    => 'client' ],
        'call_detail'    => [ 'type'     => 'field', 'datatype' => 'string', 'default'  => '' ],
        'call_start_dtm' => [ 'type'     => 'field', 'datatype' => 'string', 'default'  => '' ],
        'call_duration'  => [ 'type'     => 'field', 'datatype' => 'string', 'default'  => '' ],
        'call_result'    => [ 'type'     => 'field', 'datatype' => 'int', 'default'  => 0 ],
        'owner_id'       => [ 'type'     => 'source', 'datatype' => 'int', 'default'  => 0, 'alias'    => 'owner' ],
        'description'    => [ 'type'     => 'field', 'datatype' => 'string', 'default'  => '' ],
        'billable'       => [ 'type'     => 'field', 'datatype' => 'boolean', 'default'  => false ],
        'remind_dtm'     => [ 'type'     => 'field', 'datatype' => 'string', 'default'  => '' ],

        'owner'  => [ 'type'     => 'alias', 'datatype' => 'string', 'default'  => '', 'source'   => 'owner_id' ],
        'client' => [ 'type'     => 'alias', 'datatype' => 'string', 'default'  => '', 'source'   => 'client_id' ],
        'table'  => [ 'type'     => 'alias', 'datatype' => 'string', 'default'  => '', 'source'   => 'table_id' ],
    ];
    protected $_joins  = [
        [ 'model'  => 'User', 'on'     => [ 'owner_id' => 'id' ], 'fields' => [ 'owner' => 'login' ] ],
        [ 'model'  => 'Client', 'on'     => [ 'client_id' => 'id' ], 'fields' => [ 'client' => 'name' ] ],
        [ 'model'  => 'Table', 'on'     => [ 'table_id' => 'id' ], 'fields' => [ 'table' => 'label' ] ],
    ];
    protected $inputFilter;

    public function toActivity()
    {
        return array(
            'id'          => $this->id,
            'subject'     => $this->subject,
            'table_id'    => $this->table_id,
            'target_id'   => $this->target_id,
            'client_id'   => $this->client_id,
            'owner_id'    => $this->owner_id,
            'remind_dtm'  => $this->remind_dtm,
        );
    }

    public function getInputFilter()
    {
        if (!$this->inputFilter) {
            $inputFilter = new InputFilter();
            $factory     = new InputFactory();

            $inputFilter->add($factory->createInput(array(
                    'name'       => 'subject',
                    'required'   => true,
                    'filters'    => array(
                        array( 'name' => 'StripTags' ),
                        array( 'name' => 'StringTrim' ),
                    ),
                    'validators' => array(
                        array(
                            'name'    => 'StringLength',
                            'options' => array(
                                'encoding' => 'UTF-8',
                                'min'      => 3,
                                'max'      => 100,
                            ),
                        ),
                    ),
            )));
            $inputFilter->add($factory->createInput(array(
                    'name'     => 'call_type',
                    'required' => false,
                    'filters'  => array(
                        array( 'name' => 'Int' ),
                    ),
            )));
            $inputFilter->add($factory->createInput(array(
                    'name'     => 'call_purpose',
                    'required' => false,
                    'filters'  => array(
                        array( 'name' => 'Int' ),
                    ),
            )));
            $inputFilter->add($factory->createInput(array(
                    'name'     => 'table_id',
                    'required' => false,
                    'filters'  => array(
                        array( 'name' => 'Int' ),
                    ),
            )));
            $inputFilter->add($factory->createInput(array(
                    'name'     => 'target_id',
                    'required' => false,
                    'filters'  => array(
                        array( 'name' => 'Int' ),
                    ),
            )));
            $inputFilter->add($factory->createInput(array(
                    'name'     => 'client_id',
                    'required' => false,
                    'filters'  => array(
                        array( 'name' => 'Int' ),
                    ),
            )));
            $inputFilter->add($factory->createInput(array(
                    'name'     => 'call_detail',
                    'required' => false,
//                    'filters' => array(
//                        array( 'name' => 'Int' ),
//                    ),
            )));
            $inputFilter->add($factory->createInput(array(
                    'name'     => 'call_start_dtm',
                    'required' => false,
//                    'filters' => array(
//                        array( 'name' => 'Int' ),
//                    ),
            )));
            $inputFilter->add($factory->createInput(array(
                    'name'     => 'call_duration',
                    'required' => false,
//                    'filters' => array(
//                        array( 'name' => 'Int' ),
//                    ),
            )));
            $inputFilter->add($factory->createInput(array(
                    'name'     => 'call_result',
                    'required' => false,
                    'filters'  => array(
                        array( 'name' => 'Int' ),
                    ),
            )));
            $inputFilter->add($factory->createInput(array(
                    'name'     => 'owner_id',
                    'required' => false,
                    'filters'  => array(
                        array( 'name' => 'Int' ),
                    ),
            )));
            $inputFilter->add($factory->createInput(array(
                    'name'       => 'description',
                    'required'   => false,
                    'filters'    => array(
                        array( 'name' => 'StripTags' ),
                        array( 'name' => 'StringTrim' ),
                    ),
                    'validators' => array(
                        array(
                            'name'    => 'StringLength',
                            'options' => array(
                                'encoding' => 'UTF-8',
                                'min'      => 3,
                                'max'      => 100,
                            ),
                        ),
                    ),
            )));
            $inputFilter->add($factory->createInput(array(
                    'name'     => 'billable',
                    'required' => false,
                    'filters'  => array(
                        array( 'name' => 'Int' ),
                    ),
            )));
            $inputFilter->add($factory->createInput(array(
                    'name'       => 'remind_dtm',
                    'required'   => false,
                    'filters'    => array(
                        array( 'name' => 'StripTags' ),
                        array( 'name' => 'StringTrim' ),
                    ),
                    'validators' => array(
                        array(
                            'name'    => 'StringLength',
                            'options' => array(
                                'encoding' => 'UTF-8',
                                'min'      => 0,
                                'max'      => 20,
                            ),
                        ),
//                        array(
//                            'name'    => 'Date',
//                            'options' => array(
//                                'format' => 'Y-m-d',
//                            ),
//                        ),
//                        array(
//                            'name'    => 'Between',
//                            'options' => array(
//                                'min' => '1960-01-01',
//                                'max' => date( 'Y-m-d', time() )
//                            ),
//                        ),
                    ),
            )));
            $this->inputFilter = $inputFilter;
        }

        return $this->inputFilter;
    }

    public function getName()
    {
        return $this->name;
    }
}
