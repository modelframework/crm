<?php

namespace Wepo\Model;

use Zend\InputFilter\Factory as InputFactory;
use Zend\InputFilter\InputFilter;
use Wepo\Lib\WepoModel;

class MailText extends WepoModel
{
    public $table_name = 'mail_text';

    const TABLE_NAME  = 'mail_text';

    protected $_fields    = [
        'id'        => [ 'type'     => 'pk', 'datatype'    => 'string', 'default' => '' ],
        'from_addr' => [ 'type'     => 'field', 'datatype'    => 'string', 'default' => '' ],
        'to_addr'   => [ 'type'     => 'field', 'datatype'    => 'string', 'default' => '' ],
        'subject'   => [ 'type'     => 'field', 'datatype'    => 'string', 'default' => '' ],
        'body'      => [ 'type'     => 'field', 'datatype'    => 'string', 'default' => '' ],
    ];
    protected $inputFilter;

    public function getInputFilter()
    {
        if (!$this->inputFilter) {
            $inputFilter         = new InputFilter();
            $factory             = new InputFactory();
            $inputFilter->add($factory->createInput(array(
                    'name'     => 'from_addr',
                    'required' => true,
                    'filters'  => array(
                        array( 'name' => 'Int' ),
                    ),
            )));
            $inputFilter->add($factory->createInput(array(
                    'name'     => 'to_addr',
                    'required' => true,
                    'filters'  => array(
                        array( 'name' => 'Int' ),
                    ),
            )));
            $inputFilter->add($factory->createInput(array(
                    'name'       => 'subject',
                    'required'   => true,
                    'filters'    => array(
                        array( 'name' => 'StripTags' ),
                        array( 'name' => 'StringTrim' ),
                    ),
                    'validators' => array(
                        array(
                            'name'    => 'StringLength',
                            'options' => array(
                                'encoding' => 'UTF-8',
                                'min'      => 3,
                                'max'      => 250,
                            ),
                        ),
                    ),
            )));
            $inputFilter->add($factory->createInput(array(
                    'name'       => 'body',
                    'required'   => true,
                    'filters'    => array(
                        array( 'name' => 'StripTags' ),
                        array( 'name' => 'StringTrim' ),
                    ),
                    'validators' => array(
                        array(
                            'name'    => 'StringLength',
                            'options' => array(
                                'encoding' => 'UTF-8',
                            ),
                        ),
                    ),
            )));
            $this->inputFilter = $inputFilter;
        }

        return $this->inputFilter;
    }

    public function getName()
    {
        return $this->subject;
    }
}
