<?php

namespace Wepo\Model;

use Wepo\Lib\WepoMongoModel;

class Acl extends WepoMongoModel
{
    public $adapterName = 'wepo_company';
    protected $_fields = [
        '_id' => ['type' => 'pk', 'datatype' => 'string', 'default' => ''],
        'role_id' => ['type' => 'field', 'datatype' => 'string', 'default' => ''],
        'role' => ['type' => 'field', 'datatype' => 'string', 'default' => ''],
        'type' => ['type' => 'field', 'datatype' => 'string', 'default' => ''],
        'resorce' => ['type' => 'field', 'datatype' => 'string', 'default' => ''],
        'permissions' => ['type' => 'field', 'datatype' => 'array', 'default' => []],
        'fields' => ['type' => 'field', 'datatype' => 'array', 'default' => []],
    ];
}
