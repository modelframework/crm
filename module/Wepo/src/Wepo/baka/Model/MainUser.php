<?php

namespace Wepo\Model;

use Zend\InputFilter\Factory as InputFactory;
use Zend\InputFilter\InputFilter;
use Wepo\Lib\WepoMongoModel;

class MainUser extends WepoMongoModel
{
    public $adapterName = 'wepo_main';
    public $table_name  = 'user';

    const TABLE_NAME = 'user';

    public $_fields = [
        '_id'         => [ 'type' => 'pk', 'datatype' => 'string', 'default' => '' ],
        'company_id'  => [ 'type' => 'field', 'datatype' => 'string', 'default' => '' ],
        'role_id'     => [ 'type' => 'field', 'datatype' => 'string', 'default' => '' ],
        'status_id'   => [ 'type' => 'field', 'datatype' => 'string', 'default' => '' ],
        'created_dtm' => [ 'type' => 'field', 'datatype' => 'string', 'default' => '' ],
        'login'       => [ 'type' => 'field', 'datatype' => 'string', 'default' => '' ],
        'password'    => [ 'type' => 'field', 'datatype' => 'string', 'default' => '' ],
        'fname'       => [ 'type' => 'field', 'datatype' => 'string', 'default' => '' ],
        'lname'       => [ 'type' => 'field', 'datatype' => 'string', 'default' => '' ],
    ];
    protected $_unique = [ 'login' ];
    protected $inputFilter;

    public function getInputFilter()
    {
        if (!$this->inputFilter) {
            $inputFilter         = new InputFilter();
            $factory             = new InputFactory();
            $inputFilter->add($factory->createInput(array(
                    'name'       => 'login',
                    'required'   => true,
                    'filters'    => array(
                        array( 'name' => 'StripTags' ),
                        array( 'name' => 'StringTrim' ),
                    ),
                    'validators' => array(
                        array(
                            'name'    => 'StringLength',
                            'options' => array(
                                'encoding' => 'UTF-8',
                                'min'      => 3,
                                'max'      => 100,
                            ),
                        ),
                    ),
            )));
            $inputFilter->add($factory->createInput(array(
                    'name'       => 'password',
                    'required'   => true,
                    'filters'    => array(
                        array( 'name' => 'StripTags' ),
                        array( 'name' => 'StringTrim' ),
                    ),
                    'validators' => array(
                        array(
                            'name'    => 'StringLength',
                            'options' => array(
                                'encoding' => 'UTF-8',
                                'min'      => 3,
                                'max'      => 100,
                            ),
                        ),
                    ),
            )));
            $inputFilter->add($factory->createInput(array(
                    'name'       => 'password2',
                    'required'   => true,
                    'filters'    => array(
                        array( 'name' => 'StripTags' ),
                        array( 'name' => 'StringTrim' ),
                    ),
                    'validators' => array(
                        array(
                            'name'    => 'Wepo\Form\Validator\Confirm',
                            'options' => array(
                                'field' => 'password',
                            ),
                        ),
                    ),
            )));
            $inputFilter->add($factory->createInput(array(
                    'name'       => 'fname',
                    'required'   => true,
                    'filters'    => array(
                        array( 'name' => 'StripTags' ),
                        array( 'name' => 'StringTrim' ),
                    ),
                    'validators' => array(
                        array(
                            'name'    => 'StringLength',
                            'options' => array(
                                'encoding' => 'UTF-8',
                                'min'      => 1,
                                'max'      => 100,
                            ),
                        ),
                    ),
            )));
            $inputFilter->add($factory->createInput(array(
                    'name'       => 'lname',
                    'required'   => true,
                    'filters'    => array(
                        array( 'name' => 'StripTags' ),
                        array( 'name' => 'StringTrim' ),
                    ),
                    'validators' => array(
                        array(
                            'name'    => 'StringLength',
                            'options' => array(
                                'encoding' => 'UTF-8',
                                'min'      => 1,
                                'max'      => 100,
                            ),
                        ),
                    ),
            )));
            $this->inputFilter = $inputFilter;
        }

        return $this->inputFilter;
    }

    public function postExchange()
    {
        $this->password = md5($this->password);

        return parent::postExchange();
    }
}
