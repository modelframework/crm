<?php

namespace Wepo\Model;

use Zend\InputFilter\Factory as InputFactory;
use Zend\InputFilter\InputFilter;
use Wepo\Lib\WepoMongoModel;

class Table extends WepoMongoModel
{
    public $table_name = 'table';

//    const TABLE_NAME       = 'table';
    const USER             = "5295fdf7c5b9f222acd3c751";
    const LEAD             = "5295fdf7c5b9f222acd3c752";
    const PATIENT          = "5295fdf7c5b9f222acd3c753";
    const ACCOUNT          = "5295fdf7c5b9f222acd3c754";
    const PRODUCT          = "5295fdf7c5b9f222acd3c755";
    const EVENT_LOG        = "5295fdf7c5b9f222acd3c756";
    const CALL             = "5295fdf7c5b9f222acd3c757";
    const TASK             = "5295fdf7c5b9f222acd3c758";
    const EVENT            = "5295fdf7c5b9f222acd3c759";
    const ACTIVITY         = "5295fdf7c5b9f222acd3c75a";
    const DASHBOARD        = "5295fdf7c5b9f222acd3c75b";
    const ORDER            = "5295fdf7c5b9f222acd3c75c";
    const ORDER_DETAIL     = "5295fdf7c5b9f222acd3c75d";
    const INVOICE          = "5295fdf7c5b9f222acd3c75e";
    const INVOICE_DETAIL   = "5295fdf7c5b9f222acd3c75f";
    const PAYMENT          = "5295fdf7c5b9f222acd3c760";
    const MAIL_INFO        = "5295fdf7c5b9f222acd3c761";
    const MAIL_TEXT        = "5295fdf7c5b9f222acd3c762";
    const QUOTE            = "5295fdf7c5b9f222acd3c763";
    const QUOTE_DETAIL     = "5295fdf7c5b9f222acd3c764";
    const PRICEBOOK        = "52f4a9cc3d1f38d40a000029";
    const PRICEBOOK_DETAIL = "52f4a9f43d1f38d40a00002a";
    const DOCUMENT         = "5304cdfcbe3ac9e409000000";
    const MAIL_SETTING     = "5319cb6e770500e87144f7bf";
    const MAIL_RECEIVE_SETTING     = "5396ff71467597f24e5294e1";
    const MAIL_SEND_SETTING     = "5396ff65467597f24e5294e0";
    const MAIL             = "533048e25e7ebb408c3e55e4";
    const MAIL_CHAIN       = "536902927a35d0c6f47fecd3";
    const EMAIL       = "53a193358fda17212fef95ea";
    const WIDGET       = "53b42879bfe8cd6837d767c3";
    const NOTE       = "5432746cd928ed89b4a4a440";
    const DOCTOR       = "543cf42fd928ed89b4a4a44b";

    protected static $_transports = [
        "5295fdf7c5b9f222acd3c751" => 'User',
        "5295fdf7c5b9f222acd3c752" => 'Lead',
        "5295fdf7c5b9f222acd3c753" => 'Patient',
        "5295fdf7c5b9f222acd3c754" => 'Account',
        "5295fdf7c5b9f222acd3c755" => 'Product',
        "5295fdf7c5b9f222acd3c756" => 'EventLog',
        "5295fdf7c5b9f222acd3c757" => 'Call',
        "5295fdf7c5b9f222acd3c758" => 'Task',
        "5295fdf7c5b9f222acd3c759" => 'Event',
        "5295fdf7c5b9f222acd3c75a" => 'Activity',
        "5295fdf7c5b9f222acd3c75b" => 'Dashboard',
        "5295fdf7c5b9f222acd3c75c" => 'Order',
        "5295fdf7c5b9f222acd3c75d" => 'OrderDetail',
        "5295fdf7c5b9f222acd3c75e" => 'Invoice',
        "5295fdf7c5b9f222acd3c75f" => 'InvoiceDetail',
        "5295fdf7c5b9f222acd3c760" => 'Payment',
        "5295fdf7c5b9f222acd3c761" => 'MailInfo',
        "5295fdf7c5b9f222acd3c762" => 'MailText',
        "5295fdf7c5b9f222acd3c763" => 'Quote',
        "5295fdf7c5b9f222acd3c764" => 'QuoteDetail',
        "52f4a9cc3d1f38d40a000029" => 'Pricebook',
        "52f4a9f43d1f38d40a00002a" => 'PricebookDetail',
        "5304cdfcbe3ac9e409000000" => 'Document',
        "5319cb6e770500e87144f7bf" => 'MailSetting',
        "5396ff65467597f24e5294e0" => 'MailSendSetting',
        "5396ff71467597f24e5294e1" => 'MailReceiveSetting',
        "533048e25e7ebb408c3e55e4" => 'Mail',
        "536902927a35d0c6f47fecd3" => 'MailChain',
        "53a193358fda17212fef95ea" => 'Email',
        "53b42879bfe8cd6837d767c3" => 'Widget',
        "5432746cd928ed89b4a4a440" => 'Note',
        "543cf42fd928ed89b4a4a44b" => 'Doctor',
    ];
    protected $_fields            = [
        '_id'   => [ 'type' => 'pk', 'datatype' => 'string', 'default' => '' ],
        'id'    => [ 'type' => 'field', 'datatype' => 'int', 'default' => 0 ],
        'table' => [ 'type' => 'field', 'datatype' => 'string', 'default' => '' ],
        'label' => [ 'type' => 'field', 'datatype' => 'string', 'default' => '' ],
        'rows'  => [ 'type' => 'field', 'datatype' => 'array', 'default' => [] ],
//        'rows'  => [ 'type' => 'field', 'datatype' => 'string', 'default' => '' ]
    ];
    protected $inputFilter;

    public function getInputFilter()
    {
        if (!$this->inputFilter) {
            $inputFilter         = new InputFilter();
            $factory             = new InputFactory();
            $inputFilter->add(
                $factory->createInput(array(
                    'name'       => 'id',
                    'required'   => true,
                    'filters'    => array(
                        array( 'name' => 'Int' ),
                    ),
                    'validators' => array(
                        array(
                            'name'    => 'Digits',
                            'options' => array(),
                        ),
                        array(
                            'name'    => 'GreaterThan',
                            'options' => array(
                                'min' => 0,
                            ),
                        ),
                    ),
            )));
            $this->inputFilter = $inputFilter;
        }

        return $this->inputFilter;
    }

    public static function getTransportName($id)
    {
        return isset(self::$_transports[ (string) $id ]) ? self::$_transports[ (string) $id ] : null;
    }

    public static function getTableId($modelname)
    {
        $modelname = ucfirst($modelname);
        foreach (self::$_transports as $_key => $_value) {
            if ($modelname == $_value) {
                return $_key;
            }
        }
        throw new \Exception();
    }
}
