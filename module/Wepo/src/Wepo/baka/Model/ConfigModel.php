<?php

namespace Wepo\Model;

use Wepo\Lib\WepoMongoModel;

class ConfigModel extends WepoMongoModel
{
    public $adapterName = 'wepo_company';
    public $table_name  = 'config_model';

    const TABLE_NAME = 'config_model';

    protected $_fields = [
        '_id'     => [ 'type' => 'pk', 'datatype' => 'string', 'default' => '' ],
        'model'   => [ 'type' => 'field', 'datatype' => 'string', 'default' => '' ],
        'fields'  => [ 'type' => 'field', 'datatype' => 'array', 'default' => [ ] ],
        'joins'   => [ 'type' => 'field', 'datatype' => 'array', 'default' => [ ] ],
        'unique'  => [ 'type' => 'field', 'datatype' => 'array', 'default' => [ ] ],
        'filters' => [ 'type' => 'field', 'datatype' => 'array', 'default' => [ ] ],
        'adapter' => [ 'type' => 'field', 'datatype' => 'string', 'default' => '' ],
        'table'   => [ 'type' => 'field', 'datatype' => 'string', 'default' => '' ],
//        'gateway' => [ 'type' => 'field', 'datatype' => 'string', 'default' => '' ],
    ];
    public $_id        = '';
    public $model      = '';
    public $fields     = '';
    public $joins      = '';
    public $unique     = '';
    public $filters    = '';
    public $adapter    = '';
    public $table      = '';
//    public $gateway    = '';
}
