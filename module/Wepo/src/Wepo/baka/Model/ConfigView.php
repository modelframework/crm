<?php

namespace Wepo\Model;

use Wepo\Lib\WepoMongoModel;

class ConfigView extends WepoMongoModel
{
    public $adapterName = 'wepo_company';
    public $table_name  = 'ConfigView';
    protected $_fields  = [
        '_id'    => [ 'type' => 'pk', 'datatype' => 'string', 'default' => '' ],
        'model'  => [ 'type' => 'field', 'datatype' => 'string', 'default' => '' ],
        'view'   => [ 'type' => 'field', 'datatype' => 'string', 'default' => '' ],
        'fields' => [ 'type' => 'field', 'datatype' => 'array', 'default' => [ ] ],
        'rows'   => [ 'type' => 'field', 'datatype' => 'int', 'default' => 10 ],
    ];
}
