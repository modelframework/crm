<?php

namespace Wepo\Model;

use Zend\InputFilter\Factory as InputFactory;
use Zend\InputFilter\InputFilter;
use Wepo\Lib\WepoMongoModel;

class MainCompany extends WepoMongoModel
{
    public $adapterName = 'wepo_main';
    public $table_name  = 'company';

    const TABLE_NAME = 'company';

    public $_fields = [
        '_id'         => [ 'type' => 'pk', 'datatype' => 'string', 'default' => '' ],
        'status_id'   => [ 'type' => 'field', 'datatype' => 'string', 'default' => '' ],
        'created_dtm' => [ 'type' => 'field', 'datatype' => 'string', 'default' => '' ],
        'company'     => [ 'type' => 'field', 'datatype' => 'string', 'default' => '' ],
    ];

    public function getInputFilter()
    {
        if (!$this->inputFilter) {
            $inputFilter         = new InputFilter();
            $factory             = new InputFactory();
            $inputFilter->add($factory->createInput(array(
                    'name'       => 'company',
                    'required'   => true,
                    'filters'    => array(
                        array( 'name' => 'StripTags' ),
                        array( 'name' => 'StringTrim' ),
                    ),
                    'validators' => array(
                        array(
                            'name'    => 'StringLength',
                            'options' => array(
                                'encoding' => 'UTF-8',
                                'min'      => 3,
                                'max'      => 100,
                            ),
                        ),
                    ),
            )));
            $this->inputFilter = $inputFilter;
        }

        return $this->inputFilter;
    }
}
