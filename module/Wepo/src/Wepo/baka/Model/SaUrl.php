<?php

namespace Wepo\Model;

use Zend\InputFilter\Factory as InputFactory;
use Zend\InputFilter\InputFilter;
use Wepo\Lib\WepoMongoModel;

class SaUrl extends WepoMongoModel
{
    //    public $table_name = 'sa_url';
//
//    const TABLE_NAME = 'sa_url';

    protected $_fields = [
        '_id'   => [ 'type' => 'pk', 'datatype' => 'string', 'default' => '' ],
        'id'    => [ 'type'     => 'field', 'datatype'    => 'int', 'default' => 0 ],
        'label' => [ 'type'     => 'field', 'datatype'    => 'string', 'default' => '' ],
        'url'   => [ 'type'     => 'field', 'datatype'    => 'string', 'default' => '' ],
    ];
    protected $inputFilter;

    public function getInputFilter()
    {
        if (!$this->inputFilter) {
            $inputFilter = new InputFilter();
            $factory     = new InputFactory();

            $inputFilter->add($factory->createInput(array(
                    'name'       => 'id',
                    'required'   => true,
                    'filters'    => array(
                        array( 'name' => 'Int' ),
                    ),
                    'validators' => array(
                        array(
                            'name'    => 'Digits',
                            'options' => array(
                            ),
                        ),
//                        array(
//                            'name'    => 'InArray',
//                            'options' => array(
//                                'hastack' => self::ids(),
//                            ),
//                        ),
                    ),
            )));

            $inputFilter->add($factory->createInput(array(
                    'name'       => 'label',
                    'required'   => true,
                    'filters'    => array(
                        array( 'name' => 'StripTags' ),
                        array( 'name' => 'StringTrim' ),
                    ),
                    'validators' => array(
                        array(
                            'name'    => 'StringLength',
                            'options' => array(
                                'encoding' => 'UTF-8',
                                'min'      => 1,
                                'max'      => 50,
                            ),
                        ),
                    ),
            )));

            $inputFilter->add($factory->createInput(array(
                    'name'       => 'url',
                    'required'   => true,
                    'filters'    => array(
                        array( 'name' => 'StripTags' ),
                        array( 'name' => 'StringTrim' ),
                    ),
                    'validators' => array(
                        array(
                            'name'    => 'StringLength',
                            'options' => array(
                                'encoding' => 'UTF-8',
                                'min'      => 1,
                                'max'      => 50,
                            ),
                        ),
                    ),
            )));
            $this->inputFilter = $inputFilter;
        }

        return $this->inputFilter;
    }
}
