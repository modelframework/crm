<?php

namespace Wepo\Model;

use Zend\InputFilter\Factory as InputFactory;
use Zend\InputFilter\InputFilter;
use Wepo\Lib\WepoMongoModel;

class Quote extends WepoMongoModel
{
    public $table_name = 'quote';

    const TABLE_NAME = 'quote';

    protected $_fields = [
        '_id'          => [ 'type' => 'pk', 'datatype' => 'string', 'default' => 0 ],
        'owner_id'     => [ 'type' => 'source', 'datatype' => 'string', 'default' => 0, 'alias' => 'owner' ],
        'contact'      => [ 'type' => 'field', 'datatype' => 'string', 'default' => '' ],
        'contact_id'   => [ 'type' => 'source', 'datatype' => 'string', 'default' => 0, 'alias' => 'contact' ],
        'pricebook_id' => [ 'type' => 'source', 'datatype' => 'string', 'default' => 0, 'alias' => 'pricebook' ],
        'subject'      => [ 'type' => 'field', 'datatype' => 'string', 'default' => '' ],
        'description'  => [ 'type' => 'field', 'datatype' => 'string', 'default' => '' ],

    //product price formation fields
        'raw_price'    => [ 'type' => 'field', 'datatype' => 'string', 'default' => 0 ],

        'price_type'   => [ 'type' => 'field', 'datatype' => 'string', 'default' => '' ],
        'modifier'     => [ 'type' => 'field', 'datatype' => 'string', 'default' => 0 ],

        //tax fields
        'sales_tax'    => [ 'type' => 'field', 'datatype' => 'string', 'default' => 0 ],
        'vat'          => [ 'type' => 'field', 'datatype' => 'string', 'default' => 0 ],

        'tax'          => [ 'type' => 'field', 'datatype' => 'string', 'default' => 0 ],
        //tax fields
        'adjustment'   => [ 'type' => 'field', 'datatype' => 'string', 'default' => 0 ],

        'total_price'  => [ 'type' => 'field', 'datatype' => 'string', 'default' => 0 ],
    //product price formation fields

        'status_id'    => [ 'type' => 'source', 'datatype' => 'string', 'default' => 0, 'alias' => 'status' ],
        'created_dtm'  => [ 'type' => 'field', 'datatype' => 'string', 'default' => '' ],
        'changer_id'   => [ 'type' => 'source', 'datatype' => 'string', 'default' => 0, 'alias' => 'changer' ],
        'changed_dtm'  => [ 'type' => 'field', 'datatype' => 'string', 'default' => '' ],
        'owner'        => [ 'type' => 'alias', 'datatype' => 'string', 'default' => '', 'source' => 'owner_id' ],
        'contactfname' => [ 'type' => 'alias', 'datatype' => 'string', 'default' => '', 'source' => 'contact_id' ],
        'contactlname' => [ 'type' => 'alias', 'datatype' => 'string', 'default' => '', 'source' => 'contact_id' ],
        'changer'      => [ 'type' => 'alias', 'datatype' => 'string', 'default' => '', 'source' => 'changer_id' ],
        'status'       => [ 'type' => 'alias', 'datatype' => 'string', 'default' => '', 'source' => 'status_id' ],
        'pricebook'    => [ 'type' => 'alias', 'datatype' => 'string', 'default' => '', 'source' => 'pricebook_id' ],
        'ownerfname'   => [ 'type' => 'alias', 'datatype' => 'string', 'default' => '', 'source' => 'owner_id' ],
        'ownerlname'   => [ 'type' => 'alias', 'datatype' => 'string', 'default' => '', 'source' => 'owner_id' ],
        'changerfname' => [ 'type' => 'alias', 'datatype' => 'string', 'default' => '', 'source' => 'changer_id' ],
        'changerlname' => [ 'type' => 'alias', 'datatype' => 'string', 'default' => '', 'source' => 'changer_id' ],
        'title'        => [ 'type' => 'alias', 'datatype' => 'string', 'default' => '' ],
    ];
    protected $_joins = [
        [ 'model'  => 'User', 'on' => [ 'owner_id' => '_id' ],
          'fields' => [ 'owner' => 'login', 'ownerfname' => 'fname', 'ownerlname' => 'lname' ],
        ],
        [ 'model' => 'Contact', 'on' => [ 'contact_id' => '_id' ],
          'fields' => [ /*'contact' => 'lname',*/'contactfname' => 'fname','contactlname' => 'lname' ] ],
        [ 'model'  => 'User', 'on' => [ 'changer_id' => '_id' ],
          'fields' => [ 'changer' => 'login', 'changerfname' => 'fname', 'changerlname' => 'lname' ]
        ],
        [ 'model' => 'Status', 'on' => [ 'status_id' => '_id' ], 'fields' => [ 'status' => 'status' ] ],
        [ 'model' => 'Pricebook', 'on' => [ 'pricebook_id' => '_id' ], 'fields' => [ 'pricebook' => 'pricebook' ] ],
//        [ 'model' => 'Quote', 'on' => [ 'quote_id' => '_id' ], 'fields' => [ 'quote' => 'subject' ] ]
    ];
    protected $inputFilter;

    public function getInputFilter()
    {
        if (!$this->inputFilter) {
            $inputFilter = new InputFilter();
            $factory     = new InputFactory();
            $inputFilter->add($factory->createInput(array(
                                                          'name'     => 'owner_id',
                                                          'required' => false,
                                                          'filters'  => array(
                                                              array( 'name' => 'StripTags' ),
                                                              array( 'name' => 'StringTrim' ),
                                                          ),
                                                      )));
            $inputFilter->add($factory->createInput(array(
                                                          'name'       => 'contact_id',
                                                          'required'   => true,
                                                          'filters'    => array(
                                                              array( 'name' => 'StripTags' ),
                                                              array( 'name' => 'StringTrim' ),
                                                          ),
//                                                          'validators' => array(
//                                                              array(
//                                                                  'name'    => 'GreaterThan',
//                                                                  'options' => array(
//                                                                      'min'       => 1,
//                                                                      'inclusive' => true,
//                                                                      'messages'  => array( \Zend\Validator\GreaterThan::NOT_GREATER_INCLUSIVE => 'select item' ),
//                                                                  )
//                                                              )
//                                                          ),
                                                      )));
            $inputFilter->add($factory->createInput(array(
                                                          'name'       => 'subject',
                                                          'required'   => false,
                                                          'filters'    => array(
                                                              array( 'name' => 'StripTags' ),
                                                              array( 'name' => 'StringTrim' ),
                                                          ),
                                                          'validators' => array(
                                                              array(
                                                                  'name'    => 'StringLength',
                                                                  'options' => array(
                                                                      'encoding' => 'UTF-8',
                                                                      'min'      => 3,
                                                                      'max'      => 100,
                                                                  ),
                                                              ),
                                                          ),
                                                      )));
            $inputFilter->add($factory->createInput(array(
                                                          'name'       => 'description',
                                                          'required'   => false,
                                                          'filters'    => array(
                                                              array( 'name' => 'StripTags' ),
                                                              array( 'name' => 'StringTrim' ),
                                                          ),
                                                          'validators' => array(
                                                              array(
                                                                  'name'    => 'StringLength',
                                                                  'options' => array(
                                                                      'encoding' => 'UTF-8',
                                                                      'min'      => 3,
                                                                      'max'      => 100,
                                                                  ),
                                                              ),
                                                          ),
                                                      )));
            $inputFilter->add($factory->createInput(array(
                                                          'name'     => 'pricebook_id',
                                                          'required' => false,
                                                          'filters'  => array(
                                                              array( 'name' => 'StripTags' ),
                                                              array( 'name' => 'StringTrim' ),
                                                          ),
                                                      )));
            $inputFilter->add($factory->createInput(array(
                                                          'name'     => 'price_type',
                                                          'required' => true,
                                                      )));
            $inputFilter->add($factory->createInput(array(
                                                          'name'       => 'modifier',
                                                          'required'   => true,
                                                          'validators' => array(
                                                              array(
                                                                  'name'    => 'GreaterThan',
                                                                  'options' => array(
                                                                      'min'       => 0,
                                                                      'inclusive' => true,
                                                                  ),
                                                              ),
                                                          ),
                                                      )));
            $inputFilter->add($factory->createInput(array(
                                                            'name'       => 'sales_tax',
                                                            'required'   => true,
                                                            'validators' => array(
//                                                                array(
//                                                                    'name'    => 'GreaterThan',
//                                                                    'options' => array(
//                                                                        'min'       => 0,
//                                                                        'inclusive' => true,
//                                                                    )
//                                                                )
                                                                array(
                                                                    'name' => 'Between',
                                                                    'options' => [
                                                                        'min' => 0,
                                                                        'max' => 100,
                                                                    ],
                                                                ),
                                                            ),
                                                        )));
            $inputFilter->add($factory->createInput(array(
                                                            'name'       => 'vat',
                                                            'required'   => true,
                                                            'validators' => array(
//                                                                array(
//                                                                    'name'    => 'GreaterThan',
//                                                                    'options' => array(
//                                                                        'min'       => 0,
//                                                                        'inclusive' => true,
//                                                                    )
//                                                                )
                                                                array(
                                                                    'name' => 'Between',
                                                                    'options' => [
                                                                        'min' => 0,
                                                                        'max' => 100,
                                                                    ],
                                                                ),
                                                            ),
                                                        )));
            $inputFilter->add($factory->createInput(array(
                                                        'name'       => 'adjustment',
                                                        'required'   => true,
                                                        'validators' => array(
                                                            array(
                                                                'name' => 'Float',
                                                            ),
                                                        ),
                                                    )));
            $this->inputFilter = $inputFilter;
        }

        return $this->inputFilter;
    }

    public function getName()
    {
        return $this->subject;
    }

//    public function getModelLabel()
//    {
//        return 'Sales Order';
//    }
}
