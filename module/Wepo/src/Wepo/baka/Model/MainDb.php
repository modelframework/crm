<?php

namespace Wepo\Model;

use Wepo\Lib\WepoMongoModel;

class MainDb extends WepoMongoModel
{
    public $adapterName = 'wepo_main';
    public $table_name  = 'db';

    const TABLE_NAME = 'db';

    public $_fields = [
        '_id'            => [ 'type' => 'pk', 'datatype' => 'string', 'default' => '' ],
        'company_id'     => [ 'type' => 'field', 'datatype' => 'string', 'default' => '' ],
        'name'           => [ 'type' => 'field', 'datatype' => 'string', 'default' => '' ],
        'status_id'      => [ 'type' => 'field', 'datatype' => 'string', 'default' => '0' ],
        'driver'         => [ 'type' => 'field', 'datatype' => 'string', 'default' => '' ],
        'gateway'        => [ 'type' => 'field', 'datatype' => 'string', 'default' => '' ],
        'dsn'            => [ 'type' => 'field', 'datatype' => 'string', 'default' => '' ],
        'driver_options' => [ 'type' => 'field', 'datatype' => 'array', 'default' => [ ] ],
        'username'       => [ 'type' => 'field', 'datatype' => 'string', 'default' => '' ],
        'password'       => [ 'type' => 'field', 'datatype' => 'string', 'default' => '' ],
    ];

    public function getParamsArray()
    {
        return [
            'driver'         => $this->driver,
            'gateway'        => $this->gateway,
            'dsn'            => $this->dsn,
            'driver_options' => is_array($this->driver_options) ? $this->driver_options : unserialize($this->driver_options),
            'dbname'         => $this->name,
            'username'       => $this->username,
            'password'       => $this->password,
        ];
    }
}
