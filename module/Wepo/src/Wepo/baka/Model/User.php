<?php

namespace Wepo\Model;

use Zend\InputFilter\Factory as InputFactory;
use Zend\InputFilter\InputFilter;
use Wepo\Lib\WepoMongoModel;

class User extends WepoMongoModel
{
    public $table_name = 'user';
    public $adapterName = 'wepo_company';

    const TABLE_NAME = 'user';

    protected $_fields = [
        '_id'          => [ 'type' => 'pk', 'datatype' => 'string', 'default' => '' ],
        'main_id'      => [ 'type' => 'field', 'datatype' => 'string', 'default' => '' ],
        'company_id'   => [ 'type' => 'field', 'datatype' => 'string', 'default' => '' ],
        'flink'        => [ 'type' => 'source', 'datatype' => 'string', 'default' => '' ],
        'tlink'        => [ 'type' => 'source', 'datatype' => 'string', 'default' => '' ],
        'llink'        => [ 'type' => 'source', 'datatype' => 'string', 'default' => '' ],
        'fname'        => [ 'type' => 'field', 'datatype' => 'string', 'default' => '' ],
        'lname'        => [ 'type' => 'field', 'datatype' => 'string', 'default' => '' ],
        'role_id'      => [ 'type' => 'source', 'datatype' => 'string', 'default' => '', 'alias' => 'role' ],
        'login'        => [ 'type' => 'field', 'datatype' => 'string', 'default' => '' ],
        'password'     => [ 'type' => 'field', 'datatype' => 'string', 'default' => '' ],
        'ip'           => [ 'type' => 'field', 'datatype' => 'string', 'default' => '' ],
        'birth_date'   => [ 'type' => 'field', 'datatype' => 'string', 'default' => '' ],
        'changer_id'   => [ 'type' => 'source', 'datatype' => 'string', 'default' => '', 'alias' => 'changer' ],
        'changed_dtm'  => [ 'type' => 'field', 'datatype' => 'string', 'default' => '' ],
        'created_dtm'  => [ 'type' => 'field', 'datatype' => 'string', 'default' => '' ],
        'avatar'       => [ 'type' => 'field', 'datatype' => 'string', 'default' => 'user.jpg' ],
        'status_id'    => [ 'type' => 'source', 'datatype' => 'string', 'default' => '', 'alias' => 'status' ],
        'role'         => [ 'type' => 'alias', 'datatype' => 'string', 'default' => '', 'source' => 'role_id' ],
        'changer'      => [ 'type' => 'alias', 'datatype' => 'string', 'default' => '', 'source' => 'changer_id' ],
        'changerfname' => [ 'type' => 'alias', 'datatype' => 'string', 'default' => '', 'source' => 'changer_id' ],
        'changerlname' => [ 'type' => 'alias', 'datatype' => 'string', 'default' => '', 'source' => 'changer_id' ],
        'title'        => [ 'type' => 'alias', 'datatype' => 'string', 'default' => '' ],
        'theme'        => [ 'type' => 'field', 'datatype' => 'string', 'default' => '' ],

        'status'       => [ 'type' => 'alias', 'datatype' => 'string', 'default' => '', 'source' => 'status_id' ],

        'country'      => [ 'type' => 'address', 'datatype' => 'string', 'default' => '' ],
        'zip'          => [ 'type' => 'address', 'datatype' => 'string', 'default' => '' ],
        'state'        => [ 'type' => 'address', 'datatype' => 'string', 'default' => '' ],
        'city'         => [ 'type' => 'address', 'datatype' => 'string', 'default' => '' ],
        'address'      => [ 'type' => 'address', 'datatype' => 'string', 'default' => '' ],
        'description'  => [ 'type' => 'description', 'datatype' => 'string', 'default' => '' ],

        'newitems'     => [
            'type'    => 'alias', 'datatype' => 'array',
            'default' => [
                'Lead'    => 0,
                'Contact' => 0,
                'Product' => 0,
            ],
        ],
    ];
    protected $_joins = [
        [ 'model' => 'Role', 'on' => [ 'role_id' => '_id' ], 'fields' => [ 'role' => 'role' ] ],
        [
            'model'  => 'User', 'on' => [ 'changer_id' => '_id' ],
            'fields' => [ 'changer' => 'login', 'changerfname' => 'fname', 'changerlname' => 'lname' ]
        ],
        [ 'model' => 'Status', 'on' => [ 'status_id' => '_id' ], 'fields' => [ 'status' => 'status' ] ],
    ];
    public $_unique = [ 'login' ];

    public function getInputFilter()
    {
        if (!$this->inputFilter) {
            $inputFilter = new InputFilter();
            $factory     = new InputFactory();
            $inputFilter->add($factory->createInput(array(
                                                          'name'     => 'id',
                                                          'required' => true,
                                                          'filters'  => array(
                                                              array( 'name' => 'Int' ),
                                                          ),
                                                      )));
            $inputFilter->add($factory->createInput(array(
                                                          'name'       => 'role_id',
                                                          'required'   => false,
                                                          'filters'    => array(
                                                              array( 'name' => 'StripTags' ),
                                                              array( 'name' => 'StringTrim' ),
                                                          ),
                                                          'validators' => array(
                                                              array(
                                                                  'name'    => 'StringLength',
                                                                  'options' => array(
                                                                      'encoding' => 'UTF-8',
                                                                      'min'      => 24,
                                                                      'max'      => 24,
                                                                  ),
                                                              ),
                                                          ),
                                                      )));
            $inputFilter->add($factory->createInput(array(
                                                          'name'       => 'flink',
                                                          'required'   => false,
                                                          'filters'    => array(
                                                              array( 'name' => 'StripTags' ),
                                                              array( 'name' => 'StringTrim' ),
                                                          ),
                                                          'validators' => array(
                                                              array(
                                                                  'name'    => 'StringLength',
                                                                  'options' => array(
                                                                      'encoding' => 'UTF-8',
                                                                      'min'      => 3,
                                                                      'max'      => 100,
                                                                  ),
                                                              ),
                                                          ),
                                                      )));
            $inputFilter->add($factory->createInput(array(
                                                          'name'       => 'tlink',
                                                          'required'   => false,
                                                          'filters'    => array(
                                                              array( 'name' => 'StripTags' ),
                                                              array( 'name' => 'StringTrim' ),
                                                          ),
                                                          'validators' => array(
                                                              array(
                                                                  'name'    => 'StringLength',
                                                                  'options' => array(
                                                                      'encoding' => 'UTF-8',
                                                                      'min'      => 3,
                                                                      'max'      => 100,
                                                                  ),
                                                              ),
                                                          ),
                                                      )));
            $inputFilter->add($factory->createInput(array(
                                                          'name'       => 'llink',
                                                          'required'   => false,
                                                          'filters'    => array(
                                                              array( 'name' => 'StripTags' ),
                                                              array( 'name' => 'StringTrim' ),
                                                          ),
                                                          'validators' => array(
                                                              array(
                                                                  'name'    => 'StringLength',
                                                                  'options' => array(
                                                                      'encoding' => 'UTF-8',
                                                                      'min'      => 3,
                                                                      'max'      => 100,
                                                                  ),
                                                              ),
                                                          ),
                                                      )));
            $inputFilter->add($factory->createInput(array(
                                                          'name'       => 'fname',
                                                          'required'   => true,
                                                          'filters'    => array(
                                                              array( 'name' => 'StripTags' ),
                                                              array( 'name' => 'StringTrim' ),
                                                          ),
                                                          'validators' => array(
                                                              array(
                                                                  'name'    => 'StringLength',
                                                                  'options' => array(
                                                                      'encoding' => 'UTF-8',
                                                                      'min'      => 1,
                                                                      'max'      => 100,
                                                                  ),
                                                              ),
                                                          ),
                                                      )));
            $inputFilter->add($factory->createInput(array(
                                                          'name'       => 'lname',
                                                          'required'   => true,
                                                          'filters'    => array(
                                                              array( 'name' => 'StripTags' ),
                                                              array( 'name' => 'StringTrim' ),
                                                          ),
                                                          'validators' => array(
                                                              array(
                                                                  'name'    => 'StringLength',
                                                                  'options' => array(
                                                                      'encoding' => 'UTF-8',
                                                                      'min'      => 1,
                                                                      'max'      => 100,
                                                                  ),
                                                              ),
                                                          ),
                                                      )));
            $inputFilter->add($factory->createInput(array(
                                                          'name'       => 'login',
                                                          'required'   => true,
                                                          'filters'    => array(
                                                              array( 'name' => 'StripTags' ),
                                                              array( 'name' => 'StringTrim' ),
                                                          ),
                                                          'validators' => array(
                                                              array(
                                                                  'name'    => 'StringLength',
                                                                  'options' => array(
                                                                      'encoding' => 'UTF-8',
                                                                      'min'      => 3,
                                                                      'max'      => 100,
                                                                  ),
                                                              ),
                                                          ),
                                                      )));
            $inputFilter->add($factory->createInput(array(
                                                          'name'       => 'password',
                                                          'required'   => true,
                                                          'filters'    => array(
                                                              array( 'name' => 'StripTags' ),
                                                              array( 'name' => 'StringTrim' ),
                                                          ),
                                                          'validators' => array(
                                                              array(
                                                                  'name'    => 'StringLength',
                                                                  'options' => array(
                                                                      'encoding' => 'UTF-8',
                                                                      'min'      => 3,
                                                                      'max'      => 100,
                                                                  ),
                                                              ),
                                                          ),
                                                      )));
            $inputFilter->add($factory->createInput(array(
                                                          'name'       => 'birth_date',
                                                          'required'   => false,
                                                          'filters'    => array(
                                                              array( 'name' => 'StripTags' ),
                                                              array( 'name' => 'StringTrim' ),
                                                          ),
                                                          'validators' => array(
                                                              array(
                                                                  'name'    => 'StringLength',
                                                                  'options' => array(
                                                                      'encoding' => 'UTF-8',
                                                                      'min'      => 10,
                                                                      'max'      => 20,
                                                                  ),
                                                              ),
                                                              array(
                                                                  'name'    => 'Date',
                                                                  'options' => array(
                                                                      'format' => 'Y-m-d',
                                                                  ),
                                                              ),
                                                              array(
                                                                  'name'    => 'Between',
                                                                  'options' => array(
                                                                      'min' => '1940-01-01',
                                                                      'max' => date('Y-m-d', time()),
                                                                  ),
                                                              ),
                                                          ),
                                                      )));
            $inputFilter->add($factory->createInput(array(
                                                          'name'       => 'country',
                                                          'required'   => false,
                                                          'filters'    => array(
                                                              array( 'name' => 'StripTags' ),
                                                              array( 'name' => 'StringTrim' ),
                                                          ),
                                                          'validators' => array(
                                                              array(
                                                                  'name'    => 'StringLength',
                                                                  'options' => array(
                                                                      'encoding' => 'UTF-8',
                                                                      'min'      => 1,
                                                                      'max'      => 100,
                                                                  ),
                                                              ),
                                                          ),
                                                      )));
            $inputFilter->add($factory->createInput(array(
                                                          'name'       => 'zip',
                                                          'required'   => false,
                                                          'filters'    => array(
                                                              array( 'name' => 'StripTags' ),
                                                              array( 'name' => 'StringTrim' ),
                                                          ),
                                                          'validators' => array(
                                                              array(
                                                                  'name'    => 'StringLength',
                                                                  'options' => array(
                                                                      'encoding' => 'UTF-8',
                                                                      'min'      => 1,
                                                                      'max'      => 100,
                                                                  ),
                                                              ),
                                                          ),
                                                      )));
            $inputFilter->add($factory->createInput(array(
                                                          'name'       => 'state',
                                                          'required'   => false,
                                                          'filters'    => array(
                                                              array( 'name' => 'StripTags' ),
                                                              array( 'name' => 'StringTrim' ),
                                                          ),
                                                          'validators' => array(
                                                              array(
                                                                  'name'    => 'StringLength',
                                                                  'options' => array(
                                                                      'encoding' => 'UTF-8',
                                                                      'min'      => 1,
                                                                      'max'      => 100,
                                                                  ),
                                                              ),
                                                          ),
                                                      )));
            $inputFilter->add($factory->createInput(array(
                                                          'name'       => 'city',
                                                          'required'   => false,
                                                          'filters'    => array(
                                                              array( 'name' => 'StripTags' ),
                                                              array( 'name' => 'StringTrim' ),
                                                          ),
                                                          'validators' => array(
                                                              array(
                                                                  'name'    => 'StringLength',
                                                                  'options' => array(
                                                                      'encoding' => 'UTF-8',
                                                                      'min'      => 1,
                                                                      'max'      => 100,
                                                                  ),
                                                              ),
                                                          ),
                                                      )));
            $inputFilter->add($factory->createInput(array(
                                                          'name'       => 'address',
                                                          'required'   => false,
                                                          'filters'    => array(
                                                              array( 'name' => 'StripTags' ),
                                                              array( 'name' => 'StringTrim' ),
                                                          ),
                                                          'validators' => array(
                                                              array(
                                                                  'name'    => 'StringLength',
                                                                  'options' => array(
                                                                      'encoding' => 'UTF-8',
                                                                      'min'      => 1,
                                                                      'max'      => 100,
                                                                  ),
                                                              ),
                                                          ),
                                                      )));
            $inputFilter->add($factory->createInput(array(
                                                          'name'       => 'description',
                                                          'required'   => false,
                                                          'filters'    => array(
                                                              array( 'name' => 'StripTags' ),
                                                              array( 'name' => 'StringTrim' ),
                                                          ),
                                                          'validators' => array(
                                                              array(
                                                                  'name'    => 'StringLength',
                                                                  'options' => array(
                                                                      'encoding' => 'UTF-8',
                                                                      'min'      => 1,
                                                                      'max'      => 100,
                                                                  ),
                                                              ),
                                                          ),
                                                      )));
            $this->inputFilter = $inputFilter;
        }

        return $this->inputFilter;
    }

    public function getName()
    {
        return $this->login;
    }

    public function getRole()
    {
        return $this->role_id;
    }

    public function postExchange()
    {
        $this->password = md5($this->password);

        return parent::postExchange();
    }
}
