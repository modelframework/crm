<?php

namespace Wepo\Model;

use Zend\InputFilter\Factory as InputFactory;
use Zend\InputFilter\InputFilter;
use Wepo\Lib\WepoMongoModel;

class Doctor extends WepoMongoModel
{
    public $table_name = 'doctor';

    const TABLE_NAME = 'doctor';

    protected $_fields = [
        '_id' => [ 'type' => 'pk', 'datatype' => 'string', 'default' => ''],
        'owner_id' => [ 'type' => 'source', 'datatype' => 'string', 'default' => '', 'alias' => 'owner'],
        'changer_id' => ['type' => 'source', 'datatype' => 'string', 'default' => '', 'alias' => 'changer'],
        'name' => ['type' => 'field', 'datatype' => 'string', 'default' => ''],
        'dea' => ['type' => 'field', 'datatype' => 'string', 'default' => 'None'],
        'license' => ['type' => 'field', 'datatype' => 'string', 'default' => ''],
        'phone'        => [ 'type' => 'field', 'datatype' => 'string', 'default' => '' ],
        'fax'       => [ 'type' => 'field', 'datatype' => 'string', 'default' => '' ],
        'created_dtm' => [ 'type' => 'field', 'datatype' => 'string', 'default' => ''],
        'changed_dtm' => [ 'type' => 'field', 'datatype' => 'string', 'default' => ''],
        'status_id' => [ 'type' => 'source', 'datatype' => 'string', 'default' => '', 'alias' => 'status'],
        'status' => [ 'type' => 'alias', 'datatype' => 'string', 'default' => '', 'source' => 'status_id'],
        'owner' => ['type' => 'alias', 'datatype' => 'string', 'default' => '', 'source' => 'owner_id'],
        'changer' => ['type' => 'alias', 'datatype' => 'string', 'default' => '', 'source' => 'changer_id'],
        'ownerfname' => [ 'type' => 'alias', 'datatype' => 'string', 'default' => '', 'source' => 'owner_id'],
        'ownerlname' => [ 'type' => 'alias', 'datatype' => 'string', 'default' => '', 'source' => 'owner_id'],
        'changerfname' => [ 'type' => 'alias', 'datatype' => 'string', 'default' => '', 'source' => 'changer_id'],
        'changerlname' => [ 'type' => 'alias', 'datatype' => 'string', 'default' => '', 'source' => 'changer_id'],
        'country'      => [ 'type' => 'address', 'datatype' => 'string', 'default' => '' ],
        'zip'          => [ 'type' => 'address', 'datatype' => 'string', 'default' => '' ],
        'state'        => [ 'type' => 'address', 'datatype' => 'string', 'default' => '' ],
        'city'         => [ 'type' => 'address', 'datatype' => 'string', 'default' => '' ],
        'address'      => [ 'type' => 'address', 'datatype' => 'string', 'default' => '' ],
        'title' => ['type' => 'field', 'datatype' => 'string', 'default' => ''],
    ];
    protected $_joins = [
        [
            'model' => 'User', 'on' => [ 'owner_id' => '_id'], 'fields' => [ 'ownerfname' => 'fname', 'ownerlname' => 'lname'],
        ],
        [ 'model' => 'Status', 'on' => [ 'status_id' => '_id'], 'fields' => [ 'status' => 'status']],
        [
            'model' => 'User', 'on' => [ 'changer_id' => '_id'], 'fields' => [ 'changerfname' => 'fname', 'changerlname' => 'lname']
        ],
    ];
    protected $inputFilter;

    public function getInputFilter()
    {
        if (!$this->inputFilter) {
            $inputFilter = new InputFilter();
            $factory = new InputFactory();

            $inputFilter->add($factory->createInput(array(
                        'name' => 'owner_id',
                        'required' => true,
            )));

            $inputFilter->add($factory->createInput(array(
                        'name' => 'name',
                        'required' => true,
                        'validators' => array(
                            array(
                                'name' => 'StringLength',
                                'options' => array(
                                    'encoding' => 'UTF-8',
                                    'min' => 3,
                                    'max' => 100,
                                ),
                            ),
                        ),
            )));
            $inputFilter->add($factory->createInput(array(
                        'name' => 'dea',
                        'required' => false,
                        'validators' => array(
                            array(
                                'name' => 'StringLength',
                                'options' => array(
                                    'encoding' => 'UTF-8',
                                    'min' => 4,
                                    'max' => 15,
                                ),
                            ),
                        ),
            )));
            $inputFilter->add($factory->createInput(array(
                        'name' => 'license',
                        'required' => true,
                        'validators' => array(
                            array(
                                'name' => 'StringLength',
                                'options' => array(
                                    'encoding' => 'UTF-8',
                                    'min' => 3,
                                    'max' => 50,
                                ),
                            ),
                        ),
            )));
            $inputFilter->add($factory->createInput(array(
                                                          'name'       => 'phone',
                                                          'required'   => true,
                                                          'filters'    => array(
                                                              array( 'name' => 'StripTags' ),
                                                              array( 'name' => 'StringTrim' ),
                                                          ),
                                                          'validators' => array(
                                                              array(
                                                                  'name'    => 'StringLength',
                                                                  'options' => array(
                                                                      'encoding' => 'UTF-8',
                                                                      'min'      => 5,
                                                                      'max'      => 100,
                                                                  ),
                                                              ),
                                                          ),
                                                      )));
            $inputFilter->add($factory->createInput(array(
                                                          'name'       => 'fax',
                                                          'required'   => false,
                                                          'filters'    => array(
                                                              array( 'name' => 'StripTags' ),
                                                              array( 'name' => 'StringTrim' ),
                                                          ),
                                                          'validators' => array(
                                                              array(
                                                                  'name'    => 'StringLength',
                                                                  'options' => array(
                                                                      'encoding' => 'UTF-8',
                                                                      'min'      => 8,
                                                                      'max'      => 100,
                                                                  ),
                                                              ),
                                                          ),
                                                      )));
            $this->inputFilter = $inputFilter;
        }

        return $this->inputFilter;
    }

    public function getName()
    {
        return $this->name;
    }

    public function getModelLabel()
    {
        return 'Doctor';
    }
}
