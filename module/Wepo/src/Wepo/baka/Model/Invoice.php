<?php

namespace Wepo\Model;

use Zend\InputFilter\Factory as InputFactory;
use Zend\InputFilter\InputFilter;
use Wepo\Lib\WepoMongoModel;

class Invoice extends WepoMongoModel
{
    public $table_name = 'invoice';

    const TABLE_NAME = 'invoice';

    protected $_fields = [
        '_id'            => [ 'type' => 'pk', 'datatype' => 'string', 'default' => 0 ],
        'id'             => [ 'type' => 'field', 'datatype' => 'int', 'default' => 0 ],
        'order_id'       => [ 'type' => 'source', 'datatype' => 'string', 'default' => 0, 'alias' => 'order' ],
        'owner_id'       => [ 'type' => 'source', 'datatype' => 'string', 'default' => 0, 'alias' => 'owner' ],
        'contact_id'     => [ 'type' => 'source', 'datatype' => 'string', 'default' => 0, 'alias' => 'contact' ],
        'contact'        => [ 'type' => 'field', 'datatype' => 'string', 'default' => '' ],
        'subject'        => [ 'type' => 'field', 'datatype' => 'string', 'default' => '' ],
        'description'    => [ 'type' => 'field', 'datatype' => 'string', 'default' => '' ],
        'total_price'    => [ 'type' => 'field', 'datatype' => 'string', 'default' => 0 ],
        'raw_price'      => [ 'type' => 'field', 'datatype' => 'string', 'default' => 0 ],
        'total_discount' => [ 'type' => 'field', 'datatype' => 'string', 'default' => 0 ],
        'total_tax'      => [ 'type' => 'field', 'datatype' => 'string', 'default' => 0 ],
        'total_adjustment' => [ 'type' => 'field', 'datatype' => 'string', 'default' => 0 ],
        'status_id'      => [ 'type' => 'source', 'datatype' => 'string', 'default' => 0, 'alias' => 'status' ],
        'created_dtm'    => [ 'type' => 'field', 'datatype' => 'string', 'default' => '' ],
        'changer_id'     => [ 'type' => 'source', 'datatype' => 'string', 'default' => 0, 'alias' => 'changer' ],
        'changed_dtm'    => [ 'type' => 'field', 'datatype' => 'string', 'default' => '' ],
        'order'          => [ 'type' => 'alias', 'datatype' => 'string', 'default' => '', 'source' => 'order_id' ],
        'owner'          => [ 'type' => 'alias', 'datatype' => 'string', 'default' => '', 'source' => 'owner_id' ],
        'ownerfname'     => [ 'type' => 'alias', 'datatype' => 'string', 'default' => '', 'source' => 'owner_id' ],
        'ownerlname'     => [ 'type' => 'alias', 'datatype' => 'string', 'default' => '', 'source' => 'owner_id' ],
        'changerfname'   => [ 'type' => 'alias', 'datatype' => 'string', 'default' => '', 'source' => 'changer_id' ],
        'changerlname'   => [ 'type' => 'alias', 'datatype' => 'string', 'default' => '', 'source' => 'changer_id' ],
        'contactfname'   => [ 'type' => 'alias', 'datatype' => 'string', 'default' => '', 'source' => 'contact_id' ],
        'contactlname'   => [ 'type' => 'alias', 'datatype' => 'string', 'default' => '', 'source' => 'contact_id' ],
        'changer'        => [ 'type' => 'alias', 'datatype' => 'string', 'default' => '', 'source' => 'changer_id' ],
        'status'         => [ 'type' => 'alias', 'datatype' => 'string', 'default' => '', 'source' => 'status_id' ],
        'title'          => [ 'type' => 'alias', 'datatype' => 'string', 'default' => '' ],
    ];
    protected $_joins = [
        [ 'model' => 'Order', 'on' => [ 'order_id' => '_id' ], 'fields' => [ 'order' => 'subject' ] ],
        [
            'model'  => 'User', 'on' => [ 'owner_id' => '_id' ],
            'fields' => [ 'owner' => 'login', 'ownerfname' => 'fname', 'ownerlname' => 'lname' ]
        ],
        [ 'model' => 'Contact', 'on' => [ 'contact_id' => '_id' ], 'fields' => [ 'contactlname' => 'lname','contactfname' => 'fname' ] ],
        [
            'model'  => 'User', 'on' => [ 'changer_id' => '_id' ],
            'fields' => [ 'changer' => 'login', 'changerfname' => 'fname', 'changerlname' => 'lname' ]
        ],
        [ 'model' => 'Status', 'on' => [ 'status_id' => '_id' ], 'fields' => [ 'status' => 'status' ] ],
    ];
    protected $inputFilter;

    public function toInvoice($data)
    {
        $this->order_id    = (isset($data[ 'id' ])) ? $data[ 'id' ] : null;
        $this->owner_id    = (isset($data[ 'owner_id' ])) ? $data[ 'owner_id' ] : null;
        $this->contact_id  = (isset($data[ 'contact_id' ])) ? $data[ 'contact_id' ] : null;
        $this->subject     = (isset($data[ 'subject' ])) ? $data[ 'subject' ] : null;
        $this->description = (isset($data[ 'description' ])) ? $data[ 'description' ] : null;
        $this->total_price = (isset($data[ 'total_price' ])) ? $data[ 'total_price' ] : null;
        $this->status_id   = (isset($data[ 'status_id' ])) ? $data[ 'status_id' ] : null;
    }

    public function getInputFilter()
    {
        if (!$this->inputFilter) {
            $inputFilter = new InputFilter();
            $factory     = new InputFactory();
            $inputFilter->add($factory->createInput(array(
                                                          'name'       => 'owner_id',
                                                          'required'   => false,
                                                          'filters'    => array(
                                                              array( 'name' => 'StripTags' ),
                                                              array( 'name' => 'StringTrim' ),
                                                          ),
                                                          'validators' => array(
                                                              array(
                                                                  'name'    => 'StringLength',
                                                                  'options' => array(
                                                                      'encoding' => 'UTF-8',
                                                                      'min'      => 24,
                                                                      'max'      => 24,
                                                                  ),
                                                              ),
                                                          ),
                                                      )));
            $inputFilter->add($factory->createInput(array(
                                                          'name'       => 'contact_id',
                                                          'required'   => true,
                                                          'filters'    => array(
                                                              array( 'name' => 'StripTags' ),
                                                              array( 'name' => 'StringTrim' ),
                                                          ),
                                                          'validators' => array(
                                                              array(
                                                                  'name'    => 'StringLength',
                                                                  'options' => array(
                                                                      'encoding' => 'UTF-8',
                                                                      'min'      => 24,
                                                                      'max'      => 24,
                                                                  ),
                                                              ),
                                                          ),
                                                      )));
            $inputFilter->add($factory->createInput(array(
                                                          'name'       => 'subject',
                                                          'required'   => false,
                                                          'filters'    => array(
                                                              array( 'name' => 'StripTags' ),
                                                              array( 'name' => 'StringTrim' ),
                                                          ),
                                                          'validators' => array(
                                                              array(
                                                                  'name'    => 'StringLength',
                                                                  'options' => array(
                                                                      'encoding' => 'UTF-8',
                                                                      'min'      => 3,
                                                                      'max'      => 100,
                                                                  ),
                                                              ),
                                                          ),
                                                      )));
            $inputFilter->add($factory->createInput(array(
                                                          'name'       => 'description',
                                                          'required'   => false,
                                                          'filters'    => array(
                                                              array( 'name' => 'StripTags' ),
                                                              array( 'name' => 'StringTrim' ),
                                                          ),
                                                          'validators' => array(
                                                              array(
                                                                  'name'    => 'StringLength',
                                                                  'options' => array(
                                                                      'encoding' => 'UTF-8',
                                                                      'min'      => 3,
                                                                      'max'      => 100,
                                                                  ),
                                                              ),
                                                          ),
                                                      )));
            $inputFilter->add($factory->createInput(array(
                                                          'name'     => 'total_discount',
                                                          'required' => false,
                                                          'filters'  => array(
                                                              array( 'name' => 'Int' ),
                                                          ),
                                                      )));
            $inputFilter->add($factory->createInput(array(
                                                          'name'       => 'status_id',
                                                          'required'   => false,
                                                          'filters'    => array(
                                                              array( 'name' => 'StripTags' ),
                                                              array( 'name' => 'StringTrim' ),
                                                          ),
                                                          'validators' => array(
                                                              array(
                                                                  'name'    => 'StringLength',
                                                                  'options' => array(
                                                                      'encoding' => 'UTF-8',
                                                                      'min'      => 24,
                                                                      'max'      => 24,
                                                                  ),
                                                              ),
                                                          ),
                                                      )));
            $this->inputFilter = $inputFilter;
        }

        return $this->inputFilter;
    }

    public function getName()
    {
        return $this->subject;
    }
}
