<?php

namespace Wepo\Model;

use Zend\InputFilter\InputFilter;
use Wepo\Lib\WepoMongoModel;

class Widget extends WepoMongoModel
{
    public $table_name = 'widget';

    const TABLE_NAME = 'widget';

    protected $_fields = [
        '_id'               => [ 'type' => 'pk', 'datatype' => 'string', 'default' => 0 ],
        'name'              => [ 'type' => 'pk', 'datatype' => 'string', 'default' => 'new widget' ],
        'path'              => [ 'type' => 'field', 'datatype' => 'string', 'default' => 0 ],
        'data_model'        => [ 'type' => 'field', 'datatype' => 'string', 'default' => '' ],
        'action'            => [ 'type' => 'field', 'datatype' => 'array', 'default' => [] ],
        'model_link'        => [ 'type' => 'field', 'datatype' => 'array', 'default' => [] ],
        'where'             => [ 'type' => 'field', 'datatype' => 'array', 'default' => [] ],
        'order'             => [ 'type' => 'field', 'datatype' => 'array', 'default' => [] ],
        'limit'             => [ 'type' => 'field', 'datatype' => 'string', 'default' => 5 ],
        'output_order'      => [ 'type' => 'field', 'datatype' => 'string', 'default' => 0 ],

        'owner'             => [ 'type' => 'field', 'datatype' => 'string', 'default' => '', 'source' => 'owner_id' ],
        'owner_id'          => [ 'type' => 'source', 'datatype' => 'string', 'default' => '', 'alias' => 'owner' ],
    ];
    protected $_joins  = [
        [ 'model' => 'User', 'on' => [ 'owner_id' => '_id' ], 'fields' => [ 'owner' => 'login' ] ],
    ];
    protected $inputFilter;

    public function getName()
    {
        return $this->subject;
    }
}
