<?php

namespace Wepo\Model;

use Zend\InputFilter\Factory as InputFactory;
use Zend\InputFilter\InputFilter;
use Wepo\Lib\WepoMongoModel;

class EventType extends WepoMongoModel
{
    public $table_name = 'event_type';
    const TABLE_NAME = 'event_type';

    const ADD     = 1;
    const EDIT    = 2;
    const DELETE  = 3;
    const CONVERT = 4;
    const RESTORE = 5;
    const CLEAN   = 6;
    const PRODUCT = 7;
    const SEND    = 8;

    protected $_fields = [
        '_id'         => [ 'type' => 'pk', 'datatype' => 'string', 'default' => '' ],
        'id'          => [ 'type' => 'field', 'datatype' => 'int', 'default' => 0 ],
        'type' => [ 'type'     => 'field', 'datatype' => 'string', 'default'  => '' ],
    ];
    protected $inputFilter;

    public static function ids()
    {
        return array( self::NEW_, self::NORMAL, self::CONVERTED, self::DEAD );
    }

    public function getInputFilter()
    {
        if (!$this->inputFilter) {
            $inputFilter = new InputFilter();
            $factory     = new InputFactory();

            $inputFilter->add($factory->createInput(array(
                    'name'     => 'type',
                    'required' => true,
            )));

            $this->inputFilter = $inputFilter;
        }

        return $this->inputFilter;
    }
}
