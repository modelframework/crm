<?php

namespace Wepo\Model\Transport;

use Wepo\Lib\WepoMongoGateway;
use Wepo\Lib\WepoModel;

class Task00 extends WepoMongoGateway
{
    public function save(WepoModel $model)
    {
        $data = $model->toArray();
        $id   = (int) $model->id;
        if (!$this->isUnique($model)) {
            throw new \Exception('Data is not unique');

            return false;
        }
        $result = false;
        if ($this->find(['id' => $id ])->current()) {
            $result = $this->update($data, array( 'id' => $id ));
        } else {
            $data[ 'id' ] = $id;
            $result = $this->insert($data);
        }

        return $result;
    }
}
