<?php

namespace Wepo\Model;

use Zend\InputFilter\Factory as InputFactory;
use Zend\InputFilter\InputFilter;
use Wepo\Lib\WepoMongoModel;

class Document extends WepoMongoModel
{
    public $table_name = 'document';

    const TABLE_NAME = 'document';

    protected $_fields = [
        '_id'                  => [ 'type' => 'pk', 'datatype' => 'string', 'default' => '' ],
        //        'id'           => ['type' => 'pk', 'datatype' => 'int', 'default' => 0 ],
        'document_name'        => [ 'type' => 'field', 'datatype' => 'string', 'default' => '' ],
        'document_way'         => [ 'type' => 'field', 'datatype' => 'string', 'default' => '' ],
        'owner_id'             => [ 'type' => 'source', 'datatype' => 'string', 'default' => '', 'alias' => 'owner' ],
        'status_id'            => [ 'type' => 'source', 'datatype' => 'string', 'default' => '', 'alias' => 'status' ],
        'document_real_name'   => [ 'type' => 'field', 'datatype' => 'string', 'default' => '' ],
        'document_description' => [ 'type' => 'field', 'datatype' => 'string', 'default' => '' ],
        'document_extension'   => [ 'type' => 'field', 'datatype' => 'string', 'default' => '' ],
        'document_description' => [ 'type' => 'field', 'datatype' => 'string', 'default' => '' ],
        'file_size'            => [ 'type' => 'field', 'datatype' => 'string', 'default' => '' ],
        'created_dtm'          => [ 'type' => 'field', 'datatype' => 'string', 'default' => '' ],
        'changed_dtm'          => [ 'type' => 'field', 'datatype' => 'string', 'default' => '' ],
        'changer_id'           => [ 'type' => 'source', 'datatype' => 'string', 'default' => 0, 'alias' => 'changer' ],
        //        'table_id' => [ 'type' => 'source', 'datatype' => 'string', 'default' => '', 'alias' => 'table'],
        //        'target_id' => [ 'type' => 'source', 'datatype' => 'string', 'default' => '','alias' => 'target'],

        'attach_to'            => [
            'type'    => 'alias', 'datatype' => 'array', 'source' => '',
            'default' => [
                'Lead'    => [ ],
                'Contact' => [ ],
                'User'    => [ ],
            ],
        ],
        ////////////////////////////////////////////////////////////////////////////////////////////
        //        'target' => ['type' => 'alias', 'datatype' => 'string', 'default' => '', 'source' => 'target_id'],
        'attach_to_names'      => [
            'type'    => 'source', 'datatype' => 'array', 'alias' => '',
            'default' => [
                'Lead'    => [ ],
                'Patient' => [ ],
                'User'    => [ ],
            ],
        ],
        'owner'                => [
            'type' => 'alias', 'datatype' => 'string', 'default' => '', 'source' => 'owner_id',
        ],
        'ownerfname'           => [
            'type' => 'alias', 'datatype' => 'string', 'default' => '', 'source' => 'owner_id',
        ],
        'ownerlname'           => [
            'type' => 'alias', 'datatype' => 'string', 'default' => '', 'source' => 'owner_id',
        ],
        'status'               => [
            'type'   => 'alias', 'datatype' => 'string', 'default' => '',
            'source' => 'status_id',
        ],
        'changer'              => [
            'type'   => 'alias', 'datatype' => 'string', 'default' => '',
            'source' => 'changer_id',
        ],
        'changerfname'         => [
            'type'   => 'alias', 'datatype' => 'string', 'default' => '',
            'source' => 'changer_id',
        ],
        'changerlname'         => [
            'type'   => 'alias', 'datatype' => 'string', 'default' => '',
            'source' => 'changer_id',
        ],
        'title'                => [ 'type' => 'alias', 'datatype' => 'string', 'default' => '' ],
        //        'table' => [ 'type' => 'alias', 'datatype' => 'string', 'default' => '', 'source' => 'table_id']
    ];
    protected $_joins = [
        [
            'model' => 'User', 'on' => [ 'owner_id' => '_id' ], 'fields' => [ 'ownerfname' => 'fname', 'ownerlname' => 'lname' ],
        ],
        [ 'model' => 'Status', 'on' => [ 'status_id' => '_id' ], 'fields' => [ 'status' => 'status' ] ],
        [
            'model' => 'User', 'on' => [ 'changer_id' => '_id' ], 'fields' => [ 'changerfname' => 'fname', 'changerlname' => 'lname' ]
        ],
        //        [ 'model' => 'Table', 'on' => [ 'table_id' => '_id'], 'fields' => [ 'table' => 'table']]
    ];
    protected $inputFilter;

    public function getInputFilter()
    {
        if (!$this->inputFilter) {
            $inputFilter = new InputFilter();
            $factory     = new InputFactory();

            $inputFilter->add($factory->createInput(array(
                                                          'name'     => 'owner_id',
                                                          'required' => true,
                                                          //                    'filters'  => array(
                                                          //                        array( 'name' => 'StripTags' ),
                                                          //                        array( 'name' => 'StringTrim' ),
                                                          //                    ),
                                                      )));
//             $inputFilter -> add( $factory -> createInput( array(
//                    'name'     => 'status_id',
//                    'required' => true,
//                    'filters'  => array(
//                        array( 'name' => 'Int' ),
//                    ),
//            ) ) );
            $inputFilter->add($factory->createInput(array(
                                                          'name'       => 'document_way',
                                                          'required'   => true,
                                                          'validators' => array(
                                                              array(
                                                                  'name'    => 'StringLength',
                                                                  'options' => array(
                                                                      'encoding' => 'UTF-8',
                                                                      'min'      => 3,
                                                                      'max'      => 100,
                                                                  ),
                                                              ),
                                                          ),

                                                      )));
            $inputFilter->add($factory->createInput(array(
                                                          'name'       => 'document_name',
                                                          'required'   => true,
                                                          'filters'    => array(
                                                              array( 'name' => 'StripTags' ),
                                                              array( 'name' => 'StringTrim' ),
                                                          ),
                                                          'validators' => array(
                                                              array(
                                                                  'name'    => 'StringLength',
                                                                  'options' => array(
                                                                      'encoding' => 'UTF-8',
                                                                      'min'      => 3,
                                                                      'max'      => 100,
                                                                  ),
                                                              ),
                                                          ),
                                                      )));
            $inputFilter->add($factory->createInput(array(
                                                          'name'       => 'document_description',
                                                          'required'   => true,
                                                          'filters'    => array(
                                                              array( 'name' => 'StripTags' ),
                                                              array( 'name' => 'StringTrim' ),
                                                          ),
                                                          'validators' => array(
                                                              array(
                                                                  'name'    => 'StringLength',
                                                                  'options' => array(
                                                                      'encoding' => 'UTF-8',
                                                                      'min'      => 3,
                                                                      'max'      => 100,
                                                                  ),
                                                              ),
                                                          ),
                                                      )));

            $this->inputFilter = $inputFilter;
        }

        return $this->inputFilter;
    }

    public function getName()
    {
        return $this->document_name;
    }
}
