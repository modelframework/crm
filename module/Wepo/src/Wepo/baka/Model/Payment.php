<?php

namespace Wepo\Model;

use Zend\InputFilter\Factory as InputFactory;
use Zend\InputFilter\InputFilter;
use Wepo\Lib\WepoMongoModel;

class Payment extends WepoMongoModel
{
    public $table_name = 'payment';

    const TABLE_NAME = 'payment';

    protected $_fields = [
        '_id'           => [ 'type' => 'pk', 'datatype' => 'string', 'default' => 0 ],
        'owner_id'      => [ 'type' => 'source', 'datatype' => 'string', 'default' => 0, 'alias' => 'owner' ],
        'contact_id'    => [ 'type' => 'source', 'datatype' => 'string', 'default' => 0, 'alias' => 'contact' ],
        'contact'       => [ 'type' => 'field', 'datatype' => 'string', 'default' => '' ],
        'sum'           => [ 'type' => 'field', 'datatype' => 'string', 'default' => 0 ],
//        'discount'      => [ 'type' => 'field', 'datatype' => 'string', 'default' => 0 ],
//        'tax'           => [ 'type' => 'field', 'datatype' => 'string', 'default' => 0 ],
        'performed_dtm' => [ 'type' => 'field', 'datatype' => 'string', 'default' => '' ],
        'created_dtm'   => [ 'type' => 'field', 'datatype' => 'string', 'default' => '' ],
        'invoice_id'    => [ 'type' => 'source', 'datatype' => 'string', 'default' => 0, 'alias' => 'invoice' ],
        'description'   => [ 'type' => 'field', 'datatype' => 'string', 'default' => '' ],
        'owner'         => [ 'type' => 'alias', 'datatype' => 'string', 'default' => '', 'source' => 'owner_id' ],
        'contactfname'  => [ 'type' => 'alias', 'datatype' => 'string', 'default' => '', 'source' => 'contact_id' ],
        'contactlname'  => [ 'type' => 'alias', 'datatype' => 'string', 'default' => '', 'source' => 'contact_id' ],
        'invoice'       => [ 'type' => 'alias', 'datatype' => 'string', 'default' => '', 'source' => 'invoice_id' ],
        'ownerfname'    => [ 'type' => 'alias', 'datatype' => 'string', 'default' => '', 'source' => 'owner_id' ],
        'ownerlname'    => [ 'type' => 'alias', 'datatype' => 'string', 'default' => '', 'source' => 'owner_id' ],
        'title'         => [ 'type' => 'alias', 'datatype' => 'string', 'default' => '' ],
        'status'         => [ 'type' => 'alias', 'datatype' => 'string', 'default' => 'normal' ],
    ];
    protected $_joins = [
        [
            'model'  => 'User', 'on' => [ 'owner_id' => '_id' ],
            'fields' => [ 'owner' => 'login', 'ownerfname' => 'fname', 'ownerlname' => 'lname' ],
        ],
        [ 'model' => 'Contact', 'on' => [ 'contact_id' => '_id' ], 'fields' => [ 'contactlname' => 'lname', 'contactfname' => 'fname' ] ],
        [ 'model' => 'Invoice', 'on' => [ 'invoice_id' => '_id' ], 'fields' => [ 'invoice' => 'subject' ] ],
    ];
    protected $inputFilter;

    public function getInputFilter()
    {
        if (!$this->inputFilter) {
            $inputFilter = new InputFilter();
            $factory     = new InputFactory();
            $inputFilter->add($factory->createInput(array(
                                                          'name'     => 'owner_id',
                                                          'required' => false,
                                                          'filters'  => array(
                                                              array( 'name' => 'StripTags' ),
                                                              array( 'name' => 'StringTrim' ),
                                                          ),
                                                      )));
            $inputFilter->add($factory->createInput(array(
                                                          'name'       => 'client_id',
                                                          'required'   => true,
                                                          'filters'    => array(
                                                              array( 'name' => 'StripTags' ),
                                                              array( 'name' => 'StringTrim' ),
                                                          ),
                                                          'validators' => array(
                                                              array(
                                                                  'name'    => 'GreaterThan',
                                                                  'options' => array(
                                                                      'min'       => 1,
                                                                      'inclusive' => true,
                                                                      'messages'  => array( \Zend\Validator\GreaterThan::NOT_GREATER_INCLUSIVE => 'select item' ),
                                                                  ),
                                                              ),
                                                          ),
                                                      )));
            $inputFilter->add($factory->createInput(array(
                                                          'name'       => 'contact_id',
                                                          'required'   => true,
                                                          'filters'    => array(
                                                              array( 'name' => 'StripTags' ),
                                                              array( 'name' => 'StringTrim' ),
                                                          ),
                                                          'validators' => array(
                                                              array(
                                                                  'name'    => 'GreaterThan',
                                                                  'options' => array(
                                                                      'min'       => 1,
                                                                      'inclusive' => true,
                                                                      'messages'  => array( \Zend\Validator\GreaterThan::NOT_GREATER_INCLUSIVE => 'select item' ),
                                                                  ),
                                                              ),
                                                          ),
                                                      )));
            $inputFilter->add($factory->createInput(array(
                                                          'name'       => 'sum',
                                                          'required'   => true,
                                                          'filters'    => array(
                                                              array( 'name' => 'StripTags' ),
                                                              array( 'name' => 'StringTrim' ),
                                                          ),
                                                          'validators' => array(
                                                              array(
                                                                  'name'    => 'Regex',
                                                                  'options' => [
                                                                      'pattern'  => '/^([1-9]\\d*|0)(\\.\\d{1,2})?$/',
                                                                      "messages" => [
                                                                          "regexNotMatch" => "Input valid price",
                                                                      ],
                                                                  ],
                                                              ),
                                                          ),
                                                      )));
            $inputFilter->add($factory->createInput(array(
                                                          'name'     => 'performed_dtm',
                                                          'required' => true,
                                                          'filters'  => array(
                                                              array( 'name' => 'StripTags' ),
                                                              array( 'name' => 'StringTrim' ),
                                                          ),
                                                      )));
            $inputFilter->add($factory->createInput(array(
                                                          'name'       => 'invoice_id',
                                                          'required'   => false,
                                                          'filters'    => array(
                                                              array( 'name' => 'StripTags' ),
                                                              array( 'name' => 'StringTrim' ),
                                                          ),
                                                          'validators' => array(
                                                              array(
                                                                  'name'    => 'StringLength',
                                                                  'options' => array(
                                                                      'encoding' => 'UTF-8',
                                                                      'min'      => 0,
                                                                      'max'      => 24,
                                                                  ),
                                                              ),
                                                          ),
                                                      )));
            $inputFilter->add($factory->createInput(array(
                                                          'name'       => 'description',
                                                          'required'   => false,
                                                          'filters'    => array(
                                                              array( 'name' => 'StripTags' ),
                                                              array( 'name' => 'StringTrim' ),
                                                          ),
                                                          'validators' => array(
                                                              array(
                                                                  'name'    => 'StringLength',
                                                                  'options' => array(
                                                                      'encoding' => 'UTF-8',
                                                                      'min'      => 3,
                                                                      'max'      => 100,
                                                                  ),
                                                              ),
                                                          ),
                                                      )));
            $this->inputFilter = $inputFilter;
        }

        return $this->inputFilter;
    }

    public function getName()
    {
        return $this->description;
    }
}
