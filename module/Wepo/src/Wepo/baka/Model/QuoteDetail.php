<?php

namespace Wepo\Model;

use Zend\InputFilter\Factory as InputFactory;
use Zend\InputFilter\InputFilter;
use Wepo\Lib\WepoMongoModel;

class QuoteDetail extends WepoMongoModel
{
    public $table_name = 'quote_detail';

    const TABLE_NAME = 'quote_detail';

    protected $_fields = [
        '_id'           => [ 'type' => 'pk', 'datatype' => 'string', 'default' => 0 ],
        'quote_id'      => [ 'type' => 'source', 'datatype' => 'string', 'default' => 0, 'alias' => 'quote' ],
        'product_id'    => [ 'type' => 'source', 'datatype' => 'string', 'default' => 0, 'alias' => 'product' ],
        'pricebook_id'  => [ 'type' => 'source', 'datatype' => 'string', 'default' => 0, 'alias' => 'pricebook' ],

    //product price formation fields
        'discount_type' => [ 'type' => 'field', 'datatype' => 'string', 'default' => '' ],//new
        'discount'      => [ 'type' => 'field', 'datatype' => 'string', 'default' => 0 ],

        'product_price' => [ 'type' => 'field', 'datatype' => 'string', 'default' => 0 ],
        'qty'           => [ 'type' => 'field', 'datatype' => 'int', 'default' => 0 ],

        'final_price'   => [ 'type' => 'field', 'datatype' => 'string', 'default' => 0 ],
    //product price formation fields

        'quote'         => [ 'type' => 'alias', 'datatype' => 'string', 'default' => '', 'source' => 'quote_id' ],
        'product'       => [ 'type' => 'alias', 'datatype' => 'string', 'default' => '', 'source' => 'product_id' ],
        'pricebook'     => [ 'type' => 'alias', 'datatype' => 'string', 'default' => '', 'source' => 'pricebook_id' ],
        'title'        => [ 'type' => 'alias', 'datatype' => 'string', 'default' => '' ],
        'status'         => [ 'type' => 'alias', 'datatype' => 'string', 'default' => 'normal' ],
    ];
    protected $_joins  = [
        [ 'model' => 'Quote', 'on' => [ 'quote_id' => '_id' ], 'fields' => [ 'quote' => 'subject' ] ],
        [ 'model' => 'Product', 'on' => [ 'product_id' => '_id' ], 'fields' => [ 'product' => 'name' ] ],
        [ 'model' => 'Pricebook', 'on' => [ 'pricebook_id' => '_id' ], 'fields' => [ 'pricebook' => 'pricebook' ] ],
    ];
    protected $inputFilter;

    public function getInputFilter()
    {
        if (!$this->inputFilter) {
            $inputFilter         = new InputFilter();
            $factory             = new InputFactory();
            $inputFilter->add($factory->createInput(array(
                    'name'     => 'quote_id',
                    'required' => false,
                    'filters'  => array(
                        array( 'name' => 'StripTags' ),
                        array( 'name' => 'StringTrim' ),
                    ),
            )));
            $inputFilter->add($factory->createInput(array(
                    'name'       => 'product_id',
                    'required'   => true,
                    'filters'    => array(
                        array( 'name' => 'StripTags' ),
                        array( 'name' => 'StringTrim' ),
                    ),
                    'validators' => array(
                        array(
                            'name'    => 'GreaterThan',
                            'options' => array(
                                'min'       => 1,
                                'inclusive' => true,
                                'messages'  => array( \Zend\Validator\GreaterThan::NOT_GREATER_INCLUSIVE => 'select item' ),
                            ),
                        ),
                    ),
            )));
            $inputFilter->add($factory->createInput(array(
                    'name'     => 'pricebook_id',
                    'required' => false,
                    'filters'  => array(
                        array( 'name' => 'StripTags' ),
                        array( 'name' => 'StringTrim' ),
                    ),
            )));
            $inputFilter->add($factory->createInput(array(
                'name'       => 'product_price',
                'required'   => false,
                'filters'    => array(
                    array( 'name' => 'StripTags' ),
                    array( 'name' => 'StringTrim' ),
                ),
                'validators' => array(
                    [
                        "name"    => "Regex",
                        "options" => [
                            "pattern"  => "/^([1-9]\\d*|0)((\\.)\\d*)?$/",
                            "messages" => [
                                "regexNotMatch" => "Input valid price",
                            ],
                        ],
                    ],
                ),
            )));
            $inputFilter->add($factory->createInput(array(
                'name'       => 'discount_type',
                'required'   => false,
                'filters'    => array(
                    array( 'name' => 'StripTags' ),
                    array( 'name' => 'StringTrim' ),
                ),
                'validators' => array(
                    array(
                        'name'    => 'InArray',
                        'options' => array(
                            'haystack' => [
                                'Direct Price Reduction',
                                '% of Price',
                            ],
                        ),
                    ),
                ),
            )));
            $inputFilter->add($factory->createInput(array(
                'name'     => 'discount',
                'required' => false,
                'filters'    => array(
                    array( 'name' => 'StripTags' ),
                    array( 'name' => 'StringTrim' ),
                ),
                'validators' => array(
                    [
                        "name"    => "Regex",
                        "options" => [
                            "pattern"  => '/^[-+]?[0-9]*\.?[0-9]+([eE][-+]?[0-9]+)?$/',
                            "messages" => [
                                "regexNotMatch" => "Input valid price, example - 12.01",
                            ],
                        ],
                    ],
                ),
            )));
            $inputFilter->add($factory->createInput(array(
                    'name'       => 'qty',
                    'required'   => false,
//                    'filters'  => array(
//                        array( 'name' => 'Int' ),
//                    ),
                    'validators' => array(
                        array( 'name' => 'Digits' ),
                    ),
            )));
            $inputFilter->add($factory->createInput(array(
                    'name'     => 'final_price',
                    'required' => false,
                    'filters'  => array(
                        array( 'name' => 'Int' ),
                    ),
            )));
            $this->inputFilter = $inputFilter;
        }

        return $this->inputFilter;
    }

    public function getName()
    {
        return 'Product '.$this->product;
    }
}
