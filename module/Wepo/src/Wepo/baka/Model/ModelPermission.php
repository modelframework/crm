<?php

namespace Wepo\Model;

use Wepo\Lib\WepoMongoModel;

class ModelPermission extends WepoMongoModel
{
    public $adapterName = 'wepo_company';
    protected $_fields  = [
        '_id'        => [ 'type' => 'pk', 'datatype' => 'string', 'default' => '' ],
        'model'      => [ 'type' => 'field', 'datatype' => 'string', 'default' => '' ],
        'group_id'   => [ 'type' => 'field', 'datatype' => 'string', 'default' => '' ],
        'permission' => [ 'type' => 'field', 'datatype' => 'string', 'default' => '' ],
        'fields'     => [ 'type' => 'field', 'datatype' => 'array', 'default' => [ ] ],
    ];
}
