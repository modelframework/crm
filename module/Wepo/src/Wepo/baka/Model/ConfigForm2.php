<?php

namespace Wepo\Model;

use Wepo\Lib\WepoMongoModel;

class ConfigForm2 extends WepoMongoModel
{
    public $adapterName = 'wepo_company';
    public $table_name  = 'ConfigForm2';
    protected $_fields  = [
        '_id'             => [ 'type' => 'pk', 'datatype' => 'string', 'default' => '' ],
        'name'            => [ 'type' => 'field', 'datatype' => 'string', 'default' => '' ],
        'group'           => [ 'type' => 'field', 'datatype' => 'string', 'default' => '' ],
        'type'            => [ 'type' => 'field', 'datatype' => 'string', 'default' => '' ],
        'options'         => [ 'type' => 'field', 'datatype' => 'array', 'default' => [ ] ],
        'attributes'      => [ 'type' => 'field', 'datatype' => 'array', 'default' => [ ] ],
        'fieldsets'       => [ 'type' => 'field', 'datatype' => 'array', 'default' => [ ] ],
        'elements'        => [ 'type' => 'field', 'datatype' => 'array', 'default' => [ ] ],
        'validationGroup' => [ 'type' => 'field', 'datatype' => 'array', 'default' => [ ] ],
    ];
}
