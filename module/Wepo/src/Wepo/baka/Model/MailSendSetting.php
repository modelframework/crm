<?php

namespace Wepo\Model;

use Zend\InputFilter\Factory as InputFactory;
use Zend\InputFilter\InputFilter;
use Wepo\Lib\WepoMongoModel;

class MailSendSetting extends WepoMongoModel
{
    public $table_name            = 'mail_setting';
    private static $securityTypes = ['none', 'ssl', 'tls' ];
    private static $protocolTypes = [ 'SMTP' ];

    const TABLE_NAME = 'mail_setting';

    protected $_fields = [
        '_id'              => [ 'type' => 'pk', 'datatype' => 'string', 'default' => '' ],
        'email'            => [ 'type' => 'field', 'datatype' => 'string', 'default' => '' ],
        'setting_user'     => [ 'type' => 'field', 'datatype' => 'string', 'default' => '' ],
        'setting_protocol' => [ 'type' => 'field', 'datatype' => 'string', 'default' => '' ],
        'setting_host'     => [ 'type' => 'field', 'datatype' => 'string', 'default' => '' ],
        'setting_port'     => [ 'type' => 'field', 'datatype' => 'int', 'default' => 0 ],
        'setting_security' => [ 'type' => 'field', 'datatype' => 'string', 'default' => '' ],
        'pass'             => [ 'type' => 'field', 'datatype' => 'string', 'default' => '' ],
        'type'             => [ 'type' => 'field', 'datatype' => 'string', 'default' => 'Send' ],
        'is_default'       => [ 'type' => 'field', 'datatype' => 'string', 'default' => 'false' ],
//        'additional_settings' => [ 'type' => 'field', 'datatype' => 'array', 'default' => '' ],
        'owner_id'         => [ 'type' => 'source', 'datatype' => 'string', 'default' => '' ],
        'user_id'          => [ 'type' => 'source', 'datatype' => 'string', 'default' => '' ],
        'status_id'        => [ 'type' => 'source', 'datatype' => 'string', 'default' => '' ],
        'owner'            => [ 'type' => 'alias', 'datatype' => 'string', 'default' => '', 'source' => 'owner_id' ],
        'user'             => [ 'type' => 'alias', 'datatype' => 'string', 'default' => '', 'source' => 'user_id' ],
        'status'           => [ 'type' => 'alias', 'datatype' => 'string', 'default' => '', 'source' => 'status_id' ],
        'title'            => [ 'type' => 'alias', 'datatype' => 'string', 'default' => '' ],
        ////////////////////
        'ownerfname'       => [ 'type' => 'alias', 'datatype' => 'string', 'default' => '', 'source' => 'owner_id'],
        'ownerlname'       => [ 'type' => 'alias', 'datatype' => 'string', 'default' => '', 'source' => 'owner_id'],
    ];

    protected $_joins  = [
        [ 'model' => 'User', 'on' => [ 'owner_id' => '_id' ], 'fields' => [ 'owner' => 'login', 'ownerfname' => 'fname', 'ownerlname' => 'lname' ] ],
        [ 'model' => 'User', 'on' => [ 'user_id' => '_id' ], 'fields' => [ 'user' => 'login' ] ],
        [ 'model' => 'Status', 'on' => [ 'status_id' => '_id' ], 'fields' => [ 'status' => 'status' ] ],
    ];

    protected $_unique = [
        ['email','setting_protocol'],
    ];

    protected $inputFilter;

    public static function securityTypes()
    {
        $ret = [ ];
        foreach (self :: $securityTypes as $value) {
            $ret[ $value ] = $value;
        }

        return $ret;
    }

    public static function protocolTypes()
    {
        $res = array_combine(self::$protocolTypes, self::$protocolTypes);

        return $res;
    }

    public function getInputFilter()
    {
        if (!$this->inputFilter) {
            $inputFilter = new InputFilter();
            $factory     = new InputFactory();

            $inputFilter->add($factory->createInput(array(
                    'name'       => 'email',
                    'required'   => false,
                    'filters'    => array(
                        array( 'name' => 'StripTags' ),
                        array( 'name' => 'StringTrim' ),
                    ),
                    'validators' => array(
                        array(
                            'name'    => 'EmailAddress',
                            'options' => array(
                                'domain' => false,
                            ),
                        ),
                    ),
            )));
            $inputFilter->add($factory->createInput(array(
                    'name'     => 'setting_user',
                    'required' => false,
                    'filters'  => array(
                        array( 'name' => 'StripTags' ),
                        array( 'name' => 'StringTrim' ),
                    ),
            )));
            $inputFilter->add($factory->createInput(array(
                    'name'     => 'setting_protocol',
                    'required' => false,
                    'filters'  => array(
                        array( 'name' => 'StripTags' ),
                        array( 'name' => 'StringTrim' ),
                    ),
            )));
            $inputFilter->add($factory->createInput(array(
                    'name'     => 'setting_host',
                    'required' => false,
                    'filters'  => array(
                        array( 'name' => 'StripTags' ),
                        array( 'name' => 'StringTrim' ),
                    ),
            )));
            $inputFilter->add($factory->createInput(array(
                    'name'     => 'setting_port',
                    'required' => false,
                    'filters'  => array(
                        array( 'name' => 'Int' ),
                    ),
            )));
            $inputFilter->add($factory->createInput(array(
                    'name'     => 'setting_security',
                    'required' => false,
                    'filters'  => array(
                        array( 'name' => 'StripTags' ),
                        array( 'name' => 'StringTrim' ),
                    ),
            )));
            $inputFilter->add($factory->createInput(array(
                    'name'       => 'pass',
                    'required'   => false,
                    'filters'    => array(
                        array( 'name' => 'StripTags' ),
                        array( 'name' => 'StringTrim' ),
                    ),
                    'validators' => array(
                        array(
                            'name'    => 'StringLength',
                            'options' => array(
                                'encoding' => 'UTF-8',
                                'min'      => 3,
                                'max'      => 100,
                            ),
                        ),
                    ),
            )));
            $inputFilter->add($factory->createInput(array(
                    'name'       => 'user_id',
                    'required'   => false,
                    'filters'    => array(
                        array( 'name' => 'StripTags' ),
                        array( 'name' => 'StringTrim' ),
                    ),
                    'validators' => array(
                        array(
                            'name'    => 'GreaterThan',
                            'options' => array(
                                'min'       => 1,
                                'inclusive' => true,
                                'messages'  => array( \Zend\Validator\GreaterThan::NOT_GREATER_INCLUSIVE => 'select item' ),
                            ),
                        ),
                    ),
            )));
            $inputFilter->add($factory->createInput(array(
                    'name'     => 'owner_id',
                    'required' => false,
                    'filters'  => array(
                        array( 'name' => 'StripTags' ),
                        array( 'name' => 'StringTrim' ),
                    ),
            )));
            $inputFilter->add($factory->createInput(array(
                'name'     => 'is_default',
                'required' => false,
                'filters'  => array(
                    array( 'name' => 'StripTags' ),
                    array( 'name' => 'StringTrim' ),
                ),
            )));
            $this->inputFilter = $inputFilter;
        }

        return $this->inputFilter;
    }

    public function getModelName()
    {
        return 'MailSetting';
    }

    public function getModelLabel()
    {
        return 'Email Setting';
    }

    public function getName()
    {
        return $this->user.'  -  '.$this->email;
    }
}
