<?php

namespace Wepo\Model;

use Wepo\Lib\WepoMongoModel;

class ConfigModel2 extends WepoMongoModel
{
    public $adapterName = 'wepo_company';
    public $table_name  = 'ConfigModel2';
    public $_fields  = [
        '_id'     => [ 'type' => 'pk', 'datatype' => 'string', 'default' => '' ],
        'model'   => [ 'type' => 'field', 'datatype' => 'string', 'default' => '' ],
        'adapter' => [ 'type' => 'field', 'datatype' => 'string', 'default' => '' ],
        'fields'  => [ 'type' => 'field', 'datatype' => 'array', 'default' => [ ] ],
        'groups'  => [ 'type' => 'field', 'datatype' => 'array', 'default' => [ ] ],
        'parsed'  => [ 'type' => 'field', 'datatype' => 'array', 'default' => [ ] ],
    ];
}
