var path = window.location.pathname;
var validator;
var lastScroll = 0;

path = path.split("/");

for (var i = 0; i < path.length; i++) {
    if (/[0-9]/i.test(path[i]))
        path.splice(i);
}

if (!path[path.length - 1].length)
    path.pop();
path = path.join("/");

var availableTags = [
    "ActionScript",
    "AppleScript",
    "Asp",
    "BASIC",
    "C",
    "C++",
    "Clojure",
    "COBOL",
    "ColdFusion",
    "Erlang",
    "Fortran",
    "Groovy",
    "Haskell",
    "Java",
    "JavaScript",
    "Lisp",
    "Perl",
    "PHP",
    "Python",
    "Ruby",
    "Scala",
    "Scheme"
];

//Align for title in view modes (big small)
function view_blockCoords()
{
    window.namePosition = $('.lead_info').find('tbody tr:first td span').offset().left;
    window.blockHeight  = $('.view_block').height();

    if ($.cookie("viewHeight_cookie")) {
        var viewHeight_cookie = $.cookie("viewHeight_cookie").split(" ");
        window.blockHeight = viewHeight_cookie;
    }
    if ($.cookie("namePosition_cookie")) {
        var namePosition_cookie = $.cookie("namePosition_cookie").split(" ");
        window.namePosition = namePosition_cookie;
    }
}

function smallView(time)
{
    view_blockCoords();
    var viewMode        = $('#viewMode');
    var leadInfo        = $('.lead_info');
    var leadInfo_first  = $('.lead_info:first');

    leadInfo.not(':first').fadeOut(time).slideUp(time);
    leadInfo_first.find('tbody tr:first td span').css('position', 'absolute').offset({left: window.namePosition});
    leadInfo_first.find('tbody tr:not(:first)').fadeOut(time).slideUp(time);

    $('.photo > img').removeClass('big').addClass('small');
    $('.view_block').animate({"height": '170px'}, time);
    $('div.social').fadeOut(1);

    viewMode.fadeOut(1);
    setTimeout(function(){
        viewMode.fadeIn(300);
        leadInfo.find('table').css('border-collapse', 'inherit');
    },400);

    var interval = setInterval(function () {
        if ($(window).scrollTop() < $('#header').height() - $('#mainMenu').height()) {
            fixedMenu();
            clearInterval(interval);
        }
    }, 1);
}

function usualView()
{
    var viewMode = $('#viewMode');
    var leadInfo = $('.lead_info');

    leadInfo.find('tbody tr:not(:first)').fadeIn(500).slideDown(500);
    leadInfo.find('table').css('border-collapse', 'collapse');
    leadInfo.not(':first').fadeIn(500).slideDown(500);

    $('.photo > img').removeClass('small').addClass('big');
    $('.view_block').animate({"height": window.blockHeight}, 500);

    viewMode.fadeOut(1);
    setTimeout(function(){
        viewMode.fadeIn(300);
        $('div.social').fadeIn(300);
    },400);

}

function viewMode()
{
    var view_cookie         = [];
    var viewHeight_cookie   = [];
    var namePosition_cookie = [];

    if (!$('#viewMode').hasClass('smallMode'))
    {
        view_cookie.push('small');
        viewHeight_cookie.push(window.blockHeight);
        namePosition_cookie.push(window.namePosition);
    }
    else view_cookie.push('not_small');

    if (view_cookie.length > 0) {
        $.cookie('view_cookie', view_cookie.join(" "), {expires: 365, path: path});
        $.cookie('viewHeight_cookie', viewHeight_cookie.join(" "), {expires: 365, path: path});
        $.cookie('namePosition_cookie', namePosition_cookie.join(" "), {expires: 365, path: path});
    }
    else {
        $.removeCookie('view_cookie', {path: path}); $.removeCookie('viewHeight_cookie', {path: path}); $.removeCookie('namePosition_cookie', {path: path});
    }
}

function FirsTimeWidget()
{
    var i = 1;
    $('.widget').each(function () {
        $(this).closest('li').attr({"data-col": 1, "data-row": i, "data-sizex": 2, "data-sizey": 1});
        i++;
    });
    makeGridster();
    Reload_widgets();
}

function Reload_widgets()
{
    $('.widget').each(function ()
    {
        var children = $(this).find('tbody').children();
        var index    = $(this).closest('li').index();

        if (( children.length === 0 ) || ( ('.note #contento > li').length === 0 )) {
            gridster.resize_widget(gridster.$widgets.eq(index), false, 7);
        }

        else if (children.length === 1) {
            gridster.resize_widget(gridster.$widgets.eq(index), false, 19);
        }
        else if (children.length === 2) {
            gridster.resize_widget(gridster.$widgets.eq(index), false, 24);
        }
        else if (children.length === 3) {
            gridster.resize_widget(gridster.$widgets.eq(index), false, 29);
        }
        else if (children.length === 4) {
            gridster.resize_widget(gridster.$widgets.eq(index), false, 34);
        }
        else if (children.length === 5) {
            gridster.resize_widget(gridster.$widgets.eq(index), false, 39);
        }

        if ($(this).hasClass('minimized')) {
            gridster.resize_widget(gridster.$widgets.eq(index), false, 7);
        }
    });
}

function widgetMinimized() // MINIMIZED OR NOT COOKIE
{
    var mini_cookie = [];

    $('.widget').each(function () {
        if ($(this).hasClass('minimized'))  mini_cookie.push('is_mini');
        else mini_cookie.push('not_mini');
    });

    if (mini_cookie.length > 0) $.cookie('mini_cookie', mini_cookie.join(" "), {expires: 365, path: path});
    else $.removeCookie('mini_cookie', {path: path});
}

function widgetPosition() // WIDGET POSITION SAVE
{
    var col_cookie = [];
    var row_cookie = [];
    var size_x_cookie = [];
    var size_y_cookie = [];

    $('.widget').each(function ()
    {
        var position = gridster.serialize(gridster.$widgets.eq($(this).closest('li').index()));
        col_cookie.push(position[0].col);  size_x_cookie.push(position[0].size_x);
        row_cookie.push(position[0].row);  size_y_cookie.push(position[0].size_y);
    });

    if (col_cookie.length > 0) col = $.cookie('col_cookie', col_cookie.join(" "), {expires: 365, path: path});
    else $.removeCookie('col_cookie', {path: path});

    if (row_cookie.length > 0) row = $.cookie('row_cookie', row_cookie.join(" "), {expires: 365, path: path});
    else $.removeCookie('row_cookie', {path: path});

    if (size_x_cookie.length > 0) size_x = $.cookie('size_x_cookie', size_x_cookie.join(" "), {
        expires: 365,
        path: path
    });
    else $.removeCookie('size_x_cookie', {path: path});

    if (size_y_cookie.length > 0) size_y = $.cookie('size_y_cookie', size_y_cookie.join(" "), {
        expires: 365,
        path: path
    });
    else $.removeCookie('size_y_cookie', {path: path});
}

$(function ()
{
    if ( !(($('.widget').length) && ($.cookie("col_cookie")) && ($.cookie("row_cookie")) && ($.cookie("size_x_cookie")) && ($.cookie("size_x_cookie"))) ) {
        FirsTimeWidget();
        widgetMinimized();
    }

    if ( ($('.widget').length) && ($.cookie("col_cookie")) && ($.cookie("row_cookie")) && ($.cookie("size_x_cookie")) && ($.cookie("size_x_cookie")) )
    {
        var i = 0;
        var col_cookie = $.cookie("col_cookie").split(" ");
        var row_cookie = $.cookie("row_cookie").split(" ");
        var size_x_cookie = $.cookie("size_x_cookie").split(" ");
        var size_y_cookie = $.cookie("size_y_cookie").split(" ");

        $('.widget').each(function () {
            $(this).closest('li').attr(
            {
                "data-col"  : col_cookie[i],
                "data-row"  : row_cookie[i],
                "data-sizex": size_x_cookie[i],
                "data-sizey": size_y_cookie[i]
            });
            i++;
        });
    }

    if ( ($('.widget').length) && ($.cookie("mini_cookie")) )
    {
        var i = 0;
        var mini_cookie = $.cookie("mini_cookie").split(" ");

        $('.widget').each(function () {
            if (mini_cookie[i] === 'is_mini') {
                $(this).addClass('minimized');
            }
            i++;
        });
    }

    if ( ( $('#viewMode').length ) && ( $.cookie("view_cookie")) ) {
        var view_cookie = $.cookie("view_cookie").split(" ");
        if (view_cookie == 'small') {
            $('#viewMode').addClass('smallMode');
            smallView(0);
        }
    }
});

function makeGridster() //MAKE DRAGGABLE
{
    gridster = $(".gridster ul").gridster(
    {
        widget_margins: [5, 4],
        min_cols: 3,
        widget_base_dimensions: [$('#mainMenu').width() / 4 - 13, 0.05],
        resize:
        {
            enabled: true,
            axes: ['x'],
            min_size: [2, 2],
            stop: function ()
            {
                widgetPosition();
                if ($(window).scrollTop() < $('#header').height() - $('#mainMenu').height()) {
                    fixedMenu();
                }
            }
        },
        draggable:
        {
            handle: $('div.title'),

            drag: function (e)
            {
                $('.selectBox-dropdown-menu').hide();
                $('.selectBox').removeClass('selectBox-menuShowing');

                footer_top = $('.footer').offset().top;
                x = e.pageX;
                y = e.pageY;

                if (( x > screen.width - 45 ) || ( y >= footer_top - 10 )) {
                    $(e.target).trigger("mouseup");
                }

                $('body').mouseup(function () {
                    $(window).off('mousemove');
                });

                if ($(window).scrollTop() < $('#header').height() - $('#mainMenu').height()) {
                    fixedMenu();
                }
            },

            stop: function ()
            {
                setInterval(function ()
                {
                    if ($(window).scrollTop() < $('#header').height() - $('#mainMenu').height()) {
                        fixedMenu();
                        window.clearTimeout();
                    }
                }, 1);
                widgetPosition();
            }
        }
    }).data('gridster');
}

$(window).resize(function ()
{
    if ($('.widget').length)
    {
        if (($('.widget').width() < 605) || ($('#mainMenu').width() < 1260))
        {
            gridster.resize_widget_dimensions({
                widget_base_dimensions: [$('#mainMenu').width() / 4 - 9, 0.05]
            });
            widgetPosition();
        }
        else
        {
            gridster.resize_widget_dimensions({
                widget_base_dimensions: [$('#mainMenu').width() / 4 - 13, 0.05]
            });
            widgetPosition();
        }
    }

    $('#mainMenu').css(
    {
      'max-width': '100%',
      'min-width': '95%',
      'position': 'relative !important',
      'width': '100%'
    });

    $('#header').css(
    {
      'height': '223px !important',
      'background': '#4984af url(/css/img/header.png) repeat 0 0'
    });
});

function fixedMenu() {
    $('#header').css({'background': '#4984af url(/css/img/header.png) repeat 0 0'});
    $('#mainMenu').css('position', 'relative !important');
}

function onScroll()
{
    if ($('#header').height() > $('#mainMenu').height() && $(document).height() < $(window).height() + $('#header').height())  return false;

    width = $('#mainMenu').width();
    height = $('mainMenu').height();

    if ($(window).scrollTop() > $('#header').height() - $('#mainMenu').height())
    {
        $('#header').stop().slideUp();
        $('#mainMenu').css(
        {
           'position': 'fixed',
           'top': '0',
           'width': width,
           'height': height,
           'z-index': 100
        });
    }

    if ($(window).scrollTop() < $('#header').height() - $('#mainMenu').height()) {
        fixedMenu();
    }

    if ($(window).scrollTop() > 250)
    {
        if (lastScroll < $(window).scrollTop()) // scroll down
        {
            $('.header').stop().animate({top: '-200px'});
            if ($('ul').hasClass('visible'))
            {
                $('ul').removeClass('visible');
            }
        }
        else // scroll up
        {
            $('.header').stop().animate({top: '0px'});
        }
    }
    else {
        $('.header').stop().animate({top: '0px'});
    }
    lastScroll = $(window).scrollTop();
}

$(function ()
{

    $('#viewMode').click(function () {
        viewMode();
    });

    $('#scrollTop').click(function (event) {
        event.stopPropagation();
        top.goTop();
        return false;
    });

    $('.content a > img').mouseover(function () {
        $(this).append('<span class="tooltip top">' + $(this).attr("alt") + '</span>');
    });

    $('.content a > img').mouseleave(function () {

    });

    $('.menu > ul > li').hover(function () {
        if ($(this).position().top == 0 && $(document).scrollTop() == 0) {
            if ($(this).find('.tooltip').eq(0).hasClass('bottom'))
                $(this).find('.tooltip').eq(0).removeClass('bottom').addClass('top');
        }
        else {
            if ($(this).find('.tooltip').eq(0).hasClass('top'))
                $(this).find('.tooltip').eq(0).removeClass('top').addClass('bottom');
        }
    });

    $('.menu .tooltip').addClass('bottom');
    $('.menu .create ul .tooltip').removeClass('bottom').addClass('right');

    $('.widget a.minimize_button').click(function () {
        $(this).closest('.widget').toggleClass('minimized');

        $('.widget').each(function () {
            $(this).find('.corner').css('margin-top', -$(this).height() / 2);
        });

        Reload_widgets();
        widgetMinimized();

        setInterval(function () {
            if ($(window).scrollTop() < $('#header').height() - $('#mainMenu').height()) {
                fixedMenu();
                window.clearTimeout();
            }
        }, 1);
    });

    $('#search_query').selectize({
        maxItems: 1,
        valueField: 'title',
        labelField: 'title',
        searchField: 'title',
        options: [],
        create: true,

        load: function (query, callback) {
            if (!query.length)
                return callback();
            $('a.txtSearch', '#main_search').css('background-image', 'url(/css/img/select2-spinner.gif)');
            $('a.txtSearch', '#main_search').css('background-position', '0px 0px');
            $('.input .selectize-control.single .selectize-input.input-active', '#main_search').css('background-image', 'none');
            $.ajax({
                url: '/api/v2/search',
                type: 'GET',
                dataType: 'json',
                data: {
                    q: query
                },
                error: function () {
                    $('a.txtSearch', '#main_search').css('background-image', 'url(/css/img/sprite.png)');
                    $('a.txtSearch', '#main_search').css('background-position', '-500px 0px');
                    $('.input .selectize-control.single .selectize-input.input-active', '#main_search').css('background-image', 'url(/css/img/search_ico.png)');
                    callback();
                },
                success: function (res) {
                    $('a.txtSearch', '#main_search').css('background-image', 'url(/css/img/sprite.png)');
                    $('a.txtSearch', '#main_search').css('background-position', '-500px 0px');
                    $('.input .selectize-control.single .selectize-input.input-active', '#main_search').css('background-image', 'url(/css/img/search_ico.png)');
                    callback(res.data);
                }
            });
        }
    });

    $('.widget table tbody tr,#mainMenu li, div.lead_block table tr').bind('mouseenter', function () {
        $(this).next().addClass('borderLess');
    }).bind('mouseleave', function () {
        $(this).next().removeClass('borderLess');
    });

    $(document).click(function () {
        $('.visible').removeClass('visible');
        $("#titleMenu").stop().animate({width: 0, height: 0, opacity: 0}, 200,
            function () {
                $("#titleMenu").removeClass('visible');
            }
        );
    });

    $('#mainMenu .create a#menu-create').click(function (e) {
        e.stopPropagation();
        var this_ul = $(this).parent().find('ul');
        $('#mainMenu ul').not(this_ul).removeClass('visible');
        $('.subLinks').removeClass('visible');
        this_ul.toggleClass('visible');
        this_ul.css({'position':'absolute','z-index':'11'});

        return false;
    });

    $('#mainMenu .activity a#menu-activities').click(function (e) {
        e.stopPropagation();
        var this_ul = $(this).parent().find('ul');
        $('#mainMenu ul').not(this_ul).removeClass('visible');
        $('.subLinks').removeClass('visible');
        this_ul.toggleClass('visible');
        this_ul.css({'position':'absolute','z-index':'11'});
        this_ul.offset({left: $(this).offset().left});
        return false;
    });

    $('form a.submitLink').click(function (e) {
        $(".submitLink[type=submit]", $(this).parents("form")).removeAttr("clicked");
        $(this).attr("clicked", "true");
        e.preventDefault();
        $(this).parents('form').submit();
    });

    //$('.ajaxform').submit(function () {
    //    var goto = $(".submitLink[type=submit][clicked=true]").attr('value');
    //    goto = (goto == undefined) ? 'save' : goto;
    //    formData = new FormData(this);
    //    formData.append('redirect_action', goto);
    //    if ($('.validate').valid()) {
    //        $('.ajaxform .fieldset').addClass('is-disabled');
    //        $.ajax({
    //            url: $(location).attr('href'),
    //            type: 'POST',
    //            data: formData,
    //            mimeType: "multipart/form-data",
    //            contentType: false,
    //            cache: false,
    //            processData: false,
    //            success: function (data, textStatus, jqXHR) {
    //                data = jQuery.parseJSON(data);
    //                if (data.status) {
    //                    $('form .success_msg').removeClass('hidden');
    //                    $('form .success_msg').removeClass('redColor');
    //                    $('form .success_msg').addClass('greenColor');
    //                    $('form .success_msg i').removeClass('txtError');
    //                    $('form .success_msg i').addClass('txtSuccess');
    //                    var delay = 1000;
    //                    setTimeout(function () {
    //                        window.location = data.uri;
    //                    }, delay);
    //                }
    //                else {
    //                    $('form .success_msg').removeClass('hidden');
    //                    $('form .success_msg').removeClass('greenColor');
    //                    $('form .success_msg').addClass('redColor');
    //                    $('form .success_msg i').removeClass('txtSuccess');
    //                    $('form .success_msg i').addClass('txtError');
    //                    errors = {};
    //                    for (var fieldset in data.field_messages) {
    //                        for (var field in data['field_messages'][fieldset]) {
    //                            inputField = fieldset.concat('[', field, ']');
    //
    //                            errorMessage = '';
    //                            for (var error in data['field_messages'][fieldset][field]) {
    //                                errorMessage = errorMessage.concat(data['field_messages'][fieldset][field][error], '. ');
    //                            }
    //                            errors[inputField] = errorMessage;
    //                        }
    //                    }
    //                    validator.showErrors(errors);
    //                    $('.ajaxform .fieldset').removeClass('is-disabled');
    //                }
    //                $
    //                $('form .success_msg div').text(data.message);
    //            },
    //            error: function (jqXHR, textStatus, errorThrown) {
    //                $('.ajaxform .fieldset').removeClass('is-disabled');
    //                $('form .success_msg div').text('Can\'t connect to server. Please insure you have internet connection and try later');
    //            }
    //        });
    //    }
    //    return false;
    //});

    $('#header .with_subLinks > a').click(function (e) {
        e.stopPropagation();
        $(this).parent().find('.subLinks').toggleClass('visible');
    });

    $('#search_form .input').insideLabel();

    //$( "#form_date, .input_date input" ).datepicker( { dateFormat: "mm/dd/yy" } );

    $('.steps_popup').each(function () {
        var $t = $(this);
        $t.nav = $t.find('.popup_nav a');
        $($t.nav.filter(':first').attr('href')).addClass('active');
        $t.nav.click(function (e) {
            e.preventDefault();
            if ($(this).hasClass('active'))
                return $(this);
            $t.children('div.active').animate({left: '-110%'}, 300);
            $($(this).attr('href')).animate({left: '20px'}, 300, function () {
                $t.children('div.active').removeClass('active').removeAttr('style');
                $(this).addClass('active').removeAttr('style');

            });
        });
    });

    setCheckboxes();
    $('input[type="radio"]').customRadio();
    validator = $('form.validate').validate();
});


function countTotalPrice()
{
    var dscnt = $("#discount").val();
    var dscntType = $("#discount_type").val();
    var raw_price = parseFloat($("#raw_price").val());
    var adjstmnt = $("#adjustment").val();
    adjstmnt = adjstmnt === undefined ? 0 : parseFloat(adjstmnt);
    var vat = $("#vat").val();
    vat = vat === undefined ? 0 : parseFloat(vat);
    var sales_tax = $("#sales_tax").val();
    sales_tax = sales_tax === undefined ? 0 : parseFloat(sales_tax);
    var qty = $("#qty").val();
    qty = qty === undefined ? 1 : parseFloat(qty);
    var res_price = 0;

    if (!isNaN(raw_price)) {
        if (dscntType === 'Direct Price Reduction') {
            dscnt = dscnt === undefined ? 0 : dscnt;
            res_price = (raw_price * qty - dscnt + adjstmnt);
        }
        else {
            dscnt = dscnt === undefined ? 1 : dscnt;
            res_price = raw_price * qty * dscnt / 100 + adjstmnt;
        }
        res_price += res_price * (vat + sales_tax) / 100;

        res_price = Math.max(res_price, 0).toFixed(2);
    }

    document.getElementById("showTotalPrice").innerHTML = "<b>" + res_price + " $ </b>";
}

function PutToPrice() {
    var id = $('#product_id option:selected').val();
    if (id !== "0") {
        $('#raw_price').val(prices[id]);
    }
    else {
        $('#raw_price').val('');
    }
}

function setCheckboxes()
{
    $('.checkbox').click(function (e)
    {
        if ($(this).find('input[type=checkbox]').hasClass('checked')) {
            $(this).closest('tr').addClass('checked');

            $('#deletebtn,#restorebtn,#clearbtn,#massMail').removeClass('disabled');

            if ((!$("input:not(:checked)", "#tbody").length) || (!$(".check:not(:checked)", "#tbody").length)) {
                $('div.checkboxArea', "#c_all").addClass('checked');
            }
        }
        else {

            $('div.checkboxArea', "#c_all").removeClass('checked');
            $(this).closest('tr').removeClass('checked');

            if ((!$("input:checked", "#tbody").length) || (!$(".check:checked", "#tbody").length))
            {
                $('div.checkboxArea', "#c_all").removeClass('checked');
                $('#deletebtn,#restorebtn,#clearbtn,#massMail').addClass('disabled');
            }
        }
    });

    if ($('#tbody tr').length && !$('#No_record').length ) {
        $('#deletebtn,#restorebtn,#clearbtn,#massMail,#total_count,.selectRows,.pagination').removeClass('hidden');
    }
    else {
        $('#deletebtn,#restorebtn,#clearbtn,#massMail,#total_count,.selectRows,.pagination').addClass('hidden');

        if (!$('#No_record').length )
        {
            $("#tbody").append("<tr id='No_record' align='center'>\n\
                                <h2><td colspan = 20 style = 'font-size:20px; vertical-align: bottom; font-style: italic; color: #999999; '>\n\
                                    No records\n\
                                </td></h2>\n\
                            </tr>");
        }

    }

    $('input[type="checkbox"]').customCheckbox();
}

var scrolled = false;

$(window).bind('scroll', function (event, eventdata) {
    onScroll();
});

jQuery.fn.insideLabel = function () {
    return $(this).each(function () {
        var $t = $(this);
        $t.input = $t.find('input');
        $t.label = $t.find('label');

        if ($t.input.val() != '') {
            $t.label.hide();
        }

        $t.input.focus(function () {
            $t.label.hide();
        }).blur(function () {
            if ($t.input.val() == '') {
                $t.label.show();
            }
        });
    });
}

function startloader() {
    $('#progress').show({effect: 'fade', duration: 500});
    $('a').addClass('disable_link');
    $('a#menu-create').next('ul').removeClass('visible'); // hide + Create menu
    $('#owner_sublinks,#help_sublinks').removeClass('visible');
}

function stoploader() {
    $('#progress').hide({effect: 'fade', duration: 120});
    $('a').removeClass('disable_link');
}

function tableBody_Reload(scope)
{
    if ( $('div#tableBody').length )
    {
        $('div#tableBody').load(location.href +' '+'div#tableBody', null, function ()
        {
            stoploader();
            setCheckboxes();
        });
    }

    if( $('.other_widget').length )
    {
        if ( scope.indexOf('note') > -1 )
        {
            $('#contento').load(location.href + ' ' +'#contento', null, function ()
            {
                stoploader();
                if ( $("#contento li").size() == 0 ) {
                    $('a#insert_note').next('span').show();
                }
            });
        }

        if ( scope.indexOf('card') > -1 )
        {
            $('#cardList').load(location.href + ' ' +'#cardList', null, function ()
            {
                stoploader();
                if ($('div.other_widget table tbody').children().length == 0){
                    $('a#addCart').next('span.viewRecord').show();
                }
            });
        }
    }

    var interval = setInterval(function ()
    {
        if ($(window).scrollTop() < $('#header').height() - $('#mainMenu').height()) {
            fixedMenu();
            clearInterval(interval);
        }
    }, 1);
}

function apiDialog(param)
{
    var scope = $('#deleteForm').attr('data-scope');
    if (!scope){
        scope = window.scope;
    }
    var multiChecked  = $("input[name='checkedid[]']:checked");
    var dialogMessage = $('.dialog-message');
    var i = 0;
    var flag = true;

    $(window).on("scroll resize",function()
    {
        $('.dialog-message').dialog("option", "position", $('.dialog-message').dialog("option", "position"));
    });

    $('.dialog-message').dialog(
    {
            open: function ()
            {
                dialogMessage.text('');

                $('.ui-dialog-buttonpane').append('<span id = "dialog_record_count"></span>');

                if (param != 'single')
                {
                    multiChecked.closest('tr').find('.d_title').each(function ()
                    {
                        if ( $(this).html() != '' ) {
                            dialogMessage.append($(this).html() + '<br/>');
                        }
                        else {
                            dialogMessage.append('No Name' + '<br/>');
                        }

                        if (multiChecked.length > 1) {
                            $('#dialog_record_count').text(multiChecked.length + ' ' + 'records');
                        }
                        else {
                            $('#dialog_record_count').text(multiChecked.length + ' ' + 'record');
                        }
                    });
                }

                else
                {
                    if (window.title !== '') {
                        dialogMessage.append(window.title);
                    }
                    else {
                        dialogMessage.append('No Name');
                    }
                    $('#dialog_record_count').text('1 record');
                }
                $('.ui-widget-overlay').addClass('custom-overlay');
            },

            close: function () {
                $('.ui-widget-overlay').removeClass('custom-overlay');
            },

            show: {effect: '', duration: 600, my: "top", at: "center", of: window},
            hide: {effect: '', duration: 400},
            title: "Do you want to delete selected item(s) ?",
            modal: true,
            width: '360',
            height: '300',
            resizable: false,
            draggable: false,
            closeOnEscape: true,
            buttons: {
                Yes: function () {
                    startloader();

                    if (param != 'single')
                    {
                        multiChecked.each(function () {
                            apiDelete(scope, $(this).val());
                        });

                        $(document).ajaxComplete(function (e)
                        {
                            i++;
                            if (i == multiChecked.length) {
                                tableBody_Reload(scope);
                            }
                        });
                    }

                    else
                    {
                        apiDelete(scope, window.id);

                        $(document).ajaxComplete(function (e)
                        {
                            if (flag) {
                                flag = false;
                                tableBody_Reload(scope);
                            }
                        });
                    }
                    $(this).dialog("close");
                },
                No: function () {
                    $(this).dialog("close");
                }
            }
    });
    return false;
}

$(function () {
    $('#body').on("mouseenter", ".showhide", function () {
        var id = $(this).parent().find('a.showhide').attr('data-id');
        $(this).parent().find('#targets' + id).show();
    });

    $('#body').on("mouseleave", ".targets_hide", function () {
        var id = $(this).parent().find('a.showhide').attr('data-id');
        $(this).parent().find('#targets' + id).hide();
    });
    function show(self) {
        var id = $(self).attr('data-id');
        if ($(self).text() === '\u00A0...') {
            document.getElementById('targets' + id).style.display = 'block';
        }
    }
});

$(function () {
    //for pagination and refresh
    if ($("div.alphabet a.ajax").hasClass('active')) {
        if ($('div.alphabet a.ajax.active').text() !== 'All') {
            $('#letter').text('Search result for' + ' ' + '"' + $('div.alphabet a.ajax.active').text() + '"');
        }
    }
    //endfor
    $('#body').on("click", "div.alphabet a.ajax", function () {
        $('#letter').show();
        fixedMenu();

        var alphabet_text = ($(this).text());
        if (alphabet_text !== 'All') {
            $('#letter').text('Search result for' + ' ' + '"' + alphabet_text + '"');
        }
        else {
            $('#letter').text('');
        }
    });
});

$(function ()
{
    var scrollTop = $('#scrollTop');
    scrollTop.css('cursor', 'default');

    $(window).scroll(function () {
        var scroll = $(window).scrollTop();

        if (scroll > 0) {
            scrollTop.css({'opacity': '1', 'cursor': 'pointer'});
        }
        else {
            scrollTop.css({'opacity': '0', 'cursor': 'default'});
        }
    });
});

function twoscrolls(classname)
{
    $(classname).bind('mousewheel DOMMouseScroll', function (e) {
        var e0 = e.originalEvent,
            delta = e0.wheelDelta || -e0.detail;
        this.scrollTop += ( delta < 0 ? 1 : -1 ) * 35;
        e.preventDefault();
    });
}

$(function ()
{
    twoscrolls('.targets_hide');
    twoscrolls('.dialog-message');
    stoploader();
    makeGridster();
    onScroll();
});

$(function ()
{
    $('#help_link').click(function () {
        $('#owner_sublinks,#mainMenu ul').removeClass('visible');
    });

    $('#owner_link').click(function () {
        $('#help_sublinks,#mainMenu ul').removeClass('visible');
    });

    $('div#help_sublinks a, div#owner_sublinks a').click(function () {
        $('div.subLinks').removeClass('visible');
    });
});

$(function ()
{
    if (window.event) {
        if ($('.checkboxArea.checked').length !== 0) {
            $('#deletebtn,#restorebtn,#clearbtn,#massMail').removeClass('disabled');
        }

        if ($('#c_all').length) {
            if ($('#tbody tr').length === $('.checkboxArea.checked').length) {
                $('.checkboxArea').addClass('checked');
            }
        }
    }
});

$(function () {
    $('.widget').each(function () {
        if ($(this).find('tbody').children().length === 0) {
            $(this).find('.btn-icon').closest('div').append('<span class="no_recrd">No records</span>');
            $(this).find('.widget_content').css('display', 'none');
        }
    });
});

$(function ()
{
    $('.widget div.title').mousedown(function () {
        $(this).css({'cursor': '-webkit-grabbing'});
    }).mouseup(function ()
    {
        $(this).css({'cursor': '-webkit-grab'});
        if ($('.selectBox').hasClass('selectBox-menuShowing'))
        {
            $('.selectBox-dropdown-menu').offset(
            {
                'left': $('.selectBox-label').offset().left - 10,
                'top' : $('.selectBox-label').offset().top + 37
            });
            $('.selectBox-dropdown-menu').show();
        }
    });

    $('.widget div.title').hover(function ()
    {
        if ( ($(this).css('cursor') == '-webkit-grabbing') && !($('div.gridster').hasClass('dragging')) ) {
            $(this).css('cursor', '-webkit-grab');
        }
    });
});

$(function ()
{
    $('div#mainMenu .tooltip.bottom,a#menu-create + ul .tooltip.right').find('.quantity').remove(); //menus tooltip remove

    $('a#menu-create + ul li').click(function () {
        $('a#menu-create + ul').removeClass('visible');
    });
});

$(function ()
{
    $('select.select2').each(function (index)
    {
        var scope = $(this).attr('data-scope');
        var query = $(this).attr('data-query')||'';
        $(this).select2(
        {
            placeholder: "Please select...",
            allowClear: true,
            ajax: {
                url: '/api/v2/data/'+scope,
                dataType: 'json',
                delay: 250,
                data: function (params) {
                    data = {
                        q: params.term, // search term
                        p: params.page
                    };

                    if (query.length)
                    {
                        data.query = query;
                    }
                    return data;
                },
                processResults: function (data, params) {
                    params.page = params.page || 1;

                    return {
                        results: data.data,
                        pagination: {
                            more: (params.page * 10) < data.count
                        }
                    };
                },
                cache: true
            },
            minimumInputLength: 1,
            templateResult: function (item) {
                if (item.loading) return item.text;
                return $('<div />').text(item.title).html();
            },
            templateSelection: function (item) {
                return $('<div />').text(item.title || item.text).html();
            }
        });
    });

    $('select.static-select2').select2(
        { minimumResultsForSearch: 5 }
    );

    $('.select2-selection').click(function()
    {
        $('span.select2-search input').focus();
    });
});

function apiSaveForm($form)
{
    var formData = $($form).serializeArray();
    var json = {};

    $.each(formData, function () {
        var name = this.name.replace(/[^\[]+\[([^\]]+)\]/g, '$1');
        json[name] = this.value || '';
    });

    var scope = $form.attr('data-scope');
    var id    = null;
    var type  = null;
    var url   = null;
    id = $form.attr('data-id');

    if (id.length > 0) {
        apiPut(scope, id, json);
    }
    else {
        apiPost(scope, json)
    }
}

function apiDelete(scope, id) {
    $.ajax({
        type: "DELETE",
        url: "/api/v2/data/" + scope + "/" + id,
        dataType: "json",
        success: function (data) {

        },
        error: function (data) {
            alert('Error');
        }
    });
}

function apiPut(scope, id, jsonData) {
    $.ajax({
        type: "PUT",
        url: "/api/v2/data/" + scope + "/" + id,
        data: {data: jsonData},
        dataType: "json",
        success: function (data) {

        },
        error: function (data) {
            alert('Error apiPut');
        }
    });
}

function apiPost(scope, jsonData)
{

    $.ajax({
        type: "POST",
        url: "/api/v2/data/" + scope,
        data: {data: jsonData},
        dataType: "json",
        success: function (data) {
            window.ID = data.data._id;
        },
        error: function (data) {
            alert('Error apiPost');
        }
    });
}

function apiListPost(scope, jsonData)
{
    $.ajax({
        type: "POST",
        url: "/api/v2/listdata/" + scope,
        data: {data: jsonData},
        dataType: "json",
        success: function (data) {

        },
        error: function (data) {
            alert('Error apiListPost');
        }
    });
}

function apiGet(scope, id, func, errfunc) {
    $.ajax({
        type: "GET",
        url: "/api/v2/data/" + scope + "/" + id,
        dataType: "json",
        success: func,
        error: errfunc
    });
}

function apiList(scope, data, successHandler, errorHandler) {
    $.ajax({
        type: "GET",
        url: "/api/v2/data/" + scope,
        data: data,
        dataType: "json",
        success: successHandler,
        error: errorHandler
    });
}

function readURL(input) //for avatar and document set
{
    if (input.files && input.files[0])
    {
        var reader = new FileReader();

        reader.onload = function (e) {
            $('#imginput').fadeOut().attr('src', e.target.result).fadeIn(250);
        };
        reader.readAsDataURL(input.files[0]);
    }
}

$(function() //avatar and document miniatures
{
    if ($('input[name="social[avatar]"]').length)
    {
        $('input[name="social[avatar]"]').attr('accept', 'image/*');

        $("label[for ='idsocial[avatar]']:visible").not('.error').css('margin-top', '9px').after
        (
            '<input type="button" value="Select image" class="file_btn" id="load_avatar"/>' +
            '<img  id="imginput"  alt="avatar" /><span id="image_name"></span>'
        );

        $('input#load_avatar').click(function(){
            $('input[name="social[avatar]"]').click();
        });

        $('input[name="social[avatar]"]').change(function ()
        {
            var filename = $(this).val();
            var lastIndex = filename.lastIndexOf("\\");
            $('span#image_name').css({'color':'black','font-size':'13px'});

            if (lastIndex >= 0)
            {
                filename = filename.substring(lastIndex + 1);

                if (filename.match(/\.(jpg|jpeg|png|gif)$/)){
                    readURL(this);
                }
                else{
                    $('img#imginput').fadeOut().prop('src',$('div#avatar').text()).fadeIn(300);
                    $('span#image_name').css('color','red').css('font-size','11px');
                    filename = 'Wrong file format !';
                }
                $('span#image_name').attr('title','');
            }

            else {
            $('img#imginput').fadeOut().prop('src',$('div#avatar').text()).fadeIn(300); //default avatar miniature
            }

            if(filename.length > 25){
               $('span#image_name').attr('title',filename);
               filename = filename.substr(0,20);
               filename += '...';
            }
            $('span#image_name').text(filename);
        });
    }

    if ($('input[name="fields[document]"]').length)
    {
        $('input[name="fields[document]"]').attr('accept','image/*,.djvu,.doc,.docx,.fb2,.pdf,.rtf,.txt,.xls,.xlsx');

        $("label[for ='idfields[document]']:visible").not('.error').css('margin-top', '-1px').after
        (
            '<input type="button" value="Select file" class="file_btn" id="load_file"/>' +
            '<img id="imgfile" src="/link/public/img/document.png" alt="document" /><span id="file_name"></span>'
        );

        $('input#load_file').click(function(){
            $('input[name="fields[document]"]').click();
        });

        $('input[name="fields[document]"]').change(function ()
        {

            var filename = $(this).val();
            var lastIndex = filename.lastIndexOf("\\");
            var documentType = '';
            $('span#file_name').attr('title','');
            $('span#file_name').css('color','black').css('font-size','13px');
            readURL(this);

            if (lastIndex >= 0)
            {
                filename = filename.substring(lastIndex + 1);
                documentType = filename.substring(filename.lastIndexOf(".")+1,filename.length);
                if(filename.match(/\.(jpg|jpeg|png|gif|djvu|doc|docx|fb2|pdf|rtf|txt|xls|xlsx)$/))
                {
                    $('img#imgfile').fadeOut().prop('src','/link/public/img/'+documentType+'.png').fadeIn(300);
                }
                else {
                    $('img#imgfile').fadeOut().prop('src','/link/public/img/document.png').fadeIn(300);
                    $('span#file_name').css('color','red').css('font-size','11px');
                    filename = 'Wrong file format !';
                }
            }
            else {
                $('img#imgfile').fadeOut().prop('src','/link/public/img/document.png').fadeIn(300);
            }

            if(filename.length > 25){
                $('span#file_name').attr('title',filename);
                filename = filename.substr(0,20);
                filename += '...';
            }
            $('span#file_name').text(filename);
        });
    }

});
