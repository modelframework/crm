var path = window.location.pathname;
path = path.split( "/" );
for ( var i = 0; i < path.length; i++ )
{
    if ( /[0-9]/i.test( path[i] ) )
        path.splice( i );
}

if ( !path[path.length-1].length  )
    path.pop();
path = path.join( "/" );

var availableTags = [
    "ActionScript",
    "AppleScript",
    "Asp",
    "BASIC",
    "C",
    "C++",
    "Clojure",
    "COBOL",
    "ColdFusion",
    "Erlang",
    "Fortran",
    "Groovy",
    "Haskell",
    "Java",
    "JavaScript",
    "Lisp",
    "Perl",
    "PHP",
    "Python",
    "Ruby",
    "Scala",
    "Scheme"
];

function updateColumnsCookie()
{
    var left_cookie = [ ], right_cookie = [ ], fullwidth_cookie = [ ];
    for ( var i = 0; i < $( '.left_area .table' ).length; i++ )
        left_cookie.push( $( '.left_area .table' ).eq( i ).attr( "id" ) );

    for ( var i = 0; i < $( '.right_area .table' ).length; i++ )
        right_cookie.push( $( '.right_area .table' ).eq( i ).attr( "id" ) );

    for ( var i = 0; i < $( '.fullwidth_area .table' ).length; i++ )
        fullwidth_cookie.push( $( '.fullwidth_area .table' ).eq( i ).attr( "id" ) );


    if ( left_cookie.length > 0 )
        $.cookie( 'left_area', left_cookie.join( " " ), { expires: 365, path: path } );
    else
        $.removeCookie( 'left_area', { path: path } );

    if ( right_cookie.length > 0 )
        $.cookie( 'right_area', right_cookie.join( " " ), { expires: 365, path: path } );
    else
        $.removeCookie( 'right_area', { path: path } );

    if ( fullwidth_cookie.length > 0 )
        $.cookie( 'fullwidth_area', fullwidth_cookie.join( " " ), { expires: 365, path: path } );
    else
        $.removeCookie( 'fullwidth_area', { path: path } );
}

var lastScroll = 0;

function onScroll()
{
    if ( $( document ).height() < $( window ).height() + $( '#header' ).height() )
        return false;
//    console.log( 'document height = ' + $( document ).height() );
//    console.log( 'window height = ' + $( window ).height() );
//    console.log( 'header height = ' + $( '#header' ).height() );

    if ( $( window ).scrollTop() > 0 )
    {
        $( '#header' ).css( "position", "fixed" );
        $( '#headerBlock' ).stop().slideUp( 200,
            function() {
                $( '#mainMenu' ).stop().animate( { boxShadow: '1px 2px 3px rgba(0,0,0,0.23)' }, 200 ); //opacity: 0.9,

                $( '#header' ).css( { 'background': 'none' } );


            } );
        $( '#mainMenu' ).stop().animate( { boxShadow: '0px 2px 2px rgba(0,0,0,0.23)' }, 200 );

    }
    if ( $( window ).scrollTop() == 0 )
    {
        $( '#header' ).css( { 'background': '#01579b repeat 0 0' } ); // url(/css/img/header.png)
        $( '#mainMenu' ).stop().animate( { margin: '0px 0px', boxShadow: 'none' }, 200 ); //opacity: 1,
        $( '#headerBlock' ).stop().slideDown( 200 );
        $( '#mainMenu' ).stop().animate( { boxShadow: 'none' }, 200 );
        $( '#header' ).css( "position", "block" );
    }

    if ( $( window ).scrollTop() > 0 )
    {
        if ( lastScroll < $( window ).scrollTop() ) // scroll down
        {
            $( '#header' ).stop().animate( { opacity: 0 }, 200 );
        }
        else // scroll up
        {
            $( '#header' ).stop().animate( { opacity: 0.9 }, 200 );
        }
    }
    else
        $('#header').stop().animate( { opacity: '0.9' }, 200 ); //opacity: 1css("opacity", 0.9);

    lastScroll = $( window ).scrollTop();

}

$( function() {

    $( '#header' ).mouseover( function() {
        if ( $( window ).scrollTop() > 0 )
            $( '#header' ).stop().animate( { opacity: 0.9 }, 200 );
    } );

    $( '#header' ).mouseleave( function() {
        if ( $( window ).scrollTop() > 0 )
            $( '#header' ).stop().animate( { opacity: 0 }, 1500 );
    } );

    $('.content a > img').mouseover(function(){
            $(this).append('<span class="tooltip top">'+$(this).attr("alt")+'</span>');
    });
    
    $('.content a > img').mouseleave(function(){
        
    });

    $( '.menu > ul > li' ).hover( function() {
        if ( $( this ).position().top == 0 && $( document ).scrollTop() == 0 )
        {
            if ( $( this ).find( '.tooltip' ).eq( 0 ).hasClass( 'bottom' ) )
                $( this ).find( '.tooltip' ).eq( 0 ).removeClass( 'bottom' ).addClass( 'top' );
        }
        else
        {
            if ( $( this ).find( '.tooltip' ).eq( 0 ).hasClass( 'top' ) )
                $( this ).find( '.tooltip' ).eq( 0 ).removeClass( 'top' ).addClass( 'bottom' );
        }
    } );

    $( '.menu .tooltip' ).addClass( 'bottom' );
    $( '.menu .create ul .tooltip' ).removeClass( 'bottom' ).addClass( 'right' );

    $( '.drop_area' ).sortable( {
        connectWith: ".drop_area",
        handle: ".title",
        update: function() {
            $( '.table.placeholder' ).remove();
            updateColumnsCookie();
        }
    } );

    $( '.table .title' ).disableSelection();

    if ( $.cookie( "left_area" ) )
    {
        var left_area = $.cookie( "left_area" ).split( " " );
        for ( var i = 0; i < left_area.length; i++ )
        {
            var table = $( '.table#' + left_area[i] );
            table.detach();
            $( '.left_area' ).append( table );
        }
    }

    if ( $.cookie( "right_area" ) )
    {
        var right_area = $.cookie( "right_area" ).split( " " );
        for ( var i = 0; i < right_area.length; i++ )
        {
            var table = $( '.table#' + right_area[i] );
            table.detach();
            $( '.right_area' ).append( table );
        }
    }

    if ( $.cookie( "fullwidth_area" ) )
    {
        var right_area = $.cookie( "fullwidth_area" ).split( " " );
        for ( var i = 0; i < right_area.length; i++ )
        {
            var table = $( '.table#' + right_area[i] );
            table.detach();
            $( '.fullwidth_area' ).append( table );
        }
    }

    if ( $.cookie( "minimized" ) )
    {
        var minimized_tables = $.cookie( "minimized" ).split( " " );
        for ( var i = 0; i < minimized_tables.length; i++ )
            $( '.table#' + minimized_tables[i] ).addClass( 'minimized' );
    }

    $( '.table a.minimize_button' ).click( function() {
//        $(this).toggleClass('minimize').toggleClass('maximize');
        var table = $( this ).closest( '.table' );
        table.toggleClass( 'minimized' );

        var minimize_cookie = [ ];
        for ( var i = 0; i < $( '.table.minimized' ).length; i++ )
            minimize_cookie.push( $( '.table.minimized' ).eq( i ).attr( "id" ) );

        $.cookie( 'minimized', minimize_cookie.join( " " ), { expires: 365, path: path } );

        onScroll();
    } );

    $( ".table a.resize_button" ).on( "click", function() {
        var table = $( this ).closest( '.table' );
        var drop_area = table.closest( '.drop_area' );
        table.detach();
        if ( drop_area.hasClass( 'half_area' ) )
        {
            $( '.fullwidth_area' ).append( table );
        }
        else if ( drop_area.hasClass( 'fullwidth_area' ) )
        {
            var left_height = 0,
                right_height = 0;
            for ( var i = 0; i < $( '.left_area .table' ).length; i++ )
                left_height += $( '.left_area .table' ).eq( i ).height();
            for ( var i = 0; i < $( '.right_area .table' ).length; i++ )
                right_height += $( '.right_area .table' ).eq( i ).height();

            if ( left_height <= right_height )
            {
                $( '.left_area' ).append( table );
            }
            else
            {
                $( '.right_area' ).append( table );
            }
        }

        updateColumnsCookie();

        return false;
    } );

    $( '#search_query' ).selectize( {
        maxItems: 1,
        valueField: 'title',
        labelField: 'title',
        searchField: 'title',
        options: [ ],
        create: true,
        /* //        render: {
         //            option: function (item, escape) {
         //
         //                return '<div>' +
         //                    '<span class="title">' +
         //                    '<a href="' + item._url + '">' +
         //                    '<span class="name">' + escape(item.title) + '</span>' +
         //                    '</a>' +
         //                    '</span>' +
         //                    '</div>';
         //                var actors = [];
         //                for (var i = 0, n = item.abridged_cast.length; i < n; i++) {
         //                    actors.push('<span>' + escape(item.abridged_cast[i].name) + '</span>');
         //                }
         //
         //                return '<div>' +
         //                    '<img src="' + escape(item.posters.thumbnail) + '" alt="">' +
         //                    '<span class="title">' +
         //                    '<span class="name">' + escape(item.title) + '</span>' +
         //                    '</span>' +
         //                    '<span class="description">' + escape(item.synopsis || 'No synopsis available at this time.') + '</span>' +
         //                    '<span class="actors">' + (actors.length ? 'Starring ' + actors.join(', ') : 'Actors unavailable') + '</span>' +
         //                    '</div>';
         //            }
         //        },*/
        load: function( query, callback ) {
            if ( !query.length )
                return callback();
            $( 'a.txtSearch', '#main_search' ).css( 'background-image', 'url(/css/img/select2-spinner.gif)' );
            $( 'a.txtSearch', '#main_search' ).css( 'background-position', '0px 0px' );
            $( '.input .selectize-control.single .selectize-input.input-active', '#main_search' ).css( 'background-image', 'none' );
//            $('.input .selectize-control.single .selectize-input.input-active', '#main_search').css('background-image', 'url(/css/img/select2-spinner.gif)');
            $.ajax( {
                url: '/api/v1/search',
                type: 'GET',
                dataType: 'json',
                data: {
                    q: query
                },
                error: function() {
                    $( 'a.txtSearch', '#main_search' ).css( 'background-image', 'url(/css/img/sprite.png)' );
                    $( 'a.txtSearch', '#main_search' ).css( 'background-position', '-500px 0px' );
                    $( '.input .selectize-control.single .selectize-input.input-active', '#main_search' ).css( 'background-image', 'url(/css/img/search_ico.png)' );
                    callback();
                },
                success: function( res ) {
                    $( 'a.txtSearch', '#main_search' ).css( 'background-image', 'url(/css/img/sprite.png)' );
                    $( 'a.txtSearch', '#main_search' ).css( 'background-position', '-500px 0px' );
                    $( '.input .selectize-control.single .selectize-input.input-active', '#main_search' ).css( 'background-image', 'url(/css/img/search_ico.png)' );
                    callback( res.data );
                }
            } );
        }
    } );

    /*    $( 'input.autocomplete_input' ).autocomplete( {
     source: availableTags
     } );*/

    $( '.table table tbody tr,#mainMenu li, div.lead_block table tr' ).bind( 'mouseenter', function() {
        $( this ).next().addClass( 'borderLess' );
    } ).bind( 'mouseleave', function() {
        $( this ).next().removeClass( 'borderLess' );
    } );

    $( document ).click( function() {
        //console.log( 'clicked' );
        $( '.visible' ).removeClass( 'visible' );
        $("#titleMenu").stop().animate( { width: 0, height:0, opacity:0}, 200,
            function(){
                $("#titleMenu").removeClass('visible');
            }
        );
    } );

    $( '#mainMenu .create, #header .with_subLinks a' ).click( function() {
        event.stopPropagation();
    } );

    $( '#mainMenu .create a#menu-create' ).click( function( e ) {
//        e.preventDefault();
        e.stopPropagation();
        $( this ).parent().find( 'ul' ).toggleClass( 'visible' );
        return false;
    } );

    $( 'form a.submitLink' ).click( function( e ) {
//        e.stopPropagation();
        e.preventDefault();
        $( this ).parents( 'form' ).submit();
    } );

    $( '#header .with_subLinks > a' ).click( function( e ) {
        e.stopPropagation();
        $( this ).parent().find( '.subLinks' ).toggleClass( 'visible' );
    } );

    $( '#search_form .input' ).insideLabel();

    $( "#rightCol, #leftCol" ).sortable( {
        connectWith: ".connected",
        items: 'div.table',
        forceHelperSize: false,
        forcePlaceholderSize: false,
        //containment: '.content',
        //helper: "clone",
        cursor: "move",
        cursorAt: { left: 190, top: 20 },
        change: function( event, ui ) {
//            console.log( ui.item )
        }
    } );

//	$('select').selectBox();
//    $('.select2').select2({width:'resolve'});

    $( "#form_date, .input_date input" ).datepicker( { dateFormat: "mm/dd/yy" } );

    $( '.steps_popup' ).each( function() {
        var $t = $( this );
        $t.nav = $t.find( '.popup_nav a' );
        /*$t.find('.popup_nav:first a').each(function(){
         console.log($($(this).attr('href')));
         });*/
        $( $t.nav.filter( ':first' ).attr( 'href' ) ).addClass( 'active' );
        $t.nav.click( function( e ) {
            e.preventDefault();
            if ( $( this ).hasClass( 'active' ) )
                return $( this );
            $t.children( 'div.active' ).animate( { left: '-110%' }, 300 );
            $( $( this ).attr( 'href' ) ).animate( { left: '20px' }, 300, function() {
                $t.children( 'div.active' ).removeClass( 'active' ).removeAttr( 'style' );
                $( this ).addClass( 'active' ).removeAttr( 'style' );

            } );
        } );
    } );

    $( 'a.table_head_link' ).click( function( e ) {
        e.preventDefault();
        $( this ).toggleClass( 'active' );
        $( this ).parents( '.table, .clearfix' ).toggleClass( 'bigZindex' );
        $( this ).next( 'div.table_popup' ).toggle();
        $( 'div.popup_overlay' ).insertBefore( $( this ) ).toggle().click( function() {
            $( 'div.table_popup:visible' ).prev( 'a.table_head_link' ).click();
        } );
    } );

    $( '.checkbox' ).click( function( e ) {
        if ( $( this ).find( 'input[type=checkbox]' ).hasClass( 'checked' ) )
            $( this ).closest( 'tr' ).addClass( 'checked' );
        else
            $( this ).closest( 'tr' ).removeClass( 'checked' );
    } );
    $( 'input[type="checkbox"]' ).customCheckbox();
    $( 'input[type="radio"]' ).customRadio();

    $( 'form.validate' ).validate();

} );
var scrolled = false;

$( window ).bind( 'scroll', function( event, eventdata ) {
    onScroll();
} );

jQuery.fn.insideLabel = function() {
    return $( this ).each( function() {
        var $t = $( this );
        $t.input = $t.find( 'input' );
        $t.label = $t.find( 'label' );

        if ( $t.input.val() != '' ) {
            $t.label.hide();
        }

        $t.input.focus( function() {
            $t.label.hide();
        } ).blur( function() {
            if ( $t.input.val() == '' ) {
                $t.label.show();
            }
        } );
    } );
}
