(function(jQuery){
	// custom checkboxes module
	jQuery.fn.customCheckbox = function(_o){
		var _o = jQuery.extend({
			cS: '<div></div>',
			cDs: 'checkboxArea disabled',
			cDf: 'checkboxArea',
			cC: 'checkboxArea checked',
			fC:'default'
		}, _o);
		return this.each(function(){
			var ch = jQuery(this);
			if(!ch.hasClass('outtaHere') && ch.is(':checkbox') && !ch.hasClass(_o.fC)){
				var r = jQuery(_o.cS);
				//this._r = r;
				if(ch.is(':disabled')) {
					r.addClass(_o.cDs);
				} else if(ch.is(':checked')) {
					r.addClass(_o.cC);
				} else {
					r.addClass(_o.cDf);
				}
				r.off('click').on('click', function(){
					if(!($(this).hasClass(_o.cDs))){
						if($(this).hasClass(_o.cC)) {
							$(this).removeClass().addClass(_o.cDf);
							ch.removeClass('checked').prop("checked", false).change();
						} else {
							$(this).removeClass().addClass(_o.cC);
							ch.addClass('checked').prop("checked", true).change();
						}
					}
   				});
   				ch.off('click').on('click', function(){
					if(r.hasClass(_o.cC)) {
						r.removeClass().addClass(_o.cDf);
						ch.removeClass('checked').prop("checked", false);
					} else {
						r.removeClass().addClass(_o.cC);
						ch.addClass('checked').prop("checked", true);
					}
   				});
				r.insertBefore(ch);
				ch.addClass('outtaHere');
			}
		});
	}
	// custom radios module
	jQuery.fn.customRadio = function(_o){
		var _o = jQuery.extend({
			rS: '<div></div>',
			rDs: 'radioArea disabled',
			rDf: 'radioArea',
			rC: 'radioArea checked',
			fC:'default'
		}, _o);
		return this.each(function(){
			var t = jQuery(this);
			if(!t.hasClass('outtaHere') && t.is(':radio') && !t.hasClass(_o.fC)){
				var r = jQuery(_o.rS);
				this._r = r;
				if(t.is(':disabled')) r.addClass(_o.rDs);
				else if(t.is(':checked')) r.addClass(_o.rC);
				else r.addClass(_o.rDf);
				r.click(function(){
					if(!($(this).hasClass(_o.rDs))){
						if(jQuery(this).hasClass(_o.rDf)){
							t.attr('checked', 'checked');
							cR(t.get(0));
						}
					}
				});
				t.click(function(){
					if(!jQuery(this).hasClass(_o.rDf)){
						t.attr('checked', 'checked');
						cR(t.get(0));
					}
				});
				t.click(function(){
					cR(this);
				});
				r.insertBefore(t);
				t.addClass('outtaHere');
			}
		});
		function cR(_t){
			jQuery(_t).change();
			jQuery('input:radio[name="'+jQuery(_t).attr("name")+'"]').not(_t).each(function(){
				if(this._r && !jQuery(this).is(':disabled')) this._r.removeClass().addClass(_o.rDf);
                $(this).removeAttr('checked');
			});
			_t._r.removeClass().addClass(_o.rC);
		}
	}
})(jQuery);